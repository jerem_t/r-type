
#pragma once

#include <gEngine/Core/Component.hpp>
/**
 * @class Button
 * button
 */
class Button : public ge::Component{
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Button(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Button();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);
private:
  ge::GameObjectPtr _text;  
  bool              _over;
};


#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Box.hpp>
#include <gEngine/Data/SpriteData.hpp>
#include "Button.hpp"

Button::Button(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _text(NULL)
, _over(false) {
	_data["srcDefault"] = std::string("buttonDefault");
	_data["srcOver"] = std::string("buttonOver");
	_data["srcOnClick"] = std::string("buttonOnClick");
    _data["text"] = std::string("Test");
    _data["font-size"] = 75u;
    _data["size"] = ge::Vector3f(0.1f, 0.1f, 0.0f);
    _data["focusable"] = false;
    _data["focused"] = false;
}

Button::~Button() {}

void Button::onStart() {
    /* set default texture */
    _gameObject.component("Sprite")["texture"] = _data["srcDefault"];

    /* set size */
    if (_data.find("size") != _data.end()){
        ge::Vector3f const &size = _data["size"];
        _gameObject.component("Sprite")["size"] = size;
        _gameObject.component("Collider")["box"] = ge::Boxf(size.x, size.y, size.z);
    }

    /* set text */
    if (_data.find("text") != _data.end()){
        _text = ge::GameObjectPtr(new ge::GameObject(_gameObject.getName() + std::string("-text")));
            /* Add Text Component */
        _text->component("Text")["value"] = _data["text"];
        _text->component("Text")["font"] = std::string("font_body");
        if (_data.find("font-size") != _data.end())
            _text->component("Text")["size"] = _data["font-size"];
            /* Add transformable Component */
        _text->component("Transformable")["position"] = ge::Vector3f(0.01, 0.01, 0.01);

        /* add text child */
        _gameObject.addChild(_text);
    }

    // se serait bien de pas avoir a passer par un fichier
    // peut etre un loadFromMemory ?
    _gameObject.component("Input")["path"] = std::string("resources/data/events/button_events.xml");

    /* start components */
    _gameObject.component("Input").start();
    _gameObject.component("Sprite").start();
    _gameObject.component("Transformable").start();
    _gameObject.component("Collider").start();


    /* Events */
    _gameObject.on("Collider:collision", [this] (ge::Array &){
        bool focused = _data["focused"];
        if (!focused){
        ge::SpriteData data;
        data.setTexture(_data["srcOver"]);
        _gameObject.trigger("Sprite:change", data);
        }
        _over = true;
    });

    _gameObject.on("Collider:non-collision", [this] (ge::Array &){
        bool focused = _data["focused"];
        if (!focused){
            ge::SpriteData data;
            data.setTexture(_data["srcDefault"]);
            _gameObject.trigger("Sprite:change", data);
        }
        _over = false;
    });

    _gameObject.on("Button:mouseMoved", [this] (ge::Array &e) {
        _gameObject.trigger("Collider:check", e[1]);
    });

    _gameObject.on("Button:mousePressed", [this] (ge::Array &) {
        bool focused = _data["focused"];
        bool focusable = _data["focusable"];
        if (_over && !focused && focusable) {
            ge::SpriteData data;
            data.setTexture(_data["srcOnClick"]);
            _gameObject.trigger("Sprite:change", data);
            _gameObject.trigger("Button:clicked");
            _data["focused"] = true;
        }
        else if (!_over && focused && focusable) {
            ge::SpriteData data;
            data.setTexture(_data["srcDefault"]);
            _gameObject.trigger("Sprite:change", data);
            _data["focused"] = false;
        }
        else if (_over){
            ge::SpriteData data;
            data.setTexture(_data["srcOnClick"]);
            _gameObject.trigger("Sprite:change", data);
            _gameObject.trigger("Button:clicked");
        }
    });

    _gameObject.on("Button:mouseReleased", [this] (ge::Array &) {
        bool focusable = _data["focusable"];
        if (focusable)
            return ;
        if (_over) {
            ge::SpriteData data;
            data.setTexture(_data["srcOver"]);
            _gameObject.trigger("Sprite:change", data);
        }
    });

}

void Button::onUpdate(float) {}

GE_EXPORT(Button)



#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class NWServer
 * Simple server communication handling.
 */
class NWServer : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  NWServer(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~NWServer();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};

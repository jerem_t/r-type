
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Network/Network.hpp>

#include "NWServer.hpp"

NWServer::NWServer(ge::GameObject &gameObject)
: ge::Component(gameObject) {}

NWServer::~NWServer() {}

void NWServer::onStart() 
{
	std::cout << "NWServer start" << std::endl;
}

void NWServer::onUpdate(float) 
{
//	std::cout << "NWServer update" << std::endl;
}

GE_EXPORT(NWServer)


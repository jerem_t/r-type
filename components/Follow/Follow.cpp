
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>
#include "Follow.hpp"

Follow::Follow(ge::GameObject &gameObject)
: ge::Component(gameObject) {
	_data["target"] = std::string("");
}

Follow::~Follow() {}

void Follow::onStart() {}

void Follow::onUpdate(float) {
	std::string targetName = _data["target"];
	ge::GameObjectPtr targetObj = _gameObject.getRoot()->getChildByName(targetName);
	if (targetObj == NULL || !targetObj->has("transform"))
		return ;
	ge::TransformPtr targetTransform = targetObj->component("Transformable")["transform"];
	ge::TransformPtr objTransform = _gameObject.component("Transformable")["transform"];
	ge::Vector3f targetVec = targetTransform->getGlobalPosition();
  	objTransform->lookAt(targetVec);
}

GE_EXPORT(Follow)


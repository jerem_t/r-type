
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class Follow
 * Follow the given target.
 */
class Follow : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Follow(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Follow();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};


#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class Choice
 * Choice
 */
class Choice : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Choice(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Choice();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);
private:
  void  _updateData();
  void  _initComponents();
  void  _initChoice();
  void  _initButtons();
  void  _changeStyle(std::string const &style);

  int      _currChoice;
  ge::GameObjectPtr _left;
  ge::GameObjectPtr _right; 
  ge::GameObjectPtr _choice;
};

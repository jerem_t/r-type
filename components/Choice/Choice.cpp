
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Parsers/Xml.hpp>
#include <gEngine/Parsers/Eval.hpp>
#include <gEngine/Math/Transform.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Utils/Dictionary.hpp>
#include <vector>
#include "Choice.hpp"

Choice::Choice(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _currChoice(0)
, _left(NULL)
, _right(NULL)
, _choice(NULL) {
    _data["style"] = std::string("");
    _data["hoverStyle"] = std::string("");
    _data["activeStyle"] = std::string("");
    _data["focusStyle"] = std::string("");

	_data["previousStyle"] = std::string("style");
	_data["currentStyle"] = std::string("style");
	_data["newStyle"] = std::string("style");

  	_data["choices"] = std::vector<std::string>({"choice 1", "choice 2", "choice 3"});

  	_data["position"] = ge::Vector3f(0, 0, 0);
	_data["size"] = ge::Vector3f(0, 0, 0);
	_data["background-image"] = std::string("buttonDefault");
	_data["background-color"] = ge::Color(255,255,255,255);
	_data["font-family"] = std::string("font_body");
	_data["font-size"] = 0.08f;
    _data["font-position"] = ge::Vector3f(0.0, 0.0, 0.0);
	_data["font-color"] = ge::Color(0,0,0,255);
	_data["transition-type"] = std::string("linear");
	_data["transition-time"] = 0.0f;
}

Choice::~Choice() {}

void Choice::_updateData() {
	std::string currentStyle = _data["currentStyle"];
    if (_data[currentStyle + "Data"].hasType<std::string>())
        return ;
	ge::Dictionary style = _data[currentStyle + "Data"];
	for (auto it = style.begin(); it != style.end(); ++it){
		_data[it->first] = it->second;
	}
}

void Choice::_initChoice() {
	std::string name = _gameObject.getName();
	std::vector<std::string>	choices = _data["choices"];
	std::vector<std::string>  styles({"styleData"});

	_choice = ge::GameObjectPtr(new ge::GameObject(name + std::string("-choice")));
	_choice->component("TextBox")["text"] = choices[_currChoice];
	if (!_data["styleData"].hasType<std::string>()){
		ge::Dictionary style = _data["styleData"];
		ge::Vector3f size;
		if (style.find("size") != style.end())
			size = style["size"];
		ge::Vector3f choiceSize(size.x * 0.5, size.y, size.z);
		style["position"] =	ge::Vector3f(size.x * 0.25, 0, 0);
		style["size"] = choiceSize;
		style["background-color"] = ge::Color(0,0,0,0);
		_choice->component("TextBox")["styleData"] = style;
	}
	else {
  	_choice->component("TextBox")["styleData"] = std::string("");
	}
	_choice->on("Choice:change", [this] (ge::Array &e){
		int		current = e[0];
		std::vector<std::string>	choices = _data["choices"];
		_choice->component("TextBox")["text"] = choices[current];
		_choice->trigger("TextBox:update");
	});
}

void Choice::_initButtons() {
	std::string name = _gameObject.getName();
	std::vector<std::string>	choices = _data["choices"];
	ge::Vector3f baseSize = _data["size"];
	std::vector<std::string>  styles({"styleData", "hoverStyleData", "activeStyleData", "focusStyleData"});

	_left = ge::GameObjectPtr(new ge::GameObject(name + std::string("-left")));
	_left->component("TextBox")["text"] = std::string("<");
	for (auto it = styles.begin(); it != styles.end(); ++it){
		if (!_data[*it].hasType<std::string>()){
	   		ge::Dictionary style = _data[*it];
	   		ge::Vector3f size;
	   		if (style.find("size") != style.end())
	   			size = style["size"];
	   		ge::Vector3f buttonSize(size.x * 0.2, size.y, size.z);
	   		style["position"] =	ge::Vector3f(0, 0, 0);
			style["size"] = buttonSize;
			style["font-position"] = ge::Vector3f(buttonSize.x / 2.5f, 0, 0);
	   		_left->component("TextBox")[*it] = style;
	    }
	    else {
	    	_left->component("TextBox")[*it] = std::string("");
	    }
	}
	_left->on("Button:clicked", [this] (ge::Array &) {
		std::vector<std::string>	choices = _data["choices"];
		--_currChoice;
		if (_currChoice < 0)
			_currChoice = choices.size() - 1;
		_choice->trigger("Choice:change", _currChoice);
	});

	ge::Vector3f basePos(baseSize.x - baseSize.x * 0.2, 0, 0);
	_right = ge::GameObjectPtr(new ge::GameObject(name + std::string("-right")));
	_right->component("TextBox")["text"] = std::string(">");
	for (auto it = styles.begin(); it != styles.end(); ++it){
		if (!_data[*it].hasType<std::string>()){
	   		ge::Dictionary style = _data[*it];
	   		ge::Vector3f size = style["size"];
	   		ge::Vector3f buttonSize(size.x * 0.2, size.y, size.z);
	   		style["position"] =	basePos;
			style["size"] = buttonSize;
			style["font-position"] = ge::Vector3f(buttonSize.x / 2.5f, 0, 0);
	   		_right->component("TextBox")[*it] = style;
	    }
	    else {
	    	_right->component("TextBox")[*it] = std::string("");
	    }
	}
	_right->on("Button:clicked", [this] (ge::Array &) {
		std::vector<std::string>	choices = _data["choices"];
		++_currChoice;
		if (static_cast<unsigned int>(_currChoice) > choices.size() - 1u)
			_currChoice = 0;
		_choice->trigger("Choice:change", _currChoice);
	});
	if (_data.find("onclick") != _data.end() && _data["onclick"].hasType<std::string>()){
		std::string event = _data["onclick"];
		_left->component("TextBox")["onclick"] = event;
		_right->component("TextBox")["onclick"] = event;
		_gameObject.getParent()->on(event, [this, event] (ge::Array &){
			_gameObject.getParent()->getParent()->trigger(event);
		});
	}
}

void Choice::_initComponents() {
	_gameObject.component("Transformable")["position"] = _data["position"];
	_gameObject.component("Transformable").start();
	_initChoice();
	_initButtons();

	_gameObject.addChild(_choice);
	_gameObject.addChild(_left);
	_gameObject.addChild(_right);
}

void Choice::_changeStyle(std::string const &style) {
	_data["nextStyle"] = style;
	_data["currentStyle"] = style;
	_updateData();
}

void Choice::onStart() {
	_data["styleData"] = ge::eval(_data["style"]);
	_data["hoverStyleData"] = ge::eval(_data["hoverStyle"]);
	_data["activeStyleData"] = ge::eval(_data["activeStyle"]);
	_data["focusStyleData"] = ge::eval(_data["focusStyle"]);

	_updateData();
	_initComponents();
}

void Choice::onUpdate(float) {}

GE_EXPORT(Choice)


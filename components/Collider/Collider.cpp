
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Box.hpp>
#include "Collider.hpp"

Collider::Collider(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _save(false) {
  _data["box"] = ge::Boxf();
  _data["quadtree"] = false;
}

Collider::~Collider() {}

void Collider::onStart() {
  _transform = _gameObject.component("Transformable")["transform"];
  _gameObject.on("Collider:check", [this] (ge::Array &event) {
    bool  result = false;
    if (event[0].hasType<ge::GameObjectPtr>())
      result = _checkHitWithObj(event[0]);
    else if (event[0].hasType<ge::Vector3f>())
      result = _checkHitWithPoint(event[0]);
    else if (event[0].hasType<ge::GameObject *>())
      result = _checkHitWithPtr(event[0]);
      
    if (result && !_save) {
      _gameObject.trigger("Collider:collision", event[0], event[1]);
      _save = true;
    }
    if (!result && _save){
      _gameObject.trigger("Collider:non-collision", event[0], event[1]);
      _save = false;
    }
  });
  if (_data["quadtree"].get<bool>()) {
    _gameObject.getRoot()->trigger("Physics:add", &_gameObject);
  }
}

void Collider::onUpdate(float) {}

bool Collider::_checkHitWithObj(ge::GameObjectPtr const &other) {
  ge::Boxf &box = _data["box"];
  box.pos = _transform->getGlobalPosition();
  ge::Boxf &otherBox = other->component("Collider")["box"];
  ge::TransformPtr transform = other->component("Transformable")["transform"];
  otherBox.pos = transform->getGlobalPosition();
  return box.hit(otherBox);
}

bool Collider::_checkHitWithPtr(ge::GameObject *other) {
  ge::Boxf &box = _data["box"];
  box.pos = _transform->getGlobalPosition();
  ge::Boxf &otherBox = other->component("Collider")["box"];
  ge::TransformPtr transform = other->component("Transformable")["transform"];
  otherBox.pos = transform->getGlobalPosition();
  return box.hit(otherBox);
}


bool Collider::_checkHitWithPoint(ge::Vector3f const &pt) {
  ge::Boxf &box = _data["box"];
  box.pos = _transform->getGlobalPosition();
  return box.hit(pt);
}

GE_EXPORT(Collider)

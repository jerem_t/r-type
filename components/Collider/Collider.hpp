
#pragma once

#include <gEngine/Core/Component.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>

/**
 * @class Collider
 * Simple representation of the object to handle collisions.
 */
class Collider : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Collider(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Collider();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  bool  _checkHitWithObj(ge::GameObjectPtr const &other);
  bool  _checkHitWithPoint(ge::Vector3f const &coo);
  bool  _checkHitWithPtr(ge::GameObject *coo);

private:

  ge::TransformPtr _transform;
  bool             _save;
};

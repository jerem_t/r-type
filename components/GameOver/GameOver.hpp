
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class GameOver
 * Handle the end of the game.
 */
class GameOver : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  GameOver(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~GameOver();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  float _currentTime;
  bool _started;

};

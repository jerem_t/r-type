
#include <gEngine/Core/GameObject.hpp>
#include "GameOver.hpp"

GameOver::GameOver(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _currentTime(0)
, _started(false) {
  _data["bubble"] = std::string("bubble_win");
}

GameOver::~GameOver() {}

void GameOver::onStart() {
  _gameObject.on("die", [this] (ge::Array &) {
    _gameObject.getRoot()->trigger("Menu:mainMenu", 5.0f, _data["bubble"]);
  });
}

void GameOver::onUpdate(float) {}

GE_EXPORT(GameOver)


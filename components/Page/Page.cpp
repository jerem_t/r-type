
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Data/SpriteData.hpp>
#include <gEngine/Data/TextData.hpp>
#include "Page.hpp"

Page::Page(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["current"] = false;
}

Page::~Page() {}

void Page::onStart() {
  ge::GameObjects elems;
  ge::GameObjects choiceChildren;

  // Get all objects in the page.

  _gameObject.findAll("*", elems);

  // Hide the page

  _gameObject.on("Page:hide", [this, &elems, &choiceChildren] (ge::Array &) {
    _gameObject.forEach([this] (ge::GameObjectPtr el){

      if (!el->hasComponent("Choice")){
        if (el->hasComponent("Sprite")){
          el->component("Sprite")["color"] = ge::Color(0, 0, 0, 0);
          el->trigger("Sprite:change"); 
        }
        ge::GameObjectPtr text = el->getChildByName(el->getName() + std::string("-text"));
        if (text && text->hasComponent("Text"))
          text->component("Text")["color"] = ge::Color(0, 0, 0, 0);
        el->component("TextBox")["enabled"] = false;
        el->component("TextBox")["hover"] = false;
      }
    });
  });

  // Display the page

  _gameObject.on("Page:show", [this, &elems] (ge::Array &) {
    _gameObject.forEach([this] (ge::GameObjectPtr el){
      
      if (!el->hasComponent("Choice")){
        if (el->hasComponent("Sprite")){
            el->component("Sprite")["color"] = el->component("TextBox")["background-color"];
            el->trigger("Sprite:change"); 
        }
        ge::GameObjectPtr text = el->getChildByName(el->getName() + std::string("-text"));
        if (text && text->hasComponent("Text"))
          text->component("Text")["color"] = el->component("TextBox")["font-color"];
       el->component("TextBox")["enabled"] = true;
      }
        
    });

  });

  // First its not the current page, hide it.

  if (_data["current"].get<bool>() == false)
    _gameObject.trigger("Page:hide");

}

void Page::onUpdate(float) {}

GE_EXPORT(Page)



#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class Page
 * The page representation for GUI.
 */
class Page : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Page(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Page();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};


#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Parsers/Xml.hpp>
#include <gEngine/Parsers/Eval.hpp>
#include "Input.hpp"

Input::Input(ge::GameObject &gameObject)
: ge::Component(gameObject) {}

Input::~Input() {}

void	Input::handleEvent(ge::Array &event) {
	for (auto const &info : _infoList) {
    std::string code;
    if (event[1].hasType<std::string>())
      code = event[1].get<std::string>();
    if (info.match(event[0], code)) {
      _gameObject.trigger(info.action,
                          event[0],
                          event[1],
                          event[2],
                          event[3]);
    }
	}
}

void	Input::onStart() {
  if (_data.find("path") != _data.end()) {
    ge::Xml root;
    ge::XmlArray events;
    root.loadFromFile(_data["path"]);
    root.getChildrenByName("event", events);
    for (auto &event : events) {
      EventInfo info;
      info.action = event->getAttr("action");
      info.type = event->getAttr("type");
      if (event->has("codes")) {
        ge::Any codes = ge::eval(event->getAttr("codes"));
        if (codes.hasType<std::string>())
          info.codes.push_back(codes);
        else if (codes.hasType<ge::Array>()) {
          ge::Array arr = codes;
          for (auto &it : arr)
            info.codes.push_back(it);
        }
      }
      _infoList.push_back(info);
    }
  }
  _gameObject.getRoot()->on("Context:event", [this] (ge::Array &e) {
    handleEvent(e);
  });
}

void	Input::onUpdate(float) {}

GE_EXPORT(Input)

#pragma once

#include <map>
#include <string>
#include <utility>
#include <SFML/Window/Event.hpp>
#include <gEngine/Core/Component.hpp>
#include "EventInfo.hpp"

class Input : public ge::Component {
public:
  Input(ge::GameObject &gameObject);
  ~Input();
  void onStart();
  void onUpdate(float dt);
private:
	void handleEvent(ge::Array &event);
	std::vector<EventInfo> _infoList;
};

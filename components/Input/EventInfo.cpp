
#include <algorithm>
#include "EventInfo.hpp"
#include <iostream>

bool EventInfo::match(std::string const &t,
                      std::string const &code) const {
  if (codes.empty())
    return type == t;
  return std::find(codes.begin(), codes.end(), code) != codes.end() &&
         type == t;
}
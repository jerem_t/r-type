
#pragma once

#include <vector>
#include <string>

struct EventInfo {
  std::string action; /** The action to trigger */
  std::string type; /** The type of input event */
  std::vector<std::string> codes; /** The codes to check */

  /**
   * Check if the event info match with the given event
   */
  bool match(std::string const &, std::string const &) const;
};
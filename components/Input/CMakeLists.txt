cmake_minimum_required(VERSION 2.6)

project(gEngine_Input)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_Input SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_Input gEngine.lib)
endif (MSVC)

#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class Text
 * Simple text displayer.
 */
class Text : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Text(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Text();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  std::string _value;
  std::string _font;
  ge::Color _color;
  float _size;

};


cmake_minimum_required(VERSION 2.6)

project(gEngine_Text)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_Text SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_Text gEngine.lib)
endif (MSVC)


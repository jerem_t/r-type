
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Data/TextData.hpp>
#include "Text.hpp"

Text::Text(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["value"] = std::string("");
  _data["size"] = 0.05f;
  _data["color"] = ge::Color(0, 0, 0, 255);
}

Text::~Text() {}

void Text::onStart() {
  if (_data.find("font") == _data.end()) {
    std::string msg("Font must be specified in Text component.");
    _gameObject.getRoot()->trigger("error", msg);
    return ;
  }
  ge::TextData data;
  float size = _data["size"];
  data.setString(_data["value"]);
  data.setFont(_data["font"]);
  data.setColor(_data["color"]);
  data.setCharacterSize(size);

  _gameObject.getRoot()->trigger("Context:addText",
                                 &_gameObject, data);
}

void Text::onUpdate(float) {
  std::string const &tmpValue = _data["value"];
  float const &tmpSize = _data["size"];
  std::string const &tmpFont = _data["font"];
  ge::Color const &tmpColor = _data["color"];
  if (tmpValue != _value || tmpFont != _font ||
      tmpColor.x != _color.x || tmpColor.y != _color.y || tmpColor.z != _color.z ||
      tmpSize != _size) {
    _value = tmpValue;
    _size = tmpSize;
    _font = tmpFont;
    _color = tmpColor;

    ge::TextData data;
    data.setString(_value);
    data.setFont(_font);
    data.setColor(_color);
    data.setCharacterSize(_size);
    _gameObject.getRoot()->trigger("Context:addText", &_gameObject, data);
  }
}

GE_EXPORT(Text)



cmake_minimum_required(VERSION 2.6)

project(gEngine_Gui)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_Gui SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_Gui gEngine.lib)
endif (MSVC)


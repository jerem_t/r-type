
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Parsers/Xml.hpp>
#include <gEngine/Parsers/Style.hpp>
#include <gEngine/Parsers/Eval.hpp>
#include "Gui.hpp"

std::string Gui::_names[] = {
  "style",
  "hoverStyle",
  "activeStyle",
  "focusStyle"
};

std::string Gui::_states[] = {
  "default",
  "hover",
  "active",
  "focus"
};

Gui::Gui(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["currentPage"] = std::string("");
}

Gui::~Gui() {}

void Gui::onStart() {
  _gameObject.on("Gui:changePage", [this] (ge::Array &e) {
    _data["currentPage"] = e[0].get<std::string>();
    ge::GameObjects pages;
    _gameObject.getChildrenByTag("page", pages);
    for (auto &page : pages) {
      if (page->getName() == _name + "-" + e[0].get<std::string>()) {
        page->trigger("Page:show");
      }
      else
        page->trigger("Page:hide");
    }
  });

  if (_data.find("markup") != _data.end()) {
    ge::XmlPtr root(new ge::Xml);
    ge::XmlArray pages;
    root->loadFromFile(_data["markup"]);

    // Add the tags from the UI tags attributes.

    root->forEach([] (ge::XmlPtr &node) {
      if (node->has("tags")) {
        ge::Any tags = ge::eval(node->getAttr("tags"));
        if (tags.hasType<std::string>())
          node->addTag(tags);
        else if (tags.hasType<ge::Array>()) {
          for (auto &tag : tags.get<ge::Array>())
            node->addTag(tag);
        }
      }
    });

    // Apply the style.

    if (_data.find("style") != _data.end()) {
      ge::Style styles;
      styles.loadFromFile(_data["style"]);
      for (auto &style : styles) {
        ge::XmlArray elems;
        root->findAll(style.first, elems);
        for (auto &el : elems) {
          if (el->getName() == "page") // no style for page
            continue ;
          for (size_t i = 0; i < 4; ++i) {
            if (!el->has(_names[i]))
              (*el)[_names[i]] = std::string("");
            (*el)[_names[i]].get<std::string>() += style.second[_states[i]];
          }
        }
      }
    }

    // Load the pages.

    root->getChildrenByName("page", pages);
    bool isFirst = true;
    for (auto &page : pages) {
      std::string name = _name + "-" + page->getAttr("name");
      ge::GameObjectPtr node(new ge::GameObject(name));
      _gameObject.addChild(node);
      node->component("Page")["current"] = isFirst;
      node->addTag("page");
      if (isFirst)
        isFirst = false;
      _loadPage(page, node);
      node->forEach([] (ge::GameObjectPtr obj){
        obj->start();
      });
      node->start();
    }

  }

}

void Gui::onUpdate(float) {}

void Gui::_loadPage(ge::XmlPtr const &node,
                    ge::GameObjectPtr const &page) {

  // TextBoxes

  ge::XmlArray textBoxes;
  node->getChildrenByName("textBox", textBoxes, false);
  for (auto &textBox : textBoxes) {

    // add object to page

    ge::GameObjectPtr obj(new ge::GameObject(_name + "-textBox"));
    page->addChild(obj);

    // attribs

    obj->component("TextBox")["text"] = textBox->getText();
    if (textBox->has("focusable") && textBox->getAttr("focusable") == "true")
      obj->component("TextBox")["focusable"] = true;
    if (textBox->has("onclick"))
      obj->component("TextBox")["onclick"] = textBox->getAttr("onclick");
    if (textBox->has("target"))
      obj->component("TextBox")["target"] = textBox->getAttr("target");

    // style

    for (size_t i = 0; i < 4; ++i) {
      if (!textBox->has(_names[i]))
        (*textBox)[_names[i]] = std::string("");
      obj->component("TextBox")[_names[i]] = textBox->getAttr(_names[i]);
    }
  }

  // TextInputs

  ge::XmlArray textInputs;
  node->getChildrenByName("textInput", textInputs, false);
  for (auto &textInput : textInputs) {

    // add object to page

    ge::GameObjectPtr obj(new ge::GameObject(_name + "-textInput"));
    page->addChild(obj);

    // add attributes

    obj->component("TextInput")["text"] = textInput->getText();
    if (textInput->has("max"))
      obj->component("TextInput")["maxCharacters"] =
          ge::convert<std::string, float>(textInput->getAttr("max"));

    // add style

    for (size_t i = 0; i < 4; ++i) {
      if (!textInput->has(_names[i]))
        (*textInput)[_names[i]] = std::string("");
      obj->component("TextInput")[_names[i]] = textInput->getAttr(_names[i]);
    }

  }

  // Choices

  ge::XmlArray choices;
  node->getChildrenByName("choice", choices, false);
  for (auto &choice : choices) {

    // add object to page

    ge::GameObjectPtr obj(new ge::GameObject(_name + "-choice"));
    page->addChild(obj);

    if (choice->has("onclick")){
      obj->component("Choice")["onclick"] = choice->getAttr("onclick");
    }

    // add attributes

    ge::XmlArray elems;
    std::vector<std::string> data;
    choice->getChildrenByName("elem", elems, false);
    for (auto &el : elems)
      data.push_back(el->getText());
    obj->component("Choice")["choices"] = data;

    // add style

    for (size_t i = 0; i < 4; ++i) {
      if (!choice->has(_names[i]))
        (*choice)[_names[i]] = std::string("");
      obj->component("Choice")[_names[i]] = choice->getAttr(_names[i]);
    }

  }

}

GE_EXPORT(Gui)



#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class Gui
 * Gui
 */
class Gui : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Gui(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Gui();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  void _loadPage(ge::XmlPtr const &node,
                 ge::GameObjectPtr const &page);

  static std::string _names[];
  static std::string _states[];
};

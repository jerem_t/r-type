
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Data/SpriteData.hpp>
#include "Sprite.hpp"

Sprite::Sprite(ge::GameObject &gameObject)
: ge::Component(gameObject) {
}

Sprite::~Sprite() {}

void Sprite::onStart() {
  ge::SpriteData spriteData;
  if (_data.find("color") != _data.end())
    spriteData.setColor(_data["color"]);
  if (_data.find("texture") != _data.end())
    spriteData.setTexture(_data["texture"]);
  if (_data.find("textureRect") != _data.end())
    spriteData.setTextureRect(_data["textureRect"]);
  _gameObject.getRoot()->trigger("Context:addSprite",
  								              &_gameObject, spriteData);

  _gameObject.on("Sprite:change", [this] (ge::Array &e){
    ge::SpriteData data;
    if (e.getSize() <= 0) {
      if (_data.find("color") != _data.end())
        data.setColor(_data["color"]);
      if (_data.find("texture") != _data.end())
        data.setTexture(_data["texture"]);
      if (_data.find("textureRect") != _data.end())
        data.setTextureRect(_data["textureRect"]);
    }else{
      data = e[0];
    }
  	_gameObject.getRoot()->trigger("Context:addSprite",
  	              								  &_gameObject, data);
  });
}

void Sprite::onUpdate(float) {}

GE_EXPORT(Sprite)


#pragma once

#include <gEngine/Core/Component.hpp>

class Sprite : public ge::Component {
public:
  Sprite(ge::GameObject &gameObject);
  ~Sprite();
  void onStart();
  void onUpdate(float dt);
};

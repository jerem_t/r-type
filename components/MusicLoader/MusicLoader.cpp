
#include <gEngine/Core/GameObject.hpp>
#include "MusicLoader.hpp"

MusicLoader::MusicLoader(ge::GameObject &gameObject)
: ge::Component(gameObject) {
}

MusicLoader::~MusicLoader() {}

void MusicLoader::onStart() {
	for (auto it = _data.begin(); it != _data.end(); ++it){
		_gameObject.trigger("Audio:loadMusic", it->first, it->second);
	}
}

void MusicLoader::onUpdate(float) {}

GE_EXPORT(MusicLoader)


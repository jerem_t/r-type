
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class MusicLoader
 * MusicLoader
 */
class MusicLoader : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  MusicLoader(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~MusicLoader();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};

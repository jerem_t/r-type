
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class RandomSpawn
 * Spawn the object to a random position.
 */
class RandomSpawn : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  RandomSpawn(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~RandomSpawn();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};

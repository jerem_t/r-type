
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>
#include <gEngine/Math/Random.hpp>
#include "RandomSpawn.hpp"

RandomSpawn::RandomSpawn(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["range"] = ge::Vector2f(0, 1);
}

RandomSpawn::~RandomSpawn() {}

void RandomSpawn::onStart() {
  ge::TransformPtr transform = _gameObject.component("Transformable")["transform"];
  ge::Vector2f const &r = _data["range"];
  ge::Vector3f &axis = _data["axis"];
  ge::Vector3f position = transform->getPosition();
  for (size_t i = 0; i < ge::Vector3f::size; ++i)
    if (axis[i])
      position[i] = ge::fakeRandom(r.x, r.y);
  transform->setPosition(position);
}

void RandomSpawn::onUpdate(float) {}

GE_EXPORT(RandomSpawn)


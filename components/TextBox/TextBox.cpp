#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Parsers/Xml.hpp>
#include <gEngine/Parsers/Eval.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Utils/Dictionary.hpp>
#include <gEngine/Math/Box.hpp>
#include <gEngine/Data/SpriteData.hpp>
#include "TextBox.hpp"

TextBox::TextBox(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _text(NULL)
, _transitionStep(0)
, _nextStyle("") {
    _data["style"] = std::string("");
    _data["hoverStyle"] = std::string("");
    _data["activeStyle"] = std::string("");
    _data["focusStyle"] = std::string("");

	_data["previousStyle"] = std::string("style");
	_data["currentStyle"] = std::string("style");
	_data["newStyle"] = std::string("style");

	_data["text"] = std::string("");

	_data["position"] = ge::Vector3f(0,0,0);
	_data["size"] = ge::Vector3f(0,0,0);
	_data["background-image"] = std::string("white");
	_data["background-color"] = ge::Color(255,255,255,255);
	_data["font-family"] = std::string("font_body");
	_data["font-size"] = 0.1f;
    _data["font-position"] = ge::Vector3f(0.0, 0.0, 0.0);
	_data["font-color"] = ge::Color(0,0,0,255);
	_data["transition-type"] = std::string("linear");
	_data["transition-time"] = 0.2f;

    _data["focusable"] = false;
    _data["focused"] = false;
   	_data["hover"] = false;
   	_data["active"] = false;

    _data["enabled"] = true;

    _tween.duration(1.0f)
          .from(0.0f)
          .to(1.0f)
          .easing("linear");
}

TextBox::~TextBox() {}

void TextBox::_updateData(std::string const &s) {
	std::string currentStyle = _data[s];
    if (!_data[currentStyle + "Data"].hasType<ge::Dictionary>())
        return ;
	ge::Dictionary style = _data[currentStyle + "Data"];
	for (auto it = style.begin(); it != style.end(); ++it){
		_data[it->first] = it->second;
	}
}

void TextBox::_initComponents() {
    _gameObject.component("Transformable");
	_gameObject.component("Sprite");
	_gameObject.component("Collider");
	_gameObject.component("Input");

    if (_data["background-image"].hasType<std::string>())
    	_gameObject.component("Sprite")["texture"] = _data["background-image"];
    if (_data["background-color"].hasType<ge::Color>())
        _gameObject.component("Sprite")["color"] = _data["background-color"];

    if (_data["position"].hasType<ge::Vector3f>())
	   _gameObject.component("Transformable")["position"] = _data["position"];

    if (_data["size"].hasType<ge::Vector3f>()){
        ge::Vector3f const &size = _data["size"];
        _gameObject.component("Sprite")["size"] = size;
        _gameObject.component("Collider")["box"] = ge::Boxf(size.x, size.y, size.z);
    }

    _text = ge::GameObjectPtr(new ge::GameObject(_gameObject.getName() + std::string("-text")));

    if (_data["font-position"].hasType<ge::Vector3f>())
        _text->component("Transformable")["position"] = _data["font-position"];
    if (_data["text"].hasType<std::string>())
        _text->component("Text")["value"] = _data["text"];
    if (_data["font-family"].hasType<std::string>())
        _text->component("Text")["font"] = _data["font-family"];
    if (_data["font-size"].hasType<float>())
        _text->component("Text")["size"] = _data["font-size"];
    if (_data["font-color"].hasType<ge::Color>())
        _text->component("Text")["color"] = _data["font-color"];

    _text->start();

    _gameObject.addChild(_text);

    // se serait bien de pas avoir a passer par un fichier
    // peut etre un loadFromMemory ?
    _gameObject.component("Input")["path"] = std::string("resources/data/events/button_events.xml");

    _gameObject.component("Transformable").start();
	_gameObject.component("Input").start();
    _gameObject.component("Sprite").start();
    _gameObject.component("Collider").start();

    if (_data["transition-type"].hasType<std::string>() && _data["transition-time"].hasType<float>()){
        std::string transitionType = _data["transition-type"];
        float transitionTime = _data["transition-time"];
        _tween.duration(transitionTime)
               .from(0.0f)
               .to(1.0f)
               .easing(transitionType);
    }

}

void TextBox::_changeStyle(std::string const &style) {
    std::string currentStyle = _data["currentStyle"];
    std::string nextStyle = _data["newStyle"];
    if (currentStyle != nextStyle){
        _nextStyle = style;
        return ;
    }
    if (!_data[style + "Data"].hasType<ge::Dictionary>()){
       return ;
   }
	_data["newStyle"] = style;
    ge::Dictionary nextData = _data[style + "Data"];

    std::unordered_map<std::string, ge::Any> tmpData = _data;
    for (auto it = nextData.begin(); it != nextData.end(); ++it){
        tmpData[it->first] = it->second;
    }

	_transform->setPosition(tmpData["position"]);
	_gameObject.component("Sprite")["texture"] = tmpData["background-image"];
	_gameObject.trigger("Sprite:change");

	_text->component("Text")["value"] = tmpData["text"];
    _text->component("Text")["font"] = tmpData["font-family"];
    _tween.start();
    _transitionStep = 0;
}

void TextBox::onStart() {

    if (_data.find("styleData") == _data.end())
    	_data["styleData"] = ge::eval(_data["style"]);
    if (_data.find("hoverStyleData") == _data.end())
	   _data["hoverStyleData"] = ge::eval(_data["hoverStyle"]);
    if (_data.find("activeStyleData") == _data.end())
	   _data["activeStyleData"] = ge::eval(_data["activeStyle"]);
    if (_data.find("focusStyleData") == _data.end())
	   _data["focusStyleData"] = ge::eval(_data["focusStyle"]);

	_updateData();
	_initComponents();

	_transform = _gameObject.component("Transformable")["transform"];
	_textTransform = _text->component("Transformable")["transform"];


    _gameObject.on("Collider:collision", [this] (ge::Array &){
        bool focused = _data["focused"];
        if (!focused)
	       	_changeStyle("hoverStyle");
        _data["hover"] = true;
    });

    _gameObject.on("Collider:non-collision", [this] (ge::Array &){
        bool focused = _data["focused"];
        if (!focused)
           	_changeStyle("style");
        _data["hover"] = false;
    });

    _gameObject.on("Button:mouseMoved", [this] (ge::Array &e) {
        if (_data["enabled"].hasType<bool>() && _data["enabled"].get<bool>() == false)
            return ;
        _gameObject.trigger("Collider:check", e[1]);
    });

    _gameObject.on("Button:mousePressed", [this] (ge::Array &) {
        if (_data["enabled"].hasType<bool>() && _data["enabled"].get<bool>() == false)
            return ;
        bool focused = _data["focused"];
        bool focusable = _data["focusable"];
        bool hover = _data["hover"];
        if (hover && !focused && focusable) {
        	_changeStyle("activeStyle");
            _data["focused"] = true;
        }
        else if (hover){
            _changeStyle("focusStyle");
        }
    });

    _gameObject.on("Button:mouseReleased", [this] (ge::Array &) {
        if (_data["enabled"].hasType<bool>() && _data["enabled"].get<bool>() == false)
            return ;
        bool focusable = _data["focusable"];
        bool hover = _data["hover"];
        bool focused = _data["focused"];
        if (hover){
            _gameObject.trigger("Button:clicked");
            if (_data.find("target") != _data.end() && _data["target"].hasType<std::string>()){
                _gameObject.getParent()->getParent()->trigger("Gui:changePage", _data["target"]);
            }
            if (_data.find("onclick") != _data.end() && _data["onclick"].hasType<std::string>()){
                std::string event = _data["onclick"];
                _gameObject.getParent()->getParent()->trigger(event);
            }
        }
        if (!hover && focused && focusable) {
            _changeStyle("style");
            _data["focused"] = false;
        }
        if (focusable)
            return ;
        if (hover)
            _changeStyle("hoverStyle");

    });

    _gameObject.on("TextBox:update", [this] (ge::Array &) {
        _changeStyle(_data["currentStyle"]);
    });
}

void TextBox::_resetTransition() {
    _transitionStep = 0;
    _data["currentStyle"] = _data["newStyle"];
    _updateData();
    std::string transitionType = _data["transition-type"];
    float transitionTime = _data["transition-time"];
    _tween.duration(transitionTime)
          .from(0.0f)
          .to(1.0f)
          .easing(transitionType);
    if (_nextStyle != ""){
        _changeStyle(_nextStyle);
        _nextStyle = "";
    }
}

void TextBox::onUpdate(float dt) {
    _tween.update(dt);
    if (_data["enabled"].hasType<bool>() && _data["enabled"].get<bool>() == false){
        _resetTransition();
        return ;
    }
    std::string currentStyle = _data["currentStyle"];
    std::string nextStyle = _data["newStyle"];
    if (currentStyle != nextStyle){
        if (_tween.isFinish()) {
            _resetTransition();
            return ;
        }
        ++_transitionStep;
        if (!_data[nextStyle + "Data"].hasType<ge::Dictionary>())
            return ;
        ge::Dictionary nextData = _data[nextStyle + "Data"];
        if (_data.find("size") != _data.end() && nextData.find("size") != nextData.end()){
            ge::Vector3f prevSize = _data["size"];
            ge::Vector3f nextSize = nextData["size"];
            ge::Vector3f newSize = prevSize + (nextSize - prevSize) * _tween.get();
            _gameObject.component("Sprite")["size"] = newSize;
            _gameObject.component("Collider")["box"] = ge::Boxf(newSize.x, newSize.y, newSize.z);
            _gameObject.trigger("Sprite:change");
        }
        if (_data.find("background-color") != _data.end() && nextData.find("background-color") != nextData.end()){
            ge::Color prevColor = _data["background-color"];
            ge::Color nextColor = nextData["background-color"];
            ge::Color newColor;
            newColor.x = static_cast<float>(prevColor.x) + (static_cast<float>(nextColor.x) - static_cast<float>(prevColor.x)) * _tween.get();
            newColor.y = static_cast<float>(prevColor.y) + (static_cast<float>(nextColor.y) - static_cast<float>(prevColor.y)) * _tween.get();
            newColor.z = static_cast<float>(prevColor.z) + (static_cast<float>(nextColor.z) - static_cast<float>(prevColor.z)) * _tween.get();
            newColor.w = static_cast<float>(prevColor.w) + (static_cast<float>(nextColor.w) - static_cast<float>(prevColor.w)) * _tween.get();
            _gameObject.component("Sprite")["color"] = newColor;
            _gameObject.trigger("Sprite:change");
        }
        if (_data.find("font-size") != _data.end() && nextData.find("font-size") != nextData.end()){
            float prevSize = _data["font-size"];
            float nextSize = nextData["font-size"];
            _text->component("Text")["size"] = prevSize + (nextSize - prevSize) * _tween.get();
        }
        if (_data.find("font-color") != _data.end() && nextData.find("font-color") != nextData.end()){
            ge::Color prevColor = _data["font-color"];
            ge::Color nextColor = nextData["font-color"];
            ge::Color newColor;
            newColor.x = static_cast<float>(prevColor.x) + (static_cast<float>(nextColor.x) - static_cast<float>(prevColor.x)) * _tween.get();
            newColor.y = static_cast<float>(prevColor.y) + (static_cast<float>(nextColor.y) - static_cast<float>(prevColor.y)) * _tween.get();
            newColor.z = static_cast<float>(prevColor.z) + (static_cast<float>(nextColor.z) - static_cast<float>(prevColor.z)) * _tween.get();
            newColor.w = static_cast<float>(prevColor.w) + (static_cast<float>(nextColor.w) - static_cast<float>(prevColor.w)) * _tween.get();
            _text->component("Text")["color"] = newColor;
        }
        if (_data.find("font-position") != _data.end() && nextData.find("font-position") != nextData.end()){
            ge::Vector3f prevPosition = _data["font-position"];
            ge::Vector3f nextPosition = nextData["font-position"];
            _textTransform->setPosition(prevPosition + (nextPosition - prevPosition) * _tween.get());
        }

    }
}

GE_EXPORT(TextBox)


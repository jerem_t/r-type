
#pragma once

#include <gEngine/Core/Component.hpp>
#include <gEngine/Math/Tween.hpp>
#include <gEngine/Math/Transform.hpp>

/**
 * @class TextBox
 * TextBox
 */
class TextBox : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  TextBox(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~TextBox();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);
private:
  void  _updateData(std::string const &style = "currentStyle");
  void  _initComponents();
  void  _changeStyle(std::string const &style);
  void  _resetTransition();

  ge::GameObjectPtr _text;
  std::shared_ptr<ge::Transform> _transform;
  std::shared_ptr<ge::Transform> _textTransform;
  ge::Tween<float> _tween;
  unsigned int    _transitionStep;
  std::string _nextStyle;
};


#pragma once

#include <unordered_map>
#include <gEngine/Core/Component.hpp>
#include <gEngine/Math/Transform.hpp>
#include <gEngine/Math/Box.hpp>

/**
 * @class OutOfBounds
 * Simple component to handle object behaviour according area limits.
 */
class OutOfBounds : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  OutOfBounds(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~OutOfBounds();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  /**
   * Respawn the entity at the opposite direction.
   */
  void _loopBehaviour(ge::Boxf const &);

  /**
   * Stop the entity to stay in the area.
   */
  void _stopBehaviour(ge::Boxf const &);

  /**
   * Just trigger events when it is necessary.
   */
  void _noneBehaviour(ge::Boxf const &);

private:

  typedef void (OutOfBounds::*Behaviour)(ge::Boxf const &);

  static std::unordered_map<std::string, Behaviour> _behaviours;
  Behaviour _behaviour;
  ge::TransformPtr _transform;
  ge::Boxf _area;
  ge::Vector3f _previous;

};

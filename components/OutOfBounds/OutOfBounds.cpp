
#include <iostream>
#include <gEngine/Core/GameObject.hpp>
#include "OutOfBounds.hpp"

std::unordered_map<std::string, OutOfBounds::Behaviour>
  OutOfBounds::_behaviours = {
  { "loop", &OutOfBounds::_loopBehaviour },
  { "stop", &OutOfBounds::_stopBehaviour },
  { "none", &OutOfBounds::_noneBehaviour }
};

OutOfBounds::OutOfBounds(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["area"] = ge::Boxf(1.0f, 1.0f);
  _data["behaviour"] = std::string("loop");
  _data["event"] = false;
}

OutOfBounds::~OutOfBounds() {}

void OutOfBounds::onStart() {
  _area = _data["area"];
  _transform = _gameObject.component("Transformable")["transform"];
  _behaviour = _behaviours[_data["behaviour"]];
  _previous = _transform->getGlobalPosition();
}

void OutOfBounds::onUpdate(float) {
  ge::Boxf &box = _gameObject.component("Collider")["box"];
  box.pos = _transform->getGlobalPosition();
  (this->*_behaviour)(box);
}

void OutOfBounds::_loopBehaviour(ge::Boxf const &box) {
  ge::Vector3f vec;
  for (size_t i = 0; i < 3; ++i) {
    if (box.pos[i] + box.size[i] < _area.pos[i]) {
      vec[i] = _area.size[i] + box.size[i];
      if (_data["event"])
        _gameObject.trigger("OutOfBounds:hit");
    } else if (box.pos[i] > _area.pos[i] + _area.size[i]) {
      vec[i] = -_area.size[i] - box.size[i];
      if (_data["event"])
        _gameObject.trigger("OutOfBounds:hit");
    }
  }
  _transform->translate(vec);
}

void OutOfBounds::_noneBehaviour(ge::Boxf const &box) {
  if (!_data["event"])
    return ;
  for (size_t i = 0; i < 3; ++i) {
    if (box.pos[i] + box.size[i] < _area.pos[i])
      _gameObject.trigger("OutOfBounds:hit");
    else if (box.pos[i] > _area.pos[i] + _area.size[i])
      _gameObject.trigger("OutOfBounds:hit");
  }
}

void OutOfBounds::_stopBehaviour(ge::Boxf const &box) {
  for (size_t i = 0; i < 3; ++i) {
    if (box.pos[i] < _area.pos[i])
      _transform->setPosition(_previous);
    else if (box.pos[i] + box.size[i] > _area.pos[i] + _area.size[i])
      _transform->setPosition(_previous);
  }
  _previous = _transform->getGlobalPosition();
}

GE_EXPORT(OutOfBounds)


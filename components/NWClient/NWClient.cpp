
#include <gEngine/Core/GameObject.hpp>
#include "NWClient.hpp"

NWClient::NWClient(ge::GameObject &gameObject)
: ge::Component(gameObject) {}

NWClient::~NWClient() {}

void NWClient::onStart() {}

void NWClient::onUpdate(float) {}

GE_EXPORT(NWClient)


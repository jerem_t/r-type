
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class NWClient
 * Simple client communication handling.
 */
class NWClient : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  NWClient(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~NWClient();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};

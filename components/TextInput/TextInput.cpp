
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Vector3.hpp>
#include "TextInput.hpp"

TextInput::TextInput(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _cap(false) {
  _data["style"] = std::string("");
  _data["hoverStyle"] = std::string("");
  _data["activeStyle"] = std::string("");
  _data["focusStyle"] = std::string("");

  _data["text"] = std::string("input");

  _data["maxCharacters"] = 25.0f;
}

TextInput::~TextInput() {
}

void TextInput::_editText(ge::Array const &e){
  std::string type = e[0].get<std::string>();
  std::string &text = _gameObject.component("TextBox")["text"];
  if (e[1].hasType<std::string>()) {
      std::string code = e[1].get<std::string>();
      if (type == "KeyPressed" && code == "BackSpace" && text.length() > 0)
	      text.pop_back();
      else if (type == "KeyPressed" && code == "Space")
      	text.push_back(' ');
      else if (type == "KeyPressed" && code == "Return")
		    ;
      else if (type == "KeyPressed" && (code == "LShift" || code == "RShift"))
      	_cap = true;
      else if (type == "KeyReleased" && (code == "LShift" || code == "RShift"))
      	_cap = false;
 	}
  if (type == "TextEntered"){
    uint32_t     code = e[1].get<uint32_t>();
    float maxf = _data["maxCharacters"];
    if (code >= 41 && code <= 176 && text.length() < static_cast<unsigned int>(maxf)){
      if (!_cap && code >= 'A' && code <= 'Z')
        text.push_back(code + 32);
      else
        text.push_back(code);
    }
  }
}

void TextInput::onStart() {
  
  _gameObject.component("TextBox")["text"] = _data["text"];
  _gameObject.component("TextBox")["style"] = _data["style"];
  _gameObject.component("TextBox")["hoverStyle"] = _data["hoverStyle"];
  _gameObject.component("TextBox")["activeStyle"] = _data["activeStyle"];
  _gameObject.component("TextBox")["focusStyle"] = _data["focusStyle"];

  _gameObject.component("TextBox")["focusable"] = true;
  _gameObject.component("TextBox").start();
  _gameObject.getRoot()->on("Context:event", [this] (ge::Array &e) {
  	bool focused = _gameObject.component("TextBox")["focused"];
  	if (focused){
	    _editText(e);
    }
  });
}

void TextInput::onUpdate(float) {}

GE_EXPORT(TextInput)



cmake_minimum_required(VERSION 2.6)

project(gEngine_TextInput)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_TextInput SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_TextInput gEngine.lib)
endif (MSVC)


#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class TextInput
 * TextInput
 */
class TextInput : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  TextInput(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~TextInput();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  void _editText(ge::Array const &e);

  bool  _cap;

};

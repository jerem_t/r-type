
#pragma once

#include <gEngine/Core/Component.hpp>
#include <gEngine/System/Platform.hpp>
#include <gEngine/Math/Transform.hpp>

namespace ge {

/**
 * @class Transformable
 * Component to set the transformations.
 */
class Transformable : public Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Transformable(GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Transformable();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  ge::TransformPtr _transform;

};

}

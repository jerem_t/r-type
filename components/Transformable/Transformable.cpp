
#include <gEngine/Core/GameObject.hpp>
#include "Transformable.hpp"

namespace ge {

Transformable::Transformable(GameObject &gameObject)
: Component(gameObject) {
	_data["transform"] = TransformPtr(new Transform);
	_data["position"] = ge::Vector3f();
	_data["rotation"] = ge::Vector3f();
	_data["scale"] = ge::Vector3f(1.0f);
}

Transformable::~Transformable() {
}

void Transformable::onStart() {
	_transform = _data["transform"];
	_gameObject["transform"] = _transform;
	Vector3f const &pos = _data["position"];
	_transform->setPosition(pos);
	Vector3f const &rotation = _data["rotation"];
	Quaternionf const &rot(rotation);
	_transform->setRotation(rot);
	Vector3f const &scale = _data["scale"];
	_transform->setScale(scale);
}

void Transformable::onUpdate(float) {
	ge::GameObject *parent = _gameObject.getParent();
	ge::TransformPtr t = NULL;
	if (parent && parent->hasComponent("Transformable"))
		t = parent->component("Transformable")["transform"];
	if (t)
		_transform->calcGlobalMatrix(*t);
	else
		_transform->calcGlobalMatrix();
}

GE_EXPORT(Transformable)

}


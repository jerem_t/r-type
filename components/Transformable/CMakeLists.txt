
cmake_minimum_required(VERSION 2.6)

project(gEngine_Transformable)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_Transformable SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_Transformable gEngine.lib)
endif (MSVC)
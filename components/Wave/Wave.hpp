
#pragma once

#include <map>
#include <gEngine/Core/Component.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>

/**
 * @class Wave
 * Add some behaviour on the way of moving entities.
 */
class Wave : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Wave(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Wave();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  float _currentTime;
  float _baseLine;
  ge::TransformPtr _transform;
};

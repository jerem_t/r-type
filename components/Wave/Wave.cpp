
#include <cmath>
#include "Wave.hpp"

Wave::Wave(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _currentTime(0)
, _baseLine(0) {
  _data["width"] = 0.0f;
  _data["height"] = 0.0f;
}

Wave::~Wave() {}

void Wave::onStart() {
  _transform = _gameObject.component("Transformable")["transform"];
  _baseLine = _transform->getPosition().y;
}

void Wave::onUpdate(float) {
  ge::Vector3f pos = _transform->getPosition();
  float w = _data["width"];
  float h = _data["height"];
  pos.y = h * std::cos(pos.x / w) + _baseLine;
  _transform->setPosition(pos);
}

GE_EXPORT(Wave)

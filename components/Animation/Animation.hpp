
#pragma once

#include <map>
#include <gEngine/Core/Component.hpp>
#include <gEngine/Math/Box.hpp>

/**
 * @class Animation
 * Simple sprite animation handling.
 */
class Animation : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Animation(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Animation();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  /**
   * @struct AnimationData
   * Informations about the animation.
   */
  struct AnimationData {
    std::string name; /** The name of the animation */
    size_t firstFrame; /** The index of the first frame of the animation */
    size_t lastFrame;  /** The index of the last frame of the animation */
    bool repeat;  /** True to loop on this animation */
  };

  std::map<size_t, ge::Boxf>            _tiles;
  std::map<std::string, AnimationData>  _animations;
  AnimationData                         _currentAnimation;
  bool                                  _running;
  float                                 _currentTime;
  float                                 _frameTime;
  size_t                                _currentFrame;

};


#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Parsers/Eval.hpp>
#include <gEngine/Data/SpriteData.hpp>
#include "Animation.hpp"

Animation::Animation(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _running(false)
, _currentTime(0)
, _frameTime(0.0f)
, _currentFrame(0) {
  _data["interval"] = 0.003f;
}

Animation::~Animation() {}

void Animation::onStart() {
  if (_data.find("path") == _data.end())
    return ;

  // Parse XML

  ge::Xml root;
  if (!root.loadFromFile(_data["path"]))
    return ;
  ge::XmlArray tiles;
  root.getChildrenByName("tile", tiles);
  for (auto &it : tiles) {
    size_t i = ge::convert<std::string, size_t>(it->getAttr("id"));
    ge::Vector2f pos = ge::eval(it->getAttr("pos"));
    ge::Vector2f size = ge::eval(it->getAttr("size"));
    _tiles[i] = ge::Boxf(size.x, size.y, 0, pos.x, pos.y, 0);
  }
  ge::XmlArray anims;
  root.getChildrenByName("anim", anims);
  for (auto &it : anims) {
    std::string name = it->getAttr("name");
    size_t from = ge::convert<std::string, size_t>(it->getAttr("from"));
    size_t to = ge::convert<std::string, size_t>(it->getAttr("to"));
    bool repeat = false;
    if (it->has("repeat"))
      repeat = ge::eval(it->getAttr("repeat"));
    _animations[name] = { name, from, to, repeat };
  }

  // Set framrTime

  _frameTime = _data["interval"];

  // Add event listeners

  _gameObject.on("Animation:play", [this] (ge::Array const &event) {
    if (event[0].hasType<std::string>())
      _currentAnimation = _animations[event[0]];
    _running = true;
  });

  _gameObject.on("Animation:pause", [this] (ge::Array const &) {
    _running = false;
  });

}

void Animation::onUpdate(float dt) {
  if (_running) {
    _currentTime += dt;
    if (_currentTime > _frameTime) {
      _currentTime = 0;
      ge::SpriteData spriteData;

      spriteData.setTextureRect(_tiles[_currentFrame]);
      _gameObject.trigger("Sprite:change", spriteData);
      if (_currentFrame < _currentAnimation.lastFrame)
        ++_currentFrame;
      else {
        _currentFrame = _currentAnimation.firstFrame;
        if (!_currentAnimation.repeat) {
          _gameObject.trigger("Animation:finish", _currentAnimation.name);
          _running = false;
        }
      }
    }
  }
}

GE_EXPORT(Animation)



#include <iostream>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Resources/Resource.hpp>
#include "SFMLAudio.hpp"

SFMLAudio::SFMLAudio(ge::GameObject &gameObject)
: ge::Component(gameObject) {
   _data["sound"] = true;
}

SFMLAudio::~SFMLAudio() {
}

void SFMLAudio::onStart() {
  _gameObject.on("Context:close", [this] (ge::Array &){
    for (auto it = _sounds.begin(); it != _sounds.end();){
      it->second->stop();
      delete it->second;
      it = _sounds.erase(it);
    }
    for (auto it = _musics.begin(); it != _musics.end();){
      it->second->stop();
      delete it->second;
      it = _musics.erase(it);
    }
  });
  _gameObject.on("Audio:playSound", [this] (ge::Array &event) {
    if (_data["sound"].get<bool>())
      _playSound(event[0], event[1], event[2], event[3]);
  });
  _gameObject.on("Audio:pauseSound", [this] (ge::Array &event) {
    _pauseSound(event[0]);
  });
  _gameObject.on("Audio:stopSound", [this] (ge::Array &event) {
    _removeSound(event[0]);
  });

  _gameObject.on("Audio:playSounds", [this] (ge::Array &) {
    for (auto it = _sounds.begin(); it != _sounds.end(); ++it){
      it->second->play();
    }
  });
  _gameObject.on("Audio:pauseSounds", [this] (ge::Array &) {
    for (auto it = _sounds.begin(); it != _sounds.end(); ++it){
      it->second->pause();
    }
  });
  _gameObject.on("Audio:stopSounds", [this] (ge::Array &) {
    for (auto it = _sounds.begin(); it != _sounds.end();){
      it->second->stop();
      delete it->second;
      it = _sounds.erase(it);
    }
  });

  _gameObject.on("Audio:loadMusic", [this] (ge::Array &event) {
      _loadMusic(event[0], event[1]);
  });

  _gameObject.on("Audio:playMusic", [this] (ge::Array &event) {
    if (_data["sound"].get<bool>())
      _playMusic(event[0]);
  });

  _gameObject.on("Audio:pauseMusic", [this] (ge::Array &event) {
      std::string name = event[0];
      if (_musics[name] != NULL)
        _musics[name]->pause();
  });

  _gameObject.on("Audio:resumeMusic", [this] (ge::Array &event) {
      std::string name = event[0];
      if (_musics[name] != NULL)
        _musics[name]->play();
  });

  _gameObject.on("Audio:pauseMusics", [this] (ge::Array &) {
    for (auto it = _musics.begin(); it != _musics.end(); ++it){
      if (it->second && it->second->getStatus() == sf::SoundSource::Status::Playing){
        it->second->pause();
      }
    }
  });

}

void SFMLAudio::onUpdate(float) {
  for (auto it = _sounds.begin(); it != _sounds.end();) {
    if (it->second->getStatus() == sf::SoundSource::Status::Stopped){
      delete it->second;
      it = _sounds.erase(it);
    }
    else
      ++it;
  }
}

void SFMLAudio::_playSound(std::string const &name, bool loop, float start, float volume) {
  ge::Resource<sf::SoundBuffer> buffer(name);
  sf::Sound *sound = new sf::Sound(*buffer);
  if (start > 0)
    sound->setPlayingOffset(sf::seconds(start));
  sound->setLoop(loop);
  sound->play();
  sound->setVolume(volume);
  _sounds.push_back({name, sound});
}

void SFMLAudio::_pauseSound(std::string const &name) {
  for (auto it = _sounds.begin(); it != _sounds.end(); ++it){
    if (it->first == name)
      it->second->pause();
  }
}

void SFMLAudio::_removeSound(std::string const &name) {
  auto it = _sounds.begin();
  while (it != _sounds.end()){
    if (it->first == name){
      it->second->stop();
      delete it->second;
      it = _sounds.erase(it);
    }
    else
      ++it;
  }
}

void SFMLAudio::_loadMusic(std::string const &name, std::string const &path){
  if (_musics[name] == NULL){
    _musics[name] = new sf::Music;
    if (!_musics[name]->openFromFile(path)){
      std::cerr << "Audio : failed to open " << name << std::endl;
      delete _musics[name];
      _musics[name] = NULL;
      return ;
    }
    _musics[name]->setLoop(true);
    _musics[name]->setVolume(30);
  }
}
 
void SFMLAudio::_playMusic(std::string const &name) {
  if (_musics[name] != NULL)
    _musics[name]->play();
}

GE_EXPORT(SFMLAudio)


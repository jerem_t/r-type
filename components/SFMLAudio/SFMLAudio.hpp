
#pragma once

#include <gEngine/Core/Component.hpp>
#include <unordered_map>
#include <SFML/Audio.hpp>

/**
 * @class SFMLAudio
 * Handle sounds using SFML library.
 */
class SFMLAudio : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  SFMLAudio(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~SFMLAudio();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  void _playSound(std::string const &name, bool loop, float start, float volume);
  void _pauseSound(std::string const &name);
  void _removeSound(std::string const &name);

  void _loadMusic(std::string const &name, std::string const &path);
  void _playMusic(std::string const &name);

private:
  std::list<std::pair<std::string, sf::Sound *> > _sounds;
  std::unordered_map<std::string, sf::Music *> _musics;
};

//
// NumberConversion.hpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sat Oct 19 16:59:19 2013 jordy domingos-mansoni
// Last update Sat Oct 19 17:52:30 2013 jordy domingos-mansoni
//

#ifndef		NUMBER_CONVERSION_HPP
# define	NUMBER_CONVERSION_HPP

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <limits>

namespace conversion
{

  /*
  ** Convert a number to a string.
  */
  template		<typename NumberType>
  std::string	ToString(const NumberType &value) {
    std::ostringstream	out;
    
    out << value;
    return (out.str());
  }

}  // conversion

#endif		/* NUMBER_CONVERSION_HPP */

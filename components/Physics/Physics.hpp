
#pragma once

#include <gEngine/Core/Component.hpp>

#include "Quadtree.hpp"
#include "Point.hpp"

/**
 * @class Physics
 * Handle the Game entities' physics
 */
class Physics : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Physics(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Physics();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  Quadtree _quadtree;
};

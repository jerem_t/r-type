//
// Point.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Fri Nov  1 10:46:52 2013 jordy domingos-mansoni
// Last update Sat Nov  2 18:59:52 2013 jordy domingos-mansoni
//

#ifndef		POINT_HH_
#define		POINT_HH_

/*
** 2d point representation.
*/
template <typename CoordinateType>
class Point {
public:
  Point() : x_(0), y_(0) {};
  Point(CoordinateType x, CoordinateType y) : x_(x), y_(y) {}
  Point(const Point& point) : x_(point.x_), y_(point.y_) {}

  void SetX(CoordinateType x) { x_ = x; }
  CoordinateType x() { return x_; }

  void SetY(CoordinateType y) { y_ = y; }
  CoordinateType y() { return y_; }

  Point& operator=(const Point& point);
  
  Point operator+(const Point& point) {
    return Point(x_ + point.x_, y_ + point.y_);
  };

  Point operator-(const Point& point) {
    return Point(x_ - point.x_, y_ - point.y_);
  };

  Point operator/(CoordinateType divider) {
    return Point(x_ / divider, y_ / divider);
  };

  Point& operator+=(const Point& point) {
    x_ += point.x_;
    y_ += point.y_;
    return *this;
  };
  Point& operator-=(const Point& point) {
    x_ -= point.x_;
    y_ -= point.y_;
    return *this;
  };

  ~Point() {};
private:
  CoordinateType x_;
  CoordinateType y_;
};

template <typename CoordinateType>
bool operator==(const Point<CoordinateType>& lhs,
   const Point<CoordinateType>& rhs) {
  return lhs.x_ == rhs.x_ && lhs.y_ == rhs.y_;
}

template <typename CoordinateType>
bool operator!=(const Point<CoordinateType>& lhs,
   const Point<CoordinateType>& rhs) {
  return !(lhs == rhs);
}

template <typename CoordinateType>
Point<CoordinateType>& Point<CoordinateType>::operator=(const Point<CoordinateType>& point) {
  if (this != &point) {
    x_ = point.x_;
    y_ = point.y_;
  }
  return *this;
}

#endif		/* POINT_HH_ */

//
// StringConversion.hpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sat Oct 19 16:58:37 2013 jordy domingos-mansoni
// Last update Sat Nov  2 17:53:06 2013 jordy domingos-mansoni
//

#ifndef		STRING_CONVERSION_HPP
# define	STRING_CONVERSION_HPP

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <limits>

namespace conversion
{

  /*
  ** Convert a std::string to a number.
  */
  template		<typename NumberType>
  NumberType   	ToNumber(const std::string &value) explicit {
    std::istringstream	in(value);
    NumberType 		convert;
    
    in.precision(std::numeric_limits<NumberType>::digits10);
    in >> convert;
    return convert;
  }

  /*
  ** Convert a C-style string to a number.
  */
  template		<typename NumberType>
  NumberType	ToNumber(const char *str) {
    char	sign = 1;
    NumberType	nb = 0;
    
    while (!(*str >= '0' && *str <= '9')) {
      if (*str == '-')
	sign * -1;
      ++str;
    }
    
    while (*str != '\0') {
      if (!(*str >= '0' && *str <= '9'))
	return 0;
      nb = (nb * 10) + '0' + (*str)++;
    }
    return nb * sign;
  }

}

#endif		/* STRING_CONVERSION_HPP */

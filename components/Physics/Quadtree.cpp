//
// Quadtree.cpp for  in /home/doming_a//Project/Tech3/R-type/r-type/components/Physics
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Fri Nov 22 20:26:27 2013 jordy domingos-mansoni
// Last update Tue Nov 26 23:25:35 2013 jordy domingos-mansoni
//

#include "Quadtree.hpp"
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>
#include <unistd.h>

/*
** Implementation of a templated quadtree.
*/
namespace {

  // /*
  // ** Checks if there is a collision between the objects in |potetialCollision|.
  // */
  // template <typename Object, typename CoordinateType>
  // bool PotentialAABBCollisions(
  //      std::pair<ge::GameObject *, ge::GameObject *> &potentialCollision)
  // {
  //   Rectangle<Vector2f>	bounding_box_1 = 
  //       potentialCollision.first->BoundingBox();
  //   Rectangle<Vector2f>	bounding_box_2 =
  //       potentialCollision.second->BoundingBox();

  //   if (CheckLeftCollision(bounding_box_1, bounding_box_2) || 
  // 	CheckRightCollision(bounding_box_1, bounding_box_2) ||
  // 	CheckUpCollision(bounding_box_1, bounding_box_2) ||
  // 	CheckDownCollision(bounding_box_1, bounding_box_2))
  //     return false;
  //   return true;
  // }
}


Quadtree::Quadtree(ge::Box<float> area,
    int maxDepth, int maxObjectPerNode, int minObjectPerNode) :
  area_(area),
  // children_(NULL),
  max_depth_(maxDepth),
  min_object_per_node_(minObjectPerNode),
  max_object_per_node_(maxObjectPerNode),
  has_children_(false),
  nbObject_(0),
  depth_(1)
{
}

Quadtree::Quadtree(ge::Vector3f cornerTopLeft,
    ge::Vector3f cornerDownRight, int maxDepth, int maxObjectPerNode,
    int minObjectPerNode) :
  area_(abs(cornerTopLeft.x - cornerDownRight.x),
	abs(cornerTopLeft.y - cornerDownRight.y),
	0.0f,
	cornerTopLeft.x, cornerTopLeft.y),
  // children_(NULL),
  max_depth_(maxDepth),
  min_object_per_node_(minObjectPerNode),
  max_object_per_node_(maxObjectPerNode),
  has_children_(false),
  nbObject_(0),
  depth_(1)
{
}

Quadtree::Quadtree(Quadtree const &other) {
  operator=(other);
}

Quadtree &Quadtree::operator=(Quadtree const &other)
{
  if (this != &other)
    {
      area_ = other.area_;
      // children_ = other.children_;
      max_depth_ = other.max_depth_;
      min_object_per_node_ = other.min_object_per_node_;
      max_object_per_node_ = other.max_object_per_node_;
      has_children_ = other.has_children_;
      nbObject_ = other.nbObject_;
      objects_ = other.objects_;
      depth_ = other.depth_;
    }
  return (*this);
}

/*
** ****************************** PRIVATE METHODS ******************************
*/

Quadtree::Quadtree(ge::Vector3f cornerTopLeft,
     ge::Vector3f cornerDownRight, int depth, int maxDepth,
     int maxObjectPerNode, int minObjectPerNode) :
  area_(abs(cornerTopLeft.x - cornerDownRight.x),
	abs(cornerTopLeft.y - cornerDownRight.y),
	0.0f,
	cornerTopLeft.x, cornerTopLeft.y),
  // children_(NULL),
  max_depth_(maxDepth),
  min_object_per_node_(minObjectPerNode),
  max_object_per_node_(maxObjectPerNode),
  has_children_(false),
  nbObject_(0),
  depth_(depth)
{
}

Quadtree::Quadtree(ge::Box<float> area,
    int depth, int maxDepth, int maxObjectPerNode, int minObjectPerNode) :
  area_(area),
  // children_(NULL),
  max_depth_(maxDepth),
  min_object_per_node_(minObjectPerNode),
  max_object_per_node_(maxObjectPerNode),
  has_children_(false),
  nbObject_(0),
  depth_(depth)
{
}

void Quadtree::ObjectManager(ge::GameObject *object, ge::Vector3f pos, bool add)
{
  ge::Box<float> box = object->component("Collider")["box"];
  box.pos = pos;

  for (int x = 0; x < 2; x++)
  {
    for (int y = 0; y < 2; y++)
    {
      if (box.hit(children_[x][y]->area_)) {
      	if (add) {
	  children_[x][y]->Add(object);
	}
	else
	  children_[x][y]->RemoveObjectFromTree(object, pos);
      }
      
    }
  }
}

void Quadtree::GetObjects(std::list<ge::GameObject *> &findedObj,
    ge::GameObject *object, ge::Vector3f)
{
  ge::Box<float> box = object->component("Collider")["box"];

    for (int x = 0; x < 2; ++x) {
      for (int y = 0; y < 2; ++y) {
    	if (box.hit(children_[x][y]->area_))
    	  children_[x][y]->GetObjectList(findedObj, object);      
      }
    }
}

void Quadtree::GetObjectList(std::list<ge::GameObject *> &findedObj, ge::GameObject *obj)
{
  if (has_children_)
    GetObjects(findedObj, obj, obj->component("Transformable")["transform"].get<ge::TransformPtr>()->getGlobalPosition());

  for (auto it = objects_.begin();
       it != objects_.end(); ++it) {
    findedObj.push_back(*it);
  }
}

std::vector<ge::Box<float> > SplitRectangleInFour(
    ge::Box<float> area_to_split) {

  std::vector<ge::Box<float>> subrectangles;

  subrectangles.push_back(ge::Box<float>(area_to_split.size.x / 2, area_to_split.size.y / 2, 0.0f,
				   area_to_split.pos.x, area_to_split.pos.y));

  subrectangles.push_back(ge::Box<float>(area_to_split.size.x / 2, area_to_split.size.y / 2, 0.0f,
				   area_to_split.pos.x + area_to_split.size.x / 2,
				   area_to_split.pos.y));

  subrectangles.push_back(ge::Box<float>(area_to_split.size.x / 2, area_to_split.size.y / 2, 0.0f,
				   area_to_split.pos.x,
				   area_to_split.pos.y + area_to_split.size.y / 2));

  subrectangles.push_back(ge::Box<float>(area_to_split.size.x / 2, area_to_split.size.y / 2, 0.0f,
				   area_to_split.pos.x + area_to_split.size.x / 2,
				   area_to_split.pos.y + area_to_split.size.y / 2));
  return subrectangles;
}

void Quadtree::HaveChildren()
{
  std::vector<ge::Box<float> > rects = SplitRectangleInFour(area_);
  {
    int rects_iterator = 0;

    for (int x = 0; x < 2; ++x) {
      for (int y = 0; y < 2; ++y) {
	children_[x][y] = new Quadtree(rects[rects_iterator++], depth_ + 1, max_depth_,
			      max_object_per_node_, min_object_per_node_);
      }
    }
  }
  for (ge::GameObject *obj : objects_)
    ObjectManager(obj, obj->component("Transformable")["transform"].get<ge::TransformPtr>()->getGlobalPosition(), true);
  objects_.clear();
  has_children_ = true;
}

void	Quadtree::Add(ge::GameObject* obj)
{
  nbObject_++;
  if (!has_children_ && depth_ < max_depth_ && nbObject_ > max_object_per_node_)
    HaveChildren();
  if (has_children_)
    ObjectManager(obj, obj->component("Transformable")["transform"].get<ge::TransformPtr>()->getGlobalPosition(), true);
  else {
    obj->component("Collider")["quadtree"] = true;
    objects_.insert(obj);

    // ge::Boxf box = obj->component("Collider")["box"];
    // ge::Vector3f pos = obj->component("Transformable")["transform"].get<ge::TransformPtr>()->getGlobalPosition();
  }
}

void	Quadtree::DestroyChildren()
{
  CollectObject(objects_);
  for(int x = 0; x < 2; x++)
    {
      for(int y = 0; y < 2; y++)
	{
	  delete children_[x][y];
	}
    }
  has_children_ = false;
}

void Quadtree::RemoveObjectFromTree(ge::GameObject* object, ge::Vector3f position)
{
  if (nbObject_) {
    nbObject_--;
  }
  if (has_children_ && nbObject_ < min_object_per_node_)
    {
      DestroyChildren();
    }
  if (has_children_)
    {
      ObjectManager(object, position, false);
    }
  else
    {
      object->component("Collider")["quadtree"] = false;
      if (!objects_.erase(object)) {
        ge::Vector3f pos = object->component("Transformable")["transform"].get<ge::TransformPtr>()->getGlobalPosition();
      }
      else {
	ge::Boxf box = object->component("Collider")["box"];
      }
    }
}
  
void Quadtree::UpdateObjectPosition(ge::GameObject* object, ge::Vector3f old_position)
{
  ge::Boxf box = object->component("Collider")["box"];

  if (box.pos == old_position) {
    return ;
  }
  ge::Boxf box1 = ge::Boxf(box.size.x, box.size.y, box.size.z, old_position.x, old_position.y, old_position.z);
  
  if (object->component("Collider")["quadtree"] && box1.hit(area_)) {
    RemoveObjectFromTree(object, old_position);
  }
  
  if (!object->component("Collider")["quadtree"] && box.hit(area_)) {
    Add(object);
  } else {
    object->die();
  }
}

void Quadtree::Remove(ge::GameObject *obj)
{
  RemoveObjectFromTree(obj, obj->component("Transformable")["transform"].get<ge::TransformPtr>()->getGlobalPosition());
}

void Quadtree::CollectObject(std::set<ge::GameObject *> &objectSet)
{
  if (has_children_)
    {
      for(int x = 0; x < 2; x++)
	{
	  for(int y = 0; y < 2; y++)
	    {
	      children_[x][y]->CollectObject(objectSet);
	    }
	}
    }
  else
    {
      for(auto it = objects_.begin();
	  it != objects_.end();  it++)
	{
	  ge::GameObject* object = *it;
	  objectSet.insert(object);
	}
    }
}

void Quadtree::PotentialObjectCollisions(
    std::vector<std::pair<ge::GameObject *, ge::GameObject *> > &collisions)
{
  if (has_children_)
    {
      for(int x = 0; x < 2; x++)
	{
	  for(int y = 0; y < 2; y++)
	    {
	      children_[x][y]->PotentialObjectCollisions(collisions);
	    }
	}
    }
  else
    {
      for(auto it = objects_.begin();
	  it != objects_.end(); ++it)
	{
	  ge::GameObject *obj1 = *it;

	  for(auto it2 = objects_.begin();
	      it2 != objects_.end(); ++it2)
	    {
	      ge::GameObject *obj2 = *it2;
	      if (obj1 != obj2)
		{
		  std::pair<ge::GameObject *, ge::GameObject *>  bp(obj1, obj2);
		  collisions.push_back(bp);
		}
	    }
	}
    }
}

std::vector<std::pair<ge::GameObject *, ge::GameObject *> > Quadtree::CollectCollisions()
{
  std::vector<std::pair<ge::GameObject *, ge::GameObject *> > collisions;

  PotentialObjectCollisions(collisions);
  return collisions;
}

Quadtree::~Quadtree()
{
  if (has_children_)
    DestroyChildren();
}

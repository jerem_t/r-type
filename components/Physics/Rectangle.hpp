//
// Rectangle.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Thu Oct 31 21:44:27 2013 jordy domingos-mansoni
// Last update Sat Nov  2 22:55:10 2013 jordy domingos-mansoni
//

#ifndef		RECTANGLE_HH_
#define		RECTANGLE_HH_

#include <cmath>

#include "Point.hpp"
#include "Vector2d.hpp"

/*
** A rectangle representation.
*/
template <typename CoordinateType>
class Rectangle {
public:
  Rectangle() : length_(0.0), width_(0.0), center_(0.0, 0.0) {};
  Rectangle(CoordinateType length, CoordinateType width,
      Point<CoordinateType> center) : length_(length),
				      width_(width),
				      center_(center) {}

  Rectangle(Point<CoordinateType> top_left_corner,
	    Point<CoordinateType> down_right_corner);

  Rectangle(const Rectangle& rect) : length_(rect.length_),
				     width_(rect.width_),
				     center_(rect.center_) {}

  Rectangle &operator=(const Rectangle& rect);

  CoordinateType length() const { return length_; }
  void SetLength(CoordinateType length) { length_ = length; }

  CoordinateType width() const { return width_; }
  void SetWidth(CoordinateType width) { width_ = width; }

  Point<CoordinateType> center() const { return center_; }
  void SetCenter(Point<CoordinateType> center) { center_ = center; }

  ~Rectangle() {}
private:
  CoordinateType length_;
  CoordinateType width_;
  Point<CoordinateType> center_;
};

template <typename CoordinateType>
Rectangle<CoordinateType>::Rectangle(Point<CoordinateType> top_left_corner,
				     Point<CoordinateType> down_right_corner) {
  length_ = std::abs(down_right_corner.x() - top_left_corner.x());
  width_ = std::abs(down_right_corner.y() - top_left_corner.y());
  center_ = (top_left_corner + down_right_corner) / 2;
}

template <typename CoordinateType>
Rectangle<CoordinateType> &Rectangle<CoordinateType>::operator=(
    const Rectangle<CoordinateType>& rect) {
  if (this != &rect) {
    length_ = rect.length_;
    width_ = rect.width_;
    center_ = rect.center_;
  }
  return (*this);
}

#endif		/* RECTANGLE_HH_ */

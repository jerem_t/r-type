
#include <gEngine/Core/GameObject.hpp>
#include	<gEngine/Math/Box.hpp>
#include "Physics.hpp"
#include <thread>
#include <unistd.h>
#include <gEngine/Math/Transform.hpp>
#include <gEngine/System/Thread.hpp>

Physics::Physics(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _quadtree(ge::Box<float>(1.0f, 1.0f, 0.0f, 0.0f, 0.0f)) {}

Physics::~Physics() {}

void Physics::onStart() {
  _gameObject.on("Physics:add", [this] (ge::Array &event) {
      ge::GameObject *obj = event[0];
      
      obj->on("die", [obj, this](ge::Array &) {
	  if (obj->component("Collider")["quadtree"])
	    _quadtree.Remove(obj);
    	});
    });
  
  _gameObject.on("Physics:move", [this] (ge::Array &event) {
      _quadtree.UpdateObjectPosition(event[0], event[1]);
    });  
}

void Physics::onUpdate(float) {
  std::vector<std::pair<ge::GameObject *, ge::GameObject *> > collisions;
  collisions = _quadtree.CollectCollisions();


  for (std::vector<std::pair<ge::GameObject *, ge::GameObject *> >::iterator it = collisions.begin();
       it != collisions.end(); ++it) {
    (*it).first->trigger("Collider:check", (*it).second);
  }
}

GE_EXPORT(Physics)

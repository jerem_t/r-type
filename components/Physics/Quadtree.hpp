//
// Quadtree.hh for  in /home/doming_a//Project/Tech2/C++/bomberman/src/engine/physics
//
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
//
// Started on  Mon May 20 13:24:11 2013 jordy domingos-mansoni
// Last update Tue Nov 26 21:30:30 2013 jordy domingos-mansoni
//

#ifndef		QUADTREE_HH_
#define		QUADTREE_HH_

#include	<gEngine/Core/Component.hpp>
#include	<gEngine/Math/Box.hpp>
#include	<gEngine/Math/Vector3.hpp>
#include	<iostream>
#include	<list>
#include	<map>
#include	<vector>
#include	<set>
#include	<string>

#include	"Rectangle.hpp"

/*
** Definition of a templated quadtree.
*/
// template <typename Object, typename CoordinateType>
class Quadtree
{
public:
  /*
  ** To construct a Quadtree, you have to provide at least the
  ** top left corner position and the down right corner position.
  ** 
  ** |maxDepth| is the maximum depth of the list of sub-nodes.
  ** |maxObjectPerNode| is the maximum number of object per node.
  ** |minObjectPerNode| is the minimum number of object per node.
  **
  ** If you increase |maxDepth| there will be more division of
  ** the space, the collision detection will be faster but the 
  ** tree will be bigger.
  **
  ** If you increase |maxObjectPerNode|, the collision detection
  ** will be slower but the tree will be smaller.
  **
  */

  Quadtree(ge::Vector3f cornerTopLeft, ge::Vector3f cornerDownRight,
	   int maxDepth = 4, int maxObjectPerNode = 20,
	   int minObjectPerNode = 1);

  Quadtree(ge::Box<float> area,
	   int maxDepth = 3, int maxObjectPerNode = 20,
	   int minObjectPerNode = 1);
  
  Quadtree(Quadtree const &);

  Quadtree &operator=(Quadtree const &);

  /*
  ** This tree doesn't leak. When it is destoy, all the nodes are
  ** destroyed with it.
  */
  ~Quadtree();

  /*
  ** Add an object to the quadtree.
  */
  void Add(ge::GameObject *object);

  /*
  ** Remove an object from the quadtree.
  */
  void Remove(ge::GameObject* object);

  /*
  ** Update the object's position in the quadtree.
  */
  void UpdateObjectPosition(ge::GameObject* object,
			    ge::Vector3f OldPosition);

  /*
  ** Returns a vector which contains all the object in coliision represented
  ** by pair.
  */
  std::vector<std::pair<ge::GameObject *, ge::GameObject *> > CollectCollisions();

  /*
  ** Fill the list |findedObj| with all the objects in the same area as
  ** the |object|.
  */
  void GetObjectList(std::list<ge::GameObject *> &findedObj,
  		     ge::GameObject *object);

  /*
  ** Get all the object from the Quadtree and put them into |object_set|.
  */
  void CollectObject(std::set<ge::GameObject *> &object_set);

private:
  /*
  ** The area managed by the node.
  */
  ge::Box<float>	area_;

  /*
  ** Children of the current node.
  */
  Quadtree*	children_[2][2];

  size_t       	max_depth_;

  size_t       	min_object_per_node_;
  size_t       	max_object_per_node_;

  bool		has_children_;

  /*
  ** The number of object contained in the node. The objects are stored in the
  ** leaves of the tree.
  */
  size_t       	nbObject_;

  std::set<ge::GameObject* >	objects_;

  /*
  ** Current depth.
  */
  size_t       	depth_;

  Quadtree() {};
  Quadtree(ge::Vector3f cornerTopLeft, ge::Vector3f cornerDownRight, 
	   int depth, int maxDepth, int maxObjectPerNode,
	   int minObjectPerNode);
  Quadtree(ge::Box<float> area, int depth, int maxDepth,
	   int maxObjectPerNode, int minObjectPerNode);

  /*
  ** Move or Remove an object from the Quadtree.
  */
  void ObjectManager(ge::GameObject *object, ge::Vector3f position, bool add);

  /*
  ** Split the current |area_| in four and dispatch |objects_| into the subs-areas.
  */
  void HaveChildren();

  /*
  ** Delete all the node's children.
  */
  void DestroyChildren();

  /*
  ** Implementation of removing an object from the Quadtree.
  */
  void RemoveObjectFromTree(ge::GameObject* object, ge::Vector3f position);

  /*
  ** Collect all the object which are in collision and put them into |collisions|.
  ** The objects are grouped by pair.
  */
  void PotentialObjectCollisions(std::vector<std::pair<ge::GameObject *, ge::GameObject *> > &collisions);

  /*
  ** Get all the objects contained in the area where the |object| at the position |pos| is.
  */
  void GetObjects(std::list<ge::GameObject *> &findedObj, ge::GameObject *object, ge::Vector3f pos);
};

#endif		/* QUADTREE_HH_ */

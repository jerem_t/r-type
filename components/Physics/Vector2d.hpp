//
// Point.hpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Tue Oct 15 13:24:36 2013 jordy domingos-mansoni
// Last update Sat Nov  2 18:12:36 2013 jordy domingos-mansoni
//

#include <string>

#include "NumberConversion.hpp"

/*
** Two dimension vector representation.
*/
template <typename CoordinateType>
class Vector2d {
 public:
  Vector2d(): x_(0), y_(0) {}
  Vector2d(CoordinateType x, CoordinateType y): x_(x), y_(y) {}
  Vector2d(const Vector2d &vec): x_(vec.x_), y_(vec.y_) {}

  CoordinateType x() const { return x_; }
  void SetX(CoordinateType x) { x_ = x; } 

  CoordinateType y() const { return y_; }
  void SetY(CoordinateType y) { y_ = y; }

  // True if both components of the vector are 0.
  bool IsZero() {return !x_ && !y_;}

  // Add the components of the |other| vector to the current vector.
  void Add(const Vector2d& other);

  // Subtract the components of the |other| vector from the current vector.
  void Subtract(const Vector2d& other);

  // Divide the components of the current vector by |value|.
  void Divide(CoordinateType value);

  void operator+=(const Vector2d& other) { Add(other); }
  void operator-=(const Vector2d& other) { Subtract(other); }

  void SetToMin(const Vector2d& other) {
    x_ = x_ <= other.x_ ? x_ : other.x_;
    y_ = y_ <= other.y_ ? y_ : other.y_;
  }

  void SetToMax(const Vector2d& other) {
    x_ = x_ >= other.x_ ? x_ : other.x_;
    y_ = y_ >= other.y_ ? y_ : other.y_;
  }

  std::string ToString() const;

 private:
  CoordinateType x_;
  CoordinateType y_;
};

template <typename CoordinateType>
inline bool operator==(const Vector2d<CoordinateType>& lhs,
		       const Vector2d<CoordinateType>& rhs) {
  return lhs.x() == rhs.x() && lhs.y() == rhs.y();
}

template <typename CoordinateType>
inline bool operator!=(const Vector2d<CoordinateType>& lhs,
		       const Vector2d<CoordinateType>& rhs) {
  return !(lhs == rhs);
}

template <typename CoordinateType>
inline Vector2d<CoordinateType> operator-(const Vector2d<CoordinateType>& v) {
  return Vector2d<CoordinateType>(-v.x(), -v.y());
}

template <typename CoordinateType>
inline Vector2d<CoordinateType> operator+(const Vector2d<CoordinateType>& lhs,
					  const Vector2d<CoordinateType>& rhs) {
  Vector2d<CoordinateType> result = lhs;
  result.Add(rhs);
  return result;
}

template <typename CoordinateType>
inline Vector2d<CoordinateType> operator-(const Vector2d<CoordinateType>& lhs,
					  const Vector2d<CoordinateType>& rhs) {
  Vector2d<CoordinateType> result = lhs;
  result.Add(-rhs);
  return result;
}

template <typename CoordinateType>
inline Vector2d<CoordinateType> operator/(const Vector2d<CoordinateType>& lhs,
					  int value) {
  Vector2d<CoordinateType> result = lhs;
  result.Divide(value);
  return result;
}

template <typename CoordinateType>
void Vector2d<CoordinateType>::Add(const Vector2d<CoordinateType>& other) {
  x_ += other.x_;
  y_ += other.y_;
}

template <typename CoordinateType>
void Vector2d<CoordinateType>::Subtract(const Vector2d<CoordinateType>& other) {
  x_ -= other.x_;
  y_ -= other.y_;
}

template <typename CoordinateType>
void Vector2d<CoordinateType>::Divide(CoordinateType value) {
  x_ = x_ / value;
  y_ = y_ / value;
}

template <typename CoordinateType>
std::string Vector2d<CoordinateType>::ToString() const {
  std::string output;

  output += "x [" + conversion::ToString<CoordinateType>(x_) + "] -- ";
  output += "y [" + conversion::ToString<CoordinateType>(y_) + "]";
  return output;
}

typedef Vector2d<float> Vector2f;

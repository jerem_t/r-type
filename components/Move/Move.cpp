
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Box.hpp>
#include <gEngine/Core/GameObject.hpp>
#include "Move.hpp"

Move::Move(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["direction"] = ge::Vector3f();
  _data["rotation"] = ge::Vector3f();
  _data["speed"] = 0.0f;
  _data["duration"] = 0.0f;
}

Move::~Move() {}

void Move::onStart() {
  _transform = _gameObject.component("Transformable")["transform"];
  _source = _transform->getPosition();
  _target = _transform->getPosition();
  _tween.duration(0.5f)
        .from(0.0f)
        .to(1.0f)
        .easing("linear");
}

void Move::onUpdate(float dt) {
  ge::Vector3f const &dir = _data["direction"];
  ge::Vector3f const &rot = _data["rotation"];
  ge::Boxf &box = _gameObject.component("Collider")["box"];
  float s = _data["speed"];
  ge::Vector3f prev = box.pos;
  _target = _transform->getPosition() + (dir * s * dt);
  _transform->setPosition(_target);
  _transform->rotate(rot);
  ge::Vector3f pos = _transform->getGlobalPosition();
  _tween.update(dt);
  box.pos.x = pos.x;
  box.pos.y = pos.y;
  box.pos.z = pos.z;
  if (_gameObject.component("Collider")["quadtree"].get<bool>())
    _gameObject.getRoot()->trigger("Physics:move", &_gameObject, prev);
  if (_tween.isFinish()) {
    _tween.start();
    _source = _transform->getGlobalPosition();
  }
/*
  ge::Boxf &box = _gameObject.component("Collider")["box"];
  ge::Vector3f prev = box.pos;
  ge::Vector3f dir = _data["direction"];
//  dir.x /= float(ge::GameObject::getDefine("width"));
//  dir.y /= float(ge::GameObject::getDefine("height"));
  if (dir.x != 0.0f || dir.y != 0.0f || dir.z != 0.0f){
    ge::Vector3f const &rot = _data["rotation"];
    ge::Quaternionf q(rot);
    _target += q.rotate(dir);
  }
  ge::Vector3f current = _transform->getPosition();
  ge::Vector3f toMove = _target - current;
//  std::cout << "fuck " << _target.x << " " << _current.x << std::endl;
  float len = sqrt(toMove.x * toMove.x + toMove.y * toMove.y + toMove.z * toMove.z);
  if (len >= 0.02f){
    toMove.x /= len;
    toMove.y /= len;
    toMove.z /= len;
    float s = _data["speed"];
    _transform->translate(toMove * s * dt);
  }
  ge::Vector3f pos = _transform->getGlobalPosition();
  box.pos.x = pos.x;
  box.pos.y = pos.y;
  box.pos.z = pos.z;
  if (_gameObject.component("Collider")["quadtree"].get<bool>())
    _gameObject.getRoot()->trigger("Physics:move", &_gameObject, prev);
*/
}

GE_EXPORT(Move)


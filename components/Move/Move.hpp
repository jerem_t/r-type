
#pragma once

#include <gEngine/Math/Transform.hpp>
#include <gEngine/Math/Tween.hpp>
#include <gEngine/Core/Component.hpp>

/**
 * @class Move
 * Move the object according a specific behaviour.
 */
class Move : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Move(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Move();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  std::shared_ptr<ge::Transform> _transform;
  ge::Tween<float> _tween;
  ge::Vector3f _source;
  ge::Vector3f _target;
  ge::Vector3f _current;
  float _step;

};


#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class Sound
 * Add a sound handler to trigger sounds on this gameObject.
 */
class Sound : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Sound(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Sound();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};

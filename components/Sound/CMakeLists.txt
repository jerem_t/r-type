
cmake_minimum_required(VERSION 2.6)

project(gEngine_Sound)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_Sound SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_Sound gEngine.lib)
endif (MSVC)

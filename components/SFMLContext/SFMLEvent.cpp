
#include <gEngine/Math/Vector3.hpp>
#include "SFMLEvent.hpp"

static std::map<sf::Event::EventType, std::string> types = {
  { sf::Event::Closed, "Closed" },
  { sf::Event::Resized, "Resized" },
  { sf::Event::LostFocus, "LostFocus" },
  { sf::Event::GainedFocus, "GainedFocus" },
  { sf::Event::TextEntered, "TextEntered" },
  { sf::Event::KeyPressed, "KeyPressed" },
  { sf::Event::KeyReleased, "KeyReleased" },
  { sf::Event::MouseWheelMoved, "MouseWheel" },
  { sf::Event::MouseButtonPressed, "MousePressed" },
  { sf::Event::MouseButtonReleased, "MouseReleased" },
  { sf::Event::MouseMoved, "MouseMoved" },
  { sf::Event::MouseEntered, "MouseEntered" },
  { sf::Event::MouseLeft, "MouseLeft" },
  { sf::Event::JoystickButtonPressed, "JoystickPressed" },
  { sf::Event::JoystickButtonReleased, "JoystickReleased" },
  { sf::Event::JoystickMoved, "JoystickMoved" },
  { sf::Event::JoystickConnected, "JoystickConnected" },
  { sf::Event::JoystickDisconnected, "JoystickDisconnected" }
};

static std::map<sf::Mouse::Button, std::string> mouseButtons = {
  {sf::Mouse::Left, "Left" },
  {sf::Mouse::Right, "Right" },
  {sf::Mouse::Middle, "Middle" },
  {sf::Mouse::XButton1, "XButton1" },
  {sf::Mouse::XButton2, "XButton2" }
};

static std::map<sf::Keyboard::Key, std::string> keyCodes = {
  { sf::Keyboard::Unknown, "Unknown" },
  { sf::Keyboard::A, "A" },
  { sf::Keyboard::B, "B" },
  { sf::Keyboard::C, "C" },
  { sf::Keyboard::D, "D" },
  { sf::Keyboard::E, "E" },
  { sf::Keyboard::F, "F" },
  { sf::Keyboard::G, "G" },
  { sf::Keyboard::H, "H" },
  { sf::Keyboard::I, "I" },
  { sf::Keyboard::J, "J" },
  { sf::Keyboard::K, "K" },
  { sf::Keyboard::L, "L" },
  { sf::Keyboard::M, "M" },
  { sf::Keyboard::N, "N" },
  { sf::Keyboard::O, "O" },
  { sf::Keyboard::P, "P" },
  { sf::Keyboard::Q, "Q" },
  { sf::Keyboard::R, "R" },
  { sf::Keyboard::S, "S" },
  { sf::Keyboard::T, "T" },
  { sf::Keyboard::U, "U" },
  { sf::Keyboard::V, "V" },
  { sf::Keyboard::W, "W" },
  { sf::Keyboard::X, "X" },
  { sf::Keyboard::Y, "Y" },
  { sf::Keyboard::Z, "Z" },
  { sf::Keyboard::Num0, "Num0" },
  { sf::Keyboard::Num1, "Num1" },
  { sf::Keyboard::Num2, "Num2" },
  { sf::Keyboard::Num3, "Num3" },
  { sf::Keyboard::Num4, "Num4" },
  { sf::Keyboard::Num5, "Num5" },
  { sf::Keyboard::Num6, "Num6" },
  { sf::Keyboard::Num7, "Num7" },
  { sf::Keyboard::Num8, "Num8" },
  { sf::Keyboard::Num9, "Num9" },
  { sf::Keyboard::Escape, "Escape" },
  { sf::Keyboard::LControl, "LControl" },
  { sf::Keyboard::LShift, "LShift" },
  { sf::Keyboard::LAlt, "LAlt" },
  { sf::Keyboard::LSystem, "LSystem" },
  { sf::Keyboard::RControl, "RControl" },
  { sf::Keyboard::RShift, "RShift" },
  { sf::Keyboard::RAlt, "RAlt" },
  { sf::Keyboard::RSystem, "RSystem" },
  { sf::Keyboard::Menu, "Menu" },
  { sf::Keyboard::LBracket, "LBracket" },
  { sf::Keyboard::RBracket, "RBracket" },
  { sf::Keyboard::SemiColon, "SemiColon" },
  { sf::Keyboard::Comma, "Comma" },
  { sf::Keyboard::Period, "Period" },
  { sf::Keyboard::Quote, "Quote" },
  { sf::Keyboard::Slash, "Slash" },
  { sf::Keyboard::BackSlash, "BackSlash" },
  { sf::Keyboard::Tilde, "Tilde" },
  { sf::Keyboard::Equal, "Equal" },
  { sf::Keyboard::Dash, "Dash" },
  { sf::Keyboard::Space, "Space" },
  { sf::Keyboard::Return, "Return" },
  { sf::Keyboard::BackSpace, "BackSpace" },
  { sf::Keyboard::Tab, "Tab" },
  { sf::Keyboard::PageUp, "PageUp" },
  { sf::Keyboard::PageDown, "PageDown" },
  { sf::Keyboard::End, "End" },
  { sf::Keyboard::Home, "Home" },
  { sf::Keyboard::Insert, "Insert" },
  { sf::Keyboard::Delete, "Delete" },
  { sf::Keyboard::Add, "Add" },
  { sf::Keyboard::Subtract, "Subtract" },
  { sf::Keyboard::Multiply, "Multiply" },
  { sf::Keyboard::Divide, "Divide" },
  { sf::Keyboard::Left, "Left" },
  { sf::Keyboard::Right, "Right" },
  { sf::Keyboard::Up, "Up" },
  { sf::Keyboard::Down, "Down" },
  { sf::Keyboard::Numpad0, "Numpad0" },
  { sf::Keyboard::Numpad1, "Numpad1" },
  { sf::Keyboard::Numpad2, "Numpad2" },
  { sf::Keyboard::Numpad3, "Numpad3" },
  { sf::Keyboard::Numpad4, "Numpad4" },
  { sf::Keyboard::Numpad5, "Numpad5" },
  { sf::Keyboard::Numpad6, "Numpad6" },
  { sf::Keyboard::Numpad7, "Numpad7" },
  { sf::Keyboard::Numpad8, "Numpad8" },
  { sf::Keyboard::Numpad9, "Numpad9" },
  { sf::Keyboard::F1, "F1" },
  { sf::Keyboard::F2, "F2" },
  { sf::Keyboard::F3, "F3" },
  { sf::Keyboard::F4, "F4" },
  { sf::Keyboard::F5, "F5" },
  { sf::Keyboard::F6, "F6" },
  { sf::Keyboard::F7, "F7" },
  { sf::Keyboard::F8, "F8" },
  { sf::Keyboard::F9, "F9" },
  { sf::Keyboard::F10, "F10" },
  { sf::Keyboard::F11, "F11" },
  { sf::Keyboard::F12, "F12" },
  { sf::Keyboard::F13, "F13" },
  { sf::Keyboard::F14, "F14" },
  { sf::Keyboard::F15, "F15" },
  { sf::Keyboard::Pause, "Pause" }
};

std::map<sf::Joystick::Axis, std::string> joystickAxis = {
  { sf::Joystick::X, "X" },
  { sf::Joystick::Y, "Y" },
  { sf::Joystick::Z, "Z" },
  { sf::Joystick::R, "R" },
  { sf::Joystick::U, "U" },
  { sf::Joystick::V, "V" },
  { sf::Joystick::PovX, "PovX" },
  { sf::Joystick::PovY, "PovY" }
};

SFMLEvent::SFMLEvent() {}

SFMLEvent::~SFMLEvent() {}

bool SFMLEvent::getEvent(sf::Event const &e, ge::Array &event, float w, float h) {
  if (types.find(e.type) == types.end())
    return false;
  event[0] = types[e.type];
  switch (e.type) {
    case sf::Event::KeyPressed:
    case sf::Event::KeyReleased:
      event[1] = keyCodes[e.key.code];
      break;
    case sf::Event::MouseButtonPressed:
    case sf::Event::MouseButtonReleased:
      event[1] = mouseButtons[e.mouseButton.button];
      event[2] = ge::Vector3f(e.mouseButton.x / w, e.mouseButton.x / h, 0.0);
      break;
    case sf::Event::MouseMoved:
      event[1] = ge::Vector3f(e.mouseMove.x / w, e.mouseMove.y / h, 0.0);
      break;
    case sf::Event::MouseEntered:
    case sf::Event::MouseLeft:
      break;
    case sf::Event::TextEntered:
      event[1] = e.text.unicode;
      break;
    case sf::Event::MouseWheelMoved:
      event[1] = e.mouseWheel.delta;
      event[2] = ge::Vector3f(e.mouseWheel.x, e.mouseWheel.y, 0.0);
      break;
    case sf::Event::JoystickConnected:
    case sf::Event::JoystickDisconnected:
      event[1] = e.joystickConnect.joystickId;
      break;
    case sf::Event::JoystickButtonPressed:
    case sf::Event::JoystickButtonReleased:
      event[1] = e.joystickButton.joystickId;
      event[2] = e.joystickButton.button;
      break;
    case sf::Event::JoystickMoved:
      event[1] = e.joystickMove.joystickId;
      event[2] = joystickAxis[e.joystickMove.axis];
      event[3] = e.joystickMove.position;
      break;
    default:
      break;
  };
  return true;
}

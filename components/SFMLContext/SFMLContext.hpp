
#pragma once

#include <utility>
#include <SFML/Graphics.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Resources/Resource.hpp>
#include <gEngine/Core/Component.hpp>
#include <gEngine/Data/SpriteData.hpp>
#include <gEngine/Data/TextData.hpp>
#include "RenderLists.hpp"
#include "SFMLEvent.hpp"

/**
 * @class SFMLContext
 * This component create a sfml context. It will handle
 * events, window, and rendering stuffs. You must add this
 * component to the root gameObject of the game.
 */
class SFMLContext : public ge::Component {
public:
  SFMLContext(ge::GameObject &gameObject);
  ~SFMLContext();
  void onStart();
  void onUpdate(float dt);
private:
  void _addSprite(ge::GameObject *obj, ge::SpriteData const &data);
  void _drawSprite(sf::Sprite &sprite, ge::GameObject *obj);
  void _addText(ge::GameObject *obj, ge::TextData const &data);
  void _drawText(sf::Text &sprite,
                 ge::GameObject *obj);
private:
  typedef std::pair<std::shared_ptr<sf::Sprite>, ge::GameObject *> SpriteInfo;
  typedef std::pair<std::shared_ptr<sf::Text>, ge::GameObject *> TextInfo;
private:
  sf::RenderWindow _renderWindow;
  std::vector<SpriteInfo> _sprites;
  std::vector<TextInfo> _texts;
  RenderLists			_renderer;
  float _width;
  float _height;
};

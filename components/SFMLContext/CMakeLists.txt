cmake_minimum_required(VERSION 2.6)

project(gEngine_SFMLContext)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_SFMLContext SHARED ${SOURCES})
if(MSVC)
	target_link_libraries(gEngine_SFMLContext ${SFML_LIBRARIES})
	target_link_libraries(gEngine_SFMLContext gEngine.lib)
endif (MSVC)
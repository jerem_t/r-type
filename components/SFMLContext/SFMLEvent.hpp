#pragma once

#include <utility>
#include <string>
#include <map>
#include <SFML/Window/Event.hpp>
#include <gEngine/Utils/Array.hpp>

class SFMLEvent {
public:

  /**
   * Default constructor, do nothing.
   */
  SFMLEvent();

  /**
   * Empty destructor, do nothing.
   */
  ~SFMLEvent();

  static bool getEvent(sf::Event const &sfEvent, ge::Array &event, float w = 1.0, float h = 1.0);
};


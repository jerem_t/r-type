
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Math/Quaternion.hpp>
#include "SFMLContext.hpp"

SFMLContext::SFMLContext(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _width(0)
, _height(0) {
  _data["clearColor"] = ge::Color(0, 0, 0, 255);
  _data["width"] = 800.0f;
  _data["height"] = 600.0f;
  _data["title"] = std::string("");
  _data["keyRepeat"] = true;
  _data["fullscreen"] = false;
  _data["titlebar"] = true;
  _data["closeButton"] = true;
  _data["resize"] = false;
}

SFMLContext::~SFMLContext() {}

void SFMLContext::onStart() {
  unsigned int style = 0;

  if (_data["fullscreen"])
    style |= sf::Style::Fullscreen;
  if (_data["titlebar"])
    style |= sf::Style::Titlebar;
  if (_data["resize"])
    style |= sf::Style::Resize;
  if (_data["closeButton"])
    style |= sf::Style::Close;
  _width = _data["width"];
  _height = _data["height"];
  std::string const &title = _data["title"];
  sf::VideoMode videoMode;

  if (style & sf::Style::Fullscreen)
    videoMode = sf::VideoMode::getDesktopMode();
  else
    videoMode = sf::VideoMode(_width, _height);

  _width = videoMode.width;
  _height = videoMode.height;
  ge::GameObject::setDefine("width", float(videoMode.width));
  ge::GameObject::setDefine("height", float(videoMode.height));

  _renderWindow.create(videoMode, title, style);
  _gameObject.on("Context:close", [this] (ge::Array &) {
    _renderWindow.close();
  });
  ge::Color const &c = _data["clearColor"];
  _renderWindow.setKeyRepeatEnabled(_data["keyRepeat"]);
  _renderWindow.clear(sf::Color(c.r, c.g, c.b, c.a));

  // Listeners
  _gameObject.on("Context:update", [this, &title] (ge::Array &event) {
    _width = event[0];
    _height = event[1];
    _renderWindow.create(sf::VideoMode(_width, _height), title);
  });
  _gameObject.on("Context:addSprite", [this] (ge::Array &event) {
    _addSprite(event[0], event[1]);
  });
  _gameObject.on("Context:addText", [this] (ge::Array &event) {
    _addText(event[0], event[1]);
  });
}

void SFMLContext::onUpdate(float) {
  sf::Event event;
  ge::Array e;
  while (_renderWindow.pollEvent(event)) {
    if (SFMLEvent::getEvent(event, e, _width, _height))
      _gameObject.trigger("Context:event", e);
  }
  ge::Color const &c = _data["clearColor"];
  _renderWindow.clear(sf::Color(c.x, c.y, c.z, c.w));
  // _renderer.draw(_renderWindow);
  for (auto const &sprite : _sprites)
    _drawSprite(*(sprite.first), sprite.second);
  for (auto const &text : _texts)
    _drawText(*(text.first), text.second);
  _renderWindow.display();
}

void SFMLContext::_addSprite(ge::GameObject *obj,
                             ge::SpriteData const &data)
{
  auto it = std::find_if(_sprites.begin(),
               _sprites.end(),
               [obj] (SpriteInfo const &info) {
    return obj == info.second;
  });

  std::shared_ptr<sf::Sprite> sprite;
  if (it == _sprites.end()) {
    sprite = std::shared_ptr<sf::Sprite>(new sf::Sprite);
    _sprites.push_back({sprite, obj});
  } else
    sprite = std::shared_ptr<sf::Sprite>(it->first);
  if (data.hasTexture()) {
    ge::Resource<sf::Texture> texture(data.getTexture());
    if (texture.isReady()) {
      sprite->setTexture(*texture);    
    } else
      throw std::runtime_error("Cannot find texture " + data.getTexture() + ".");
  }
 if (data.hasTextureRect()) {
    sprite->setTextureRect(sf::IntRect(data.getTextureRect().pos.x,
                                       data.getTextureRect().pos.y,
                                       data.getTextureRect().size.x,
                                       data.getTextureRect().size.y));
  }
  if (data.hasColor()) {
    sprite->setColor(sf::Color(data.getColor().r,
                     data.getColor().g,
                     data.getColor().b,
                     data.getColor().a));
  }
  obj->on("die", [sprite] (ge::Array &) {
    sprite->setColor(sf::Color(0, 0, 0, 0));
  });
}

void SFMLContext::_drawSprite(sf::Sprite &sprite, ge::GameObject *obj){
  if (obj->has("transform")) {
    ge::TransformPtr transform = obj->component("Transformable")["transform"];
    ge::Vector3f pos = transform->getGlobalPosition();
    sprite.setPosition(pos.x * _width, pos.y * _height);
    ge::Vector3f scale = transform->getGlobalScale();
    ge::Vector3f size;
    if (obj->component("Sprite").has("size")){
      size = obj->component("Sprite")["size"]; 
      size.x *= _width;
      size.y *= _height;
      sprite.setScale(scale.x * size.x / sprite.getTexture()->getSize().x,
                      scale.y * size.y / sprite.getTexture()->getSize().y);
    }
    else
      sprite.setScale(scale.x, scale.y);
    ge::Quaternionf    rotation = transform->getGlobalRotation();
    sprite.setRotation(-rotation.getRotate().z);
  }
  _renderWindow.draw(sprite);
}

void SFMLContext::_addText(ge::GameObject *obj, ge::TextData const &data) {

  auto it = std::find_if(_texts.begin(),
               _texts.end(),
               [obj] (TextInfo const &info) {
    return obj == info.second;
  });

  std::shared_ptr<sf::Text> text;
  if (it == _texts.end()) {
    text = std::shared_ptr<sf::Text>(new sf::Text);
    _texts.push_back({text, obj});
  } else
    text = it->first;

  if (data.hasFont()) {
    ge::Resource<sf::Font> font(data.getFont());
    if (font.isReady()) {
      text->setFont(*font);
    }
  }
  if (data.hasString())
    text->setString(data.getString());
  if (data.hasCharacterSize())
    text->setCharacterSize(data.getCharacterSize() * _height);
  if (data.hasColor())
    text->setColor(sf::Color(data.getColor().r,
                             data.getColor().g,
                             data.getColor().b,
                             data.getColor().a));
}

void SFMLContext::_drawText(sf::Text &text, ge::GameObject *obj) {
  if (obj->has("transform")) {
    ge::TransformPtr transform = obj->component("Transformable")["transform"];
    ge::Vector3f pos = transform->getGlobalPosition();
    text.setPosition(pos.x * _width, pos.y * _height);
    ge::Vector3f scale = transform->getGlobalScale();
    text.setScale(scale.x, scale.y);
    ge::Quaternionf    rotation = transform->getGlobalRotation();
    text.setRotation(rotation.getRotate().z);
  }
  _renderWindow.draw(text);
}

GE_EXPORT(SFMLContext)

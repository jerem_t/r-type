
#include "Batch.hpp"
#include <string>
#include <gEngine/Math/Transform.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Quaternion.hpp>
#include <gEngine/Core/Component.hpp>

Batch::Batch() :
	_va(sf::Quads)
{
}

Batch::~Batch()
{
}

void 		Batch::_updateSpriteTransform(sf::Vector2u const &texSize,
										 sf::IntRect const &uvs,
								  		 ge::GameObject *obj,
								  		 unsigned int idx)
{
  ge::TransformPtr transform = obj->component("Transformable")["transform"];
  ge::Vector3f 						center = transform->getGlobalPosition();
  ge::Vector3f 						scale = transform->getGlobalScale();
  ge::Quaternionf 						rotation = transform->getGlobalRotation();

  ge::Vector3f 						down = rotation.rotate(ge::Vector3f(0, 1, 0));
  ge::Vector3f 						right = rotation.rotate(ge::Vector3f(1, 0, 0));

  ge::Vector3f						vertPos;

	sf::Vertex 									&v1 = _va[idx * 4 + 0];
	sf::Vertex 									&v2 = _va[idx * 4 + 1];
	sf::Vertex 									&v3 = _va[idx * 4 + 2];
	sf::Vertex 									&v4 = _va[idx * 4 + 3];

	vertPos = center
			  - (right * static_cast<float>(texSize.x / 2) * scale.x)
			  - (down * static_cast<float>(texSize.y / 2) * scale.y);
	v1.position = sf::Vector2f(vertPos.x, vertPos.y);
	v1.texCoords = sf::Vector2f(uvs.left, uvs.top);
	v1.color = sf::Color::White;
	vertPos = center
			  + (right * static_cast<float>(texSize.x / 2) * scale.x)
			  - (down * static_cast<float>(texSize.y / 2) * scale.y);
	v2.position = sf::Vector2f(vertPos.x, vertPos.y);
	v2.texCoords = sf::Vector2f(uvs.left + uvs.width, uvs.top);
	v2.color = sf::Color::White;
	vertPos = center
			  + (right * static_cast<float>(texSize.x / 2) * scale.x)
			  + (down * static_cast<float>(texSize.y / 2) * scale.y);
	v3.position = sf::Vector2f(vertPos.x, vertPos.y);
	v3.texCoords = sf::Vector2f(uvs.left + uvs.width, uvs.top + uvs.height);
	v3.color = sf::Color::White;
	vertPos = center
			  - (right * static_cast<float>(texSize.x / 2) * scale.x)
			  + (down * static_cast<float>(texSize.y / 2) * scale.y);
	v4.position = sf::Vector2f(vertPos.x, vertPos.y);
	v4.texCoords = sf::Vector2f(uvs.left, uvs.top + uvs.height);
	v4.color = sf::Color::White;
}


bool 		Batch::addSprite(std::shared_ptr<sf::Sprite> const &sprite, ge::GameObject *obj)
{
	std::map<ge::GameObject *,
			 SSpriteInfos>::const_iterator 	it = _sprites.find(obj);
	unsigned int 							nbr = _va.getVertexCount() / 4;

	sf::Vertex 								vertex;

	if (it != _sprites.end())
		return (false);
	SSpriteInfos 		&added = _sprites[obj];

	added.idx = nbr;
	added.sprite = sprite;

	_va.resize(nbr * 4 + 4);
	_updateSpriteTransform(sprite->getTexture()->getSize(),
						  sprite->getTextureRect(),
						  obj,
						  nbr);
	return (true);
}

bool 		Batch::removeSprite(ge::GameObject *obj)
{
	std::map<ge::GameObject *,
			 SSpriteInfos>::const_iterator 	it = _sprites.find(obj);
	unsigned int 							size = _va.getVertexCount();

	if (it == _sprites.end())
		return (false);
	for (unsigned int i = it->second.idx * 4 + 4; i < size; ++i)
		_va[i - 4] = _va[i];
	_va.resize(size - 4);
	_sprites.erase(it);
	return (true);
}

void    	Batch::draw(sf::Texture const *texture, sf::RenderWindow &window)
{
	std::map<ge::GameObject *, SSpriteInfos>::const_iterator 	it = _sprites.begin();

	while (it != _sprites.end())
	{
	   ge::TransformPtr transform = it->first->component("Transformable")["transform"];

		if (transform->getGlobalMatrixChanged())
		{
			_updateSpriteTransform(it->second.sprite->getTexture()->getSize(),
								  it->second.sprite->getTextureRect(),
								  it->first,
								  it->second.idx);
			transform->resetGlobalMatrixChanged(false);
		}
		++it;
	}
	sf::Texture::bind(texture);
	window.draw(_va);
}

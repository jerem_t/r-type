
#include "RenderLists.hpp"

RenderLists::RenderLists()
{

}

RenderLists::~RenderLists()
{

}

bool  RenderLists::addSprite(std::shared_ptr<sf::Sprite> const &sprite, ge::GameObject *obj)
{
	return (_batchs[sprite->getTexture()].addSprite(sprite, obj));
}

bool  RenderLists::removeSprite(std::shared_ptr<sf::Sprite> const &sprite, ge::GameObject *obj)
{
	return (_batchs[sprite->getTexture()].removeSprite(obj));
}

void  RenderLists::draw(sf::RenderWindow &window)
{
	std::map<sf::Texture const *, Batch>::iterator 	it = _batchs.begin();

	while (it != _batchs.end())
	{
		it->second.draw(it->first, window);
		++it;
	}
}


#pragma once

#include <SFML/Graphics.hpp>

#include <gEngine/Core/GameObject.hpp>

#include <map>

class Batch
{
private:
  struct  SSpriteInfos
  {
    unsigned int						idx;
    std::shared_ptr<sf::Sprite> 		sprite;
  };

	sf::VertexArray					   _va;
	std::map<ge::GameObject *,
			     SSpriteInfos> 		 _sprites;

  void _updateSpriteTransform(sf::Vector2u const &texSize,
                        sf::IntRect const &uvs,
                        ge::GameObject *obj,
                        unsigned int idx);

public:
	Batch();
	~Batch();

	bool 		addSprite(std::shared_ptr<sf::Sprite> const &sprite, ge::GameObject *obj);
	bool 		removeSprite(ge::GameObject *obj);

  void    draw(sf::Texture const *texture, sf::RenderWindow &window);

};
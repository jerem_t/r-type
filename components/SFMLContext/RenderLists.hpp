
#pragma once

#include "Batch.hpp"

class RenderLists
{
private:
  std::map<sf::Texture const *, Batch>  _batchs;

public:
  RenderLists();
  ~RenderLists();

  bool  addSprite(std::shared_ptr<sf::Sprite> const &sprite, ge::GameObject *obj);
  bool  removeSprite(std::shared_ptr<sf::Sprite> const &sprite, ge::GameObject *obj);

  void  draw(sf::RenderWindow &window);

};

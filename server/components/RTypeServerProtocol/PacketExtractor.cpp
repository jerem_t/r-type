
#include "PacketExtractor.hpp"

PacketExtractor::PacketExtractor()
{
}

PacketExtractor::~PacketExtractor()
{
}

bool	PacketExtractor::stringExtractor(size_t nbr, std::vector<char> const &readBuffer, size_t &size)
{
	size_t	totalSize = 0;

	for (size_t i = 0; i < nbr; ++i)
	{
		NWPacket		curSize;
		size_t			strSize;

		totalSize += sizeof(size_t);
		// get size of str1
		if (readBuffer.size() >= totalSize)
			curSize.setData(std::vector<char>(readBuffer.begin() + totalSize - sizeof(size_t),
											  readBuffer.begin() + totalSize));
		else
			return (false);
		curSize >> strSize;
		totalSize += strSize;
	}
	size = totalSize;
	return (true);
}

bool	PacketExtractor::createRoom(std::vector<char> &readBuffer, NWPacket &input)
{
	NWPacket		size;
	size_t			nameSize;

	if (readBuffer.size() >= 1 + sizeof(size_t))
		size.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + 1 + sizeof(size_t)));
	else
		return (false);
	size >> nameSize;
	if (readBuffer.size() >= 1 + sizeof(size_t) + nameSize)
		input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + 1 + sizeof(size_t)+nameSize));
	else
		return (false);
	readBuffer = std::vector<char>(readBuffer.begin() + 1 + sizeof(size_t) + nameSize, readBuffer.end());
	return (true);
}

bool	PacketExtractor::emptyExtract(std::vector<char> &readBuffer, NWPacket &input)
{
	readBuffer = std::vector<char>(readBuffer.begin() + 1, readBuffer.end());
	input.clear();
	return (true);
}

// pos rot scale
bool	PacketExtractor::shoot(std::vector<char> &readBuffer, NWPacket &input)
{
	if (readBuffer.size() >= sizeof(float)* 10 + 1)
		input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + sizeof(float)* 10 + 1));
	else
		return (false);
	readBuffer = std::vector<char>(readBuffer.begin() + 10 * sizeof(float) + 1, readBuffer.end());
	return (true);
}

bool	PacketExtractor::move(std::vector<char> &readBuffer, NWPacket &input)
{
	size_t		transformSize = 2 * sizeof(ge::Vector3f) + sizeof(ge::Quaternionf);

	if (readBuffer.size() >= transformSize + 1)
		input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + transformSize + 1));
	else
		return (false);
	readBuffer = std::vector<char>(readBuffer.begin() + transformSize + 1, readBuffer.end());
	return (true);
}

bool	PacketExtractor::startGame(std::vector<char> &readBuffer, NWPacket &input)
{
	size_t		size;

	if (stringExtractor(3, std::vector<char>(readBuffer.begin() + 1, readBuffer.end()), size))
		input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + 1 + size));
	else
	    return (false);
	readBuffer = std::vector<char>(readBuffer.begin() + 1 + size, readBuffer.end());
	return (true);
}

bool	PacketExtractor::joinRoom(std::vector<char> &readBuffer, NWPacket &input)
{
	size_t	size;

	std::cout << "extract joinRoom : " << std::endl;
	if (stringExtractor(1, std::vector<char>(readBuffer.begin() + 1, readBuffer.end()), size))
		input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + 1 + size));
	else
		return (false);
	readBuffer = std::vector<char>(readBuffer.begin() + 1 + size, readBuffer.end());
	return (true);
}

#pragma once

#include <string>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Network/NWPacket.hpp>

class RTypeServerProtocol;

class CommandManager
{
public:
  CommandManager(RTypeServerProtocol &component);
  ~CommandManager();

  void	createRoom(NWPacket &input, NWPacket &output);
  void	gameOver(NWPacket &input, NWPacket &output);
  void	checkRooms(NWPacket &input, NWPacket &output);
  void	shoot(NWPacket &input, NWPacket &output);
  void	move(NWPacket &input, NWPacket &output);
  void	joinRoom(NWPacket &input, NWPacket &output);
  void	startGame(NWPacket &input, NWPacket &output);
  void	leave(NWPacket &input, NWPacket &output);

private:
	RTypeServerProtocol		&_server;
};

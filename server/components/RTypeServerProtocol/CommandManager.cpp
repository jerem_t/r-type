#include "CommandManager.hpp"

#include "RTypeServerProtocol.hpp"

CommandManager::CommandManager(RTypeServerProtocol &component) :
  _server(component)
{}

CommandManager::~CommandManager()
{}

// in : 3 (id cmd) - name | out : 0 or 1
void	CommandManager::createRoom(NWPacket &input, NWPacket &output)
{
  std::string		name;

  input >> name;
  if (_server.addRoom(name))
	  output << static_cast<char>(10) << static_cast<char>(1);
  else
	  output << static_cast<char>(10) << static_cast<char>(0);
}

void	CommandManager::checkRooms(NWPacket &input, NWPacket &output)
{
  for (auto r : _server.getRooms())
    {
      size_t		nbr = 0;

      for (size_t i = 0; i < 4; ++i)
	{
	  if (r.second.connected[i] != NULL)
	    ++nbr;
	}
      output << static_cast<char>(7) << r.second.name << nbr;
    }
}


// in : 4 (id - cmd) | out : 4 (id cmd gameover)
void	CommandManager::gameOver(NWPacket &input, NWPacket &output)
{
	_server.getLastClient()->room->gameOver = true;
	output << static_cast<char>(13);
}

void	CommandManager::joinRoom(NWPacket &input, NWPacket &output)
{
	std::string			name;

	input >> name;
	std::cout << "joinRoom : " << name << std::endl;
	if (_server.joinRoom(name))
		output << static_cast<char>(15) << static_cast<char>(1);
	else
		output << static_cast<char>(15) << static_cast<char>(0);
}

void	CommandManager::shoot(NWPacket &input, NWPacket &output)
{
  (void)input;
  (void)output;
}

void	CommandManager::startGame(NWPacket &input, NWPacket &output)
{
  std::string		mode, map, level;
  size_t			playersInRoom = 0;

  std::cout << "Start game" << std::endl;
  if (_server.getLastClient()->room == NULL)
    return;
  input >> mode >> map >> level;

	std::cout << "start game with mode " << mode << " map " << map << " and level " << level << std::endl;

	output << static_cast<char>(12) << mode << map << level;
	for (size_t i = 0; i < 4; ++i)
	{
		if (_server.getLastClient()->room->connected[i] != NULL)
			playersInRoom += 1;
	}
	output << playersInRoom;
	std::cout << "players in the room = " << playersInRoom << std::endl;
	for (size_t i = 0; i < 4; ++i)
	{
	  if (_server.getLastClient()->room->connected[i] != NULL)
	    {
	      output << _server.getLastClient()->room->connected[i]->clientID;
	      std::cout << "\tclient nbr " << _server.getLastClient()->room->connected[i]->clientID;
	    }
	}

}

void	CommandManager::move(NWPacket &input, NWPacket &output)
{
	ge::Vector3f		position;
	ge::Vector3f		scale;
	ge::Quaternionf		rotation;

	input >> position >> rotation >> scale;
	output << static_cast<char>(4) << position << rotation << scale;
}

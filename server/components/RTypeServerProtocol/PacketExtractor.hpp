
#pragma once

#include <vector>
#include <gEngine/Network/NWPacket.hpp>

class PacketExtractor
{
public:
	PacketExtractor();
	~PacketExtractor();

	static bool		stringExtractor(size_t nbr, std::vector<char> const &readBuffer, size_t &size);

	static bool		createRoom(std::vector<char> &readBuffer, NWPacket &input);
	static bool		emptyExtract(std::vector<char> &readBuffer, NWPacket &input);
	static bool		shoot(std::vector<char> &readBuffer, NWPacket &input);
	static bool		move(std::vector<char> &readBuffer, NWPacket &input);
	static bool		startGame(std::vector<char> &readBuffer, NWPacket &input);
	static bool		joinRoom(std::vector<char> &readBuffer, NWPacket &input);
};

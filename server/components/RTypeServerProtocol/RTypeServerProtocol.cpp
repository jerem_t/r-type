
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/System/Time.hpp>
#include <gEngine/System/Clock.hpp>
#include "RTypeServerProtocol.hpp"
#include "CommandManager.hpp"

#include "PacketExtractor.hpp"

RTypeServerProtocol::RTypeServerProtocol(ge::GameObject &gameObject)
  : ge::Component(gameObject),
    _manager(*this)
{
	_lastClient = NULL;
	_clientID = 0;
	_data["port"] = 4242.0f;

	_commands[2] = std::make_pair(PacketExtractor::createRoom, &CommandManager::createRoom);
	_commands[5] = std::make_pair(PacketExtractor::emptyExtract, &CommandManager::checkRooms);
	_commands[7] = std::make_pair(PacketExtractor::shoot, &CommandManager::shoot);
	_commands[9] = std::make_pair(PacketExtractor::move, &CommandManager::move);
	_commands[11] = std::make_pair(PacketExtractor::startGame, &CommandManager::startGame);
	_commands[3] = std::make_pair(PacketExtractor::emptyExtract, &CommandManager::gameOver);
	_commands[14] = std::make_pair(PacketExtractor::joinRoom, &CommandManager::joinRoom);
	//	_commands =
//	{
//		{ 2, { PacketExtractor::createRoom, &CommandManager::createRoom } },
//		{ 5, { PacketExtractor::checkRooms, &CommandManager::checkRooms } },
//		{ 7, { PacketExtractor::createBullet, &CommandManager::createBullet } }
//		{ 9, { PacketExtractor::input, CommandManager::input } }
//	};
}

RTypeServerProtocol::~RTypeServerProtocol()
{
	delete _tcp;
	delete _udp;
}

void RTypeServerProtocol::onStart() 
{
	std::cout << "Start server" << std::endl;
	unsigned short port = _data["port"].get<float>();

	_tcp = new NWTcpListener();
	_udp = new NWUdp();

	if (_tcp->listen(port) != INWAbstract::SC_DONE)
	{
		std::cerr << "TCP Listener error" << std::endl;
	}
	if (_udp->bind(port) != INWAbstract::SC_DONE)
	{
		std::cerr << "UDP bind error" << std::endl;
	}
}

void RTypeServerProtocol::_resetSelector()
{
	_selector.clear();

	_selector.addRead(*_tcp);
	_selector.addRead(*_udp);

	for (auto &c : _clients)
	{
		_selector.addRead(*c.second.socket);
		if (c.second.writeBuffer.empty() == false)
			_selector.addWrite(*c.second.socket);
	}
}

void	RTypeServerProtocol::_handleTcp()
{
	std::list<size_t>		toRemove;

	for (auto &c : _clients)
	{
		std::cout << "	-iter on client" << std::endl;
		if (_selector.isReadyToReceive(*c.second.socket))
		{
			std::cout << "	-client can receive" << std::endl;
			size_t			readed;
			// If the client is disconnected, add it to the toRemove list
			if (c.second.socket->receive(c.second.readBuffer, readSize, readed) != INWAbstract::SC_DONE)
			{
				std::cout << "Error on receive" << std::endl;
				toRemove.push_back(c.second.clientID);
			}
		}

		// Handle received packets
		while (_dispatchCommands(c.second));

		// write data to the client
		if (_selector.isReadyToWrite(*c.second.socket))
		{
			std::cout << "	-can send on client" << std::endl;
			if (c.second.socket->send(c.second.writeBuffer) != INWAbstract::SC_DONE)
			{
				std::cout << "Error on send" << std::endl;
				toRemove.push_back(c.second.clientID);
			}
			c.second.writeBuffer.clear();
		}
	}
	// remove disconnected clients
	for (auto r : toRemove)
	{
		SClient			&toRm = _clients[r];

		if (toRm.room != NULL)
		{
			size_t		isEmpty = 0;

			for (size_t i = 0; i < 4; ++i)
			{
				if (toRm.room->connected[i] == &toRm)
					toRm.room->connected[i] = NULL;
				if (toRm.room->connected[i] == NULL)
					++isEmpty;
			}
			if (isEmpty == 4)
				_rooms.erase(toRm.room->name);
		}
		_clients.erase(r);
		std::cout << "Client disconnected" << std::endl;
	}

}

void	RTypeServerProtocol::_handleUdp()
{
	// Handle udp packages
	if (_selector.isReadyToReceive(*_udp))
	{
		std::cout << "	-can receive from udp" << std::endl;
		unsigned short	port = _data["port"].get<float>();
		size_t			received;

		// Read inputs
		_udp->receive("127.0.0.1", port, _udpReadBuff, readSize, received);

		while (_dispatchUdp());
	}
}

void	RTypeServerProtocol::_handleConnection()
{
	// Handle connection
	if (_selector.isReadyToReceive(*_tcp))
	{
		// Create client
		SClient						&client = _clients[_clientID];
		NWPacket					answer;

		client.socket = std::shared_ptr<NWTcp>(new NWTcp);
		client.clientID = _clientID++;
		client.room = NULL;
		if (_tcp->accept(*client.socket) != INWAbstract::SC_DONE)
			throw std::runtime_error("Accept fail");
		std::cout << "client IP: " << client.socket->getIp() << std::endl;
		// answer
		answer << static_cast<char>(1) << client.clientID;
		_sendPacket(answer, client);
		std::cout << "New client connected" << std::endl;
	}
}

/*
** Main update function
** Handle all udp/tcp commands
*/
void RTypeServerProtocol::onUpdate(float) 
{
	_resetSelector();

	_selector.wait();

	_handleConnection();

	_handleTcp();

	_handleUdp();

	_clearClients();
}

void	RTypeServerProtocol::_sendPacket(NWPacket const &packet, SClient &client)
{
	client.writeBuffer.insert(client.writeBuffer.end(),
							  packet.getData().begin(),
							  packet.getData().end());
}

bool	RTypeServerProtocol::addRoom(std::string const &name)
{
	auto res = _rooms.find(name);

	if (res != _rooms.end())
		return (false);
	SRoom	&cur = _rooms[name];
	cur.name = name;
	_lastClient->room = &cur;
	cur.connected[0] = _lastClient;
	std::cout << "client " << _lastClient->clientID << " has created the room " << name << std::endl;
	return (true);
}

std::map<std::string, RTypeServerProtocol::SRoom>	&RTypeServerProtocol::getRooms()
{
	return (_rooms);
}

bool	RTypeServerProtocol::joinRoom(std::string const &name)
{
	size_t		i = 0;
	auto		res = _rooms.find(name);

	if (res == _rooms.end())
		return (false);
	while (res->second.connected[i] != NULL)
		++i;
	if (i == 4)
		return (false);
	_lastClient->room = &res->second;
	res->second.connected[i] = _lastClient;
	return (true);
}

RTypeServerProtocol::SClient	*RTypeServerProtocol::getLastClient() const
{
	return (_lastClient);
}

bool	RTypeServerProtocol::_dispatchUdp()
{
	// port
	unsigned short	port = _data["port"].get<float>();

	// tmp buffer
	std::vector<char>	subBuff;

	// input and output package
	NWPacket			input;
	NWPacket			output;

	// Udp header
	NWPacket			udpHeader;
	size_t				clientId;
	size_t				timestamp;
	char				commandId;

	if (_udpReadBuff.size() >= udpInputSize)
		udpHeader.setData(std::vector<char>(_udpReadBuff.begin(), _udpReadBuff.begin() + udpInputSize));
	else
		return (false);
	//extract data from header
	udpHeader >> clientId >> timestamp >> commandId;

	auto			it = _commands.find(commandId);
	auto			cIt = _clients.find(clientId);

	if (cIt == _clients.end())
		return (false);
	SClient			&client = cIt->second;
	_lastClient = &client;
	// We want the command id in the subBuff, so we substract 1
	subBuff = std::vector<char>(_udpReadBuff.begin() + udpInputSize - 1, _udpReadBuff.end());

	if (it == _commands.end() ||
		it->second.first(subBuff, input) == false)
		return (false);
	_udpReadBuff = subBuff;
	(_manager.*(it->second.second))(input, output);

	std::vector<char>	udpBuff;

	udpHeader.clear();
	udpHeader << clientId << timestamp;
	
	udpBuff = udpHeader.getData();
	udpBuff.insert(udpBuff.end(), output.getData().begin(), output.getData().end());
	output.clear();
	output.setData(udpBuff);
	if (commandId == 9) // dispatch on all clients of the room
	{
		if (client.room)
		{
			for (size_t i = 0; i < 4; ++i)
			{
				if (client.room->connected[i] != NULL &&
					client.room->connected[i] != &client)
				{
					_udp->send(client.room->connected[i]->socket->getIp(), port, output);
				}
			}
		}
	}
	else // write just on the sender
	{
		_udp->send(client.socket->getIp(), port, output);
	}
	return (true);
}

bool	RTypeServerProtocol::_dispatchCommands(SClient &client)
{
	NWPacket		input;
	NWPacket		output;

	if (client.readBuffer.empty())
		return (false);
	_lastClient = &client;
	char			commandId = client.readBuffer[0];
	auto			it = _commands.find(commandId);

	if (it == _commands.end() ||
		it->second.first(client.readBuffer, input) == false)
		return (false);
	(_manager.*(it->second.second))(input, output);
	if (commandId == 7 ||
		commandId == 9 ||
		commandId == 3 ||
		commandId == 16) // the commands that needs dispatch over the different clients of the room except himself
	{
		if (client.room)
		{
			for (size_t i = 0; i < 4; ++i)
			{
				if (client.room->connected[i] != NULL &&
					client.room->connected[i] != &client)
				{
					_sendPacket(output, *client.room->connected[i]);
				}
			}
		}
	}
	else if (commandId == 11) // dispatch on all clients on the room
	{
		if (client.room)
		{
			for (size_t i = 0; i < 4; ++i)
			{
				if (client.room->connected[i] != NULL)
				  {
					_sendPacket(output, *client.room->connected[i]);
				  }
			}
		}
	}
	else // write just on the sender
	{
		_sendPacket(output, client);
	}
	return (true);
}

void	RTypeServerProtocol::_clearClients()
{
	std::list<std::string>	roomsToRemove;

	for (auto &r : _rooms)
	{
		if (r.second.gameOver)
		{
			for (size_t i = 0; i < 4; ++i)
				_clients.erase(r.second.connected[i]->clientID);
			roomsToRemove.push_back(r.first);
		}
	}
	for (auto &r : roomsToRemove)
	{
		_rooms.erase(r);
	}
}

GE_EXPORT(RTypeServerProtocol)


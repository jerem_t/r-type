
#pragma once

#include <gEngine/Core/Component.hpp>

#include <gEngine/Network/NWTcpListener.hpp>
#include <gEngine/Network/NWTcp.hpp>
#include <gEngine/Network/NWUdp.hpp>
#include <gEngine/Network/NWSelector.hpp>

#include "CommandManager.hpp"

const size_t			readSize = 4096;
const size_t			udpInputSize = sizeof(size_t)+sizeof(size_t)+1;

/**
 * @class RTypeServerProtocol
 * 
 */
class RTypeServerProtocol : public ge::Component {
private:
	struct  SRoom;
	struct	SClient;

	typedef	bool(*t_extract)(std::vector<char> &, NWPacket &);
	typedef	void(CommandManager::*t_command)(NWPacket &, NWPacket &);

	std::map<char, std::pair<t_extract, t_command> >		_commands;

	CommandManager		_manager;

public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  RTypeServerProtocol(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~RTypeServerProtocol();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);


  bool							addRoom(std::string const &name);
  std::map<std::string, SRoom>	&getRooms();

  bool							joinRoom(std::string const &name);

  SClient						*getLastClient() const;

private:
	struct	SClient
	{
		std::shared_ptr<NWTcp>			socket;
		size_t							clientID;

		std::vector<char>				readBuffer;
		std::vector<char>				writeBuffer;

		SRoom							*room;
	};

	struct  SRoom
	{
		std::string		name;
		SClient			*connected[4];

		bool			gameOver;

		SRoom()
		{
			connected[0] = NULL;
			connected[1] = NULL;
			connected[2] = NULL;
			connected[3] = NULL;
			gameOver = false;
		}
	};

	std::map<size_t, SClient>		_clients;
	std::map<std::string, SRoom>	_rooms;

	NWTcpListener					*_tcp;
	NWUdp							*_udp;

	NWSelector						_selector;

	std::vector<char>				_udpReadBuff;
	std::vector<char>				_udpWriteBuff;

	size_t							_clientID;

	SClient							*_lastClient;

	void							_handleTcp();
	void							_handleUdp();
	void							_handleConnection();
	void							_resetSelector();

	bool							_dispatchUdp();
	bool							_dispatchCommands(SClient &client);

	void							_sendPacket(NWPacket const &packet, SClient &client);

	void							_clearClients();
};

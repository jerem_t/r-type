// Debug
#include <iostream>

// gEngine
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Core/ComponentManager.hpp>
#include <gEngine/System/FrameHandler.hpp>

int		main()
{
  try {

  // Load components

  if (!ge::ComponentManager::getInstance().loadFromFile("resources/server_components.xml"))
  return 1;

  // Load game

  ge::GameObjectPtr root(new ge::GameObject("root"));
  if (!root->loadFromFile("resources/data/server.xml"))
  return 1;
  root->on("error", [](ge::Array &event) {
  throw std::runtime_error(event[0].get<std::string>());
  });
  root->start();

  // Start game loop

  ge::FrameHandler	loop;
  loop.setFrameRate(60);
  loop.on("frame", [&root](ge::Array &event) {
  root->update(event[0]);
  });
  loop.start();

  // Free component manager
  ge::ComponentManager::destroyInstance();

  } catch (const std::runtime_error &e) {
    std::cerr << e.what() << std::endl;
    getchar();
  }
  return 0;
}

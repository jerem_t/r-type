gEngine
=======

gEngine is a generic game engine with a easy-to-use and powerful entity/component system.

HOW TO USE
----------

### Linux

1. Compilation

```shell
$ mkdir build && cd build && cmake .. && make && cd ..
```

2. Lancement du client

```shell
$ ./rType
```

3. Lancement du server

```shell
$ ./rTypeServer
```

### Windows

TODO

TODO
----

- Bien clear le score quand on revient au menu
- Implementer le protocol
- SegFault GUI [done]
- GUI depuis le XML [done]
- Robot [done]
- THREADER !!! [done]
- Faire la communication reseau [done]

- Faire les animations [done]
- Faire la GUI [done]
- Faire les abstractions reseaux [done]
- Stopper le scroll du starfield [done]
- Loader les components depuis le XML [done]
- Loader les gameObjects depuis le XML [done]
- Rajouter des randoms de formes sur les vecteurs (SPHERE, CUBE...) [done]
- Utiliser Callable dans les threads. [done]

### Jeremie & Maxime

- changer de level
- boundingbox boss [done]
- Bonus [done]
- temporiser la fin [done]
- Fin de jeu [done]

### Random

- linear random [done]
- gauss random [done]
- diskRand [done]
- ballRand [done]
- circularRand [done]
- sphericalRand [done]

### Parsers

- Parser CSS [done]
- Parser XML [done]
- Utiliser Node.hpp dans Xml [done]
- Parser Data [done]

### Boss

- DeathStar [done]
- Troll [done]
- Jordy

### Monsters

- RedWave [done]
- GreenLine [done]
- YellowShooter [done]
- BlueFollow [done]

### Listes de sons

- Musique d'ambiance
- Tir des missiles [done]
- Mort d'un monstre [done]
- Collision avec un asteroid [done]
- Collision avec un monstre [done]

Bonus
-----

### Simple

- Mode spectateurs
- Son
- Credits
- Options [done]
- Bonus power [done]
- Armes [done]
- Invincible [done]

### Un peu chiant

- Blur en fonction de la distance
- Creation de server depuis la gui
- Faire un editeur de niveaux
- Boss [done]

### Tres relou

- Jeu multiplan
- passage 2d/3d
- dessiner la scene au fur et a mesure

How to use
-----------

### Linux

1. Compile the project

```sh
$ mkdir build && cd build && cmake .. && make
$ ./rType
```

2. Launch server

```sh

$ ./rType_server 4242

```

3. Launch client

```sh

$ ./rType

```

### Windows

TODO

How to create your own component
--------------------------------

Use the script in utils folder to generate your component:

```shell
$ ./gEngine.py --create-component=path/to/component
> Name: Dummy
> Description: Simple dummy component to try to work with components.
```

And modify the Dummy class (you can add attributes and modify onStart and onUpdate methods), for example:

```cpp

void Dummy::onStart() {
  std::cout << "Start the component!" << std::endl;
}

void Dummy::onUpdate(float dt) {
  std::cout << "Deltatime " << dt << std::endl;
}

```

Finally, just add it to your application:

```cpp

ge::GameObjectPtr test(new ge::GameObject("test"));

ge::ComponentManager::getInstance().add("components/Dummy", "Dummy");

test->component("Dummy");
test->start();

```

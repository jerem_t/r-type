#!/usr/bin/env python

import argparse
import os

cmake_data = """
cmake_minimum_required(VERSION 2.6)

project(gEngine_{{COMPONENT_NAME}})
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_{{COMPONENT_NAME}} SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_{{COMPONENT_NAME}} gEngine.lib)
endif(MSVC)

"""

header_data = """
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class {{COMPONENT_NAME}}
 * {{DESCRIPTION}}
 */
class {{COMPONENT_NAME}} : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  {{COMPONENT_NAME}}(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~{{COMPONENT_NAME}}();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};
"""

source_data = """
#include <gEngine/Core/GameObject.hpp>
#include "{{COMPONENT_NAME}}.hpp"

{{COMPONENT_NAME}}::{{COMPONENT_NAME}}(ge::GameObject &gameObject)
: ge::Component(gameObject) {}

{{COMPONENT_NAME}}::~{{COMPONENT_NAME}}() {}

void {{COMPONENT_NAME}}::onStart() {}

void {{COMPONENT_NAME}}::onUpdate(float) {}

GE_EXPORT({{COMPONENT_NAME}})

"""

xml_data = """<component name="{{COMPONENT_NAME}}">
  <description>{{DESCRIPTION}}</description>
  {{DEPENDENCIES}}
</component>

"""

def _replace(data, name, description, dependencies=None):
  data = data.replace("{{COMPONENT_NAME}}", name)
  data = data.replace("{{DESCRIPTION}}", description)
  xml = ""
  if dependencies and len(dependencies):
    arr = dependencies.split(' ')
    xml = "<dependencies>"
    for el in arr:
      xml += "\n    <component name=\"" + el + "\" />"
    xml += "\n  </dependencies>"
  data = data.replace("{{DEPENDENCIES}}", xml)
  return data

def create_component(path):
  """Create a component.

  Arguments:
  path -- The path to the component destination folder

  """
  print "\n  Create component in folder \"" + path + "\""
  print ""
  name = raw_input("    The name of your component is ")
  description = raw_input("    and the description is ")
  dependencies = raw_input("    finally, dependencies are ")
  print ""
  if len(path) is 0:
    path = "."
  full_path = path + "/" + name
  if os.path.exists(full_path):
    print "\n  Error: This component already exists, you should find another name.\n"
    exit()
  os.makedirs(full_path)
  with open(full_path + "/config.xml", 'w') as xml_file:
    xml_file.write(_replace(xml_data, name, description, dependencies))
  with open(full_path + "/" + name + ".hpp", 'w') as header_file:
    header_file.write(_replace(header_data, name, description))
  with open(full_path + "/" + name + ".cpp", 'w') as source_file:
    source_file.write(_replace(source_data, name, description))
  with open(full_path + "/CMakeLists.txt", 'w') as cmake_file:
    cmake_file.write(_replace(cmake_data, name, description))

CMAKELIST_FILE="""
cmake_minimum_required(VERSION 2.6)

include_directories(include/)

file(GLOB_RECURSE CLI_SOURCES src/*.cpp)
add_executable(rType ${CLI_SOURCES})
target_link_libraries(rType -lgEngine)

add_subdirectory(components)
"""

def create_game(path):
  print "\n  Create the game in folder \"" + path + "\""
  name = raw_input("    The name of the game is ")
  ctx = raw_input("    The context to handle window and events is ")
  components = raw_input("    The built-in components used are ")
  print ""
  full_path = path + "/" + name
  if os.path.exists(full_path):
    print "\nError: The folder " + full_path + " already exists.\n"
    exit()
  os.makedirs(full_path)
  os.makedirs(full_path + "/components")
  os.makedirs(full_path + "/include")
  os.makedirs(full_path + "/src")
  os.makedirs(full_path + "/resources")
  components += ""
  ctx += ""

parser = argparse.ArgumentParser(description="Handle generation of components and entities of your application.")
parser.add_argument("-c", "--component", metavar='PATH', help="""Create a component to the given path.
You must provide its name, and you can also precise description and dependencies.""")
parser.add_argument("-g", "--game", metavar="PATH", help="""Create a game with gEngine.
""")
args = parser.parse_args()
if args.component is not None:
  create_component(args.component)
elif args.game is not None:
  create_game(args.game)
else:
  print "You must provide an option, use --help to see all options."
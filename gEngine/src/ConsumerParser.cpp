
#include <gEngine/Parsers/ConsumerParser.hh>

#include <algorithm>
#include <list>

ConsumerParser::ConsumerParser(AParserFeeder *feeder) :
	_currentRecord(std::string::npos),
	_feeder(feeder),
	_index(0),
	_size(0)
{
}

ConsumerParser::~ConsumerParser()
{
}

bool 			ConsumerParser::fillBuffer()
{
	std::string 	chunk;
	size_t 			chunkSize;

	chunk = _feeder->getChunk();
	if ((chunkSize = chunk.size()) == 0)
		return (false);
	_size += chunkSize;
	_content += chunk;
	return (true);
}

void 			ConsumerParser::clearOldRecords()
{
	recordsIt 								it = _records.begin();
	std::list<recordsIt>					toRemove;
	std::list<recordsIt>::const_iterator	RmIt;

	while (it != _records.end())
	{
		if (it->second > _index)
			toRemove.push_back(it);
		++it;
	}
	RmIt = toRemove.begin();
	while (RmIt != toRemove.end())
	{
		_records.erase(*RmIt);
		++RmIt;
	}
}

AParserFeeder 	*ConsumerParser::getFeeder() const
{
	return (_feeder);
}

void 			ConsumerParser::setFeeder(AParserFeeder *feeder)
{
	_feeder = feeder;
}

bool 		ConsumerParser::readChar(char c)
{
	if (_index == _size &&
		fillBuffer() == false)
		return (false);
	if (_content[_index] != c)
		return (false);
	++_index;
	return (true);
}

bool		ConsumerParser::readRange(char begin, char end)
{
	if (_index == _size &&
		fillBuffer() == false)
		return (false);
	if (_content[_index] < begin ||
		_content[_index] > end)
		return (false);
	++_index;
	return (true);
}

bool		ConsumerParser::readString(std::string const &str)
{
	size_t 	strSize = str.size();

	while (strSize + _index > _size)
	{
		if (fillBuffer() == false)
			return (false);
	}
	if (std::equal(str.begin(), str.end(),
		_content.begin() + _index) == false)
		return (false);
	_index += strSize;
	return (true);
}

bool 		ConsumerParser::readUntil(char c)
{
	size_t 	idx = 0;

	if (_index == _size &&
		fillBuffer() == false)
		return (false);
	while (_content[_index + idx] != c)
	{
		++idx;
		if (_index + idx == _size &&
			fillBuffer() == false)
			return (false);
	}
	_index += idx + 1;
	return (true);
}

bool		ConsumerParser::readUntil(std::string const &str)
{
	size_t 	idx = 0;
	size_t 	strSize = str.size();

	if (_index + strSize >= _size &&
		fillBuffer() == false)
		return (false);
	while (std::equal(str.begin(), str.end(),
		   _content.begin() + _index + idx) == false)
	{
		++idx;
		if (_index + _index + idx >= _size &&
			fillBuffer() == false)
			return (false);
	}
	_index += strSize;
	return (true);
}

bool 		ConsumerParser::isEof()
{
	return (_index == _size && fillBuffer() == false);
}

void 		ConsumerParser::beginRecord(std::string const &name)
{
	_records[name] = _index;
}

std::string ConsumerParser::getRecord(std::string const &name)
{
	size_t 		rec = _records[name];

	_records.erase(name);
	return (_content.substr(rec, _index - rec));
}

void 		ConsumerParser::saveContext()
{
	_contexts.push(_index);
}

void 		ConsumerParser::restoreContext()
{
	_index = _contexts.top();
	_contexts.pop();
	clearOldRecords();
}

void 		ConsumerParser::popContext()
{
	_contexts.pop();
}

void 		ConsumerParser::clear()
{
	while (!_contexts.empty())
		_contexts.pop();
	_records.clear();
	_currentRecord = 0;
	_content.clear();
	_size = _index = 0;
}
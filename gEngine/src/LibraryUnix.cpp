
#include <gEngine/System/Platform.hpp>
#include <gEngine/Utils/Path.hpp>

#ifdef GE_SYSTEM_UNIX

# include <iostream>
# include <gEngine/System/Library.hpp>
# include <dlfcn.h>

namespace ge {

void Library::close() {
  dlclose(_handler);
}

bool Library::loadFromFile(std::string const &path) {
  _path = "./" + ge::Path::base(path) + "/lib" + ge::Path::fileName(path) + ".so";
  _handler = dlopen(_path.c_str(), RTLD_LAZY);
  if (_handler)
		return true;
  std::cerr << dlerror() << std::endl;
  return false;
}

void *Library::_getSymbol(std::string const &name) const {
  void *sym = dlsym(_handler, name.c_str());
  if (sym)
		return sym;
  std::cerr << dlerror() << std::endl;
  return NULL;
}

}

#endif

// #include <gEngine/Scripts/PythonScript.hpp>
// #include <gEngine/Utils/Path.hpp>
// #include <algorithm>

// #define S const_cast<char *>

// namespace ge {

// std::vector<std::string> PythonScript::_paths;

// PythonScript::PythonScript() {}

// PythonScript::~PythonScript() {}

// bool PythonScript::loadFromFile(std::string const &path) {
//   ge::Path p(path);
//   if (std::find(_paths.begin(), _paths.end(), p.getBase()) == _paths.end()) {
//     PyObject *sysPath = PySys_GetObject(S("path"));
//     PyObject *newPath = PyString_FromString(p.getBase().c_str());
//     if (PyList_Insert(sysPath, 0, newPath) == -1) {
//       PyErr_Print();
//       return false;
//     }
//     _paths.push_back(p.getBase());
//   }
//   _module = PyImport_ImportModule(p.getFileName().c_str());
//   if (!_module) {
//     PyErr_Print();
//     return false;
//   }
//   _class = PyObject_GetAttrString(_module, p.getFileName().c_str());
//   if (!_class) {
//     PyErr_Print();
//     return false;
//   }
//   _instance = PyInstance_New(_class, NULL, NULL);
//   if (!_instance) {
//     PyErr_Print();
//     return false;
//   }
//   return true;
// }

// void PythonScript::start() {
//   PyObject_CallMethod(_instance, S("onStart"), S("()"));
// }

// void PythonScript::update(float dt) {
//   PyObject_CallMethod(_instance, S("onUpdate"),
//                                  S("f"), dt);
// }

// #define GEN_SETTER(type, fn) \
// template<> \
// void PythonScript::set<type>(std::string const &name, \
//                              type const &value) { \
//   if (PyObject_SetAttrString(_instance, name.c_str(), fn(value)) == -1) \
//     PyErr_Print(); \
// }

// GEN_SETTER(int, PyInt_FromLong)
// GEN_SETTER(char *, PyString_FromString)
// GEN_SETTER(float, PyFloat_FromDouble)

// #undef GEN_SETTER

// #define GEN_GETTER(type, fn) \
//   template<> \
//   type PythonScript::get<type>(std::string const &name) { \
//     PyObject *item = PyObject_GetAttrString(_instance, name.c_str()); \
//     if (!item) \
//       PyErr_Print(); \
//     return fn(item); \
//   }

// GEN_GETTER(int, PyInt_AsLong)
// GEN_GETTER(char *, PyString_AsString)
// GEN_GETTER(float, PyFloat_AsDouble)

// #undef GEN_GETTER

// }

#include <gEngine/Network/NWSelector.hpp>

NWSelector::NWSelector() : _maxFd(0)
{
	clear();
}

NWSelector::~NWSelector()
{
	clear();
}

void	NWSelector::addRead(INWAbstract &socket)
{
	if (_maxFd <= socket.getSocket())
		_maxFd = socket.getSocket();
	FD_SET(socket.getSocket(), &_setfdrs);
}

void	NWSelector::addWrite(INWAbstract &socket)
{
	if (_maxFd <= socket.getSocket())
		_maxFd = socket.getSocket();
	FD_SET(socket.getSocket(), &_setfdws);
}

void	NWSelector::removeRead(INWAbstract &socket)
{
	FD_CLR(socket.getSocket(), &_setfdrs);
	FD_CLR(socket.getSocket(), &_readfds);
}

void	NWSelector::removeWrite(INWAbstract &socket)
{
	FD_CLR(socket.getSocket(), &_setfdws);
	FD_CLR(socket.getSocket(), &_writefds);
}

void	NWSelector::clear()
{
	FD_ZERO(&_setfdrs);
	FD_ZERO(&_setfdws);
	FD_ZERO(&_readfds);
	FD_ZERO(&_writefds);
	_maxFd = 0;
}

bool	NWSelector::wait(bool block, int second, int microseconds)
{
	int	ret;
	_timer.tv_sec	= second;
	_timer.tv_usec	= microseconds;
	_readfds		= _setfdrs;
	_writefds		= _setfdws;

	if ((ret = ::select(_maxFd + 1, &_readfds, &_writefds, NULL, (block) ? (NULL) : (&_timer))) == SOCKET_ERROR)
	{
		throw std::runtime_error("Select failed");
	}
	return ret;
}

bool	NWSelector::isReadyToReceive(INWAbstract &socket)
{
	return FD_ISSET(socket.getSocket(), &_readfds);
}

bool	NWSelector::isReadyToWrite(INWAbstract &socket)
{
	return FD_ISSET(socket.getSocket(), &_writefds);
}
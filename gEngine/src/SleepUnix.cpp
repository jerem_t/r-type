
#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_UNIX

#include <gEngine/System/Sleep.hpp>
#include <unistd.h>

namespace ge {

void sleep(Seconds seconds) {
  usleep(seconds * 1000000);
}

}

#endif
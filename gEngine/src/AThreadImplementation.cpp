//
// AThreadImplementation.cpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Mon Nov  4 11:53:58 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:21:51 2013 jordy domingos-mansoni
//

#include <gEngine/System/AThreadImplementation.hpp>
#include <gEngine/System/Thread.hpp>

void* AThreadImplementation::launch(void *userData) {
  Thread* owner = static_cast<Thread*>(userData);
    
  owner->run();
  return NULL;
}

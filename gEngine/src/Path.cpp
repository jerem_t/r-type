
#include <gEngine/Utils/Path.hpp>

namespace ge {

Path::Path(std::string const &path) {
  size_t offset = path.find_last_of('/');
  size_t extOffset = path.find_last_of('.');
  bool noExtension = extOffset == std::string::npos;
  if (noExtension)
    extOffset = path.size();
  offset = offset == std::string::npos ? 0 : offset + 1;
  _base = path.substr(0, offset);
  _fileName = path.substr(offset, extOffset - offset);
  if (!noExtension)
    _extension = path.substr(extOffset + 1);
}

Path::~Path() {}

std::string const &Path::getExtension() const {
  return _extension;
}

std::string const &Path::getFileName() const {
  return _fileName;
}

std::string const &Path::getBase() const {
  return _base;
}

std::string Path::extension(std::string const &path) {
  Path p(path);
  return p.getExtension();
}
std::string Path::fileName(std::string const &path) {
  Path p(path);
  return p.getFileName();
}
std::string Path::base(std::string const &path) {
  Path p(path);
  return p.getBase();
}

}

#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Core/Component.hpp>
#include <gEngine/Parsers/Eval.hpp>

namespace ge {

Component::Component(GameObject &gameObject)
: _gameObject(gameObject)
, _isEnabled(true)
, _hasStarted(false)
, _isThreadable(false) {}

Component::~Component() {}

void Component::start() {
  if (_hasStarted)
    return ;
  for (std::string const &dependency : _dependencies) {
    if (!_gameObject.hasComponent(dependency))
      _gameObject.component(dependency).start();
  }
  onStart();
  _hasStarted = true;
}

void Component::update(float dt) {
  onUpdate(dt);
}

bool Component::isEnabled() const {
  return _isEnabled;
}

void Component::enable() {
  _isEnabled = true;
}

void Component::disable() {
  _isEnabled = false;
}

bool Component::loadFromFile(std::string const &path) {
  Xml root;
  if (!root.loadFromFile(path))
    return false;
  XmlPtr component = root.getChildByName("component");
  if (component)
    return loadFromXml(component);
  return false;
}

bool Component::loadFromXml(XmlPtr const &node) {
  if (!ge::GameObject::getScope().empty() && node->has("scope") &&
      node->getAttr("scope") != ge::GameObject::getScope()) {
    disable();
    return true;
  }
  XmlArray params;
  node->getChildrenByName("param", params, false);
  for (auto &param : params)
    _data[param->getAttr("name")] = ge::eval(param->getAttr("value"));
  return true;
}

void Component::setDependencies(std::list<std::string> const &deps) {
  _dependencies = deps;
}


}

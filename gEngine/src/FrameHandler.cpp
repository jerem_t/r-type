
#include <gEngine/System/Sleep.hpp>
#include <gEngine/System/FrameHandler.hpp>

namespace ge {

FrameHandler::FrameHandler()
: _frameRate(60)
, _async(false)
, _started(false)
, _fps(60) {}

FrameHandler::~FrameHandler() {}

void 	FrameHandler::setFrameRate(size_t frames) {
	_frameRate = frames;
}

size_t	FrameHandler::getFrameRate() const {
	return _frameRate;
}

size_t	FrameHandler::getFps() const {
	return _fps;
}

void FrameHandler::setAsync(bool async) {
	_async = async;
}

void	FrameHandler::start() {
	_started = true;
	// thread if async
	_loop();
}

void FrameHandler::stop() {
	// join the thread
	_started = false;
}

void FrameHandler::_loop() {
	float	toWait = 0.;
	float	prevTime = 0.;

	_clock.start();
	while (_started) {
		_fps = 1.0 / (_clock.getTotalElapsed() - prevTime);
		prevTime = _clock.getTotalElapsed();
		trigger("frame", _clock.getElapsed(), _fps);
		toWait = 1000000.0 / _frameRate - (_clock.getTotalElapsed() - prevTime) * 1000000.0;
		if (toWait > 0)
			ge::sleep(ge::MicroSeconds(toWait));
	}
}
	
}
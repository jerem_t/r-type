//
// MutexImplementation_Unix.cpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Mon Nov  4 14:20:41 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:22:05 2013 jordy domingos-mansoni
//

#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_UNIX

#include <gEngine/System/MutexImplementation.hpp>

MutexImplementation::MutexImplementation() : status_(UNLOCK) {
  pthread_mutex_init(&mutex_, NULL);
}

void MutexImplementation::lock() {
  status_ = LOCK;
  pthread_mutex_lock(&mutex_);
}

void MutexImplementation::trylock() {
  pthread_mutex_trylock(&mutex_);
}

void MutexImplementation::unlock() {
  status_ = UNLOCK;
  pthread_mutex_unlock(&mutex_);
}

MutexImplementation::~MutexImplementation() {
  pthread_mutex_destroy(&mutex_);
}

#endif	/* GE_SYSTEM_UNIX */

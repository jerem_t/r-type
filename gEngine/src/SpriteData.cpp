
#include <gEngine/Data/SpriteData.hpp>

namespace ge {

SpriteData::SpriteData()
: _color(255, 255, 255, 255)
, _fields(0) {}

SpriteData::SpriteData(SpriteData const &other)
: _texture(other._texture)
, _textureRect(other._textureRect)
, _color(other._color)
, _fields(other._fields) {}

SpriteData &SpriteData::operator=(SpriteData const &other) {
  if (this != &other) {
    _texture = other._texture;
    _textureRect = other._textureRect;
    _color = other._color;
    _fields = other._fields;
  }
  return *this;
}

SpriteData::~SpriteData() {}

void SpriteData::setTexture(std::string const &texture) {
  _fields |= Texture;
  _texture = texture;
}

void SpriteData::setTextureRect(ge::Boxf const &textureRect) {
  _fields |= TextureRect;
  _textureRect = textureRect;
}

void SpriteData::setColor(ge::Color const &color) {
  _fields |= Color;
  _color = color;
}

std::string const &SpriteData::getTexture() const {
  return _texture;
}

ge::Boxf const &SpriteData::getTextureRect() const {
  return _textureRect;
}

ge::Color const &SpriteData::getColor() const {
  return _color;
}

bool SpriteData::hasTexture() const {
  return _fields & Texture;
}

bool SpriteData::hasTextureRect() const {
  return _fields & TextureRect;
}

bool SpriteData::hasColor() const {
  return _fields & Color;
}


}
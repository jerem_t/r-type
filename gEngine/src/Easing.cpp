#define	_USE_MATH_DEFINES
#include <cmath>
#include <gEngine/Math/Easing.hpp>

namespace ge {

// Linear

float easing::linear(float t,float b , float c, float d) {
  return c * t / d + b;
}

// Back

float easing::backIn(float t, float b, float c, float d) {
  float s = 1.70158f;
  t /= d;
  return c * t * t * ((s + 1) * t - s) + b;
}

float easing::backOut(float t, float b, float c, float d) {
  float s = 1.70158f;
  t = t / d - 1;
  return c * (t * t* ((s + 1) * t + s) + 1) + b;
}

float easing::backInOut(float t, float b, float c, float d) {
  float s = 1.70158f;
  t /= d / 2;
  s *= 1.525f;
  if (t < 1)
    return c / 2 * (t * t * ((s + 1) * t - s)) + b;
  t -= 2;
  return c / 2 * (t * t * ((s + 1) * t + s) + 2) + b;
}

// Bounce

float easing::bounceIn(float t, float b, float c, float d) {
  return c - bounceOut(d - t, 0, c, d) + b;
}

float easing::bounceOut(float t, float b, float c, float d) {
  t /= d;
  if (t < 1.0f / 2.75f)
    return c * (7.5625f * t * t) + b;
  if (t < 2.0f / 2.75f) {
    t -= 1.5f / 2.75f;
    return c * (7.5625f * t * t + .75f) + b;
  }
  if (t < 2.5f / 2.75f) {
    t -= 2.25f / 2.75f;
    return c * (7.5625f * t * t + .9375f) + b;
  }
  t -= 2.625f / 2.75f;
  return c * (7.5625f * t * t + .984375f) + b;
}

float easing::bounceInOut(float t, float b, float c, float d) {
  if (t < d / 2)
    return bounceIn(t * 2, 0, c, d) * .5f + b;
  return bounceOut(t * 2 - d, 0, c, d) * .5f + c * .5f + b;
}

// // Circle

float easing::circleIn(float t, float b, float c, float d) {
  t /= d;
  return -c * (sqrt(1 - t * t) - 1) + b;
}

float easing::circleOut(float t, float b, float c, float d) {
  t = t / d - 1;
  return c * sqrt(1 - t * t) + b;
}

float easing::circleInOut(float t, float b, float c, float d) {
  t /= d / 2;
  if (t < 1)
    return -c / 2 * (sqrt(1 - t * t) - 1) + b;
  t -= 2;
  return c / 2 * (sqrt(1 - t * t) + 1) + b;
}

// // Cubic

float easing::cubicIn(float t, float b, float c, float d) {
  t /= d;
  return c * t * t * t + b;
}

float easing::cubicOut(float t, float b, float c, float d) {
  t = t / d - 1.0f;
  return c * (t * t * t + 1.0f) + b;
}

float easing::cubicInOut(float t, float b, float c, float d) {
  t /= d / 2.0f;
  if (t < 1.0f)
    return c / 2.0f * t * t * t + b;
  t -= 2.0f;
  return c / 2.0f * (t * t * t + 2.0f) + b;
}

// // Elastic

float easing::elasticIn(float t, float b, float c, float d) {
  if (t == 0)
    return b;
  t /= d;
  if (t == 1)
    return b + c;
  --t;
  float p = d * .3f;
  float s = p / 4;
  return -(c * pow(2, 10 * t) * sin((t * d - s) * (2 * M_PI) / p)) + b;
}

float easing::elasticOut(float t, float b, float c, float d) {
  if (t == 0)
    return b;
  t /= d;
  if (t == 1)
    return b + c;
  float p = d * .3f;
  float s = p / 4;
  return c * pow(2, -10 * t) * sin((t * d - s) * (2 * M_PI) / p) + c + b;
}

float easing::elasticInOut(float t, float b, float c, float d) {
  if (t == 0)
    return b;
  t /= d / 2;
  if (t == 2)
    return b + c;
  float p = d*(.3f*1.5f);
  float s = p/4;
  --t;
  if (t < 0)
    return -.5f * (c * pow(2, 10 * t) * sin((t * d - s) *
           (2 * M_PI) / p)) + b;
  return c * pow(2, -10 * t) * sin((t * d - s) *
         (2 * M_PI) / p) * .5f + c + b;
}

// Exponential

float easing::expoIn(float t, float b, float c, float d) {
  return t == 0 ? b : c * pow(2, 10 * (t / d - 1)) + b;
}

float easing::expoOut(float t, float b, float c, float d) {
  return t == d ? b + c : c * (-pow(2, -10 * t / d) + 1) + b;
}

float easing::expoInOut(float t, float b, float c, float d) {
  if (t == 0)
    return b;
  if (t == d)
    return b + c;
  t /= d / 2;
  if (t < 1)
    return c / 2 * pow(2, 10 * (t - 1)) + b;
  return c / 2 * (-pow(2, -10 * (t - 1)) + 2) + b;
}

// Quad

float easing::quadIn(float t,float b , float c, float d) {
  t /= d;
  return c * t * t + b;
}

float easing::quadOut(float t,float b , float c, float d) {
  t /= d;
  return -c * t * (t - 2) + b;
}

float easing::quadInOut(float t,float b , float c, float d) {
  t /= d / 2;
  if (t < 1)
    return ((c / 2) * (t * t)) + b;
  --t;
  return -c / 2 * (((t - 2) * t) - 1) + b;
}

// Quart

float easing::quartIn(float t,float b , float c, float d) {
  t /= d;
  return c * t * t * t * t + b;
}

float easing::quartOut(float t,float b , float c, float d) {
  t = t / d - 1;
  return -c * (t * t * t * t - 1) + b;
}

float easing::quartInOut(float t,float b , float c, float d) {
  t /= d / 2;
  if (t < 1)
    return c / 2 * t * t * t * t + b;
  t -= 2;
  return -c / 2 * (t * t * t * t - 2) + b;
}

// Quint

float easing::quintIn(float t,float b , float c, float d) {
  t /= d;
  return c * t * t * t * t * t + b;
}

float easing::quintOut(float t,float b , float c, float d) {
  t = t / d - 1;
  return c * (t * t * t * t * t + 1) + b;
}

float easing::quintInOut(float t,float b , float c, float d) {
  t /= d / 2;
  if (t < 1)
    return c / 2 * t * t * t * t * t + b;
  t -= 2;
  return c / 2 * (t * t * t * t * t + 2) + b;
}

// Sinus

float easing::sineIn(float t,float b , float c, float d) {
  return -c * cos(t / d * (M_PI / 2)) + c + b;
}

float easing::sineOut(float t,float b , float c, float d) {
  return c * sin(t / d * (M_PI / 2)) + b;
}

float easing::sineInOut(float t,float b , float c, float d) {
  return -c / 2 * (cos(M_PI * t / d) - 1) + b;
}

}
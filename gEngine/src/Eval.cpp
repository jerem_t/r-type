
#include <sstream>
#include <gEngine/Math/Vector2.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Math/Box.hpp>
#include <gEngine/Utils/Array.hpp>
#include <gEngine/Utils/Dictionary.hpp>
#include <gEngine/Parsers/Eval.hpp>
#include <gEngine/Core/GameObject.hpp>

namespace ge {

// std::noskipws 

static std::string _clearString(std::string const &str) {
  std::istringstream iss(str);
  std::string result;
  iss >> result;
  return result;
}

static bool _parseNumber(std::string const &str, Any &result) {
  std::istringstream iss(str);
  float f;
  iss >> f;
  result = f;
  return !iss.fail();
}

static bool _parseBoolean(std::string const &str, Any &result) {
  std::istringstream iss(str);
  bool b;
  iss >> std::boolalpha >> b;
  result = b;
  return !iss.fail();
}

static bool _parseVector(std::string const &str, Any &result) {
  std::istringstream iss(str);
  std::string name;
  unsigned char comma;
  float x, y, z, w;
  std::getline(iss, name, '(');
  if (name.find("vec2") != std::string::npos) {
    iss >> x >> comma >> y;
    result = ge::Vector2f(x, y);
  } else if (name.find("vec3") != std::string::npos) {
    iss >> x >> comma >> y >> comma >> z;
    result = ge::Vector3f(x, y, z);
  } else if (name.find("vec4") != std::string::npos) {
    iss >> x >> comma >> y >> comma >> z >> comma >> w;
    result = ge::Vector4f(x, y, z, w);
  } else if (name.find("color") != std::string::npos) {
    iss >> x >> comma >> y >> comma >> z >> comma >> w;
    result = ge::Color(x, y, z, w);
  } else if (name.find("box") != std::string::npos) {
    iss >> x >> comma >> y >> comma >> z;
    result = ge::Boxf(x, y, z);
  } else
    return false;
  std::getline(iss, name, ')');
  return !iss.fail();
}

static void _parseString(std::string const &str, Any &result) {
  result = _clearString(str);
}

static void _parseType(std::string const &str, Any &result) {
  if (_parseNumber(str, result))
    return ;
  if (_parseBoolean(str, result))
    return ;
  if (_parseVector(str, result))
    return ;
  _parseString(str, result);
}

static bool _parseArray(std::string const &str, Any &result) {
  std::istringstream iss(str);
  std::string token;
  ge::Array array;
  size_t i = 0;
  while (!iss.eof()) {
    std::getline(iss, token, ';');
    if (token.size())
      _parseType(token, array[i]);
    ++i;
  }
  if (i < 2)
    return false;
  result = array;
  return !iss.fail();
}

static bool _parseDictionary(std::string const &str, Any &result) {
  std::istringstream iss(str);
  std::string key, val;
  ge::Dictionary dict;
  if (str.find(':') == std::string::npos)
    return false;
  while (!iss.eof()) {
    std::getline(iss, key, ':');
    std::getline(iss, val, ';');
    if (key.size() && val.size())
      _parseType(_clearString(val), dict[_clearString(key)]);
  }
  result = dict;
  return true;
}
static void _addDefines(std::string &str) {
  while (true) {
    size_t first = str.find_first_of('$');
    if (first == std::string::npos)
      return ;
    std::string name = str.substr(first + 1);
    size_t last = name.find_first_not_of("abcdefghijklmnopqrstuvwxyz_");
    if (last == 0)
      return ;
    name = name.substr(0, last);
    std::string value;
    if (GameObject::isDefined(name))
      value = toString(GameObject::getDefine(name));
    if (last == std::string::npos)
      last = str.size();
    else
      last++;
    str.replace(first, last, value);
  }
}


Any eval(std::string const &str) {
  std::string tmp = str;
  Any result;
  _addDefines(tmp);
  if (_parseDictionary(tmp, result))
    return result;
  if (_parseArray(tmp, result))
    return result;
  _parseType(tmp, result);
  return result;
}

}
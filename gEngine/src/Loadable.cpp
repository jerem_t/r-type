
#include <gEngine/Resources/Loadable.hpp>

namespace ge {

Loadable::Loadable() {}

Loadable::~Loadable() {}

bool Loadable::loadFromFile(std::string const &path) {
  std::ifstream f(path.c_str());
  return !f.is_open() || loadFromStream(f);
}

bool Loadable::loadFromStream(std::istream &stream) {
  return loadFromMemory(std::string(
    std::istreambuf_iterator<char>(stream),
    (std::istreambuf_iterator<char>())
  ));
}

std::istream &Loadable::operator<<(std::istream &stream) {
  loadFromStream(stream);
  return stream;
}

}
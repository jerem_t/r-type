//
// Thread.cpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sun Nov  3 11:43:22 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:22:17 2013 jordy domingos-mansoni
//

#include <gEngine/System/Thread.hpp>

bool Thread::joinable() const {
  return impl_->joinable();
}

void Thread::join() {
  impl_->join();
}

void Thread::detach() {
  impl_->detach();
}

void Thread::run() {
  func_abstraction_->call(); 
}

Thread::~Thread() {
  delete impl_;
}

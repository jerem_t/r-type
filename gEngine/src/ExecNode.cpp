
#include <gEngine/Parsers/ExecNode.hh>

#include <list>

// create map for qualifiers
std::map<ExecNode::ERuleQualifier, ExecNode::t_qualif> ExecNode::_qualifiers =
{
	{ExecNode::STAR, &ExecNode::star},
	{ExecNode::PLUS, &ExecNode::plus},
	{ExecNode::QUES, &ExecNode::ques},
	{ExecNode::NONE, &ExecNode::none}
};

ExecNode::ExecNode(ERuleLinker link) :
	_link(link),
	_qualifier(NONE),
	_basicRule(NULL),
	_callId(0),
	_attach(true)
{
}

ExecNode::~ExecNode()
{
}

#include <iostream>

std::shared_ptr<ParsingNode> 	ExecNode::star(t_rule rule,
											 BnfParser &parser) const
{
	std::shared_ptr<ParsingNode> 	ret(new ParsingNode(true)); // create an additional node
	bool 							run = true;

	while (run)
	{
		std::shared_ptr<ParsingNode> 	cur = (this->*rule)(parser);

		run = cur->getResult();
		if (run)
			ret->addChild(cur);
	}
	return (ret);
}

std::shared_ptr<ParsingNode> 	ExecNode::plus(t_rule rule,
											 BnfParser &parser) const
{
	std::shared_ptr<ParsingNode> 	ret(new ParsingNode()); // create an additional node
	bool 							run = true;

	while (run)
	{
		std::shared_ptr<ParsingNode> 	cur = (this->*rule)(parser);

		run = cur->getResult();
		if (run)
		{
			ret->addChild(cur);
			ret->setResult(true);
		}
	}
	return (ret);
}

std::shared_ptr<ParsingNode> 	ExecNode::ques(t_rule rule,
											 BnfParser &parser) const
{
	std::shared_ptr<ParsingNode> 	cur = (this->*rule)(parser);

	cur->setResult(true);
	return (cur);
}

std::shared_ptr<ParsingNode> 	ExecNode::none(t_rule rule,
											 BnfParser &parser) const
{
	return ((this->*rule)(parser));
}

std::shared_ptr<ParsingNode> 	ExecNode::callRule(BnfParser &parser) const
{
	return (parser.callRule(_call));
}

std::shared_ptr<ParsingNode> 	ExecNode::basicRule(BnfParser &parser) const
{
	return (
		std::shared_ptr<ParsingNode>(
			new ParsingNode((parser.*_basicRule)(_basicRuleParam))
									)
			);
}

std::shared_ptr<ParsingNode> 	ExecNode::execSons(BnfParser &parser) const
{
	std::shared_ptr<ParsingNode> 				ret(new ParsingNode(false)); // create an additional node
	std::list<std::shared_ptr<ParsingNode> >	tested;
	ExecNode::const_iterator 					it = begin();
	bool 										lastFailed = true;

	parser.saveContext();
	while (it != end())
	{
		std::shared_ptr<ParsingNode> 	cur;

		cur = (*it)->executeParsing(parser);
		if (cur->getResult() == true)
		{
			lastFailed = false;
			if ((*it)->attachToFather())
				tested.push_back(cur);
			++it;
			if (it != end() && (*it)->getLink() == OR)
				it = end();
		}
		else
		{
			lastFailed = true;
			tested.clear();
			do
			{
				++it;
			}
			while (it != end() && (*it)->getLink() != OR);
			parser.restoreContext();
			parser.saveContext();
		}
	}
	parser.popContext();
	if (lastFailed)
		return (ret);
	std::list<std::shared_ptr<ParsingNode> >::iterator 		toAdd = tested.begin();

	while (toAdd != tested.end())
	{
		ret->addChild(*toAdd);
		++toAdd;
	}
	ret->setResult(true);
	return (ret);
}

#include <iostream>

std::shared_ptr<ParsingNode> 	ExecNode::executeParsing(BnfParser &parser)
{
	std::shared_ptr<ParsingNode> 	ret;

	/*
	**	Begin record
	*/
	std::cout << "exec" << std::endl;
	if (_name.empty() == false)
		parser.beginRecord(_name + std::to_string(_callId++));
	if (isCallBack()) // is a user callback
	{
		ret = std::shared_ptr<ParsingNode>(new ParsingNode);
		ret->setResult(parser.callCallBack(_callBackName, _callBackParams));
	}
	else if (_call.empty() == false) // if is a call to a rule
		ret = (this->*(_qualifiers[_qualifier]))(&ExecNode::callRule, parser);
	else if (_basicRule != NULL) // if is a basic rule
		ret = (this->*(_qualifiers[_qualifier]))(&ExecNode::basicRule, parser);
	else // is just a sub rule
		ret = (this->*(_qualifiers[_qualifier]))(&ExecNode::execSons, parser);
	/*
	**	End record
	*/
	if (_name.empty() == false)
	{
		ret->setValue(parser.getRecord(_name + std::to_string(--_callId)));
		parser.registerResult(_name, ret);
	}
	return (ret);
}

ExecNode::ERuleLinker 	ExecNode::getLink() const
{
	return (_link);
}

void 			ExecNode::setQualifier(ERuleQualifier qual)
{
	_qualifier = qual;
}

ExecNode::ERuleQualifier 	ExecNode::getQualifier() const
{
	return (_qualifier);
}

void 			ExecNode::setCall(std::string const &name)
{
	_call = name;
}

void 			ExecNode::setAttachToFather(bool attach)
{
	_attach = attach;
}

bool 			ExecNode::attachToFather() const
{
	return (_call.empty() == false && _attach);
}

void 			ExecNode::setBasicRule(t_basicRule rule, std::string const &param)
{
	_basicRule = rule;
	_basicRuleParam = param;
}

void 			ExecNode::setName(std::string const &name)
{
	_name = name;
}

std::string 	ExecNode::getName() const
{
	return (_name);
}

void 			ExecNode::setCallBack(std::string const &callBack,
									std::list<std::string> const &params)
{
	_callBackName = callBack;
	_callBackParams = params;
}

bool 			ExecNode::isCallBack() const
{
	return (_callBackName.empty() == false);
}

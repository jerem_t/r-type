
#include <gEngine/System/Timer.hpp>

namespace ge {

Timer::Timer()
: _finish(-1.0f)
, _interval(1.0f)
, _current(0.0f)
, _currentTick(0)
, _total(0.0f)
, _repeat(false)
, _started(false) {}

Timer::~Timer() {}

void Timer::setRepeat(bool repeat) {
  _repeat = repeat;
}

bool Timer::hasStarted() const {
  return _started;
}

void Timer::start() {
  _started = true;
  _current = 0.0f;
  _currentTick = 0;
  _total = 0.0f;
  trigger("tick", 0);
}

void Timer::stop() {
  _started = false;
}

void Timer::setFinish(Seconds seconds) {
  _finish = seconds;
}

Seconds Timer::getFinish() const {
  return _finish;
}

void Timer::setInterval(Seconds seconds) {
  _interval = seconds;
}

Seconds Timer::getInterval() const {
  return _interval;
}

void Timer::update(Seconds dt) {
  if (!_started)
    return ;
  _current += dt;
  if (_current > _interval) {
    _total += _current;
    _current = 0;
    ++_currentTick;
    trigger("tick", _currentTick);
  }
  if (_finish > 0 && _total > _finish) {
    stop();
    trigger("finish");
    if (_repeat == true)
      start();
  }
}

}
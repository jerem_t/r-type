
#include <iostream>
#include <gEngine/Parsers/Json.hpp>
#include <gEngine/Core/ComponentManager.hpp>
#include <gEngine/System/Platform.hpp>
#include <gEngine/Parsers/Xml.hpp>
#include <gEngine/Utils/Singleton.inl>

namespace ge {

ComponentManager::ComponentManager() {}

ComponentManager::~ComponentManager() {}

bool ComponentManager::add(std::string const &path) {
  Library library;
  Xml config;
  std::string configPath = path + "/config.xml";
  if (!config.loadFromFile(configPath))
    return false;
  XmlPtr deps = config.getChildByName("dependencies");
  std::string name = config.getChildByName("component")->getAttr("name");
  if (deps) {
    XmlArray components;
    config.getChildrenByName("component", components);
    for (auto &c : components)
      _components[name].dependencies.push_back(c->getAttr("name"));
  }
  if (!library.loadFromFile("gEngine_" + name))
	  return false;
  _components[name].creator = library.getSymbol<Creator>("create");
  return true;
}

std::shared_ptr<Component> ComponentManager::create(std::string const &name,
								                       GameObject &gameObject) {
  if (_components.find(name) == _components.end())
    throw std::runtime_error("Unknown component \"" + name + "\".");
  std::shared_ptr<Component> component(_components[name].creator(gameObject));
  component->setName(name);
  component->setDependencies(_components[name].dependencies);
  return component;
}

bool ComponentManager::loadFromFile(std::string const &path) {
  Xml xml;
  XmlArray components;

  if (!xml.loadFromFile(path))
    return false;
  xml.getChildrenByName("component", components);
  for (auto const &component : components)
    add((*component)["path"]);
  return true;
}

std::size_t	ComponentManager::getSizeComponents() const
{
	return _components.size();
}

}

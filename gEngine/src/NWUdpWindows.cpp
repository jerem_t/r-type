#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_WINDOWS

#include "gEngine/Network/NWUdp.hpp"

NWUdp::NWUdp() : m_socket(INVALID_SOCKET), m_blocking(true)
{}

NWUdp::~NWUdp()
{
	close();
}

/*
** Public methods
*/

INWAbstract::Status	NWUdp::send(const std::string &ip, USHORT port, const std::vector<char> &data)
{
	WSABUF				buffer;
	DWORD				flag, bytesSent;
	SOCKADDR_IN			remote;

	// Checking whether data or size is different than 0
	if (data.empty())
	{
		return INWAbstract::SC_ERROR;
	}

	// Configuration for sending data
	remote.sin_family = AF_INET;
	remote.sin_addr.s_addr = inet_addr(ip.c_str());
	remote.sin_port = htons(port);

	// Sending data
	buffer.buf = const_cast<CHAR *>(data.data());
	buffer.len = data.size();

	flag = 0;
	if (WSASendTo(m_socket, &buffer, 1, &bytesSent, flag, reinterpret_cast<SOCKADDR *>(&remote), sizeof(remote), 0, 0) == SOCKET_ERROR)
	{
		close();
		return INWAbstract::SC_ERROR;
	}
	return INWAbstract::SC_DONE;
}

INWAbstract::Status	NWUdp::send(const std::string &ip, USHORT port, const NWPacket &data)
{
	return (send(ip, port, data.getData()));
}

INWAbstract::Status	NWUdp::receive(const std::string &ip, USHORT port, std::vector<char> &buffer, std::size_t size, std::size_t &received)
{
	std::size_t			ret;
	WSABUF				tmp;
	DWORD				flag, bytesReceived;
	SOCKADDR_IN			service;
	int					serviceLength = sizeof(service);

	// Checking whether size is different than 0
	if (size == 0)
	{
		return INWAbstract::SC_ERROR;
	}

	// Configuration for sending data
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(ip.c_str());
	service.sin_port = htons(port);

	// Sending data
	tmp.buf = new char[size]();
	tmp.len = size;

	// Cleaning buffer
	ZeroMemory(tmp.buf, tmp.len);

	flag = 0;
	received = 0;
	if ((ret = WSARecvFrom(m_socket, &tmp, 1, &bytesReceived, &flag, reinterpret_cast<SOCKADDR *>(&service), &serviceLength, 0, 0)) == NO_ERROR &&
		bytesReceived > 0)
	{
		received = bytesReceived;
		buffer.resize(received);
		std::memcpy(buffer.data(), tmp.buf, received);
		ZeroMemory(tmp.buf, tmp.len);
	}
	if (ret == SOCKET_ERROR)
	{
		close();
		return INWAbstract::SC_ERROR;
	}
	return INWAbstract::SC_DONE;
}

INWAbstract::Status	NWUdp::receive(const std::string &ip, USHORT port, NWPacket &packet, std::size_t size, std::size_t &received)
{
	std::vector<char>	buffer;
	INWAbstract::Status	status;

	if ((status = receive(ip, port, buffer, size, received)) == INWAbstract::SC_DONE)
		packet.setData(buffer);
	return status;
}

INWAbstract::Status	NWUdp::bind(USHORT port)
{
	SOCKADDR_IN			local;

	// Create socket
	create();

	// Configuration for binding
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = inet_addr("127.0.0.1");
	local.sin_port = htons(port);
	
	// Binding
	if (::bind(m_socket, reinterpret_cast<SOCKADDR *>(&local), sizeof(local)) == SOCKET_ERROR)
	{
		return INWAbstract::SC_ERROR;
	}
	return INWAbstract::SC_DONE;
}

void				NWUdp::unbind()
{
	closesocket(m_socket);
	m_socket = INVALID_SOCKET;
}

INWAbstract::Type	NWUdp::getType() const
{
	return INWAbstract::SC_UDP;
}

void				NWUdp::setBlocking(bool blocking)
{
	u_long mode = blocking ? 0 : 1;
	if (ioctlsocket(m_socket, FIONBIO, &mode) == 0)
	{
		close();
		throw std::exception("blocking mode failed");
	}
}

bool				NWUdp::isBlocking() const
{
	return m_blocking;
}

void				NWUdp::setSocket(NWSOCKET socket)
{
	// Close everything if there is a valid socket
	close();

	// Initialization
	create();
	m_socket = socket;
}

NWSOCKET			NWUdp::getSocket() const
{
	return m_socket;
}

/*
** Private methods
*/

void	NWUdp::create()
{
	WSADATA	wsaData;

	// Initialize WinSock 2
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != NO_ERROR)
	{
		throw std::exception("WSAStartup failed");
	}

	// Initialize the socket
	// If the socket is valid we do nothing
	if (m_socket == INVALID_SOCKET)
	{
		if ((m_socket = WSASocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP, 0, 0, 0)) == INVALID_SOCKET)
		{
			close();
			throw std::exception("WSASocket failed");
		}
	}

	// Enable broadcast for UDP sockets
	int	enabled = 1;
	if (setsockopt(m_socket, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char *>(&enabled), sizeof(enabled)) == -1)
	{
		close();
		throw std::exception("Setsockopt broadcast failed");
	}
}

void	NWUdp::close()
{
	unbind();
	if (m_socket != INVALID_SOCKET)
	{
		WSACleanup();
	}
}

#endif

#include <gEngine/Parsers/AParserFeeder.hh>

AParserFeeder::AParserFeeder(size_t chunkSize) :
	_chunkSize(chunkSize)
{
}

AParserFeeder::~AParserFeeder()
{
}

size_t			AParserFeeder::getChunkSize() const
{
	return (_chunkSize);
}

void			AParserFeeder::setChunkSize(size_t size)
{
	_chunkSize = size;
}

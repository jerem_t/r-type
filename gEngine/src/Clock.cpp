
#include <gEngine/System/Clock.hpp>

namespace ge {

Clock::Clock() {}

Clock::~Clock() {}

void	Clock::start(){
	reset();
}

Seconds	Clock::getElapsed(){
	Seconds	result = std::chrono::duration_cast<std::chrono::duration<Seconds>>(SysClock::now() - _previousTime).count() - _currTimePause;
	_previousTime = SysClock::now();
	_currTimePause = 0;
	return (result);
}

Seconds	Clock::getTotalElapsed(){
	Seconds	result = std::chrono::duration_cast<std::chrono::duration<Seconds>>(SysClock::now() - _timeAtBeginning).count() - _totalTimePause;
	return (result);
}

void	Clock::reset(){
	_currTimePause = 0;
	_totalTimePause = 0;
	_timeAtBeginning = SysClock::now();
	_previousTime = SysClock::now();
}

void	Clock::pause(){ 
	_timeAtPause = SysClock::now();
}

void	Clock::resume(){
	Seconds t = std::chrono::duration_cast<std::chrono::duration<Seconds>>(SysClock::now() - _timeAtPause).count();
	_totalTimePause += t;
	_currTimePause = t;
}

size_t	Clock::getTimeStamp()
{
	std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
	std::chrono::system_clock::duration dtn = tp.time_since_epoch();

	return (std::chrono::duration_cast<std::chrono::milliseconds>(dtn).count());
}

}

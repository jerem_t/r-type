
#include <gEngine/Data/TextData.hpp>

namespace ge {

TextData::TextData()
: _str("")
, _font("")
, _color(255, 255, 255, 255)
, _size(0)
, _fields(0) {
}

TextData::TextData(TextData const &other)
: _str(other._str)
, _font(other._font)
, _color(other._color)
, _size(other._size)
, _fields(other._fields) {
}

TextData &TextData::operator=(TextData const &other) {
  if (this != &other) {
    _str = other._str;
    _font = other._font;
    _color = other._color;
    _size = other._size;
    _fields = other._fields;
  }
  return *this;
}

TextData::~TextData() {}

void TextData::setString(std::string const &str) {
  _str = str;
  _fields |= String;
}

void TextData::setFont(std::string const &font) {
  _font = font;
  _fields |= Font;
}

void TextData::setColor(ge::Color const &color) {
  _color = color;
  _fields |= Color;
}
void TextData::setCharacterSize(float size) {
  _size = size;
  _fields |= CharacterSize;
}

std::string const &TextData::getString() const {
  return _str;
}

std::string const &TextData::getFont() const {
  return _font;
}

ge::Color const &TextData::getColor() const {
  return _color;
}

float TextData::getCharacterSize() const {
  return _size;
}

bool TextData::hasString() const {
  return _fields & String;
}
bool TextData::hasFont() const {
  return _fields & Font;
}
bool TextData::hasColor() const {
  return _fields & Color;
}
bool TextData::hasCharacterSize() const {
  return _fields & CharacterSize;
}

}
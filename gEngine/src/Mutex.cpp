//
// Mutex.cpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Mon Nov  4 14:32:36 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:21:18 2013 jordy domingos-mansoni
//

#include <gEngine/System/Mutex.hpp>

Mutex::Mutex() : impl_(new MutexImplementation) {
}

void Mutex::lock() {
  impl_->lock();
}

void Mutex::trylock() {
  impl_->trylock();
}
void Mutex::unlock() {
  impl_->unlock();
}

Mutex::~Mutex() {
  delete impl_;
}

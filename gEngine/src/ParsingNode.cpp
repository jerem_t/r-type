
#include <gEngine/Parsers/ParsingNode.hh>

ParsingNode::ParsingNode(bool result) :
	_result(result)
{
}

ParsingNode::~ParsingNode()
{
}

bool 		ParsingNode::getResult() const
{
	return (_result);
}

void 		ParsingNode::setResult(bool res)
{
	_result = res;
}

std::string	ParsingNode::getValue() const
{
	return (_value);
}

void 		ParsingNode::setValue(std::string const &value)
{
	_value = value;
}

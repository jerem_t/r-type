//
// WThread.cpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sun Nov  3 15:18:45 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:20:22 2013 jordy domingos-mansoni
//

#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_WINDOWS

#include <gEngine/System/ThreadImplementation.hpp>

ThreadImplementation::ThreadImplementation(Thread *owner) : status_(STARTED) {
	thread_ = CreateThread(NULL, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(&AThreadImplementation::launch), owner, 0, &id_);
}

bool ThreadImplementation::joinable() const {
  return status_ == STARTED;
}
#include <iostream>
void ThreadImplementation::join() {
	WaitForSingleObject(thread_, INFINITE);
}

void ThreadImplementation::detach() {
  CloseHandle(thread_);
  status_ = FINISHED;
}

#endif		/* GE_SYSTEM_WINDOWS */

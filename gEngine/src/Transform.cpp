
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>
#include <iostream>

namespace ge {

Transform::Transform()
: _position(0.0, 0.0, 0.0),
  _rotation(0.0, 0.0, 0.0),
  _scale(1.0, 1.0, 1.0),
  _localMatrixUpdate(true),
  _globalMatrixUpdate(true),
  _globalMatrixUpdated(false)
{}

Transform::~Transform() {}

Vector3f const&   Transform::getPosition() const {
  return _position;
}

Vector3f const&   Transform::getGlobalPosition() const {
  return _globalPosition;
}


Quaternionf const&   Transform::getRotation() const {
  return _rotation;
}

Quaternionf const&   Transform::getGlobalRotation() const {
  return _globalRotation;
}


Vector3f const&   Transform::getScale() const {
  return _scale;
}

Vector3f const&   Transform::getGlobalScale() const {
  return _globalScale;
}


void    Transform::setPosition(float x, float y, float z, bool relative) {
  if (relative) {
    _position.x = x * float(ge::GameObject::getDefine("width"));
    _position.y = y * float(ge::GameObject::getDefine("height"));
    _position.z = z;
  } else {
    _position.x = x;
    _position.y = y;
    _position.z = z;
  }
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::setPosition(Vector3f const &pos, bool relative) {
  if (relative) {
    _position.x = pos.x * float(ge::GameObject::getDefine("width"));
    _position.y = pos.y * float(ge::GameObject::getDefine("height"));
    _position.z = pos.z;
  } else
    _position = pos;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::translate(float x, float y, float z, bool relative) {
  if (relative) {
    _position.x += x * float(ge::GameObject::getDefine("width"));
    _position.y += y * float(ge::GameObject::getDefine("height"));
    _position.z += z;
  } else {
    _position.x += x;
    _position.y += y;
    _position.z += z;
  }
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::translate(Vector3f const &pos, bool relative) {
  if (relative) {
    _position.x += pos.x * float(ge::GameObject::getDefine("width"));
    _position.y += pos.y * float(ge::GameObject::getDefine("height"));
    _position.z += pos.z;
  } else
    _position += pos;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::setRotation(float x, float y, float z) {
  _rotation = Quaternionf(x, y, z);
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::setRotation(Vector3f const &rot) {
  _rotation = Quaternionf(rot);
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::setRotation(Quaternionf const &rot) {
  _rotation = rot;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::rotate(float x, float y, float z) {
  _rotation *= Quaternionf(x, y, z);
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::rotate(Vector3f const &rot) {
  _rotation *= Quaternionf(rot);
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::rotate(Quaternionf const &rot) {
  _rotation += rot;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::setScale(float x, float y, float z) {
  _scale.x = x;
  _scale.y = y;
  _scale.z = z;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::setScale(Vector3f const &sca) {
  _scale = sca;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::scale(float x, float y, float z) {
  _scale.x += x;
  _scale.y += y;
  _scale.z += z;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

void    Transform::scale(Vector3f const &sca) {
  _scale.x += sca.x;
  _scale.y += sca.y;
  _scale.z += sca.z;
  _localMatrixUpdate = true;
  _globalMatrixUpdate = true;
}

Matrix4f const &Transform::calcLocalMatrix() {
    if (_localMatrixUpdate) {
      _localMatrix = Matrix4f();
      _localMatrix.translate(_position);
      _localMatrix.rotate(_rotation);
      //_localMatrix.scale(_scale);
      _localMatrixUpdate = false;
    }
  return (_localMatrix);
}

Matrix4f const &Transform::getLocalMatrix() const {
  return (_localMatrix);
}


Matrix4f const &Transform::calcGlobalMatrix() {
    if (_globalMatrixUpdate) {
      calcLocalMatrix();
      _globalMatrix = _localMatrix;
      _globalPosition = _position;
      _globalRotation = _rotation;
      _globalScale = _scale;
      _globalMatrixUpdate = false;
      _globalMatrixUpdated = true;
    }
  return (_globalMatrix);
}

Matrix4f const &Transform::calcGlobalMatrix(Transform const &parent) {
    if (_globalMatrixUpdate) {
      calcLocalMatrix();
      _globalMatrix = _localMatrix;
      _globalMatrix.combine(parent.getGlobalMatrix());
      _globalPosition = parent._globalRotation.rotate(_position) + parent._globalPosition;
      _globalRotation = _rotation * parent._globalRotation;
      _globalScale = _scale * parent._globalScale;
      _globalMatrixUpdate = false;
      _globalMatrixUpdated = true;
    }
  return (_globalMatrix);
}

Matrix4f const &Transform::getGlobalMatrix() const {
  return (_globalMatrix);
}

void      Transform::lookAt(Vector3f const &target){
    _globalMatrix.lookAt(_globalPosition, target, ge::Vector3f(0.0f, 1.0f, 0.0f));

    float w = sqrtf(1.0f + _globalMatrix(0,0) + _globalMatrix(1,1) + _globalMatrix(2,2)) / 2.0f;
    float w4 = (4.0f * w);
    /* verifier la forume ici (j'ai fait du bricolage) */
    /* il faut aussi checker la formule de la rotation des matrices */
    float x = (_globalMatrix(2,1) + _globalMatrix(1,2)) / w4;
    float y = (_globalMatrix(0,2) + _globalMatrix(2,0)) / w4;
    float z = (_globalMatrix(1,0) + _globalMatrix(0,1)) / w4;

    //ge::Quaternionf rotation(x, y, z, w);
    _globalRotation = ge::Quaternionf(x, y, z, w);
    //std::cout << "rotation : " << rotation.getRotate().z << std::endl;
    _globalPosition = ge::Vector3f(_globalMatrix(3,0), _globalMatrix(3,1), _globalMatrix(3,2));
    //std::cout << "position : " << _globalPosition.x << " " << _globalPosition.y << " " << _globalPosition.z << std::endl;
}


bool    Transform::getGlobalMatrixChanged() const {
  return _globalMatrixUpdated;
}

void    Transform::resetGlobalMatrixChanged(bool b) {
  _globalMatrixUpdated = b;
}



};


/*



float tr = _globalMatrix(0,0) + _globalMatrix(1,1) + _globalMatrix(2,2);

if (tr > 0) { 
  float S = sqrt(tr+1.0) * 2; // S=4*qw 
  w = 0.25 * S;
  x = (_globalMatrix(2,1) - _globalMatrix(1,2)) / S;
  y = (_globalMatrix(0,2) - _globalMatrix(2,0)) / S; 
  z = (_globalMatrix(1,0) - _globalMatrix(0,1)) / S; 
} else if ((_globalMatrix(0,0) > _globalMatrix(1,1)) && (_globalMatrix(0,0) > _globalMatrix(2,2))) { 
  float S = sqrt(1.0 + _globalMatrix(0,0) - _globalMatrix(1,1) - _globalMatrix(2,2)) * 2; // S=4*qx 
  w = (_globalMatrix(2,1) - _globalMatrix(1,2)) / S;
  x = 0.25 * S;
  y = (_globalMatrix(0,1) + _globalMatrix(1,0)) / S; 
  z = (_globalMatrix(0,2) + _globalMatrix(2,0)) / S; 
} else if (_globalMatrix(1,1) > _globalMatrix(2,2)) { 
  float S = sqrt(1.0 + _globalMatrix(1,1) - _globalMatrix(0,0) - _globalMatrix(2,2)) * 2; // S=4*qy
  w = (_globalMatrix(0,2) - _globalMatrix(2,0)) / S;
  x = (_globalMatrix(0,1) + _globalMatrix(1,0)) / S; 
  y = 0.25 * S;
  z = (_globalMatrix(1,2) + _globalMatrix(2,1)) / S; 
} else { 
  float S = sqrt(1.0 + _globalMatrix(2,2) - _globalMatrix(0,0) - _globalMatrix(1,1)) * 2; // S=4*qz
  w = (_globalMatrix(1,0) - _globalMatrix(0,1)) / S;
  x = (_globalMatrix(0,2) + _globalMatrix(2,0)) / S;
  y = (_globalMatrix(1,2) + _globalMatrix(2,1)) / S;
  z = 0.25 * S;
}

*/
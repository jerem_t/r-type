
#include <gEngine/Parsers/BnfParser.hh>
#include <gEngine/Parsers/ExecNode.hh>
#include <gEngine/Parsers/StringFeeder.hh>

#include <iostream>

BnfParser::BnfParser(AParserFeeder *feeder) :
	ConsumerParser(feeder)
{
}

BnfParser::~BnfParser()
{
}

void 		BnfParser::_addUtilsRules()
{
	std::shared_ptr<ExecNode> 	rule(new ExecNode(ExecNode::AND));

	rule->setBasicRule(&BnfParser::ruleIsEof, "");
	_rules["eof"] = rule;
}

bool 		BnfParser::_ignore()
{
	while (readChar(' ') ||
		   readChar('\n') ||
		   readChar('\t'));
	return (true);
}

bool 		BnfParser::_idChar()
{
	if (readRange('A', 'Z') ||
		readRange('a', 'z') ||
		readChar('_'))
		return (true);
	return (false);
}

bool 		BnfParser::_getIdentifier(std::string &id)
{
	beginRecord("identifier");
	if (_idChar())
	{
		while (_idChar() || readRange('0', '9'));
		id = getRecord("identifier");
		return (true);
	}
	return (false);
}

bool 		BnfParser::_identifier(std::shared_ptr<ExecNode> const &father)
{
	std::string 	id;

	if (_getIdentifier(id))
	{
		father->setCall(id);
		return (true);
	}
	return (false);
}

bool 		BnfParser::_rule()
{
	std::string 	ruleName;
	bool 			succeed = true;

	_addUtilsRules();
	while (succeed)
	{
		std::shared_ptr<ExecNode> 	callableRule(new ExecNode(ExecNode::AND));

		saveContext();
		_ignore();
		if (readChar('~'))
			callableRule->setAttachToFather(false);
		_ignore();
		if (_getIdentifier(ruleName))
		{
			_ignore();
			if (readString("::=") &&
				_ignore() &&
				_expression(callableRule) &&
				readString(";;"))
			{
				std::cout << "New Rule: " << ruleName << "$" << std::endl;
				_rules[ruleName] = callableRule;
				succeed = true;
				std::cout << "Rule parsing succeed!" << std::endl;
			}
			else
				succeed = false;
		}
		else
			succeed = false;
	}
	std::cout << "pardsing bnf end" << std::endl;
	return (isEof());
}

bool 		BnfParser::_qualifier(std::shared_ptr<ExecNode> const &father)
{
	if (readChar('*'))
	{
		father->setQualifier(ExecNode::STAR);
		return (true);
	}
	if (readChar('+'))
	{
		father->setQualifier(ExecNode::PLUS);
		return (true);
	}
	if (readChar('?'))
	{
		father->setQualifier(ExecNode::QUES);
		return (true);
	}
	father->setQualifier(ExecNode::NONE);
	return (true);
}

bool 		BnfParser::_getNode(std::shared_ptr<ExecNode> const &father)
{
	std::string 	id;

	saveContext();
	if (readChar(':') && _getIdentifier(id))
	{
		popContext();
		father->setName(id);
		std::cout << "_getNode -> " << id << std::endl;
	}
	else
		restoreContext();
	return (true);
}

bool 		BnfParser::_callback(std::shared_ptr<ExecNode> const &father)
{
	std::string 			callBackName;
	std::string 			tmp;
	std::list<std::string>	params;
	bool 					search = true;

	saveContext();
	if (readChar('#') && _getIdentifier(callBackName) &&
		readChar('('))
	{
		if (_ignore() && _getIdentifier(tmp) && _ignore())
		{
			params.push_back(tmp);
			while (search)
			{
				saveContext();
				if (readChar(',') && _ignore() &&
					_getIdentifier(tmp) && _ignore())
				{
					popContext();
					params.push_back(tmp);
				}
				else
				{
					search = false;
					restoreContext();					
				}
			}
			if (readChar(')'))
			{
				popContext();
				father->setCallBack(callBackName, params);
			}
			else
				restoreContext();
		}
		if (readChar(')'))
		{
			popContext();
			father->setCallBack(callBackName, params);
		}
		else
			restoreContext();
	}
	else
		restoreContext();
	return (true);
}

bool 		BnfParser::_andExpression(std::shared_ptr<ExecNode> const &father)
{
	bool 			run = true;
	bool 			ret = false;
	ExecNode::ERuleLinker 	link = ExecNode::OR;

	std::cout << "_andExpression" << std::endl;
	while (run)
	{
		std::shared_ptr<ExecNode> 	expr(new ExecNode(link));
		std::shared_ptr<ExecNode> 	fnc(new ExecNode(ExecNode::AND));

		saveContext();
		if (_ignore() && _basicRules(expr) &&
			_getNode(expr) && _ignore() &&
			_qualifier(expr) && _ignore() && _callback(fnc))
		{
			std::cout << "case1" << std::endl;
			ret = true;
			popContext();
		}
		else
		{
			restoreContext();
			saveContext();
			if (_ignore() && _identifier(expr) &&
				_getNode(expr) && _ignore() &&
				_qualifier(expr) && _ignore() && _callback(fnc))
			{
				std::cout << "case2" << std::endl;
				ret = true;
				popContext();
			}
			else
			{
				restoreContext();
				saveContext();
				if (_ignore() &&  readChar('[') &&
					_expression(expr) && readChar(']') &&
					_getNode(expr) && _ignore() &&
					_qualifier(expr) && _ignore() && _callback(fnc))
				{
					std::cout << "case3" << std::endl;
					ret = true;
					popContext();
				}
				else
				{
					std::cout << "fail" << std::endl;
					restoreContext();
					run = false;			
				}
			}
		}
		if (run)
		{
			father->addChild(expr);
			if (fnc->isCallBack())
				father->addChild(fnc);
			link = ExecNode::AND;
		}
	}
	std::cout << "_andExpression end" << std::endl;
	return (ret);
}

bool 		BnfParser::_orExpression(std::shared_ptr<ExecNode> const &father)
{
	bool 			run = true;

	while (run)
	{
		saveContext();
		if (readChar('|') && _andExpression(father))
		{
			popContext();
		}
		else
		{
			run = false;
			restoreContext();
		}
	}
	return (true);
}

bool 		BnfParser::_expression(std::shared_ptr<ExecNode> const &father)
{
	saveContext();
	if (_andExpression(father) &&
		_orExpression(father))
	{
		popContext();
		return (true);
	}
	else
		restoreContext();
	return (false);
}

bool 		BnfParser::_basicRules(std::shared_ptr<ExecNode> const &father)
{
	saveContext();
	if ((_range(father) ||
		 _char(father) ||
		 _string(father) ||
		 _readUntilChar(father) ||
		 _readUntilString(father)) &&
		_ignore() &&
		_qualifier(father))
	{
		popContext();		
		return (true);
	}
	restoreContext();
	return (false);
}

bool 		BnfParser::_getChar(char *c)
{
	std::string 	res;

	beginRecord("char");
	saveContext();
	if (readChar('\'') && readRange('\a', '~') && readChar('\''))
	{
		res = getRecord("char");
		*c = res[1];
		popContext();
		return (true);
	}
	restoreContext();
	return (false);
}

bool 		BnfParser::_char(std::shared_ptr<ExecNode> const &father)
{
	char 	c;

	if (_getChar(&c))
	{
		father->setBasicRule(&BnfParser::ruleReadChar, std::string(1, c));
		return (true);
	}
	return (false);
}

bool 		BnfParser::_range(std::shared_ptr<ExecNode> const &father)
{
	char 	a, b;

	saveContext();
	if (_getChar(&a) && readString("..") && _getChar(&b))
	{
		father->setBasicRule(&BnfParser::ruleReadRange, std::string(1, a) + std::string(1, b));
		popContext();
		return (true);
	}
	restoreContext();
	return (false);
}

bool 		BnfParser::_getString(std::string &str)
{
	beginRecord("string");
	saveContext();
	if (readChar('"') && readUntil('"'))
	{
		str = getRecord("string");
		str = str.substr(1, str.size() - 2);
		popContext();
		return (true);
	}
	return (false);
}

bool 		BnfParser::_string(std::shared_ptr<ExecNode> const &father)
{
	std::string 	str;

	if (_getString(str))
	{
		father->setBasicRule(&BnfParser::ruleReadString, str);
		return (true);
	}
	return (false);
}

bool 		BnfParser::_readUntilChar(std::shared_ptr<ExecNode> const &father)
{
	char 	c;

	saveContext();
	if (readString("->") && _ignore() && _getChar(&c))
	{
		father->setBasicRule(&BnfParser::ruleReadUntilChar, std::string(1, c));
		popContext();
		return (true);
	}
	restoreContext();
	return (false);
}

bool 		BnfParser::_readUntilString(std::shared_ptr<ExecNode> const &father)
{
	std::string 	str;

	saveContext();
	if (readString("->") && _ignore())
	{
		if (_getString(str))
		{
			father->setBasicRule(&BnfParser::ruleReadUntilString, str);
			popContext();
			return (true);
		}
	}
	restoreContext();
	return (false);
}

bool		BnfParser::ruleReadChar(std::string const &str)
{
//	std::cout << "readChar " << str[0] << std::endl;
	return (readChar(str[0]));
}

bool		BnfParser::ruleReadString(std::string const &str)
{
//	std::cout << "readString " << str << std::endl;
	return (readString(str));
}

bool		BnfParser::ruleReadUntilChar(std::string const &str)
{
//	std::cout << "readUntilChar " << str[0] << std::endl;
	return (readUntil(str[0]));
}

bool		BnfParser::ruleReadUntilString(std::string const &str)
{
//	std::cout << "readUntilString " << str << std::endl;
	return (readUntil(str));
}

bool		BnfParser::ruleReadRange(std::string const &str)
{
//	std::cout << "readRange " << str[0] << ", " << str[1] << std::endl;
	return (readRange(str[0], str[1]));
}

bool		BnfParser::ruleIsEof(std::string const &str)
{
//	std::cout << "isEof" << std::endl;
	(void)str;
	return (isEof());
}

bool 		BnfParser::buildExecTree()
{
	return (_rule());
}

std::shared_ptr<ParsingNode> 	BnfParser::callRule(std::string const &name)
{
	t_bnfRules::iterator 	it = _rules.find(name);

	if (it != _rules.end())
		return (_rules[name]->executeParsing(*this));
	else
		return (std::shared_ptr<ParsingNode>(new ParsingNode(false)));
}

bool 		BnfParser::setBnf(std::string const &bnf)
{
	AParserFeeder 	*bnfFeeder = new StringFeeder(bnf);
	AParserFeeder 	*othFeeder = getFeeder();
	bool			ret;

	clear();
	setFeeder(bnfFeeder);
	ret = buildExecTree();
	clear();
	setFeeder(othFeeder);
	delete bnfFeeder;
	return (ret);
}

void 		BnfParser::setEntryPoint(std::string const &entry)
{
	_entryPoint = entry;
}

std::shared_ptr<ParsingNode> 	BnfParser::parse()
{
	t_bnfRules::iterator 	it = _rules.find(_entryPoint);

	if (it == _rules.end())
		std::cerr << "Entry point doesnt exist." << std::endl;
	return (it->second->executeParsing(*this));
}

bool 		BnfParser::callCallBack(std::string const &name,
									std::list<std::string> const &paramNames)
{
	std::vector<std::shared_ptr<ParsingNode> > 	params;
	std::list<std::string>::const_iterator 		it = paramNames.begin();
	t_parsingResult::const_iterator 			res;

	while (it != paramNames.end())
	{
		if ((res = _parsingResults.find(*it)) == _parsingResults.end())
		{
			std::cerr << "param " << *it << " doesn't exist." << std::endl;
			return (false);
		}
		params.push_back(res->second);
		++it;
	}
	return (_callBacks[name](params));
}

void 		BnfParser::addCallBack(std::string const &name, t_callBack fnc)
{
	_callBacks[name] = fnc;
}

void 		BnfParser::registerResult(std::string const &name,
									std::shared_ptr<ParsingNode> const &result)
{
	_parsingResults[name] = result;
}


#include <stdexcept>
#include <gEngine/Parsers/Style.hpp>
#include <gEngine/Parsers/Tokenize.hpp>
#include <gEngine/Parsers/EraseComments.hpp>
#include <vector>

namespace ge {

Style::Style() {}

Style::~Style() {}

bool Style::loadFromMemory(std::string const &str) {
  std::string tmp = str;
  tmp.erase(std::remove_if(tmp.begin(), tmp.end(), isspace),
            tmp.end());
  ge::eraseComments("/*", "*/", tmp);
  while (true) {
    size_t beginPos = tmp.find_first_of('{');
    size_t endPos = tmp.find_first_of('}');
    if (beginPos == std::string::npos)
      return true;
    if (endPos < beginPos)
      return false;
    std::string query = tmp.substr(0, beginPos);
    std::vector<std::string> selectors;

    ge::tokenize(query, ",", selectors);
    for (auto &selector : selectors) {
      std::string props = tmp.substr(beginPos + 1, endPos - beginPos - 1);
      if (props.back() != ';')
        throw std::runtime_error("Missing ';' while parsing Style file.");
      size_t colonPos = selector.find_first_of(':');

      std::string state;
      if (colonPos == std::string::npos)
        state = "default";
      else {
        state = selector.substr(colonPos + 1);
        selector = selector.substr(0, colonPos);
      }
      _data[selector][state] += props;
    }

    tmp = tmp.substr(endPos + 1);
  }
  return true;
}

}

#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_WINDOWS

#include <iostream>
#include <gEngine/System/Library.hpp>
#include <windows.h>

namespace ge 
{
	typedef void	*(__cdecl *MYPROC)(void *);

	void Library::close() 
	{
		FreeLibrary(reinterpret_cast<HMODULE>(_handler));
	}

	bool Library::loadFromFile(std::string const &path) 
	{
		_path = path;
		_handler = LoadLibrary((path + ".dll").c_str());
		return (!_handler ? false : true);
	}

	void *Library::_getSymbol(std::string const &name) const 
	{
		MYPROC	address = (MYPROC)GetProcAddress(reinterpret_cast<HMODULE>(_handler), name.c_str());
		return (address);
	}
}

#endif
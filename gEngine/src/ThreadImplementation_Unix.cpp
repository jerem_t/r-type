//
// UnixThread.cpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sun Nov  3 15:09:01 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:20:37 2013 jordy domingos-mansoni
//

#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_UNIX

#include <gEngine/System/Thread.hpp>
#include <gEngine/System/ThreadImplementation.hpp>

#include <iostream>

ThreadImplementation::ThreadImplementation(Thread *owner) : status_(STARTED), id_(getpid()) {
  pthread_create(&thread_, NULL, &AThreadImplementation::launch, owner);
}

bool ThreadImplementation::joinable() const {
  return status_ == STARTED;
}

void ThreadImplementation::join() {
  pthread_join(thread_, NULL);
}

void ThreadImplementation::detach() {
  status_ = FINISHED;
  pthread_cancel(thread_);
}

#endif		/* GE_SYSTEM_UNIX */


#include <stdexcept>
#include <gEngine/Utils/Array.hpp>

namespace ge {

Array::Array() {}

Array::~Array() {}

Array::Array(Array const &other)
: _data(other._data) {}

Array &Array::operator=(Array const &other) {
  if (this != &other)
    _data = other._data;
  return *this;
}

Any const &Array::operator[](size_t i) const {
  if (i >= getSize())
    throw std::out_of_range("No data set in Array to this index.");
  return _data[i];
}

Any &Array::operator[](size_t i) {
  if (i >= getSize()) {
    _data.push_back(Any());
    return (*this)[i];
  }
  return _data[i];
}

size_t Array::getSize() const {
  return _data.size();
}

}
#include "gEngine/Network/NWPacket.hpp"
#include "gEngine/Core/Component.hpp"
#include "gEngine/Math/Transform.hpp"

NWPacket::NWPacket() : _cursor(0)
{}

NWPacket::~NWPacket()
{}

void		NWPacket::append(const void *data, std::size_t size)
{
	if (!data || size <= 0)
	{
		throw std::runtime_error("NWPacket::append failed");
	}
	_data.resize(_data.size() + size);
	std::memcpy(&_data[_data.size() - size], data, size);
}

void		NWPacket::clear()
{
	_data.clear();
	_cursor = 0;
}

const std::vector<char>	&NWPacket::getData() const
{
	return _data;
}

void	NWPacket::setData(const std::vector<char> &data)
{
	_cursor = 0;
	_data = data;
}

std::size_t	NWPacket::getDataSize() const
{
	return _data.size();
}

/////////////////////////////////
// Data serialization
/////////////////////////////////
NWPacket	&NWPacket::operator<<(ge::Vector2f const &data)
{
	for (std::size_t i = 0; i < 2; ++i)
		append(&data.data[i], sizeof(data.data[i]));

	return *this;
}

NWPacket	&NWPacket::operator<<(ge::Vector3f const &data)
{
	for (std::size_t i = 0; i < 3; ++i)
		append(&data.data[i], sizeof(data.data[i]));

	return *this;
}

NWPacket	&NWPacket::operator<<(ge::Vector4f const &data)
{
	for (std::size_t i = 0; i < 4; ++i)
		append(&data.data[i], sizeof(data.data[i]));

	return *this;
}

NWPacket	&NWPacket::operator<<(ge::Boxf const &data)
{
	for (std::size_t i = 0; i < 6; ++i)
		append(&data.data[i], sizeof(data.data[i]));

	return *this;
}

NWPacket	&NWPacket::operator<<(ge::Quaternionf const &data)
{
	float	scalar = data.getScalar();
	*this << data.getRotate();
	*this << scalar;
	return *this;
}

NWPacket	&NWPacket::operator<<(std::size_t data)
{
	append(&data, sizeof(data));
	return *this;
}

NWPacket	&NWPacket::operator<<(char data)
{
	append(&data, sizeof(data));
	return *this;
}

NWPacket	&NWPacket::operator<<(int data)
{
	append(&data, sizeof(data));
	return *this;
}

NWPacket	&NWPacket::operator<<(float data)
{
	append(&data, sizeof(data));
	return *this;
}

NWPacket	&NWPacket::operator<<(std::string const &data)
{
	std::size_t	size = data.length();
	append(&size, sizeof(size));
	append(&data[0], size);
	return *this;
}

NWPacket	&NWPacket::operator<<(Testicule data)
{
	append(&data, sizeof(Testicule));
	return *this;
}

NWPacket	&NWPacket::operator<<(ge::Transform const &data)
{
	*this << data.getPosition();
	*this << data.getRotation();
	*this << data.getScale();
	return *this;
}

NWPacket	&NWPacket::operator<<(ge::GameObject &data)
{
	ge::Transform	tr = data.component("Transformable")["transform"];
	float			life = data.component("Life")["quantity"];
	std::size_t		id = data.getId();
	std::string		name = data.getName();

	*this << tr;
	*this << id;
	*this << name;
	*this << life;
	return *this;
}

////////////////////////////////
// Data unserialization
////////////////////////////////

NWPacket	&NWPacket::operator>>(ge::Vector2f &data)
{
	for (std::size_t i = 0; i < 2; ++i)
	{
		std::memcpy(&data.data[i], &_data[_cursor], sizeof(data.data[i]));
		_cursor += sizeof(data.data[i]);
	}
	return *this;
}

NWPacket	&NWPacket::operator>>(ge::Vector3f &data)
{
	for (std::size_t i = 0; i < 3; ++i)
	{
		std::memcpy(&data.data[i], &_data[_cursor], sizeof(data.data[i]));
		_cursor += sizeof(data.data[i]);
	}
	return *this;
}

NWPacket	&NWPacket::operator>>(ge::Vector4f &data)
{
	for (std::size_t i = 0; i < 4; ++i)
	{
		std::memcpy(&data.data[i], &_data[_cursor], sizeof(data.data[i]));
		_cursor += sizeof(data.data[i]);
	}
	return *this;
}

NWPacket	&NWPacket::operator>>(ge::Boxf &data)
{
	for (std::size_t i = 0; i < 6; ++i)
	{
		std::memcpy(&data.data[i], &_data[_cursor], sizeof(data.data[i]));
		_cursor += sizeof(data.data[i]);
	}
	return *this;
}

NWPacket	&NWPacket::operator>>(ge::Quaternionf &data)
{
	ge::Vector3f	v;
	float			s;

	*this >> v;
	*this >> s;
	data.setRotate(v);
	data.setScalar(s);
	return *this;
}

NWPacket	&NWPacket:: operator>>(std::size_t &data)
{
	std::memcpy(&data, &_data[_cursor], sizeof(data));
	_cursor += sizeof(data);
	return *this;
}

NWPacket	&NWPacket::operator>>(char &data)
{
	std::memcpy(&data, &_data[_cursor], sizeof(data));
	_cursor += sizeof(data);
	return *this;
}

NWPacket	&NWPacket::operator>>(int &data)
{
	std::memcpy(&data, &_data[_cursor], sizeof(data));
	_cursor += sizeof(data);
	return *this;
}

NWPacket	&NWPacket::operator>>(float &data)
{
	std::memcpy(&data, &_data[_cursor], sizeof(data));
	_cursor += sizeof(data);
	return *this;
}

NWPacket	&NWPacket::operator>>(std::string &data)
{
	std::size_t	size = 0;
	*this >> size;
	data.append(&_data[_cursor], size);
	_cursor += size;
	return *this;
}

NWPacket	&NWPacket::operator>>(Testicule &data)
{
	std::memcpy(&data, &_data[_cursor], sizeof(Testicule));
	_cursor += sizeof(Testicule);
	return *this;
}

NWPacket	&NWPacket::operator>>(ge::Transform &data)
{
	ge::Vector3f	position, scale;
	ge::Quaternionf	rotation;

	*this >> position;
	*this >> rotation;
	*this >> scale;

	data.setPosition(position);
	data.setRotation(rotation);
	data.setScale(scale);
	return *this;
}

NWPacket	&NWPacket::operator>>(ge::GameObject &data)
{
	ge::Transform	tr;
	std::size_t		id;
	std::string		name;
	float			life;

	*this >> tr;
	*this >> id;
	*this >> name;
	*this >> life;
	data.component("Transformable")["transform"] = tr;
	data.component("Life")["quantity"] = life;
	data.setName(name);
	data.setId(id);
	return *this;
}

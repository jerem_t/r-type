
#include <gEngine/Parsers/StringFeeder.hh>

StringFeeder::StringFeeder(std::string const &src) :
	AParserFeeder(src.size())
{
	_src = src;
}

StringFeeder::~StringFeeder()
{
}

std::string 	StringFeeder::getChunk()
{
	std::string 	ret = _src;

	_src = "";
	return (ret);
}

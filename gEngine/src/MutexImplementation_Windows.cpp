//
// MutexImplementation_Windows.cpp for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Mon Nov  4 14:41:38 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:21:04 2013 jordy domingos-mansoni
//

#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_WINDOWS

#include <gEngine/System/MutexImplementation.hpp>

MutexImplementation::MutexImplementation() : status_(UNLOCK) {
  InitializeCriticalSection(&mutex_);
}

void MutexImplementation::lock() {
  status_ = LOCK;
  EnterCriticalSection(&mutex_);
}

void MutexImplementation::trylock() {
	TryEnterCriticalSection(&mutex_);
}

void MutexImplementation::unlock() {
  status_ = UNLOCK;
  LeaveCriticalSection(&mutex_);
}

MutexImplementation::~MutexImplementation() {
  DeleteCriticalSection(&mutex_);
}

#endif	/* GE_SYSTEM_WINDOWS */

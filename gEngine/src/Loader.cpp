
#include <regex>
#include <gEngine/Resources/Resource.hpp>
#include <gEngine/Resources/Loader.hpp>
#include <gEngine/Parsers/Xml.hpp>

namespace ge {

Loader::Loader() {}

Loader::~Loader() {}

bool Loader::loadFromFile(std::string const &path, bool async) {
  if (async) {
    // use thread.
  }
  std::unordered_map<std::string, std::string> resources;
  if (!_parseXML(path, resources))
    return false;
  float progress = 0;
  size_t total = resources.size();
  float step = 1.0 / total;
  for (auto const &resource : resources) {
    for (auto const &loader : _loaders) {
      std::regex r(".*(" + loader.first + ")");
      if (std::regex_match(resource.second, r)) {
        if (!loader.second(resource)) {
          std::string msg = "Cannot load resource " + resource.first + ".";
          trigger("error", msg, resource.first, resource.second);
          return false;
        }
        progress += step;
        trigger("progress", progress, resource.first, resource.second);
        break;
      }
    }
  }
  trigger("finish", total);
  return true;
}

bool Loader::_parseXML(std::string const &path,
                       std::unordered_map<std::string, std::string> &resources) {
  Xml parser;
  XmlArray data;

  if (!parser.loadFromFile(path))
    return false;
  parser.getChildrenByName("resource", data);
  for (auto const &row : data)
    resources[row->getAttr("name")] = row->getAttr("path");
  return true;
}

}

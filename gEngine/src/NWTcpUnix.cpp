#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_LINUX

#include "gEngine/Network/NWTcp.hpp"

NWTcp::NWTcp() : m_socket(~0), m_blocking(true), _ip("")
{}

NWTcp::~NWTcp()
{
	disconnect();
	close();
}

/*
** Public methods
*/

INWAbstract::Status	NWTcp::connect(const std::string &ip, const std::string &port)
{
	m_ip = ip;
	m_port = port;

	// Create a tcp socket
	create();

	// Resolve the remote adress
	struct addrinfo	hints, *remote = NULL;

	std::memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	if (getaddrinfo(m_ip.c_str(), m_port.c_str(), &hints, &remote) != NO_ERROR)
	{
		close();
		return INWAbstract::SC_ERROR;
	}

	// Connection to the remote server
	if (::connect(m_socket, remote->ai_addr, (int)remote->ai_addrlen) == SOCKET_ERROR)
	{
		disconnect();
		close();
		freeaddrinfo(remote);
		return INWAbstract::SC_ERROR;
	}

	return INWAbstract::SC_DONE;
}

void				NWTcp::disconnect()
{
	::close(m_socket);
	m_socket = ~0;
}

INWAbstract::Status	NWTcp::send(const std::vector<char> &data)
{
	// Check whether the data isn't null and the size isn't equals 0
	if (data.empty())
	{
		return INWAbstract::SC_ERROR;
	}

	// Send the data to the remote server
	if (::send(m_socket, data.data(), data.size(), MSG_NOSIGNAL) == SOCKET_ERROR)
	{
		disconnect();
		close();
		return INWAbstract::SC_ERROR;
	}

	return INWAbstract::SC_DONE;
}

INWAbstract::Status	NWTcp::send(const NWPacket &data)
{
	return (send(data.getData()));
}

INWAbstract::Status	NWTcp::receive(std::vector<char> &buffer, std::size_t size, std::size_t &received)
{
	char		*tmp = new char[size]();
	int		ret;

	received = 0;

	// Clean buffers
	std::memset(tmp, 0, size);
	buffer.empty();

	// Receive the data
	if ((ret = ::recv(m_socket, tmp, size, MSG_NOSIGNAL)) > 0)
	{
	  received = ret;
	  buffer.resize(received);
	  std::memcpy(buffer.data(), tmp, received);
	}
	if (ret == SOCKET_ERROR)
	{
	  disconnect();
	  close();
	  return INWAbstract::SC_ERROR;
	}
	else if (received > 0)
	  return INWAbstract::SC_DONE;
	return INWAbstract::SC_DISCONNECTED;
}

INWAbstract::Status	NWTcp::receive(NWPacket &packet, std::size_t size, std::size_t &received)
{
	std::vector<char>	buffer;
	INWAbstract::Status	status;

	if ((status = receive(buffer, size, received)) == INWAbstract::SC_DONE)
		packet.setData(buffer);
	return status;
}

const std::string	&NWTcp::getRemoteIp() const
{
	return m_ip;
}

const std::string	&NWTcp::getRemotePort() const
{
	return m_port;
}

const std::string	&NWTcp::getIp() const
{
  return _ip;
}

void			NWTcp::setIp(const std::string &ip)
{
  _ip = ip;
}

void				NWTcp::setBlocking(bool blocking)
{
	u_long mode = blocking ? 0 : 1;
	if (ioctl(m_socket, FIONBIO, &mode) == 0)
	{
		disconnect();
		close();
		throw std::runtime_error("blocking mode failed");
	}
}

bool				NWTcp::isBlocking() const
{
	return m_blocking;
}

INWAbstract::Type	NWTcp::getType() const
{
	return INWAbstract::SC_TCP;
}

void				NWTcp::setSocket(NWSOCKET socket)
{
	// Close everything if there is a valid socket
	disconnect();
	close();

	// Initialization
	create();
	m_socket = socket;
}

NWSOCKET			NWTcp::getSocket() const
{
	return m_socket;
}

/*
** Private methods
*/

void	NWTcp::create()
{
	if (m_socket == INVALID_SOCKET)
	{
		// Checking whether socket is invalid for creating it
		if ((m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
		{
			close();
			throw std::runtime_error("socket failed");
		}
	}
}

void	NWTcp::close()
{
	// Nothing to clean up
}

bool	NWTcp::operator==(const NWTcp &other)
{
	return m_socket == other.m_socket;
}

#endif // GE_UNIX

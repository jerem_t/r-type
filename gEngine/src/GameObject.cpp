
#include <iostream>
#include	<gEngine/Math/Box.hpp>

#include <algorithm>
#include <gEngine/Core/ComponentManager.hpp>
#include <gEngine/Core/Component.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Parsers/Eval.hpp>
#include <gEngine/Utils/Any.hpp>

namespace ge {

GameObject::GameObject(std::string const &name)
: Node(name)
, _isEnabled(true)
, _currentTime(0)
, _spawnTime(0)
, _isDead(false)
, _hasStarted(false) {}

GameObject::~GameObject() {}

GameObject::GameObject(GameObject const &other)
: Node(other)
, _isEnabled(other._isEnabled)
, _components(other._components) {}

GameObject &GameObject::operator=(GameObject const &other) {
  if (this != &other) {
    _isEnabled = other._isEnabled;
    _components = other._components;
    _parent = other._parent;
    _id = other._id;
    _name = other._name;
    _tags = other._tags;
    _children = other._children;
    _data = other._data;
  }
  return *this;
}

void GameObject::start() {
  if (_currentTime > _spawnTime)
    return ;
  for (auto &component : _components)
    component.second->start();
  for (auto &gameObject : _children)
    gameObject->start();
  _hasStarted = true;
}

void GameObject::update(float dt) {
  if (_currentTime < _spawnTime) {
    _currentTime += dt;
    return ;
  } else if (!_hasStarted)
    start();
  if (!_isEnabled)
    return ;
  for (auto &component : _components) {
    if (component.second && component.second->isEnabled()) {
      component.second->update(dt);    
    }
  }

  for (auto &gameObject : _children) {
    if (!gameObject->hasComponent("Life") || !gameObject->_isDead)
      gameObject->update(dt);
  }

  for (auto gameObject : _children) {
    if (gameObject->_isDead && gameObject->_isEnabled) {
      gameObject->disable();
    }
  }

}

bool GameObject::loadFromFile(std::string const &path) {
  Xml root;
  if (!root.loadFromFile(path))
    return false;
  XmlPtr component = root.getChildByName("gameObject");
  if (component)
    return loadFromXml(component);
  return false;
}

bool GameObject::loadFromXml(XmlPtr const &node) {

  // defines

  XmlArray defines;
  node->getChildrenByName("define", defines, false);
  for (auto &define : defines)
    setDefine(define->getAttr("name"),
              ge::eval(define->getAttr("value")));

  // attributes

  if (node->has("name"))
    setName(node->getAttr("name"));

  if (node->has("spawnTime") && getSpawnTime() == 0)
    setSpawnTime(convert<std::string, float>(
      node->getAttr("spawnTime")));

  // tags

  if (node->has("tags")) {
    Any tags = ge::eval(node->getAttr("tags"));
    if (tags.hasType<std::string>())
      addTag(tags);
    else if (tags.hasType<ge::Array>()) {
      ge::Array arr = tags;
      for (auto &it : arr)
        addTag(it);
    }
  }

  // scope

  XmlPtr scope = node->getChildByName("scope");
  if (scope && scope->has("value"))
    setScope(scope->getAttr("value"));

  // params

  XmlArray params;
  node->getChildrenByName("param", params, false);
  for (auto &param : params)
    _data[param->getAttr("name")] = ge::eval(param->getAttr("value"));

  // components

  XmlArray components;
  node->getChildrenByName("component", components, false);
  for (auto &c : components) {
    if (!ge::GameObject::getScope().empty() && c->has("scope") &&
        c->getAttr("scope") != ge::GameObject::getScope())
      continue ;
    component(c->getAttr("name")).loadFromXml(c);
  }

  // children

  XmlArray gameObjects;
  node->getChildrenByName("gameObject", gameObjects, false);
  for (auto gameObject : gameObjects) {
    GameObjectPtr child(new GameObject);
    addChild(child);
    if (gameObject->has("path") &&
      !child->loadFromFile(gameObject->getAttr("path")))
        return false;
    if (!child->loadFromXml(gameObject))
      return false;
    if (node->has("spawnTime"))
      child->setSpawnTime(convert<std::string, float>(
        node->getAttr("spawnTime")));
  }

  return true;
}

Component &GameObject::component(std::string const &name) {
  auto it = std::find_if(_components.begin(), _components.end(),
                         [&name] (ComponentData &data) {
    return data.first == name;
  });
  if (it == _components.end()) {
    ComponentData data;
    data.first = name;
    data.second = ComponentManager::getInstance().create(name, *this);
    _components.push_back(data);
    return *data.second;
  }
  return *it->second;
}
  
void GameObject::die() {
  if (_isDead)
    return ;
  _isDead = true;
  trigger("die");
  for (auto &gameObject : _children)
    gameObject->trigger("die");
}

bool GameObject::isDead() const {
  return _isDead;
}

bool GameObject::hasComponent(std::string const &name) const {
  return std::find_if(_components.begin(), _components.end(), [&name] (ComponentData const &data) {
    return data.first == name;
  }) != _components.end();
}

bool GameObject::isEnabled() const {
  return _isEnabled;
}

void GameObject::enable() {
  _isEnabled = true;
}

void GameObject::disable() {
  _isEnabled = false;
}

void GameObject::setSpawnTime(float seconds) {
  _spawnTime = seconds;
}

float GameObject::getSpawnTime() const {
  return _spawnTime;
}

std::map<std::string, Any> GameObject::_defines;

void GameObject::setDefine(std::string const &name,
                                  Any const &any) {
  _defines[name] = any;
}

Any &GameObject::getDefine(std::string const &name) {
  return _defines[name];
}

bool GameObject::isDefined(std::string const &name) {
  return _defines.find(name) != _defines.end();
}

std::string GameObject::_scope;

void GameObject::setScope(std::string const &name) {
  _scope = name;
}

std::string const &GameObject::getScope() {
  return _scope;
}

}

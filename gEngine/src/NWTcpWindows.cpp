#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_WINDOWS

#include "gEngine/Network/NWTcp.hpp"

NWTcp::NWTcp() : m_socket(INVALID_SOCKET), m_blocking(true), _ip("")
{}

NWTcp::~NWTcp()
{
	disconnect();
	close();
}

/*
** Public methods
*/

INWAbstract::Status	NWTcp::connect(const std::string &ip, const std::string &port)
{
	m_ip = ip;
	m_port = port;

	// Create a tcp socket
	create();

	// Resolve the remote adress
	ADDRINFO	hints, *remote = NULL;

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.  ai_protocol = IPPROTO_TCP;

	if (getaddrinfo(m_ip.c_str(), m_port.c_str(), &hints, &remote) != NO_ERROR)
	{
		close();
		return INWAbstract::SC_ERROR;
	}

	// Connection to the remote server
	if (WSAConnect(m_socket, remote->ai_addr, (int)remote->ai_addrlen, 0, 0, 0, NULL) == SOCKET_ERROR)
	{
		disconnect();
		close();
		freeaddrinfo(remote);
		return INWAbstract::SC_ERROR;
	}

	return INWAbstract::SC_DONE;
}

void				NWTcp::disconnect()
{
	closesocket(m_socket);
	m_socket = INVALID_SOCKET;
}

INWAbstract::Status	NWTcp::send(const std::vector<char> &data)
{
	WSABUF	buffer;
	DWORD	bytesSent;

	// Check whether the data isn't null and the size isn't equals 0
	if (data.empty())
	{
		return INWAbstract::SC_ERROR;
	}
	
	// Initialize the buffer
	buffer.buf = const_cast<CHAR *>(data.data());
	buffer.len = data.size();

	// Send the data to the remote server
	if (WSASend(m_socket, &buffer, 1, &bytesSent, 0, 0, 0) == SOCKET_ERROR)
	{
		disconnect();
		close();
		return INWAbstract::SC_ERROR;
	}
	return INWAbstract::SC_DONE;
}

INWAbstract::Status	NWTcp::send(const NWPacket &data)
{
	return (send(data.getData()));
}

INWAbstract::Status	NWTcp::receive(std::vector<char> &buffer, std::size_t size, std::size_t &received)
{
	WSABUF	tmp;
	DWORD	bytesReceived, flag = 0;
	int		ret;

	received = 0;

	// Intialize the buffer
	tmp.buf = new char[size]();
	tmp.len = size;

	// Clean the buffer
	ZeroMemory(tmp.buf, tmp.len);

	// Receive the data
	if ((ret = WSARecv(m_socket, &tmp, 1, &bytesReceived, &flag, 0, 0)) == NO_ERROR && bytesReceived > 0)
	{
		received = bytesReceived;
		buffer.resize(received);
		std::memcpy(buffer.data(), tmp.buf, received);
	}
	if (ret == SOCKET_ERROR)
	{
		disconnect();
		close();
		return INWAbstract::SC_ERROR;
	}
	else if (received > 0)
		return INWAbstract::SC_DONE;
	return INWAbstract::SC_DISCONNECTED;
}

INWAbstract::Status	NWTcp::receive(NWPacket &packet, std::size_t size, std::size_t &received)
{
	std::vector<char>	buffer;
	INWAbstract::Status	status;

	if ((status = receive(buffer, size, received)) == INWAbstract::SC_DONE)
		packet.setData(buffer);
	return status;
}

const std::string	&NWTcp::getIp() const
{
  return _ip;
}

const std::string	&NWTcp::getRemoteIp() const
{
	return m_ip;
}

const std::string	&NWTcp::getRemotePort() const
{
	return m_port;
}

void			NWTcp::setIp(const std::string &ip)
{
  _ip = ip;
}

void				NWTcp::setBlocking(bool blocking)
{
	u_long mode = blocking ? 0 : 1;
	if (ioctlsocket(m_socket, FIONBIO, &mode) == 0)
	{
		disconnect();
		close();
		throw std::exception("blocking mode failed");
	}
}

bool				NWTcp::isBlocking() const
{
	return m_blocking;
}

INWAbstract::Type	NWTcp::getType() const
{
	return INWAbstract::SC_TCP;
}

void				NWTcp::setSocket(NWSOCKET socket)
{
	// Close everything if there is a valid socket
	disconnect();
	close();

	// Initialization
	create();
	m_socket = socket;
}

NWSOCKET			NWTcp::getSocket() const
{
	return m_socket;
}

/*
** Private methods
*/

void	NWTcp::create()
{
	WSADATA	wsaData;
	
	// Initializing WinSock 2
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != NO_ERROR)
	{
		throw std::exception("WSAStartup failed");
	}

	if (m_socket == INVALID_SOCKET)
	{
		// Checking whether socket is invalid for creating it
		if ((m_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, 0)) == INVALID_SOCKET)
		{
			close();
			throw std::exception("WSASocket failed");
		}
	}
}

void	NWTcp::close()
{
	if (m_socket != INVALID_SOCKET)
	{
		WSACleanup();
	}
}

bool	NWTcp::operator==(const NWTcp &other)
{
	return m_socket == other.m_socket;
}


#endif


#include <gEngine/Parsers/Json.hpp>

namespace ge {

Json::Json()
: _value("")
, _type(Undefined) {}

Json::Json(Type t, std::string const &v)
  : _value(v)
  , _type(t) {}

Json::Json(Json const &other)
: _value(other._value)
, _type(other._type)
, _props(other._props)
, _index(other._index)
, _arr(other._arr) {}

Json::Json(std::string const &value)
: _value(value)
, _type(String) {}

Json::Json(char const *value)
: _value(value)
, _type(String) {}

Json::Json(double value)
: _value(_convert<double, std::string>(value))
, _type(Number) {}

Json::Json(bool value)
: _value(value ? "true" : "false")
, _type(Bool) {}

Json &Json::operator=(Json const &other) {
  if (this != &other) {
    _value = other._value;
    _type = other._type;
    _props = other._props;
    _index = other._index;
    _arr = other._arr;
  }
  return *this;
}

Json &Json::operator=(unsigned int value) {
  _value = _convert<unsigned int, std::string>(value);
  _type = Number;
  return *this;
}

Json &Json::operator=(double value) {
  _value = _convert<double, std::string>(value);
  _type = Number;
  return *this;
}

Json &Json::operator=(int value) {
  _value = _convert<int, std::string>(value);
  _type = Number;
  return *this;
}

Json &Json::operator=(bool value) {
  _value = value ? "true" : "false";
  _type = Bool;
  return *this;
}

Json &Json::operator=(char const *value) {
  _value = value;
  _type = String;
  return *this;
}

void Json::clear() {
  _type = Undefined;
  _value.clear();
  _props.clear();
  _index.clear();
  _arr.clear();
  _tokens.clear();
}

std::string Json::toString() const {
  return _toString(1);
}

Json::Type Json::getType() {
  return _type;
}

void Json::push(std::string key, Json const &v) {
  _index[key] = _props.size();
  _props.push_back(make_pair(key, v));
}

void Json::push(Json const &v) {
  _arr.push_back(v);
}

Json::operator double() const {
  return _convert<std::string, double>(_value);
}

bool Json::is() const {
  return _value == "true";
}

Json::operator std::string const &() const {
  return _value;
}

size_t Json::getSize() const {
  switch (_type) {
    case Array:  return _arr.size();
    case Object: return _props.size();
    case String: return _value.size();
    case Number: return 1;
    default: return 0;
  }
  return 0;
}

Json &Json::operator[](size_t i) {
  if (_type == Undefined)
    _type = Array;
  if (_type != Array) {
    JSON_THROW(std::range_error, "Json: cannot dereference this type of value.")
  }
  if (i >= _arr.size())
  {
    size_t n = _arr.size();
    while (n++ <= i) push(Json());
  }
  return _arr.at(i);
}

Json &Json::operator[](std::string const &s) {
  if (_type == Undefined)
    _type = Object;
  if (_type != Object) {
    JSON_THROW(std::range_error, "Json: cannot dereference this type of value.")
  }
  if (_index.find(s) == _index.end())
    push(s, Json());
  return _props[_index[s]].second;
}

bool Json::operator==(Type type) {
  return _type == type;
}

bool Json::loadFromMemory(std::string const &str) {
  if (str.size()) {
    int k;
    _tokenize(str);
    *this = _parse(_tokens, 0, k);
  }
  else
    *this = Json();
  return true;
}

void Json::operator>>(std::string &input) {
  input = toString();
}

std::string Json::_addSpaces(int d) const {
  std::string s = "";
  while (d--)
    s += "  ";
  return s;
}

std::string Json::_toString(int d) const {
  std::string s;

  switch (_type)
  {
    case String:
      return "\"" + _value + "\"";

    case Number:
    case Bool:
      return _value;

    case Null:
      return "null";

    case Object:
      s = "{\n";
      for (size_t i = 0; i < _props.size(); ++i)
      {
        s += _addSpaces(d) + "\"" + _props[i].first + "\": ";
        s += _props[i].second._toString(d + 1);
        s += i == _props.size() - 1 ? "" : ",";
        s += "\n";
      }
      s += _addSpaces(d-1) + "}";
      return s;

    case Array:
      s = "[";
      for (size_t i = 0; i < _arr.size(); ++i)
      {
      if (i) s += ", ";
      s += _arr[i]._toString(d + 1);
      }
      s += "]";
      return s;

    default: break;
  }
  return "undefined";
}

bool Json::_isSpace(char c) const {
  return std::string(" \n\r ").find(c) != std::string::npos;
}

int Json::_nextSpace(std::string const &data, int i) const {
  int size = data.size();

  while (i <  size) {
    if (data[i] == '"') {
      ++i;
      while (i < size && (data[i] != '"' || data[i-1] == '\\'))
        ++i;
    }
    if (data[i] == '\'') {
      ++i;
      while (i < size && (data[i] != '\'' || data[i-1] == '\\'))
        ++i;
    }
    if (_isSpace(data[i]))
      return i;
    ++i;
  }
  return size;
}

int Json::_skipSpaces(std::string const &data, int i) const {
  int size = data.size();
  while (i < size) {
    if (!_isSpace(data[i]))
      return i;
    ++i;
  }
  return -1;
}

void Json::_check_string(size_t &k, std::string const &token) {
  size_t i = k + 1;
  while (i < token.size() && (token[i] != '"' || token[i - 1] == '\\'))
    ++i;
  _tokens.push_back(Token(token.substr(k+1, i-k-1), T_STRING));
  k = i + 1;
}

void Json::_check_comma(size_t &k, std::string const &) {
  _tokens.push_back(Token(",", T_COMMA));
  ++k;
}

void Json::_check_true(size_t &k, std::string const &token) {
  std::string tok;
  tok = k+3 < token.size() ? token.substr(k, 4) : "";
  if (tok != "true") {
    JSON_THROW(std::runtime_error, "Json: unknown token \"" + tok + "\".")
  }
  _tokens.push_back(Token("true", T_BOOLEAN));
  k += 4;
}

void Json::_check_false(size_t &k, std::string const &token) {
  std::string tok;
  tok = k+4 < token.size() ? token.substr(k, 5) : "";
  if (tok != "false") {
    JSON_THROW(std::runtime_error, "Json: unknown token \"" + tok + "\".")
  }
  _tokens.push_back(Token("false", T_BOOLEAN));
  k += 5;
}

void Json::_check_null(size_t &k, std::string const &token) {
  std::string tok;
  tok = k+3 < token.size() ? token.substr(k, 4) : "";
  if (tok != "null") {
    JSON_THROW(std::runtime_error, "Json: unknown token \"" + tok + "\".")
  }
  _tokens.push_back(Token("null", T_NULL));
  k += 4;
}

void Json::_check_undefined(size_t &k, std::string const &token) {
  std::string tok;
  tok = k+8 < token.size() ? token.substr(k, 9) : "";
  if (tok != "undefined") {
    JSON_THROW(std::runtime_error, "Json: unknown token \"" + tok + "\".")
  }
  _tokens.push_back(Token("undefined", T_UNDEFINED));
  k += 9;
}

void Json::_check_right_brace(size_t &k, std::string const &) {
  _tokens.push_back(Token("}", T_RIGHT_BRACE));
  k++;
}

void Json::_check_left_brace(size_t &k, std::string const &) {
  _tokens.push_back(Token("{", T_LEFT_BRACE));
  k++;
}

void Json::_check_right_bracket(size_t &k, std::string const &) {
  _tokens.push_back(Token("]", T_RIGHT_BRACKET));
  ++k;
}

void Json::_check_left_bracket(size_t &k, std::string const &) {
  _tokens.push_back(Token("[", T_LEFT_BRACKET));
  ++k;
}

void Json::_check_colon(size_t &k, std::string const &) {
  _tokens.push_back(Token(":", T_COLON));
  ++k;
}

bool Json::_check_digit(size_t &k, std::string const &token) {
  if (token[k] == '-' || (token[k] <= '9' && token[k] >= '0'))
  {
    size_t i = k;
    if (token[i] == '-')
      ++i;
    while (i < token.size() &&
            ((token[i] <= '9' && token[i] >= '0') ||
              token[i] == '.' ||
              token[i] == 'e' ||
              token[i] == '+' ||
              token[i] == '-'))
    ++i;
    _tokens.push_back(Token(token.substr(k, i-k), T_NUMBER));
    k = i;
    return true;
  }
  return false;
}

void Json::_tokenize(std::string const &data) {
  std::string token;
  int next = 0;
  size_t size, k;
  int index;

  _tokens.clear();
  while ((index = _skipSpaces(data, next)) >= 0) {
    next = _nextSpace(data, index);
    token = data.substr(index, next-index);
    size = token.size();
    k = 0;
    while (k < size) {
      switch (token[k]) {
        case '"': _check_string(k, token);        break;
        case ',': _check_comma(k, token);         break;
        case 't': _check_true(k, token);          break;
        case 'f': _check_false(k, token);         break;
        case 'n': _check_null(k, token);          break;
        case 'u': _check_undefined(k, token);     break;
        case '}': _check_right_brace(k, token);   break;
        case '{': _check_left_brace(k, token);    break;
        case ']': _check_right_bracket(k, token); break;
        case '[': _check_left_bracket(k, token);  break;
        case ':': _check_colon(k, token);         break;
        default:
          if (_check_digit(k, token) == false) {
            JSON_THROW(std::runtime_error, "Json: unknown token \"" + token.substr(k) + "\".")
          }
          break;
      }
    }
  }
}

Json Json::_parse(std::vector<Token> v, int i, int& r) const {
  Json current;
  int j;
  int k;

  switch (v[i].type)
  {
    case T_LEFT_BRACE:
      current._type = Object;
      k = i + 1;
      while (v[k].type != T_RIGHT_BRACE)
      {
        std::string key = v[k].value;
        k += 2;
        j = k;
        Json vv = _parse(v, k, j);
        current.push(key, vv);
        k = j;
        if (v[k].type == T_COMMA) k++;
      }
      r = k + 1;
      return current;

    case T_LEFT_BRACKET:
      current._type = Array;
      k = i + 1;
      while (v[k].type != T_RIGHT_BRACKET)
      {
        j = k;
        Json vv = _parse(v, k, j);
        current.push(vv);
        k = j;
        if (v[k].type == T_COMMA) k++;
      }
      r = k + 1;
      return current;

    case T_NUMBER:
      current._type = Number;
      current._value = v[i].value;
      r = i + 1;
      return current;

    case T_STRING:
      current._type = String;
      current._value = v[i].value;
      r = i + 1;
      return current;

    case T_BOOLEAN:
      current._type = Bool;
      current._value = v[i].value;
      r = i + 1;
      return current;

    case T_NULL:
      current._type = Null;
      current._value = "null";
      r = i + 1;
      return current;

    case T_UNDEFINED:
      current._type = Undefined;
      current._value = "undefined";
      r = i + 1;
      return current;

    default:
      JSON_THROW(std::runtime_error, "invalid json data.")
      break;
  }
  return current;
}

}

#include "gEngine/Network/NWTcpListener.hpp"
#include "gEngine/Network/NWTcp.hpp"

#ifdef GE_SYSTEM_UNIX

NWTcpListener::NWTcpListener() : m_socket(INVALID_SOCKET), m_blocking(true)
{}

NWTcpListener::~NWTcpListener()
{
	disconnect();
	close();
}

/*
** Public methods
*/

NWSOCKET			NWTcpListener::getSocket() const
{
	return m_socket;
}

INWAbstract::Type	NWTcpListener::getType() const
{
	return INWAbstract::SC_TCP;
}

void				NWTcpListener::setBlocking(bool blocking)
{
	u_long mode = blocking ? 0 : 1;
	if (ioctl(m_socket, FIONBIO, &mode) == 0)
	{
		close();
		throw std::runtime_error("blocking mode failed");
	}
}

bool				NWTcpListener::isBlocking() const
{
	return m_blocking;
}

INWAbstract::Status	NWTcpListener::listen(USHORT port)
{
	SOCKADDR_IN	service;

	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr("127.0.0.1");
	service.sin_port = htons(port);

	// Initialize the socket
	create();

	// Bind the socket with service
	if (bind(m_socket, reinterpret_cast<SOCKADDR *>(&service), sizeof(service)) == SOCKET_ERROR)
	{
		disconnect();
		close();
		return INWAbstract::SC_ERROR;
	}

	// Listening on the socket server
	if (::listen(m_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		disconnect();
		close();
		return INWAbstract::SC_ERROR;
	}
	return INWAbstract::SC_DONE;
}

INWAbstract::Status	NWTcpListener::accept(NWTcp &socket)
{
	// Check whether the listener socket isn't invalid
	if (m_socket == INVALID_SOCKET)
	{
		return INWAbstract::SC_ERROR;
	}

	// Accept a new socket
	NWSOCKET	client;
	SOCKADDR_IN	address;
	int			length = sizeof(address);

	if ((client = ::accept(m_socket, reinterpret_cast<SOCKADDR *>(&address), reinterpret_cast<socklen_t *>(&length))) == INVALID_SOCKET)
	{
		return INWAbstract::SC_ERROR;
	}

	// Set the client socket with the socket passed as parameter
	socket.setSocket(client);
	socket.setIp(inet_ntoa(address.sin_addr));

	return INWAbstract::SC_DONE;
}

void				NWTcpListener::disconnect()
{
  ::close(m_socket);
  m_socket = INVALID_SOCKET;
}

void				NWTcpListener::close()
{
  // Nothing to clean up
}

void				NWTcpListener::setSocket(NWSOCKET socket)
{
	// Close everything if there is a valid socket
	disconnect();
	close();

	// Initialization
	create();
	m_socket = socket;
}

/*
** Private methods
*/

void	NWTcpListener::create()
{
	if (m_socket == INVALID_SOCKET)
	{
		// Checking whether socket is invalid for creating it
		if ((m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == INVALID_SOCKET)
		{
			close();
			throw std::runtime_error("socket failed");
		}
	}
}

#endif

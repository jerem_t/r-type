
#include <stdexcept>
#include <gEngine/Parsers/Xml.hpp>

namespace ge {

Xml::Xml() {}

Xml::Xml(Xml const &other)
: Node(other)
, _text(other._text)
{}

Xml &Xml::operator=(Xml const &other) {
  if (this != &other) {
    _children = other._children;
    _data = other._data;
    _name = other._name;
    _text = other._text;
  }
  return *this;
}

Xml::~Xml() {}

std::string const &Xml::getText() const {
  return _text;
}

std::string const &Xml::getAttr(std::string const &name) const {
  if (_data.find(name) == _data.end())
    throw std::out_of_range("Cannot get attribute " + name + " from XML.");
  return _data.at(name);
}

bool Xml::loadFromFile(std::string const &path) {
  std::ifstream in(path);
  if (!in.is_open())
    return false;
  return loadFromStream(in);
}

bool Xml::loadFromStream(std::istream &in) {
  char token = in.get();
  _startParsing();
  while (in.good()) {
    _parseToken(token);
    token = in.get();
  }
  _endParsing();
  return true;
}

bool Xml::loadFromMemory(std::string const &data) {
  _startParsing();
  for (char token : data)
    _parseToken(token);
  _endParsing();
  return true;
}

std::string Xml::toString(unsigned char indent,
                          unsigned char currentIndent) const {
  std::string str;
  std::string tabs;

  for (unsigned char i = 0; i < currentIndent; ++i)
    tabs += " ";
  if (_children.empty()) {
    str += tabs + _openingTagToString();
    str += _text;
    str += "</" + _name + ">\n";
  } else {
    str += tabs + _openingTagToString() + "\n";
    for (auto const &child : _children)
      str += child->toString(indent, currentIndent + indent);
    str += tabs + "</" + _name + ">\n";
  }
  return str;
}

std::string Xml::_openingTagToString() const {
  return "<" + _name + _attribsToString() + ">";
}

std::string Xml::_attribsToString() const {
  std::string str;
  for (auto attr : _data)
    str += " " + attr.first + "=\""
               + attr.second.get<std::string>() + "\"";
  return str;
}

void Xml::_startParsing() {
  // _stack.erase(); TODO
  _state = Out;
  _name = "root";
  _str = "";
  _currentAttribute = "";
  _stack.push(this);
}

void Xml::_endParsing() {
  _stack.top()->_text = _cutName(_stack.top()->_text);
  _stack.pop();
  if (!_stack.empty())
    throw std::runtime_error("Cannot find unclosed tag from XML.");
}

void Xml::_parseToken(unsigned char token) {
  switch (_state) {
    case Out:
      if (token == '<')
        _state = TagOpening;
      else if (!_isWhiteSpace(token))
        _stack.top()->_text += token;
      else if (_stack.top()->_text.size() > 0 &&
               _lastIsSpace(_stack.top()->_text))
        _stack.top()->_text += ' ';
    break;
    case TagOpening:
      if (!_isWhiteSpace(token)) {
        if (token == '/')
          _state = TagClosing;
        else {
          _str += token;
          _state = NameOpening;
        }
      }
      break;
    case NameOpening:
      if (!_isWhiteSpace(token) && token != '>')
        _str += token;
      else if (_str == "!--" || _str == "?xml") {
        _str = "";
        _state = Comment;
      } else {
        Xml *x = new Xml();
        x->_name = _str;
        _str = "";
        _stack.top()->_children.push_back(std::shared_ptr<Xml>(x));
        _stack.push(x);
        if (token != '>')
          _state = TagIn; 
        else
          _state = Out;
      }
      break;
    case TagIn:
      if (!_isWhiteSpace(token)) {
        if (token == '/')
          _state = TagEnded;
        else if (token == '>')
          _state = Out;
        else {
          _str += token;
          _state = AttribName;
        }
      }
      break;
    case AttribName:
      if (!_isWhiteSpace(token)) {
        if (token == '=') {
          _currentAttribute = _str;
          _str = "";
          _state = WaitForValue;
        } else
          _str += token;
      } else {
        _currentAttribute = _str;
        _str = "";
        _state = WaitForEq;
      }
      break;
    case WaitForEq:
      if (!_isWhiteSpace(token)) {
        if (token == '=')
          _state = WaitForValue;
        else
          throw std::runtime_error("Missing '=' from XML attribute " + _currentAttribute + ".");
      }
      break;
    case WaitForValue:
      if (!_isWhiteSpace(token)) {
        if (token == '"')
          _state = AttribValue;
        else
          throw std::runtime_error("Missing attribute value from XML.");
      }
      break;
    case AttribValue:
      if (token == '"') {
        _stack.top()->_data[_currentAttribute] = _str;
        _str = "";
        _state = TagIn;
      } else
        _str += token;
      break;
    case TagClosing:
      if (!_isWhiteSpace(token)) {
        _str += token;
        _state = NameClosing;
      }
      break;
    case NameClosing:
      if (!_isWhiteSpace(token) && token != '>')
        _str += token;
      else if (token == '>') {
        _stack.top()->_text = _cutName(_stack.top()->_text);
        if (_stack.top()->getName() != _str)
          throw std::runtime_error("Closing tag name doesnt match with opening tag from XML.");
        else {
          _stack.pop();
          _state = Out;
          _str = "";
        }
      } else if (_stack.top()->getName() != _str) {
        throw std::runtime_error("Closing tag name doesnt match with opening tag from XML.");
      } else {
        _str = "";
        _state = TagEnded;
      }
      break;
    case TagEnded:
      if (!_isWhiteSpace(token)) {
        if (token == '>') {
          _stack.top()->_text = _cutName(_stack.top()->_text);
          _stack.pop();
          _state = Out;
        } else
          throw std::runtime_error("Xml: Parsing error, tag not closed.");
      }
      break;
    case Comment:
      if (!_isWhiteSpace(token))
        _str += token;
      else {
        if (_str == "-->" || _str == "?>")
          _state = Out;
        _str = "";
      }
      break;
    default:
      throw std::runtime_error("Unknown state while parsing XML.");
  }
}

bool Xml::_isWhiteSpace(char cur) {
  if (cur >= 33 && cur <= 126) {
    return false;
  }
  return true;
}

std::string Xml::_cutName(std::string const &name) {
  // if (_lastIsSpace(name))
  //   return name.substr(0, name.size() - 1);
  return name;
}

bool Xml::_lastIsSpace(std::string const &name) {
  return name[name.size() - 1] == ' ';
}

}
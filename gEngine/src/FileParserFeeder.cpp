
#include <gEngine/Parsers/FileParserFeeder.hh>

#include <stdexcept>

FileParserFeeder::FileParserFeeder(std::string const &file) :
	AParserFeeder(4096),
	_file(file.c_str()),
	_alreadyRead(0),
	_fileSize(0)
{
	if (_file)
	{
		_file.seekg(0, _file.end);
    	_fileSize = _file.tellg();
    	_file.seekg(0, _file.beg);
	}
}

FileParserFeeder::~FileParserFeeder()
{
	if (_file.is_open())
		_file.close();
}

std::string 	FileParserFeeder::getChunk()
{
	size_t 		toRead;
	std::string toStr;
	char 		*ret;

	if (!_file)
		throw std::runtime_error("Cannot Parse: file was not opened correctly.");
	if (_fileSize - _alreadyRead < _chunkSize)
		toRead = _fileSize - _alreadyRead;
	else
		toRead = _chunkSize;
	ret = new char[toRead + 1];
	_file.read(ret, toRead);
	if (_file)
		ret[toRead] = 0;
	else
		throw std::runtime_error("Cannot Parse: file was not opened correctly.");
	_alreadyRead += toRead;
	toStr = std::string(ret);
	delete[] ret;
	return (toStr);
}

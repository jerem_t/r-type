#pragma once

# include <utility>
# include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

	/**
	 * @class Singleton
	 * Generic singleton pattern with variadic template
	 * initialization.
	 */
template<typename Type>
class GE_DLL Singleton
{
public:
	template<typename ...Args>
	inline static Type &getInstance(Args &&...args);
	inline static Type &getInstance();
	inline static void destroyInstance();
private:
	static Type *_instance;
	// static std::mutex _mutex; TODO ge::Mutex
};

#include <gEngine/Utils/Singleton.inl>

}

#pragma once

#include <unordered_map>
#include <string>
#include <gEngine/Utils/Pool.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class TagBase
 * Simple class to optimize tag comparaision
 * by transform string to int.
 */
template<typename IncreasePolicy>
class GE_DLL TagBase {
public:

  /**
   * Default tag constructor, do nothing.
   */
  TagBase();

  /**
   * Empty destructor, do nothing.
   */
  ~TagBase();

  /**
   * Generate a tag from the given key.
   * @param The string key to represente this tag.
   */
  TagBase(std::string const &key);

  /**
   * Copy constructor.
   */
  TagBase(TagBase const &other);

  /**
   * Assignement operator.
   */
  TagBase &operator=(TagBase const &other);

  /**
   * Set the given key to this tag.
   * @param The string key to represente this tag.
   */
  void operator=(std::string const &key);

  /**
   * Call this method to release the given tag, which
   * gonna add a free slot.
   */
  void release() const;

  /**
   * Use this tag with the integer representation for
   * fast comparations.
   */
  operator size_t() const;

  /**
   * Compare this tag with the given one.
   */
  bool operator==(TagBase const &other);

private:

  static Pool<size_t, IncreasePolicy> _pool;
  static std::unordered_map<std::string, size_t> _keys;
  size_t _value;

};

/**
 * @typedef Tag
 * The default tag.
 */
typedef TagBase<DefaultIncreasePolicy<size_t>> Tag;

/**
 * @typedef Tag
 * The tag used with bitfields.
 */
typedef TagBase<BitShiftIncreasePolicy<size_t>> BitTag;

#include <gEngine/Utils/Tag.inl>

}
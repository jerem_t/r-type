
#pragma once

template<typename IncreasePolicy>
Pool<size_t, IncreasePolicy> TagBase<IncreasePolicy>::_pool;

template<typename IncreasePolicy>
std::unordered_map<std::string, size_t> TagBase<IncreasePolicy>::_keys;

template<typename IncreasePolicy>
TagBase<IncreasePolicy>::TagBase()
: _value(0) {}

template<typename IncreasePolicy>
TagBase<IncreasePolicy>::~TagBase() {}

template<typename IncreasePolicy>
TagBase<IncreasePolicy>::TagBase(std::string const &key) {
  *this = key;
}

template<typename IncreasePolicy>
void TagBase<IncreasePolicy>::release() const {
  _pool.release(_value);
}

template<typename IncreasePolicy>
TagBase<IncreasePolicy>::TagBase(TagBase const &other)
: _value(other._value) {}

template<typename IncreasePolicy>
TagBase<IncreasePolicy> &TagBase<IncreasePolicy>::operator=(TagBase const &other) {
  if (this != &other)
    _value = other._value;
  return *this;
}

template<typename IncreasePolicy>
void TagBase<IncreasePolicy>::operator=(std::string const &key) {
  if (_keys.find(key) == _keys.end())
    _keys[key] = _pool.get();
  _value = _keys[key];  
}

template<typename IncreasePolicy>
TagBase<IncreasePolicy>::operator size_t() const {
  return _value;
}

template<typename IncreasePolicy>
bool TagBase<IncreasePolicy>::operator==(TagBase<IncreasePolicy> const &other) {
  return _value == other._value;
}
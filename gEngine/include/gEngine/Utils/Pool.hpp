
#pragma once

#include <stack>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class DefaultIncreasePolicy
 * Increase 1 by 1 the value of `Type` (operator++).
 */
template<typename Type>
class GE_DLL DefaultIncreasePolicy {
public:
  DefaultIncreasePolicy();
  ~DefaultIncreasePolicy();
  Type &operator()(Type &value) const;
};

/**
 * Increase the value using power of two (operator<<)
 */
template<typename Type>
class GE_DLL BitShiftIncreasePolicy {
public:
  BitShiftIncreasePolicy();
  ~BitShiftIncreasePolicy();
  Type &operator()(Type &value);
private:
  size_t _currentShift;
};

/**
 * @class Pool
 * Generate and manager some unique data.
 */
template<typename Type,
         typename IncreasePolicy = DefaultIncreasePolicy<Type>>
class GE_DLL Pool {
public:

  /**
   * Empty constructor, do nothing.
   */
  Pool();

  /**
   * Empty destructor, do nothing.
   */
  ~Pool();

  /**
   * Get a free id from the pool.
   */
  Type const &get();

  /**
   * Release the given id.
   */
  void release(Type const &id);

private:
  std::stack<Type> _released;
  IncreasePolicy _increase;
  static Type _currentOffset;
};

#include <gEngine/Utils/Pool.inl>

}
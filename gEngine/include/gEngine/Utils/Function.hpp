
#pragma once

#include <functional>
#include <stdexcept>
#include <typeinfo>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @namespace details
 * Contains all private stuffs.
 */
namespace details {

template <typename Lambda>
struct FunctionHelper
  : public FunctionHelper<decltype(&Lambda::operator())>
{};

template <typename ClassType, typename ReturnType, typename ...Args>
struct FunctionHelper<ReturnType(ClassType::*)(Args...) const> {
  typedef std::function<ReturnType(Args...)> function;
};

template <typename Lambda>
typename FunctionHelper<Lambda>::function makeFunction(Lambda& lambda) {
  return static_cast<typename FunctionHelper<Lambda>::function>(lambda);
}

}

/**
 * @class Function
 * Simple class to store function from lambda.
 */
class Function {
public:

  /**
   * Empty constructor, do nothing.
   */
  Function()
  : _function(NULL)
  , _signature(NULL) {}

  /**
   * Create a function from the given lambda.
   */
  template<typename Lambda>
  Function(Lambda &&lambda) {
    auto function = new decltype(details::makeFunction(lambda))
                                (details::makeFunction(lambda));
    _function  = static_cast<void*>(function);
    _signature = &typeid(function);
  }

  /**
   * Execute the function with the given arguments, return
   * false if it hasn't been initialized.
   */
  template<typename ...Args>
  bool operator()(Args ...args) {
    if (_function == NULL || _signature == NULL)
      return false;
    auto function = static_cast<std::function<void(Args...)>*>(
      _function);
    if (typeid(function) != *(_signature))
      throw std::bad_cast();
    (*function)(args...);
    return true;
  }

  ~Function() {
  }

private:

  void *_function;
  std::type_info const *_signature;

};

}
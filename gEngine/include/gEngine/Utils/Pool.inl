
#pragma once

template<typename Type>
DefaultIncreasePolicy<Type>::DefaultIncreasePolicy() {}

template<typename Type>
DefaultIncreasePolicy<Type>::~DefaultIncreasePolicy() {}

template<typename Type>
Type &DefaultIncreasePolicy<Type>::operator()(Type &value) const {
  ++value;
  return value;
}

template<typename Type>
BitShiftIncreasePolicy<Type>::BitShiftIncreasePolicy()
: _currentShift(0) {}

template<typename Type>
BitShiftIncreasePolicy<Type>::~BitShiftIncreasePolicy() {}

template<typename Type>
Type &BitShiftIncreasePolicy<Type>::operator()(Type &value) {
  value = 1 << _currentShift;
  // if (_currentShift == sizeof(Type))
    // throw
  ++_currentShift;
  return value;
}

template<typename Type, typename IncreasePolicy>
Type Pool<Type, IncreasePolicy>::_currentOffset = 0;

template<typename Type, typename IncreasePolicy>
Pool<Type, IncreasePolicy>::Pool() {}

template<typename Type, typename IncreasePolicy>
Pool<Type, IncreasePolicy>::~Pool() {}

template<typename Type, typename IncreasePolicy>
Type const &Pool<Type, IncreasePolicy>::get() {
  if (_released.empty())
    return _increase(_currentOffset);
  Type const &id = _released.top();
  _released.pop();
  return id;
}

template<typename Type, typename IncreasePolicy>
void Pool<Type, IncreasePolicy>::release(Type const &id) {
  _released.push(id);
}

#pragma once

#include <map>
#include <gEngine/Utils/Any.hpp>

namespace ge {

typedef std::map<std::string, Any> Dictionary;

}

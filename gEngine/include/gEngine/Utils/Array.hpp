
#pragma once

#include <iostream>
#include <vector>
#include <gEngine/Utils/Any.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Array
 * A kind of js-like array data structure.
 */
class GE_DLL Array {
public:

  typedef std::vector<Any>::iterator iterator;
  typedef std::vector<Any>::const_iterator const_iterator;

  inline iterator begin() {
    return _data.begin();
  }

  inline const_iterator begin() const {
    return _data.begin();
  }

  inline iterator end() {
    return _data.end();
  }

  inline const_iterator end() const {
    return _data.end();
  }

  /**
   * Default empty constructor, do nothing.
   */
  Array();

  /**
   * Create an Array with a list of data.
   */
  template<class ...Args>
  Array(Args const &...args);

  /**
   * Copy constructor.
   */
  Array(Array const &other);

  /**
   * Assignement operator.
   */
  Array &operator=(Array const &other);

  /**
   * Empty destructor, do nothing.
   */
  ~Array();

  /**
   * Return the data at the given index.
   */
  Any const &operator[](size_t i) const;

  /**
   * Return the data at the given index.
   */
  Any &operator[](size_t i);

  /**
   * Return the number of data for this Array.
   */
  size_t getSize() const;

private:

  template<class First, class ...Args>
  void _push(First const &first, Args const &...args);
  inline void _push();

private:
  std::vector<Any> _data;

};

template<class ...Args>
Array::Array(Args const &...args) {
  _push(args...);
}

template<class First, class ...Args>
void Array::_push(First const &first, Args const &...args) {
  _data.push_back(first);
  _push(args...);
}

void Array::_push() {}

}
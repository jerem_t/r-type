
#pragma once

#include <sstream>

#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * Simple function to convert value from a type to another.
 */
template<typename From, typename To>
inline To GE_DLL convert(From const &from) {
  std::stringstream converter;
  To to;
  converter << from;
  converter >> to;
  return to;
}

}

#pragma once

#include <string>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Path
 * Some path parsing utilities.
 */
class GE_DLL Path {
public:
  Path(std::string const &path);
  ~Path();
  static std::string extension(std::string const &path);
  static std::string fileName(std::string const &path);
  static std::string base(std::string const &path);
  std::string const &getExtension() const;
  std::string const &getFileName() const;
  std::string const &getBase() const;
private:
  std::string _extension;
  std::string _fileName;
  std::string _base;
};

}
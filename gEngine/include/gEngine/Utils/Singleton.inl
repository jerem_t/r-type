#pragma once

/**
 * Instanciate instance to nullptr.
 */
template<typename Type>
Type *Singleton<Type>::_instance = nullptr;

// template<typename Type>
// std::mutex Singleton<Type>::_mutex;
// template<typename Type>
// ge::Mutex Singleton<Type>::_mutex;

/**
 * Return the instance or create it from default constructor.
 */
template<typename Type>
inline Type &Singleton<Type>::getInstance() {
  if (!_instance) {
    // std::lock_guard<std::mutex> lock(_mutex);
    // ge::LockGuard<ge::Mutex> lock(_mutex);
    _instance = new Type();
  }
  return *_instance;
}

/**
 * Destroy the instance.
 */
template<typename Type>
inline void Singleton<Type>::destroyInstance() {
  // std::lock_guard<std::mutex> lock(_mutex);
  // ge::LockGuard<ge::Mutex> lock(_mutex);
  delete _instance;
  _instance = nullptr;
}


#pragma once

EventHandler::EventHandler() {}

template <typename ListenerType>
void EventHandler::on(std::string const &key,
                           ListenerType &&listener) {
  _listeners[ge::Tag(key)].push_back(std::forward<ListenerType>(listener));
}

template<typename ...Args>
void EventHandler::trigger(Tag const &key,
                              Args const &...args) const {
  if (_listeners.find(key) == _listeners.end())
    return ;
  for (auto const &listener : _listeners.at(key))
    listener(args...);
}

void EventHandler::off(std::string const &key) {
  ge::Tag tag(key);
  if (_listeners.find(tag) != _listeners.end())
    _listeners[tag].clear();
}

template<typename ...Args>
void EventHandler::trigger(std::string const &key,
                              Args const &...args) const {
  trigger(ge::Tag(key), args...);
}
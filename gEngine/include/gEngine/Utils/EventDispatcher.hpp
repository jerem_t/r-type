
#pragma once

#include <functional>
#include <utility>
#include <unordered_map>
#include <list>
#include <gEngine/Utils/Any.hpp>
#include <gEngine/Utils/Tag.hpp>
#include <gEngine/Utils/Array.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class EventDispatcher
 * Simple publish/subscribe event dispatcher pattern.
 */
class GE_DLL EventDispatcher {
  public:

    /**
     * @typedef Listener
     * Typedef on listener type.
     */
    typedef std::function<void(Array &)> Listener;

    /**
     * Empty constructor, do nothing.
     */
    inline EventDispatcher();

    /**
     * Empty destructor, do nothing.
     */
    virtual ~EventDispatcher() {}

    /**
     * Add a new event listener.
     * @param key the event key to register listener.
     * @param listener the listener to call on dispatch.
     */
    template <typename ListenerType>
    void on(std::string const &key, ListenerType &&listener);

    /**
     * Remove all listeners about the given event.
     */
    inline void off(std::string const &key);

    /**
     * Call all listeners for the given event key.
     * @param key The event key.
     * @param event The event data.
     */
    template<typename ...Args>
    inline void trigger(Tag const &key, Args const &...args) const;

    /**
     * Call all listeners for the given string corresponding
     * to a specific tag.
     * @param key The string event key.
     * @param event The event data.
     */
    template<typename ...Args>
    inline void trigger(std::string const &key, Args const &...args) const;

  private:

    /**
     * All listener for each event keys.
     */
    std::unordered_map<size_t, std::list<Listener>> _listeners;

};

#include <gEngine/Utils/EventDispatcher.inl>

}


#pragma once

#include <tuple>
#include <cstddef>
#include <memory>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @namespace details
 * Contains all private stuffs.
 */
namespace details {

/**
 * @class Callable
 * Simple interface to create a opaque Callable type.
 */
class GE_DLL Callable {
public:
  virtual ~Callable() {}
  virtual void call() = 0;
};

/**
 * @struct CallableHelper
 * Simple helper to call the Callable.
 */
template<size_t I>
struct CallableHelper {
  template<class F, class T, class... Arg>
  void operator()(F f, const T& tuple, Arg&... arg) const {
    CallableHelper<I-1>()(f, tuple, std::get<I-1>(tuple), arg...);
  }
};

/**
 * @struct CallableHelper<0>
 * End-specialization of CallableHelper.
 */
template<>
struct CallableHelper<0> {
  template<class F, class T, class... Arg>
  void operator()(F f, T const &, Arg const &... arg) const {
    f(arg...);
  }
};

/**
 * @class CallableBase
 * Templated Callable caller.
 */
template<typename Lambda, typename... Args>
struct CallableBase : Callable {
public:

  CallableBase(Lambda &function, Args const &... args)
  : _args(args...)
  , _function(function)
  {}

  virtual void call() {
    CallableHelper<sizeof...(Args)>()(_function, _args);
  }

  ~CallableBase() {};

private:
  std::tuple<Args...> _args;
  Lambda _function;
};

}

/**
 * @typedef CallablePtr
 * Used to store a pointer on Callable type.
 */
typedef std::shared_ptr<details::Callable> CallablePtr;

/**
 * Used to genrate a CallablePtr without expliciting template
 * parameters.
 */
template<typename Lambda, typename ...Args>
CallablePtr makeCallablePtr(Lambda &&lambda, Args const &...args) {
  return CallablePtr(new details::CallableBase<Lambda, Args...>(
    lambda, args...));
}

}

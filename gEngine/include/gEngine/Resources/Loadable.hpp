
#pragma once

#include <fstream>
#include <string>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Loadable
 * Inherit from this class and implemente loadFromMemory to make
 * it loadable.
 */
class GE_DLL Loadable {
public:
  Loadable();
  virtual ~Loadable();

  bool loadFromFile(std::string const &path);
  virtual bool loadFromStream(std::istream &stream);
  virtual bool loadFromMemory(std::string const &str) = 0;
  std::istream &operator<<(std::istream &is);

};

}
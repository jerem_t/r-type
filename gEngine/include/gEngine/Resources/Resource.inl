
#pragma once

template<class DataType>
std::map<std::string, std::shared_ptr<DataType>> Resource<DataType>::_map;

template<class DataType>
Resource<DataType>::Resource(std::string const &id)
: _id(id)
, _data(new DataType)
, _isReady(false) {
  if (_map.find(_id) != _map.end()) {
    _isReady = true;
    _data = _map[_id];
  }
}

template<class DataType>
Resource<DataType>::Resource(Resource const &other)
: _id(other._id)
, _data(other._data)
, _isReady(other._isReady) {}

template<class DataType>
Resource<DataType> &Resource<DataType>::operator=(Resource const &other) {
  if (this != &other) {
    _id = other._id;
    _data = other._data;
    _isReady = other._isReady;
  }
  return *this;
}

template<class DataType>
Resource<DataType>::~Resource() {}

template<class DataType>
bool Resource<DataType>::isReady() const {
  return _isReady;
}


template<class DataType>
DataType &Resource<DataType>::operator*() {
  if (!_data)
    throw std::runtime_error("Resource " + _id + " hasn't been loaded.");
  return *_data;
}

template<class DataType>
DataType *Resource<DataType>::operator->() {
  if (!_data)
    throw std::runtime_error("Resource " + _id + " hasn't been loaded.");
  return _data.get();
}

template<typename DataType>
template<typename Function>
bool Resource<DataType>::load(Function function) {
  if (_map.find(_id) == _map.end()) {
    _map[_id] = _data;
    if (function(*_data)) {
      _isReady = true;
      return true;
    }
    return false;
  }
  _data = _map[_id];
  _isReady = true;
  return true;
}

template<class DataType>
bool Resource<DataType>::fromFile(std::string const &path) {
  return load([&path] (DataType &data) -> bool {
    return data.loadFromFile(path);
  });
}

template<class DataType>
bool Resource<DataType>::fromMemory(std::string const &buffer) {
  return load([&buffer] (DataType &data) -> bool {
    return data.loadFromMemory(buffer);
  });
}

template<class DataType>
bool Resource<DataType>::fromStream(std::istream &in) {
  return load([&in] (DataType &data) {
    return data.loadFromStream(in);
  });
}

#pragma once

#include <memory>
#include <string>
#include <map>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Resource
 * Handle resource management.
 */
template<class DataType>
class GE_DLL Resource {
public:

  /**
   * Instanciate a resource.
   * @param id The id of the resource to instanciate.
   */
  explicit Resource(std::string const &id);

  /**
   * Copy constructor.
   */
  Resource(Resource const &other);

  /**
   * Assign operator.
   */
  Resource &operator=(Resource const &other);

  /**
   * Destructor.
   */
  ~Resource();

  /**
   * Load the resource from a custom callback.
   */
  template<typename Function>
  bool load(Function function);

  /**
   * Load resource from file.
   * @param path The path to the file to load.
   */
  bool fromFile(std::string const &path);

  /**
   * Load resource from a string in memory.
   * @param data The string with the data to load.
   */
  bool fromMemory(std::string const &data);

  /**
   * Load resource from the given stream.
   * @param in The input stream which contains the data
   *           to load.
   */
  bool fromStream(std::istream &in);

  /**
   * Return true if the resource is ready to use.
   */
  bool isReady() const;

  /**
   * Return the containing data.
   */
  DataType &operator*();

  /**
   * Use containing data methods.
   */
  DataType *operator->();


private:
  static std::map<std::string, std::shared_ptr<DataType>> _map;
  std::string _id;
  std::shared_ptr<DataType> _data;
  bool _isReady;
};

#include <gEngine/Resources/Resource.inl>

}
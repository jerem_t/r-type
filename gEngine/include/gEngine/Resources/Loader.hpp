
#pragma once

#include <gEngine/Utils/EventDispatcher.hpp>
#include <gEngine/Resources/Resource.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

class GE_DLL Loader : public EventDispatcher {
public:

  /**
   * Default constructor, do nothing.
   */
  Loader();

  /**
   * Empty destructor, do nothing.
   */
  ~Loader();

  /**
   * Load all resources from xml config file.
   * @param path The pat to the xml file.
   * @param async True to load into a thread.
   */
  bool loadFromFile(std::string const &path, bool async = true);

  template<typename Type>
  void addType(std::string const &extensions);

private:
  bool _parseXML(std::string const &path,
                 std::unordered_map<std::string, std::string> &resources);
private:
  typedef std::pair<std::string, std::string> Info;
  typedef bool (*func)(Info const &);
  std::unordered_map<std::string, func> _loaders;

};

template<typename Type>
void Loader::addType(std::string const &extensions) {
  _loaders[extensions] = [] (Info const &info) {
    Resource<Type> r(info.first);
    return r.fromFile(info.second);
  };
}

}
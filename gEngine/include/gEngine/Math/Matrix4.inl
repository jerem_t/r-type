
#pragma once

template<typename Type>
Matrix4<Type>::Matrix4() {
  identity();
}

template<typename Type>
Matrix4<Type>::Matrix4( Type v1, Type v2, Type v3, Type v4,
              Type v5, Type v6, Type v7, Type v8,
              Type v9, Type v10, Type v11, Type v12,
              Type v13, Type v14, Type v15, Type v16) {
    _matrix[0] = v1;  _matrix[1] = v2;  _matrix[2]  = v3; _matrix[3] = v4;
    _matrix[4] = v5;  _matrix[5] = v6;  _matrix[6]  = v7; _matrix[7] = v8;
    _matrix[8] = v9;  _matrix[9] = v10; _matrix[10] = v11;  _matrix[11] = v12;
    _matrix[12] = v13;  _matrix[13] = v14;  _matrix[14] = v15;  _matrix[15] = v16;
}

template<typename Type>
Matrix4<Type>::~Matrix4() {}

template<typename Type>
Type const *Matrix4<Type>::get() const {
  return _matrix;
}

template<typename Type>
Type const *Matrix4<Type>::operator *() const {
  return _matrix;
}

template<typename Type>
Matrix4<Type> &Matrix4<Type>::operator=(Matrix4<Type> const &oth){
  for (int i = 0; i < 16; ++i){
    _matrix[i] = oth._matrix[i];
  }
  return (*this);
}

template<typename Type>
Type const &Matrix4<Type>::operator()(size_t x, size_t y) const {
  return _matrix[y * 4 + x];
}

template<typename Type>
void    Matrix4<Type>::identity() {
  for (size_t i = 0; i < 16; ++i)
    _matrix[i] = i % 5 ? 0.0 : 1.0;
    _matrix[0] = 1;  _matrix[1] = 0;  _matrix[2]  = 0; _matrix[3] = 0;
    _matrix[4] = 0;  _matrix[5] = 1;  _matrix[6]  = 0; _matrix[7] = 0;
    _matrix[8] = 0;  _matrix[9] = 0; _matrix[10] = 1;  _matrix[11] = 0;
    _matrix[12] = 0;  _matrix[13] = 0;  _matrix[14] = 0;  _matrix[15] = 1;
}

template<typename Type>
Type   Matrix4<Type>::_calcCombine(int row, int col,
                                   Matrix4<Type> const &other) {
  Type   result = 0.0;
  for (int i = 0; i < 4; ++i)
    result += _matrix[row * 4 + i] * other._matrix[col + i * 4];
  return (result);
}

template<typename Type>
void    Matrix4<Type>::combine(Matrix4<Type> const &m) {
  *this = Matrix4<Type>(_calcCombine(0, 0, m),
                        _calcCombine(0, 1, m),
                        _calcCombine(0, 2, m),
                        _calcCombine(0, 3, m),
                        _calcCombine(1, 0, m),
                        _calcCombine(1, 1, m),
                        _calcCombine(1, 2, m),
                        _calcCombine(1, 3, m),
                        _calcCombine(2, 0, m),
                        _calcCombine(2, 1, m),
                        _calcCombine(2, 2, m),
                        _calcCombine(2, 3, m),
                        _calcCombine(3, 0, m),
                        _calcCombine(3, 1, m),
                        _calcCombine(3, 2, m),
                        _calcCombine(3, 3, m));
}

template<typename Type>
void    Matrix4<Type>::translate(Type x, Type y, Type z)
{
  Matrix4<Type>  tmp(1.0, 0.0, 0.0, x,
                     0.0, 1.0, 0.0, y,
                     0.0, 0.0, 1.0, z,
                     0.0, 0.0, 0.0, 1.0);
  combine(tmp);
}

template<typename Type>
void    Matrix4<Type>::translate(Vector3<Type> const &vec)
{
  Matrix4<Type>  tmp(1.0, 0.0, 0.0, vec.x,
                     0.0, 1.0, 0.0, vec.y,
                     0.0, 0.0, 1.0, vec.z,
                     0.0, 0.0, 0.0, 1.0);
  combine(tmp);
}

template<typename Type>
void    Matrix4<Type>::rotate(Type x, Type y, Type z)
{
  float cx = cos(x);
  float cy = cos(y);
  float cz = cos(z);
  float sx = sin(x);
  float sy = sin(y);
  float sz = sin(z);
  Matrix4<Type>  tmp(cy*cz, cy*sz, sy, 0.0,
                     -sx*sy*cz-cx*sz, -sx*sy*sz+cx*cz, sx*cy, 0.0,
                     -cx*sy*cz+sx*sz, -cx*sy*sz-sx*cz, cx*cy, 0.0,
                     0.0, 0.0, 0.0, 1.0);
  combine(tmp);
}

template<typename Type>
void    Matrix4<Type>::rotate(Vector3<Type> const &v)
{
  float cx = cos(v.x);
  float cy = cos(v.y);
  float cz = cos(v.z);
  float sx = sin(v.x);
  float sy = sin(v.y);
  float sz = sin(v.z);
  Matrix4<Type>  tmp(cy*cz, cy*sz, sy, 0.0,
                     -sx*sy*cz-cx*sz, -sx*sy*sz+cx*cz, sx*cy, 0.0,
                     -cx*sy*cz+sx*sz, -cx*sy*sz-sx*cz, cx*cy, 0.0,
                     0.0, 0.0, 0.0, 1.0);
  combine(tmp);
}

template<typename Type>
void    Matrix4<Type>::rotate(Quaternion<Type> const &q)
{
  Type x = q.getRotate().x * DEGTORAD;
  Type y = q.getRotate().y * DEGTORAD;
  Type z = q.getRotate().z * DEGTORAD;
  float cx = cos(x);
  float cy = cos(y);
  float cz = cos(z);
  float sx = sin(x);
  float sy = sin(y);
  float sz = sin(z);
  Matrix4<Type>  tmp(cy*cz, cy*sz, sy, 0.0,
                     -sx*sy*cz-cx*sz, -sx*sy*sz+cx*cz, sx*cy, 0.0,
                     -cx*sy*cz+sx*sz, -cx*sy*sz-sx*cz, cx*cy, 0.0,
                     0.0, 0.0, 0.0, 1.0);
  combine(tmp);
}


template<typename Type>
void Matrix4<Type>::transpose(Matrix4 const &other) {
  if (*this == other) {
    Type _01 = other._matrix[1], _02 = other._matrix[2],
         _03 = other._matrix[3], _12 = other._matrix[6],
         _13 = other._matrix[7], _23 = other._matrix[11];
    _matrix[1] = other._matrix[4];
    _matrix[2] = other._matrix[8];
    _matrix[3] = other._matrix[12];
    _matrix[4] = _01;
    _matrix[6] = other._matrix[9];
    _matrix[7] = other._matrix[13];
    _matrix[8] = _02;
    _matrix[9] = _12;
    _matrix[11] = other._matrix[14];
    _matrix[12] = _03;
    _matrix[13] = _13;
    _matrix[14] = _23;
  } else {
    _matrix[0] = other._matrix[0];
    _matrix[1] = other._matrix[4];
    _matrix[2] = other._matrix[8];
    _matrix[3] = other._matrix[12];
    _matrix[4] = other._matrix[1];
    _matrix[5] = other._matrix[5];
    _matrix[6] = other._matrix[9];
    _matrix[7] = other._matrix[13];
    _matrix[8] = other._matrix[2];
    _matrix[9] = other._matrix[6];
    _matrix[10] = other._matrix[10];
    _matrix[11] = other._matrix[14];
    _matrix[12] = other._matrix[3];
    _matrix[13] = other._matrix[7];
    _matrix[14] = other._matrix[11];
    _matrix[15] = other._matrix[15];
  }
}

template<typename Type>
void Matrix4<Type>::lookAt(Vector3<Type> const &eye,
                           Vector3<Type> const &center,
                           Vector3<Type> const &up) {

  Type x0, x1, x2, y0, y1, y2, z0, z1, z2, len,
       eyex = eye[0], eyey = eye[1], eyez = eye[2],
       upx = up[0], upy = up[1], upz = up[2],
       centerx = center[0], centery = center[1], centerz = center[2];

  if (ge::abs(eyex - centerx) < Epsilon &&
      ge::abs(eyey - centery) < Epsilon &&
      ge::abs(eyez - centerz) < Epsilon) {
    identity();
    return ;
  }

  z0 = centerx - eyex;
  z1 = centery - eyey;
  z2 = centerz - eyez;

  len = 1.0 / std::sqrt(z0 * z0 + z1 * z1 + z2 * z2);
  z0 *= len;
  z1 *= len;
  z2 *= len;

  x0 = upy * z2 - upz * z1;
  x1 = upz * z0 - upx * z2;
  x2 = upx * z1 - upy * z0;
  len = std::sqrt(x0 * x0 + x1 * x1 + x2 * x2);
  if (!len) {
    x0 = 0;
    x1 = 0;
    x2 = 0;
  } else {
    len = 1 / len;
    x0 *= len;
    x1 *= len;
    x2 *= len;
  }

  y0 = z1 * x2 - z2 * x1;
  y1 = z2 * x0 - z0 * x2;
  y2 = z0 * x1 - z1 * x0;

  len = std::sqrt(y0 * y0 + y1 * y1 + y2 * y2);
  if (!len) {
    y0 = 0;
    y1 = 0;
    y2 = 0;
  } else {
    len = 1 / len;
    y0 *= len;
    y1 *= len;
    y2 *= len;
  }

  _matrix[0] = x0;
  _matrix[1] = y0;
  _matrix[2] = z0;
  _matrix[3] = eyex;
  _matrix[4] = x1;
  _matrix[5] = y1;
  _matrix[6] = z1;
  _matrix[7] = eyey;
  _matrix[8] = x2;
  _matrix[9] = y2;
  _matrix[10] = z2;
  _matrix[11] = eyez;
  _matrix[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
  _matrix[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
  _matrix[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
  _matrix[15] = 1;
  /*
  ge::Matrix4f orientation(x0, x1, x2, 0,
                           y0, y1, y2, 0,
                           z0, z1, z2, 0,
                           0, 0, 0, 1);
  ge::Matrix4f translation(1, 0, 0, -eyex,
                           0, 1, 0, -eyey,
                           0, 0, 1, -eyez,
                           0, 0, 0, 1);
  orientation.combine(translation);
  *this = orientation;
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
  Type len;
  Type z0, z1, z2;

  z0 = eye[0] -  center[0];
  z1 = eye[1] -  center[1];
  z2 = eye[2] -  center[2];
  len = std::sqrt(z0 * z0 + z1 * z1 + z2 * z2);
  if (!len) {
    z0 = 0;
    z1 = 0;
    z2 = 0;
  } else {
    len = 1 / len;
    z0 *= len;
    z1 *= len;
    z2 *= len;
  }

  Type x0, x1, x2;

  x0 = up[1] * z2 - up[2] * z1;
  x1 = up[2] * z0 - up[0] * z2;
  x2 = up[0] * z1 - up[1] * z0;
  len = std::sqrt(x0 * x0 + x1 * x1 + x2 * x2);
  if (!len) {
    x0 = 0;
    x1 = 0;
    x2 = 0;
  } else {
    len = 1 / len;
    x0 *= len;
    x1 *= len;
    x2 *= len;
  }

  Type y0, y1, y2;

  y0 = z1 * x2 - z2 * x1;
  y1 = z2 * x0 - z0 * x2;
  y2 = z0 * x1 - z1 * x0;
  len = std::sqrt(y0 * y0 + y1 * y1 + y2 * y2);
  if (!len) {
    y0 = 0;
    y1 = 0;
    y2 = 0;
  } else {
    len = 1 / len;
    y0 *= len;
    y1 *= len;
    y2 *= len;
  }

  _matrix[0] = x0;
  _matrix[1] = y0;
  _matrix[2] = z0;
  _matrix[3] = eye[0];
  _matrix[4] = x1;
  _matrix[5] = y1;
  _matrix[6] = z1;
  _matrix[7] = eye[1];
  _matrix[8] = x2;
  _matrix[9] = y2;
  _matrix[10] = z2;
  _matrix[11] = eye[2];
  _matrix[12] = -(x0 * eye[0] + x1 * eye[1] + x2 * eye[2]);
  _matrix[13] = -(y0 * eye[0] + y1 * eye[1] + y2 * eye[2]);
  _matrix[14] = -(z0 * eye[0] + z1 * eye[1] + z2 * eye[2]);
  _matrix[15] = 1;
  */
}

template<typename Type>
void Matrix4<Type>::perspective(Type const &fov, Type const &aspect,
                                Type const &near, Type const &far) {
  Type f = 1.0 / std::tan(fov / 2.0),
       nf = 1.0 / (near - far);
  _matrix[0] = f / aspect;
  _matrix[1] = 0;
  _matrix[2] = 0;
  _matrix[3] = 0;
  _matrix[4] = 0;
  _matrix[5] = f;
  _matrix[6] = 0;
  _matrix[7] = 0;
  _matrix[8] = 0;
  _matrix[9] = 0;
  _matrix[10] = (far + near) * nf;
  _matrix[11] = -1;
  _matrix[12] = 0;
  _matrix[13] = 0;
  _matrix[14] = (2 * far * near) * nf;
  _matrix[15] = 0;
}

template<typename Type>
void Matrix4<Type>::ortho(Type const &left, Type const &right,
                          Type const &bottom, Type const &top,
                          Type const &near, Type const &far) {
  Type lr = 1 / (left - right),
       bt = 1 / (bottom - top),
       nf = 1 / (near - far);
  _matrix[0] = -2 * lr;
  _matrix[1] = 0;
  _matrix[2] = 0;
  _matrix[3] = 0;
  _matrix[4] = 0;
  _matrix[5] = -2 * bt;
  _matrix[6] = 0;
  _matrix[7] = 0;
  _matrix[8] = 0;
  _matrix[9] = 0;
  _matrix[10] = 2 * nf;
  _matrix[11] = 0;
  _matrix[12] = (left + right) * lr;
  _matrix[13] = (top + bottom) * bt;
  _matrix[14] = (far + near) * nf;
  _matrix[15] = 1;
}

template<typename Type>
bool Matrix4<Type>::operator==(Matrix4 const &other) const {
  for (size_t i = 0; i < 16; ++i)
    if (_matrix[i] != other._matrix[i])
      return false;
  return true;
}
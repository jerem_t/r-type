
#pragma once

#include <gEngine/Utils/Convert.hpp>
#include <gEngine/Math/VectorUtils.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Vector4
 * Simple 3 dimentions vector object.
 */
template<typename Type>
struct GE_DLL Vector4 {

  Vector4();

  Vector4(Type const &value);

  Vector4(Type const &x,
          Type const &y,
          Type const &z,
          Type const &w = 1);

  Vector4(Vector4 const &other);

  Vector4 &operator=(Vector4 const &other);

  ~Vector4();

  inline void set(Type const &x,
                  Type const &y,
                  Type const &z,
                  Type const &w = 1);

  inline void reset();

  Type const &operator[](size_t i) const;

  Type &operator[](size_t i);

  /**
   * Get the string representation of the Vector4.
   */
  std::string toString() const;

  /**
   * Create the vector4 from its string representation.
   */
  void fromString(std::string const &str);

  bool operator==(Vector4 const &other) const;
  bool operator!=(Vector4 const &other) const;


  Vector4 &operator+=(Vector4 const &);
  Vector4 &operator-=(Vector4 const &);
  Vector4 &operator*=(Vector4 const &);
  Vector4 &operator/=(Vector4 const &);

  Vector4 operator+(Vector4 const &) const;
  Vector4 operator-(Vector4 const &) const;
  Vector4 operator*(Vector4 const &) const;
  Vector4 operator/(Vector4 const &) const;

  Vector4 &operator*=(float);
  Vector4 operator*(float) const;

  void min(Vector4 const &other);

  void max(Vector4 const &other);

  /**
   * Number of fields.
   */
  enum { size = 4 };

  /**
   * Data reprensentation.
   */
  union {
    Type data[size];
    struct { Type x, y, z, w; };
    struct { Type r, g, b, a; };
  };
};

typedef Vector4<float> Vector4f;
typedef Vector4<int> Vector4i;
typedef Vector4<size_t> Vector4u;
typedef Vector4<float> Colorf;
typedef Vector4<unsigned char> Color;

#include <gEngine/Math/Vector4.inl>

}
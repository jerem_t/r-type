
#pragma once

template<typename Type>
Vector2<Type>::Vector2(Type const &value) {
  set(value, value);
}

template<typename Type>
Vector2<Type>::Vector2(Type const &x, Type const &y) 
{
	set(x, y);
}

template<typename Type>
Vector2<Type>::Vector2(Vector2 const &other)
: x(other.x)
, y(other.y) {}

template<typename Type>
Vector2<Type> &Vector2<Type>::operator=(Vector2 const &other) {
  if (this != &other) {
    x = other.x;
    y = other.y;
  }
  return *this;
}

template<typename Type>
Vector2<Type>::~Vector2() {}

template<typename Type>
void Vector2<Type>::set(Type const &x, Type const &y) {
  data[0] = x;
  data[1] = y;
}

template<typename Type>
void Vector2<Type>::reset() {
  data[0] = 0;
  data[1] = 0;
}

template<typename Type>
bool Vector2<Type>::operator==(Vector2 const &other) {
  return VectorUtils<Vector2>::equals(*this, other);
}

template<typename Type>
bool Vector2<Type>::operator!=(Vector2 const &other) {
  return !VectorUtils<Vector2>::equals(*this, other);
}

template<typename Type>
Type const &Vector2<Type>::operator[](size_t i) const {
  return data[i];
}

template<typename Type>
Type &Vector2<Type>::operator[](size_t i) {
  return data[i];
}

template<typename Type>
Vector2<Type> &Vector2<Type>::operator+=(Vector2<Type> const &other) {
  return (*this = *this + other);
}

template<typename Type>
Vector2<Type> &Vector2<Type>::operator-=(Vector2<Type> const &other) {
  return (*this = *this - other);
}

template<typename Type>
Vector2<Type> &Vector2<Type>::operator*=(Vector2<Type> const &other) {
  return (*this = *this * other);
}

template<typename Type>
Vector2<Type> &Vector2<Type>::operator/=(Vector2<Type> const &other) {
  return (*this = *this / other);
}

template<typename Type>
Vector2<Type> Vector2<Type>::operator+(Vector2 const &other) const {
  return VectorUtils<Vector2>::add(*this, other);
}

template<typename Type>
Vector2<Type> Vector2<Type>::operator-(Vector2 const &other) const {
  return VectorUtils<Vector2>::sub(*this, other);
}

template<typename Type>
Vector2<Type> Vector2<Type>::operator*(Vector2 const &other) const {
  return VectorUtils<Vector2>::mul(*this, other);
}

template<typename Type>
Vector2<Type> Vector2<Type>::operator/(Vector2 const &other) const {
  return VectorUtils<Vector2>::div(*this, other);
}

template<typename Type>
Type Vector2<Type>::dot() const {
  return VectorUtils<Vector2>::dot(*this);
}

template<typename Type>
Type Vector2<Type>::squareLength() const {
  return VectorUtils<Vector2>::squareLength(*this);
}

template<typename Type>
Type Vector2<Type>::length() const {
  return VectorUtils<Vector2>::length(*this);
}

template<typename Type>
void Vector2<Type>::rotate(Type const &deg) {
  Type angle = deg * DegToRad;
  Type s = sin(angle);
  Type c = cos(angle);
  Type newX = x * c + y * s;
  Type newY = x * s + y * c;
  x = newX;
  y = newY;
}

template<typename Type>
void Vector2<Type>::min(Vector2 const &other) {
  *this = VectorUtils<Vector2>::min(*this, other);
}

template<typename Type>
void Vector2<Type>::max(Vector2 const &other) {
  *this = VectorUtils<Vector2>::max(*this, other);
}

template<typename Type>
std::string Vector2<Type>::toString() const {
  return "vec2(" + ge::convert<Type, std::string>(x)
          + ", " + ge::convert<Type, std::string>(y) + ")";
}

template<typename Type>
void Vector2<Type>::fromString(std::string const &) {
  //todo
}


#pragma once

template<typename VectorType>
VectorType VectorUtils<VectorType>::add(VectorType const &first,
                                        VectorType const &second) {
  VectorType result;
  for (size_t i = 0; i < VectorType::size; ++i)
    result[i] = first[i] + second[i];
  return result;
}

template<typename VectorType>
VectorType VectorUtils<VectorType>::sub(VectorType const &first,
                                        VectorType const &second) {
  VectorType result;
  for (size_t i = 0; i < VectorType::size; ++i)
    result[i] = first[i] - second[i];
  return result;
}

template<typename VectorType>
VectorType VectorUtils<VectorType>::mul(VectorType const &first,
                                        VectorType const &second) {
  VectorType result;
  for (size_t i = 0; i < VectorType::size; ++i)
    result[i] = first[i] * second[i];
  return result;
}

template<typename VectorType>
VectorType VectorUtils<VectorType>::div(VectorType const &first,
                                        VectorType const &second) {
  VectorType result;
  for (size_t i = 0; i < VectorType::size; ++i)
    result[i] = first[i] / second[i];
  return result;
}

template<typename VectorType>
template<typename Type>
Type VectorUtils<VectorType>::dot(VectorType const &first, VectorType const &second) {
  Type result(0);
  for (size_t i = 0; i < VectorType::size; ++i)
    result += first[i] * second[i];
  return result;
}

template<typename VectorType>
template<typename Type>
Type VectorUtils<VectorType>::squareLength(VectorType const &vec) {
  return dot(vec, vec);
}

template<typename VectorType>
template<typename Type>
Type VectorUtils<VectorType>::length(VectorType const &vec) {
  return sqrt(squareLength(vec));
}

template<typename VectorType>
void VectorUtils<VectorType>::normalize(VectorType &vec) {
  vec /= length(vec);
}

template<typename VectorType>
VectorType VectorUtils<VectorType>::min(VectorType const &first,
                                        VectorType const &second) {
  VectorType result;
  for (size_t i = 0; i < VectorType::size; ++i)
    result[i] = first[i] <= second[i] ? first[i] : second[i];
  return result;
}

template<typename VectorType>
VectorType VectorUtils<VectorType>::max(VectorType const &first,
                                        VectorType const &second) {
  VectorType result;
  for (size_t i = 0; i < VectorType::size; ++i)
    result[i] = first[i] >= second[i] ? first[i] : second[i];
  return result;
}

template<typename VectorType>
bool VectorUtils<VectorType>::equals(VectorType const &first,
                                     VectorType const &second) {
  for (size_t i = 0; i < VectorType::size; ++i)
    if (first[i] != second[i])
      return false;
  return true;
}


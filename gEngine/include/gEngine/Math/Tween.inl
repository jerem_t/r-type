
#pragma once

template<typename Type>
std::map<std::string, easing::Func> Tween<Type>::_easeFn = {
  { "linear", &easing::linear },
  { "backIn", &easing::backIn },
  { "backOut", &easing::backOut },
  { "backInOut", &easing::backInOut },
  { "bounceIn", &easing::bounceIn },
  { "bounceOut", &easing::bounceOut },
  { "bounceInOut", &easing::bounceInOut },
  { "circleIn", &easing::circleIn },
  { "circleOut", &easing::circleOut },
  { "circleInOut", &easing::circleInOut },
  { "cubicIn", &easing::cubicIn },
  { "cubicOut", &easing::cubicOut },
  { "cubicInOut", &easing::cubicInOut },
  { "elasticIn", &easing::elasticIn },
  { "elasticOut", &easing::elasticOut },
  { "elasticInOut", &easing::elasticInOut },
  { "expoIn", &easing::expoIn },
  { "expoOut", &easing::expoOut },
  { "expoInOut", &easing::expoInOut },
  { "quadIn", &easing::quadIn },
  { "quadOut", &easing::quadOut },
  { "quadInOut", &easing::quadInOut },
  { "quartIn", &easing::quartIn },
  { "quartOut", &easing::quartOut },
  { "quartInOut", &easing::quartInOut },
  { "quintIn", &easing::quintIn },
  { "quintOut", &easing::quintOut },
  { "quintInOut", &easing::quintInOut },
  { "sineIn", &easing::sineIn },
  { "sineOut", &easing::sineOut },
  { "sineInOut", &easing::sineInOut }
};

template<typename Type>
Tween<Type>::Tween()
: _delayTime(0.0f)
, _delayDuration(0.0f)
, _animTime(0.0f)
, _animDuration(1.0f)
, _progress(0.0f)
, _paused(false)
, _repeat(false)
, _ease(easing::linear) {}

template<typename Type>
Tween<Type>::~Tween() {}

// Setters

template<typename Type>
Tween<Type> &Tween<Type>::duration(Seconds duration) {
  _animDuration = duration;
  return *this;
}

template<typename Type>
Tween<Type> &Tween<Type>::repeat(bool repeat) {
  _repeat = repeat;
  return *this;
}

template<typename Type>
Tween<Type> &Tween<Type>::from(Type const &from) {
  _from = from;
  return *this;
}

template<typename Type>
Tween<Type> &Tween<Type>::to(Type const &to) {
  _to = to;
  return *this;
}

template<typename Type>
Tween<Type> &Tween<Type>::easing(std::string const &name) {
  if (_easeFn.find(name) == _easeFn.end())
    throw std::runtime_error("Unknown easing " + name + ".");
  _ease = _easeFn[name];
  return *this;
}

template<typename Type>
Tween<Type> &Tween<Type>::easing(easing::Func ease) {
  _ease = ease;
  return *this;
}

template<typename Type>
Tween<Type> &Tween<Type>::delay(Seconds delay) {
  _delayDuration = delay;
  return *this;
}

// Getters

template<typename Type>
bool Tween<Type>::isFinish() const {
  return _animTime > _animDuration;
}

template<typename Type>
Type const &Tween<Type>::get() const {
  return _value;
}

template<typename Type>
Tween<Type>::operator Type const &() const {
  return _value;
}

template<typename Type>
Seconds Tween<Type>::getProgress() const {
  return _progress;
}

template<typename Type>
Seconds Tween<Type>::getDuration() const {
  return (_animDuration);
}

// Functions

template<typename Type>
void Tween<Type>::start() {
  stop();
  _value = _from;
  _progress = 0;
  _paused = false;
}

template<typename Type>
void Tween<Type>::pause() {
  _paused = true;
}

template<typename Type>
void Tween<Type>::stop() {
  pause();
  _delayTime = 0;
  _animTime = 0;
  _value = _to;
}

template<typename Type>
void Tween<Type>::update(Seconds dt) {
  if (_paused)
    return ;
  if (isFinish()) {
    stop();
    if (!_repeat)
      return;
    start();
  }
  if (_delayTime < _delayDuration) {
    _delayTime += dt;
    return ;
  }
  _animTime += dt;
  _progress = _animDuration ? _animTime / _animDuration : 1.0f;
  _value = _animDuration ? _ease(_animTime, _from, _to, _animDuration) : _to;
}
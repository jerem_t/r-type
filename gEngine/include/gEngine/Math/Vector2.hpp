
#pragma once

#include <gEngine/Utils/Convert.hpp>
#include <gEngine/Math/Utils.hpp>
#include <gEngine/Math/VectorUtils.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Vector2
 * Simple 2 dimentions vector object.
 */
template<typename Type>
struct GE_DLL Vector2 {

  Vector2(Type const &value = 0);

  Vector2(Type const &x, Type const &y);

  Vector2(Vector2 const &other);

  Vector2 &operator=(Vector2 const &other);

  ~Vector2();

  inline void set(Type const &x, Type const &y);

  inline void reset();

  bool operator==(Vector2 const &other);

  bool operator!=(Vector2 const &other);

  Type const &operator[](size_t i) const;

  Type &operator[](size_t i);

  Vector2 &operator+=(Vector2 const &other);
  Vector2 &operator-=(Vector2 const &other);
  Vector2 &operator*=(Vector2 const &other);
  Vector2 &operator/=(Vector2 const &other);

  Vector2 operator+(Vector2 const &other) const;
  Vector2 operator-(Vector2 const &other) const;
  Vector2 operator*(Vector2 const &other) const;
  Vector2 operator/(Vector2 const &other) const;

  Type dot() const;

  Type squareLength() const;

  Type length() const;

  void rotate(Type const &deg);

  void min(Vector2 const &other);

  void max(Vector2 const &other);

  std::string toString() const;

  void fromString(std::string const &str);

  /**
   * Number of fields.
   */
  enum { size = 2 };

  union {
    Type data[size];
    struct { Type x, y; };
    struct { Type u, v; };
    struct { Type s, t; };
  };
};

typedef Vector2<float> Vector2f;
typedef Vector2<int> Vector2i;
typedef Vector2<size_t> Vector2u;
typedef Vector2<float> Pointf;
typedef Vector2<int> Pointi;
typedef Vector2<size_t> Pointu;

#include <gEngine/Math/Vector2.inl>

}
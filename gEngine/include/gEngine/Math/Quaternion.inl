
#pragma once

template<typename Type>
Quaternion<Type>::Quaternion() {}

template<typename Type>
Quaternion<Type>::Quaternion(Type x, Type y, Type z, Type s) : _vec(x, y, z), _s(s) {}

template<typename Type>
Quaternion<Type>::Quaternion(Type x, Type y, Type z)  {
    x *= DEGTORAD;
    y *= DEGTORAD;
    z *= DEGTORAD;

    float cosz = cosf(z / 2.0f);
    float cosy = cosf(y / 2.0f);
    float cosx = cosf(x / 2.0f);

    float sinz = sinf(z / 2.0f);
    float siny = sinf(y / 2.0f);
    float sinx = sinf(x / 2.0f);

    _s = cosz * cosy * cosx + sinz * siny * sinx;
    _vec.x = cosz * cosy * sinx - sinz * siny * cosx;
    _vec.y = cosz * siny * cosx + sinz * cosy * sinx;
    _vec.z = sinz * cosy * cosx - cosz * siny * sinx;
}

template<typename Type>
Quaternion<Type>::Quaternion(Vector3<Type> vec) {
    vec.x *= DEGTORAD;
    vec.y *= DEGTORAD;
    vec.z *= DEGTORAD;

    float cosz = cosf(vec.z / 2.0f);
    float cosy = cosf(vec.y / 2.0f);
    float cosx = cosf(vec.x / 2.0f);

    float sinz = sinf(vec.z / 2.0f);
    float siny = sinf(vec.y / 2.0f);
    float sinx = sinf(vec.x / 2.0f);

    _s = cosz * cosy * cosx + sinz * siny * sinx;
    _vec.x = cosz * cosy * sinx - sinz * siny * cosx;
    _vec.y = cosz * siny * cosx + sinz * cosy * sinx;
    _vec.z = sinz * cosy * cosx - cosz * siny * sinx;
}

template<typename Type>
Quaternion<Type>::~Quaternion() {}

template<typename Type>
Quaternion<Type> &Quaternion<Type>::operator=(Quaternion<Type> const &oth){
  _vec = oth._vec;
  _s = oth._s;
  return (*this);
}

template<typename Type>
Vector3<Type>    Quaternion<Type>::getRotate() const {
    float x2 = _vec.x * _vec.x;
    float y2 = _vec.y * _vec.y;
    float z2 = _vec.z * _vec.z;

    ge::Vector3f result;
    result.x = atan2f(2.f * (_vec.z*_vec.y + _vec.x*_s), 1.f - 2.f * (x2 + y2)) * RADTODEG;
    result.y = asinf(2.f * (_vec.x*_vec.z - _vec.y*_s)) * RADTODEG;
    result.z = atan2f(2.f * (_vec.x*_vec.y + _vec.z*_s), 1.f - 2.f * (y2 + z2)) * RADTODEG;  
    return result;
}

template<typename Type>
Type         Quaternion<Type>::getScalar() const {
  return _s;
}

template<typename Type>
void                    Quaternion<Type>::setRotate(Type x, Type y, Type z) {
    x *= DEGTORAD;
    y *= DEGTORAD;
    z *= DEGTORAD;

    float cosz = cosf(z / 2.0f);
    float cosy = cosf(y / 2.0f);
    float cosx = cosf(x / 2.0f);

    float sinz = sinf(z / 2.0f);
    float siny = sinf(y / 2.0f);
    float sinx = sinf(x / 2.0f);

    _s = cosz * cosy * cosx + sinz * siny * sinx;
    _vec.x = cosz * cosy * sinx - sinz * siny * cosx;
    _vec.y = cosz * siny * cosx + sinz * cosy * sinx;
    _vec.z = sinz * cosy * cosx - cosz * siny * sinx;
}

template<typename Type>
void                    Quaternion<Type>::setRotate(Vector3<Type> &vec) {
    vec.x *= DEGTORAD;
    vec.y *= DEGTORAD;
    vec.z *= DEGTORAD;

    float cosz = cosf(vec.z / 2.0f);
    float cosy = cosf(vec.y / 2.0f);
    float cosx = cosf(vec.x / 2.0f);

    float sinz = sinf(vec.z / 2.0f);
    float siny = sinf(vec.y / 2.0f);
    float sinx = sinf(vec.x / 2.0f);

    _s = cosz * cosy * cosx + sinz * siny * sinx;
    _vec.x = cosz * cosy * sinx - sinz * siny * cosx;
    _vec.y = cosz * siny * cosx + sinz * cosy * sinx;
    _vec.z = sinz * cosy * cosx - cosz * siny * sinx;
}

template<typename Type>
void		Quaternion<Type>::setScalar(Type scalar)
{
	_s = scalar;
}

template<typename Type>
void         Quaternion<Type>::conjugate() {
  _vec = ge::Vector3f(_vec.x * -1.0f, _vec.y * -1.0f, _vec.z * -1.0f);
}

template<typename Type>
void         Quaternion<Type>::normalized() {
  float len = sqrt(_s * _s + sqrt(_vec.x * _vec.x + _vec.y * _vec.y + _vec.z * _vec.z));
  _vec.x /= len;
  _vec.y /= len;
  _vec.z /= len;
  _s /= len;
}

template<typename Type>
ge::Vector3f         Quaternion<Type>::rotate(ge::Vector3f const &vec) const {
  ge::Quaternionf V(vec.x, vec.y, vec.z, 0);
  ge::Quaternionf conjugate(*this);
  conjugate.conjugate();
  ge::Vector3f result = (*this * V * conjugate)._vec;
  return result;
}

template<typename Type>
Quaternion<Type> const  &Quaternion<Type>::operator*=(Quaternion<Type> const &oth){
  Type x = _vec.x, y= _vec.y, z= _vec.z, sn= _s*oth._s - _vec.x*oth._vec.x - _vec.y*oth._vec.y - _vec.z*oth._vec.z;
  _vec.x= y*oth._vec.z - z*oth._vec.y + _s*oth._vec.x + x*oth._s;
  _vec.y= z*oth._vec.x - x*oth._vec.z + _s*oth._vec.y + y*oth._s;
  _vec.z= x*oth._vec.y - y*oth._vec.x + _s*oth._vec.z + z*oth._s;
  _s= sn;
  return *this;
}

template<typename Type>
Quaternion<Type>   Quaternion<Type>::operator*(Quaternion<Type> const &oth) const{
  return Quaternion<Type>(_vec.y * oth._vec.z - _vec.z * oth._vec.y + _s * oth._vec.x + _vec.x * oth._s,
            _vec.z * oth._vec.x - _vec.x * oth._vec.z + _s * oth._vec.y + _vec.y * oth._s,
            _vec.x * oth._vec.y - _vec.y * oth._vec.x + _s * oth._vec.z + _vec.z * oth._s,
            _s * oth._s - _vec.x*oth._vec.x - _vec.y*oth._vec.y - _vec.z*oth._vec.z);
}

template<typename Type>
Quaternion<Type> const &Quaternion<Type>::operator+=(Quaternion<Type> const &oth){
  _vec = _vec + oth._vec;
  _s += oth._s;
  return (*this);
}
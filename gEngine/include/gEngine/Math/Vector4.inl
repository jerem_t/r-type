
#pragma once

template<typename Type>
Vector4<Type>::Vector4() {
  set(0, 0, 0, 1);
}

template<typename Type>
Vector4<Type>::Vector4(Type const &value) {
  set(value, value, value, 1);
}

template<typename Type>
Vector4<Type>::Vector4(Type const &x,
                       Type const &y,
                       Type const &z,
                       Type const &w) {
  set(x, y, z, w);
}

template<typename Type>
Vector4<Type>::Vector4(Vector4 const &other)
: x(other.x)
, y(other.y)
, z(other.z)
, w(other.w) {}

template<typename Type>
Vector4<Type> &Vector4<Type>::operator=(Vector4 const &other) {
  if (this != &other) {
    x = other.x;
    y = other.y;
    z = other.z;
    w = other.w;
  }
  return *this;
}

template<typename Type>
Vector4<Type>::~Vector4() {}

template<typename Type>
void Vector4<Type>::set(Type const &x,
                        Type const &y,
                        Type const &z,
                        Type const &w) {
  data[0] = x;
  data[1] = y;
  data[2] = z;
  data[3] = w;
}

template<typename Type>
void Vector4<Type>::reset() {
  data[0] = 0;
  data[1] = 0;
  data[2] = 0;
  data[3] = 0;
}

template<typename Type>
std::string Vector4<Type>::toString() const {
  return "vec4(" + ge::convert<Type, std::string>(x)
          + ", " + ge::convert<Type, std::string>(y)
          + ", " + ge::convert<Type, std::string>(z)
          + ", " + ge::convert<Type, std::string>(w) + ")";
}

template<typename Type>
void Vector4<Type>::fromString(std::string const &) {
  // TODO
}

template<typename Type>
bool Vector4<Type>::operator==(Vector4<Type> const &other) const {
  return VectorUtils<Vector4>::equals(*this, other);
}

template<typename Type>
bool Vector4<Type>::operator!=(Vector4<Type> const &other) const {
  return !VectorUtils<Vector4>::equals(*this, other);
}

template<typename Type>
Type const &Vector4<Type>::operator[](size_t i) const {
  return data[i];
}

template<typename Type>
Type &Vector4<Type>::operator[](size_t i) {
  return data[i];
}

template<typename Type>
Vector4<Type> &Vector4<Type>::operator*=(float n) {
  for (size_t i = 0; i < Vector4::size; ++i)
    data[i] *= n;
  return *this;
}

template<typename Type>
Vector4<Type> Vector4<Type>::operator*(float n) const {
  Vector4<Type> result = *this;
  result *= n;
  return result;
}

template<typename Type>
Vector4<Type> &Vector4<Type>::operator+=(Vector4<Type> const &other) {
  return (*this = *this + other);
}

template<typename Type>
Vector4<Type> &Vector4<Type>::operator-=(Vector4<Type> const &other) {
  return (*this = *this - other);
}

template<typename Type>
Vector4<Type> &Vector4<Type>::operator*=(Vector4<Type> const &other) {
  return (*this = *this * other);
}

template<typename Type>
Vector4<Type> &Vector4<Type>::operator/=(Vector4<Type> const &other) {
  return (*this = *this / other);
}


template<typename Type>
Vector4<Type> Vector4<Type>::operator+(Vector4 const &other) const {
  return VectorUtils<Vector4>::add(*this, other);
}

template<typename Type>
Vector4<Type> Vector4<Type>::operator-(Vector4 const &other) const {
  return VectorUtils<Vector4>::sub(*this, other);
}

template<typename Type>
Vector4<Type> Vector4<Type>::operator*(Vector4 const &other) const {
  return VectorUtils<Vector4>::mul(*this, other);
}

template<typename Type>
Vector4<Type> Vector4<Type>::operator/(Vector4 const &other) const {
  return VectorUtils<Vector4>::div(*this, other);
}

template<typename Type>
void Vector4<Type>::min(Vector4 const &other) {
  *this = VectorUtils<Vector4>::min(*this, other);
}

template<typename Type>
void Vector4<Type>::max(Vector4 const &other) {
  *this = VectorUtils<Vector4>::max(*this, other);
}
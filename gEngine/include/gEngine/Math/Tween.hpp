
#pragma once

#include <map>
#include <string>
#include <gEngine/System/Time.hpp>
#include <gEngine/Math/Easing.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Tween
 * Handle animations from easing curves.
 * @example
 *   // Create the animation
 *   ge::Tween<int> tween;
 *   tween.from(1).to(10)
 *        .duration(ge::Second(1))
 *        .easing(ge::easing::linearEaseIn)
 *        .start();
 *   // Update the animation
 *   tween.update(dt);
 */
template<typename Type>
class Tween {
public:

  Tween();

  ~Tween();

  // setters
  Tween &duration(ge::Seconds duration);
  Tween &from(Type const &from);
  Tween &to(Type const &to);
  Tween &easing(std::string const &name);
  Tween &easing(easing::Func ease);
  Tween &delay(ge::Seconds delay);
  Tween &repeat(bool repeat = true);

  // functions
  void start();
  void pause();
  void stop();

  // getters
  bool isFinish() const;
  Type const &get() const;
  Seconds getProgress() const;
  ge::Seconds getDuration() const;
  operator Type const &() const;
  
  /**
   * Update the animation according the delta time.
   * @param dt The delta time since the previous frame.
   */
  void update(ge::Seconds dt);

private:
  Type _value;
  Type _from;
  Type _to;
  Seconds _delayTime;
  Seconds _delayDuration;
  Seconds _animTime;
  Seconds _animDuration;
  Seconds _progress;
  bool _paused;
  bool _repeat;
  easing::Func _ease;
  static std::map<std::string, easing::Func> _easeFn;
};

#include <gEngine/Math/Tween.inl>

}
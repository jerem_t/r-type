#pragma once


#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Vector2.hpp>
#include <iostream>

#define DEGTORAD (0.0174532925f)
#define RADTODEG (1.0f / 0.0174532925f)
/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Quaternion
 * Data structure to reprensent matrix.
 */
template<typename Type>
class GE_DLL Quaternion {
public:
  /**
   * Default constructor, do nothing.
  */
  Quaternion();

  /**
    * Contructor with initialization.
  */
  Quaternion(Type x, Type y, Type z);

  Quaternion(Vector3<Type> vec);

  Quaternion(Type x, Type y, Type z, Type s);

  /**
   * Empty destructor, do nothing.
  */
  ~Quaternion();

  Quaternion &operator=(Quaternion const &);

  Quaternion const  &operator*=(Quaternion const &oth);
  Quaternion   operator*(Quaternion const &oth) const;

  Quaternion const &operator+=(Quaternion const &oth);

  Vector3<Type>          getRotate() const;
  Type                   getScalar() const;

  void                    conjugate();
  Vector3f                rotate(Vector3f const &) const;
  void                    normalized();

  void                    setRotate(Type x = 0, Type y = 0, Type z = 0);
  void                    setRotate(Vector3<Type> &vec);
  void					  setScalar(Type scalar);

private:
  Vector3<Type>    _vec; /* rotate x, y z */
  Type             _s; /* scalar */
};

typedef Quaternion<float> Quaternionf;
typedef Quaternion<int> Quaternioni;

#include <gEngine/Math/Quaternion.inl>

}


#pragma once

#include <gEngine/Math/Vector3.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Box
 * Simple reprensentation of a n-dimensions box volume.
 */
template <typename Type>
struct GE_DLL Box {
  Box(Type const &width = 0,
      Type const &height = 0,
      Type const &depth = 1,
      Type const &x = 0,
      Type const &y = 0,
      Type const &z = 0);
  Box(Box const &other);
  Box &operator=(Box const &other);
  ~Box();
  bool hit(Box const &other) const;
  bool hit(Vector3<Type> const &pt) const;
  Vector3<Type> center() const;
  Type const &operator[](size_t i) const;
  Type &operator[](size_t i);
  std::string toString() const;
  void fromString(std::string const &str);
  union {
    Type data[6];
    struct {
      Vector3<Type> pos;
      Vector3<Type> size;
    };
  };
};

#include <gEngine/Math/Box.inl>

typedef Box<float> Boxf;
typedef Box<int> Boxi;
typedef Box<unsigned int> Boxu;

}

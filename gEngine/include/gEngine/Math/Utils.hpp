
#pragma once

#include <cmath>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

const float Pi = 3.14159265359;

const float Epsilon = 0.000001f;

const float DegToRad = 0.0174532925f;

/**
 * Compute a linear interpolation between src and dest.
 * @param src The source value.
 * @param dest The destination value.
 * @param delta The delta value of the interpolation.
 */
template<typename Type, typename DeltaType>
inline Type GE_DLL lerp(Type const &src,
                 Type const &dest,
                 DeltaType const &delta) {
  return dest + (dest - src) * delta;
}

/**
 * Return the absolute value of the given number. It will be
 * always positive even if the input is negative.
 * @param a The number to check.
 */
template<typename Type>
inline Type GE_DLL abs(Type const &a) {
  return a < 0 ? -a : a;
}

}
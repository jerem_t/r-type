
#pragma once

#include <iostream>
#include <cstdlib>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Vector2.hpp>
#include <gEngine/Math/Utils.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

void fakeSeed(size_t seed);

double fakeRandom();

template<typename Type>
Type fakeRandom(Type const &min, Type const &max) {
  return fakeRandom() * (max - min) + min;
}

/**
 * Compute a random Vector2 according linear distribution.
 */
template<typename Type>
Vector2<Type> fakeRandom(Vector2<Type> const &min,
                         Vector2<Type> const &max) {
  return Vector2<Type>(fakeRandom(min.x, max.x),
                       fakeRandom(min.y, max.y));
}

/**
 * Compute a random Vector3 according linear distribution.
 */
template<typename Type>
Vector3<Type> fakeRandom(Vector3<Type> const &min,
                           Vector3<Type> const &max) {
  return Vector3<Type>(fakeRandom(min.x, max.x),
                       fakeRandom(min.y, max.y),
                       fakeRandom(min.z, max.z));
}

/**
 * Compute a random according linear distribution.
 */
template<typename Type>
Type linearRandom(Type const &min, Type const &max) {
  return (std::rand() / double(RAND_MAX)) * (max - min) + min;
}

/**
 * Compute a random Vector2 according linear distribution.
 */
template<typename Type>
Vector2<Type> linearRandom(Vector2<Type> const &min,
                         Vector2<Type> const &max) {
  return Vector2<Type>(linearRandom(min.x, max.x),
                       linearRandom(min.y, max.y));
}

/**
 * Compute a random Vector3 according linear distribution.
 */
template<typename Type>
Vector3<Type> linearRandom(Vector3<Type> const &min,
                           Vector3<Type> const &max) {
  return Vector3<Type>(linearRandom(min.x, max.x),
                       linearRandom(min.y, max.y),
                       linearRandom(min.z, max.z));
}

/**
 * Compute a random according gaussian distribution.
 */
template<typename Type>
Type gaussRandom(Type const &min, Type const &max) {
  Type w, x1, x2;
  do {
    x1 = linearRandom(Type(-1), Type(1));
    x2 = linearRandom(Type(-1), Type(1));
    w = x1 * x1 + x2 * x2;
  } while(w > Type(1));
  return x2 * max * max * sqrt((Type(-2) * log(w)) / w) + min;
}

/**
 * Compute a random Vector2 according gaussian distribution.
 */
template<typename Type>
Vector2<Type> gaussRandom(Vector2<Type> const &min,
                          Vector2<Type> const &max) {
  return Vector2<Type>(gaussRandom(min.x, max.x),
                       gaussRandom(min.y, max.y));
}

/**
 * Compute a random Vector3 according gaussian distribution.
 */
template<typename Type>
Vector3<Type> gaussRandom(Vector3<Type> const &min,
                          Vector3<Type> const &max) {
  return Vector3<Type>(gaussRandom(min.x, max.x),
                       gaussRandom(min.y, max.y),
                       gaussRandom(min.z, max.z));
}

/**
 * Compute a random Vector2 which coordinates are regulary
 * distributed within the area a disk of the given radius.
 */
template<typename Type>
Vector2<Type> diskRandom(Type const &radius) {
  Vector2<Type> result;
  do {
    result = linearRand(Vector2<Type>(-radius), Vector2<Type>(radius));
  } while(result.length() > radius);
  return result;
}

/**
 * Compute a random Vector3 which coordinates are regulary
 * distributed within the volume a ball of the given radius.
 */
template<typename Type>
Vector3<Type> ballRandom(Type const &radius) {
  Vector3<Type> result;
  do {
    result = linearRand(Vector3<Type>(-radius), Vector3<Type>(radius));
  } while(result.length() > radius);
  return result;
}

/**
 * Compute a random Vector2 which coordinates are regulary
 * distributed on a circle of the given radius.
 */
template<typename Type>
Vector2<Type> circularRandom(Type const &radius) {
  Type angle = linearRand(Type(0), Type(ge::Pi * 2.0f));
  return Vector2<Type>(cos(angle), sin(angle)) * radius;
}

/**
 * Compute a random Vector3 which coordinates are regulary
 * distributed on a sphere of the given radius.
 */
template<typename Type>
Vector3<Type> sphericalRandom(Type const &radius) {
  Type z = linearRand(Type(-1), Type(1));
  Type a = linearRand(Type(0), Type(ge::Pi * 2.0f));
  Type r = sqrt(Type(1) - z * z);
  Type x = r * cos(a);
  Type y = r * sin(a);
  return Vector3<Type>(x, y, z) * radius;
}

}
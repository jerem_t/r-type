
#pragma once

#include <gEngine/Math/Utils.hpp>
#include <gEngine/Math/Vector2.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Vector4.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

namespace easing {

typedef float (*Func)(float, float, float, float);

float GE_DLL linear(float t, float b, float c, float d);

float GE_DLL backIn(float t, float b, float c, float d);
float GE_DLL backOut(float t, float b, float c, float d);
float GE_DLL backInOut(float t, float b, float c, float d);

float GE_DLL bounceIn(float t, float b, float c, float d);
float GE_DLL bounceOut(float t, float b, float c, float d);
float GE_DLL bounceInOut(float t, float b, float c, float d);

float GE_DLL circleIn(float t, float b, float c, float d);
float GE_DLL circleOut(float t, float b, float c, float d);
float GE_DLL circleInOut(float t, float b, float c, float d);

float GE_DLL cubicIn(float t, float b, float c, float d);
float GE_DLL cubicOut(float t, float b, float c, float d);
float GE_DLL cubicInOut(float t, float b, float c, float d);

float GE_DLL elasticIn(float t, float b, float c, float d);
float GE_DLL elasticOut(float t, float b, float c, float d);
float GE_DLL elasticInOut(float t, float b, float c, float d);

float GE_DLL expoIn(float t, float b, float c, float d);
float GE_DLL expoOut(float t, float b, float c, float d);
float GE_DLL expoInOut(float t, float b, float c, float d);

float GE_DLL quadIn(float t, float b, float c, float d);
float GE_DLL quadOut(float t, float b, float c, float d);
float GE_DLL quadInOut(float t, float b, float c, float d);

float GE_DLL quartIn(float t, float b, float c, float d);
float GE_DLL quartOut(float t, float b, float c, float d);
float GE_DLL quartInOut(float t, float b, float c, float d);

float GE_DLL quintIn(float t, float b, float c, float d);
float GE_DLL quintOut(float t, float b, float c, float d);
float GE_DLL quintInOut(float t, float b, float c, float d);

float GE_DLL sineIn(float t, float b, float c, float d);
float GE_DLL sineOut(float t, float b, float c, float d);
float GE_DLL sineInOut(float t, float b, float c, float d);

// template<typename Type>
// void compute(Vector2<Type> &vec, Func &&fn, float t, 
//              Vector2<Type> const &b, 
//              Vector2<Type> const &c,
//              float d) {
//   for (size_t i = 0; i < Vector2<Type>::size; ++i)
//     vec[i] = fn(t, b[i], c[i], d);
// }

template<typename Type>
void GE_DLL compute(Vector3<Type> &vec, Func &&fn, float t,
             Vector3<Type> const &b, 
             Vector3<Type> const &c,
             float d) {
  for (size_t i = 0; i < Vector3<Type>::size; ++i)
    vec[i] = fn(t, b[i], c[i], d);
}

// template<typename Type>
// void compute(Vector4<Type> &vec, Func &&fn, float t, 
//              Vector4<Type> const &b, 
//              Vector4<Type> const &c,
//              float d) {
//   for (size_t i = 0; i < Vector4<Type>::size; ++i)
//     vec[i] = fn(t, b[i], c[i], d);
// }

// template<typename T>
// void compute(T &value, Func &&fn, float t, float b, float c, float d) {
//   value = fn(t, b, c, d);
// }

}

}

#pragma once

template<typename Type>
Vector3<Type>::Vector3(Type const &value) {
  set(value, value, value);
}

template<typename Type>
Vector3<Type>::Vector3(Type const &x, Type const &y, Type const &z)
{
	set(x, y, z);
}

template<typename Type>
Vector3<Type>::Vector3(Vector3 const &other)
: x(other.x)
, y(other.y)
, z(other.z) {}

template<typename Type>
Vector3<Type> &Vector3<Type>::operator=(Vector3 const &other) {
  if (this != &other) {
    x = other.x;
    y = other.y;
    z = other.z;
  }
  return *this;
}

template<typename Type>
Vector3<Type>::~Vector3() {}

template<typename Type>
void Vector3<Type>::set(Type const &x, Type const &y, Type const &z) {
  data[0] = x;
  data[1] = y;
  data[2] = z;
}

template<typename Type>
void Vector3<Type>::reset() {
  data[0] = 0;
  data[1] = 0;
  data[2] = 0;
}

template<typename Type>
std::string Vector3<Type>::toString() const {
  return "vec3(" + ge::convert<Type, std::string>(x)
          + ", " + ge::convert<Type, std::string>(y)
          + ", " + ge::convert<Type, std::string>(z) + ")";
}

template<typename Type>
void Vector3<Type>::fromString(std::string const &) {
  // TODO
}

template<typename Type>
bool Vector3<Type>::operator==(Vector3<Type> const &other) const {
  return VectorUtils<Vector3>::equals(*this, other);
}

template<typename Type>
bool Vector3<Type>::operator!=(Vector3<Type> const &other) const {
  return !VectorUtils<Vector3>::equals(*this, other);
}

template<typename Type>
Type const &Vector3<Type>::operator[](size_t i) const {
  return data[i];
}

template<typename Type>
Type &Vector3<Type>::operator[](size_t i) {
  return data[i];
}

template<typename Type>
Vector3<Type> &Vector3<Type>::operator*=(float n) {
  for (size_t i = 0; i < 3; ++i)
    data[i] *= n;
  return *this;
}

template<typename Type>
Vector3<Type> Vector3<Type>::operator*(float n) const {
  Vector3<Type> result = *this;
  result *= n;
  return result;
}

template<typename Type>
Vector3<Type> &Vector3<Type>::operator+=(Vector3<Type> const &other) {
  return (*this = *this + other);
}

template<typename Type>
Vector3<Type> &Vector3<Type>::operator-=(Vector3<Type> const &other) {
  return (*this = *this - other);
}

template<typename Type>
Vector3<Type> &Vector3<Type>::operator*=(Vector3<Type> const &other) {
  return (*this = *this * other);
}

template<typename Type>
Vector3<Type> &Vector3<Type>::operator/=(Vector3<Type> const &other) {
  return (*this = *this / other);
}


template<typename Type>
Vector3<Type> Vector3<Type>::operator+(Vector3 const &other) const {
  return VectorUtils<Vector3>::add(*this, other);
}

template<typename Type>
Vector3<Type> Vector3<Type>::operator-(Vector3 const &other) const {
  return VectorUtils<Vector3>::sub(*this, other);
}

template<typename Type>
Vector3<Type> Vector3<Type>::operator*(Vector3 const &other) const {
  return VectorUtils<Vector3>::mul(*this, other);
}

template<typename Type>
Vector3<Type> Vector3<Type>::operator/(Vector3 const &other) const {
  return VectorUtils<Vector3>::div(*this, other);
}

template<typename Type>
Vector3<Type> Vector3<Type>::operator^(Vector3<Type> const &other) const {
  Vector3 result = *this;
  return result.cross(other);
}

template<typename Type>
Vector3<Type> &Vector3<Type>::operator^=(Vector3 const &other) const {
  return cross(other);
}

template<typename Type>
Vector3<Type> &Vector3<Type>::cross(Vector3 const &other) {
  (*this)[0] = (*this)[1] * other[2] - (*this)[2] * other[1];
  (*this)[1] = (*this)[2] * other[0] - (*this)[0] * other[2];
  (*this)[2] = (*this)[0] * other[1] - (*this)[1] * other[0];
  return *this;
}

template<typename Type>
Type Vector3<Type>::dot() const {
  return VectorUtils<Vector3>::dot(*this);
}

template<typename Type>
Type Vector3<Type>::squareLength() const {
  return VectorUtils<Vector3>::squareLength(*this);
}

template<typename Type>
Type Vector3<Type>::length() const {
  return VectorUtils<Vector3>::length(*this);
}

template<typename Type>
void Vector3<Type>::normalize() {
  VectorUtils<Vector3>::normalize(*this);
}

template<typename Type>
void Vector3<Type>::min(Vector3 const &other) {
  *this = VectorUtils<Vector3>::min(*this, other);
}

template<typename Type>
void Vector3<Type>::max(Vector3 const &other) {
  *this = VectorUtils<Vector3>::max(*this, other);
}

#pragma once

#include <gEngine/Utils/Convert.hpp>
#include <gEngine/Math/VectorUtils.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Vector3
 * Simple 3 dimentions vector object.
 */
template<typename Type>
struct GE_DLL Vector3 {

  Vector3(Type const &value = 0);

  Vector3(Type const &x, Type const &y, Type const &z);

  Vector3(Vector3 const &other);

  Vector3 &operator=(Vector3 const &other);

  ~Vector3();

  inline void set(Type const &x, Type const &y, Type const &z);

  inline void reset();

  Type const &operator[](size_t i) const;

  Type &operator[](size_t i);

  /**
   * Get the string representation of the Vector3.
   */
  std::string toString() const;

  /**
   * Create the vector3 from its string representation.
   */
  void fromString(std::string const &str);

  bool operator==(Vector3 const &other) const;
  bool operator!=(Vector3 const &other) const;


  Vector3 &operator+=(Vector3 const &);
  Vector3 &operator-=(Vector3 const &);
  Vector3 &operator*=(Vector3 const &);
  Vector3 &operator/=(Vector3 const &);

  Vector3 operator+(Vector3 const &) const;
  Vector3 operator-(Vector3 const &) const;
  Vector3 operator*(Vector3 const &) const;
  Vector3 operator/(Vector3 const &) const;

  Vector3 operator^(Vector3 const &) const;
  Vector3 &operator^=(Vector3 const &) const;

  Vector3 &cross(Vector3 const &other);

  Type dot() const;

  Type squareLength() const;

  Type length() const;

  void normalize();

  Vector3 &operator*=(float);
  Vector3 operator*(float) const;

  void min(Vector3 const &other);

  void max(Vector3 const &other);

  /**
   * Number of fields.
   */
  enum { size = 3 };

  /**
   * Data reprensentation.
   */
  union {
    Type data[size];
    struct { Type x, y, z; };
    struct { Type r, g, b; };
  };
};

typedef Vector3<float> Vector3f;
typedef Vector3<int> Vector3i;
typedef Vector3<size_t> Vector3u;

#include <gEngine/Math/Vector3.inl>

}


#pragma once

#include <gEngine/Math/Matrix4.hpp>
#include <gEngine/Math/Quaternion.hpp>
#include <memory>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Transform
 * Data structure to reprensent the position, rotation and scale of an object.
 */
class GE_DLL Transform {
public:
  /**
   * Default constructor, do nothing.
  */
  Transform();
  /**
   * Empty destructor, do nothing.
  */
  ~Transform();
  /**
   * Getters
  */
  Vector3f const&   getPosition() const;
  Vector3f const&   getGlobalPosition() const;

  Quaternionf const&   getRotation() const;
  Quaternionf const&   getGlobalRotation() const;

  Vector3f const&   getScale() const;
  Vector3f const&   getGlobalScale() const;

  /**
   * Setters.
  */
  void    setPosition(float x, float y, float z, bool relative = false);
  void    setPosition(Vector3f const &, bool relative = false);

  void    translate(float x, float y, float z, bool relative = false);
  void    translate(Vector3f const &, bool relative = false);

  void    setRotation(float x, float y, float z);
  void    setRotation(Vector3f const &);
  void    setRotation(Quaternionf const &);

  void    rotate(float x, float y, float z);
  void    rotate(Vector3f const &);
  void    rotate(Quaternionf const &);

  void    setScale(float x, float y, float z);
  void    setScale(Vector3f const &);

  void    scale(float x, float y, float z);
  void    scale(Vector3f const &);

  Matrix4f const &calcLocalMatrix();
  Matrix4f const &getLocalMatrix() const;

  Matrix4f const &calcGlobalMatrix();
  Matrix4f const &calcGlobalMatrix(Transform const &parent);
  Matrix4f const &getGlobalMatrix() const;

  bool    getGlobalMatrixChanged() const;
  void    resetGlobalMatrixChanged(bool b);

  void    lookAt(Vector3f const &);
  
private:
  Vector3f  _position;
  Vector3f  _globalPosition;
  Quaternionf      _rotation;
  Quaternionf      _globalRotation;
  Vector3f  _scale;
  Vector3f  _globalScale;
  bool            _localMatrixUpdate;
  Matrix4f        _localMatrix;
  bool            _globalMatrixUpdate;
  Matrix4f        _globalMatrix;
  bool            _globalMatrixUpdated;
};

/**
 * @typedef TransformPtr
 * Typedef on transform class.
 */
typedef std::shared_ptr<Transform> TransformPtr;

}


#pragma once

#include <gEngine/System/Platform.hpp>

namespace ge {

template<typename VectorType>
struct GE_DLL VectorUtils {

  template<typename BoxType>
  static VectorType cubicRandom(BoxType const &box);

  static VectorType add(VectorType const &first,
                        VectorType const &second);

  static VectorType sub(VectorType const &first,
                        VectorType const &second);

  static VectorType mul(VectorType const &first,
                        VectorType const &second);

  static VectorType div(VectorType const &first,
                        VectorType const &second);

  static VectorType min(VectorType const &first,
                        VectorType const &second);

  static VectorType max(VectorType const &first,
                        VectorType const &second);

  template<typename Type>
  static Type dot(VectorType const &first, VectorType const &second);

  template<typename Type>
  static Type squareLength(VectorType const &vec);

  template<typename Type>
  static Type length(VectorType const &vec);

  static void normalize(VectorType &vec);

  static bool equals(VectorType const &first,
                     VectorType const &second);

};

#include <gEngine/Math/VectorUtils.inl>

}
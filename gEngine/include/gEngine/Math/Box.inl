
#pragma once

template<typename Type>
Box<Type>::Box(Type const &width,
               Type const &height,
               Type const &depth,
               Type const &x,
               Type const &y,
               Type const &z)
: pos{x, y, z}
, size{width, height, depth} {}

template<typename Type>
Box<Type>::~Box() {}

template<typename Type>
Box<Type>::Box(Box const &other)
: pos(other.pos)
, size(other.size) {}

template<typename Type>
Box<Type> &Box<Type>::operator=(Box const &other) {
  if (this != &other) {
    pos = other.pos;
    size = other.size;
  }
  return *this;
}

template<typename Type>
Type const &Box<Type>::operator[](size_t i) const {
  return data[i];
}

template<typename Type>
Type &Box<Type>::operator[](size_t i) {
  return data[i];
}

template<typename Type>
Vector3<Type> Box<Type>::center() const {
  return Vector3<Type>(pos.x + size.x / 2, pos.y + size.y / 2, pos.z + size.z / 2);
}


template<typename Type>
bool Box<Type>::hit(Box const &other) const {
  return (pos.x + size.x >= other.pos.x && pos.x <= other.pos.x + other.size.x
     && (pos.y + size.y >= other.pos.y && pos.y <= other.pos.y + other.size.y)
     && (pos.z + size.z >= other.pos.z && pos.z <= other.pos.z + other.size.z));
}

template<typename Type>
bool Box<Type>::hit(Vector3<Type> const &pt) const {
  return pt.x >= pos.x && pt.x <= pos.x + size.x &&
         pt.y >= pos.y && pt.y <= pos.y + size.y &&
         pt.z >= pos.z && pt.z <= pos.z + size.z;
}

template<typename Type>
std::string Box<Type>::toString() const {
  return "box(" + ge::convert<Type, std::string>(size.x)
         + ", " + ge::convert<Type, std::string>(size.y)
         + ", " + ge::convert<Type, std::string>(size.z)
         + ", " + ge::convert<Type, std::string>(pos.x)
         + ", " + ge::convert<Type, std::string>(pos.y)
         + ", " + ge::convert<Type, std::string>(pos.z) + ")";
}
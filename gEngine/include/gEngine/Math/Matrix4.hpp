#pragma once

#include <gEngine/Math/Utils.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Vector2.hpp>
#include <gEngine/Math/Quaternion.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Matrix4
 * Data structure to reprensent matrix.
 */
template<typename Type>
class GE_DLL Matrix4 {
public:
  /**
   * Default constructor, do nothing.
  */
  Matrix4();

  /**
    * Contructor with initialization.
  */
  Matrix4( Type v1, Type v2, Type v3, Type v4,
          Type v5, Type v6, Type v7, Type v8,
          Type v9, Type v10, Type v11, Type v12,
          Type v13, Type v14, Type v15, Type v16);

  /**
   * Empty destructor, do nothing.
  */
  ~Matrix4();

  Matrix4 &operator=(Matrix4 const &);

  /**
    * Getter
  */
  const Type *get() const;
  Type const *operator *() const;

  Type const &operator()(size_t x, size_t y) const;

  /**
    * Translation
  */
  void        translate(Type x = 0, Type y = 0, Type z = 0);
  void        translate(Vector3<Type> const &);

  /**
    * Rotation
  */
  void        rotate(Type x = 0, Type y = 0, Type z = 0);
  void        rotate(Vector3<Type> const &);
  void        rotate(Quaternion<Type> const &);


  /**
    * Scale
  */
  void        scale(Type x = 0, Type y = 0, Type z = 0);
  void        scale(Vector3<Type> const &);

  /**
    * Combine
  */
  void     combine(Matrix4 const &);

  /**
   * Transpose
   */
  void transpose(Matrix4 const &);

  /**
   * Look at
   */
  void lookAt(ge::Vector3<Type> const &eye,
              ge::Vector3<Type> const &center,
              ge::Vector3<Type> const &up);

  /**
   * Perspective
   */
  void perspective(Type const &fov, Type const &aspect,
                   Type const &near, Type const &far);


  /**
   * Ortho
   */
  void ortho(Type const &left, Type const &right,
             Type const &bottom, Type const &top,
             Type const &near, Type const &far);


  /**
    * Identity
  */
  void     identity();

  bool operator==(Matrix4 const &) const;

private:
  Type _calcCombine(int col, int row, Matrix4 const &other);
  Type   _matrix[16];
};

typedef Matrix4<float> Matrix4f;
typedef Matrix4<int> Matrix4i;
typedef Matrix4<size_t> Matrix4s;

#include <gEngine/Math/Matrix4.inl>

}

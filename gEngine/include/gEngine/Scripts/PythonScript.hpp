
// #pragma once

// #include <Python.h>
// #include <string>
// #include <vector>
// #include <gEngine/Scripts/Script.hpp>

// /**
//  * @namespace ge
//  * Generic game engine with a easy-to-use and powerful
//  * entity/component system.
//  */
// namespace ge {

// class PythonScript : public Script {
// public:
//   PythonScript();
//   ~PythonScript();
//   bool loadFromFile(std::string const &path);
//   void start();
//   void update(float dt);
//   template<typename Type>
//   void set(std::string const &name, Type const &value);
//   template<typename Type>
//   Type get(std::string const &name);
// private:
//   static std::vector<std::string> _paths;
//   PyObject *_module;
//   PyObject *_class;
//   PyObject *_instance;
// };

// }

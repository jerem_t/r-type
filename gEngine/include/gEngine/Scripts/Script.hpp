
#pragma once

#include <string>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

class Script {
public:
  virtual ~Script() {}
  virtual bool loadFromFile(std::string const &path) = 0;
  virtual void start() = 0;
  virtual void update(float dt) = 0;
};

}
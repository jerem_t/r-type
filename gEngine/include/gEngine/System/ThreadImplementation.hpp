//
// WThread.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sun Nov  3 14:49:22 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:17:02 2013 jordy domingos-mansoni
//

#ifndef	WTHREAD_HH_
#define	WTHREAD_HH_

#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_UNIX
# include <pthread.h>
# include <sys/types.h>
# include <unistd.h>
  typedef pid_t ThreadId;
  typedef pthread_t ThreadType;
#elif defined(GE_SYSTEM_WINDOWS)
# include <windows.h>
  typedef DWORD ThreadId;
  typedef HANDLE ThreadType;
#endif

class Thread;

#include <gEngine/System/AThreadImplementation.hpp>

class GE_DLL ThreadImplementation : public AThreadImplementation
{
public:
  ThreadImplementation(Thread *owner);
  
  bool joinable() const;
  void join();
  void detach();

  ~ThreadImplementation() {};
private:
  ThreadImplementation(const ThreadImplementation& other);
  ThreadImplementation &operator=(const ThreadImplementation& other);

  AThreadImplementation::Status status_;
  ThreadType	thread_;
  ThreadId     	id_;
};


#endif	/* WTHREAD_HH_ */

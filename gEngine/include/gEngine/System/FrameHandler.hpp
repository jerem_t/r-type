
#pragma once

#include <gEngine/Utils/EventDispatcher.hpp>
#include <gEngine/System/Clock.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class FrameHandler
 * Handle frame regulation, trigger "frame" events for each
 * frames, and limit the number or frames for one second.
 */
class GE_DLL FrameHandler : public EventDispatcher {
public:

  /**
   * Default constructor, do nothing.
   */
  FrameHandler();

  /**
   * Empty destructor, do nothing.
   */
  ~FrameHandler();

  /**
   * Set the number of frames to display in one second.
   */
  void setFrameRate(size_t frames);

  /**
   * Return the number of frames to display in one second.
   */
  size_t getFrameRate() const;


  size_t getFps() const;
  
  /**
   * True if you want to run the loop into a specific thread,
   * false otherwise.
   */
  void setAsync(bool async);

  /**
   * Start the frame handler into a loop.
   * @param async 
   */
  void start();

  /**
   * Stop the loop.
   */
  void stop();

private:

  /**
   * Start the loop.
   */
  void _loop();

  size_t  _frameRate; /** The number of frames for one second */
  bool    _async;     /** Is the loop asynchrounous or not? */
  bool    _started;   /** True if the loop in running */
  Clock   _clock;     /** Clock in order to know elapsed time */
  float   _fps;

};

}
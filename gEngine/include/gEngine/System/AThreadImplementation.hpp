//
// ThreadImplementation.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sun Nov  3 21:34:47 2013 jordy domingos-mansoni
// Last update Mon Nov  4 12:43:00 2013 jordy domingos-mansoni
//

#ifndef	ATHREAD_IMPLEMENTATION_HH_
#define	ATHREAD_IMPLEMENTATION_HH_

#include <gEngine/System/Platform.hpp>

class Thread;

template<typename Callable, typename... Args>
struct FunctionAbstraction;

class GE_DLL AThreadImplementation {
public:  
  enum	Status
    {
      STARTED,
      FINISHED,
      NONE
    };

  static void* launch(void *userData);
  virtual bool joinable() const = 0;
  virtual void join() = 0;
  virtual void detach() = 0;

  virtual ~AThreadImplementation() {};
};

#endif /* THREAD_IMPLEMENTATION_HH_ */

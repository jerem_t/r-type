
#pragma once

#include <chrono>
#include <gEngine/System/Time.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Clock
 * Simple class to simplify time utilisation
 * by using std::chrono
 */
class GE_DLL Clock {
public:

  /**
   * Default tag constructor, do nothing.
   */
  Clock();

  /**
   * Empty destructor, do nothing.
   */
  ~Clock();

  /**
    * Start the clock
    */
  void          start();

  /**
    * Reset the clock
    */
  void          reset();

  /**
    * Pause the clock
    */
  void          pause();

  /**
    * Resume the clock
    */
  void          resume();

  /**
    * Get elapsed time
    */
  Seconds        getElapsed();

  /**
    * Get the total elapsed time
    */
  Seconds        getTotalElapsed();

  static size_t	 getTimeStamp();

private:
  /**
   * @typedef Clock
   * Shortcut on the clock to use.
   */
  typedef std::chrono::high_resolution_clock SysClock;

private:

  SysClock::time_point  _previousTime;
  SysClock::time_point  _timeAtBeginning;
  SysClock::time_point  _timeAtPause;
  Seconds               _totalTimePause;
  Seconds               _currTimePause;
  SysClock              _clock;

};

}
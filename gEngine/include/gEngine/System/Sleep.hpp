
#pragma once

#include <gEngine/System/Time.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * Sleep for the given time into the current process.
 */
void GE_DLL sleep(Seconds seconds);

}

#pragma once

#include <gEngine/System/Platform.hpp>

#include <chrono>
/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

typedef float	Seconds;
typedef std::chrono::system_clock::duration	TimeStamp;

inline Seconds GE_DLL MicroSeconds(float value) {
  return value / 1000000.0f;
}

inline Seconds GE_DLL MilliSeconds(float value) {
  return value / 1000.0f;
}

inline Seconds GE_DLL Minute(float value) {
  return value * 60.0f;
}

inline Seconds GE_DLL Hour(float value) {
  return value * 3600.0f;
}

}
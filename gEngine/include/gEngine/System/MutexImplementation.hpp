//
// MutexImplementation.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Mon Nov  4 12:50:30 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:17:35 2013 jordy domingos-mansoni
//

#ifndef		MUTEX_IMPLEMENTATION_HH_
#define		MUTEX_IMPLEMENTATION_HH_

#include <gEngine/System/Platform.hpp>

#if defined(GE_SYSTEM_WINDOWS)
# include <windows.h>
  typedef CRITICAL_SECTION MutexType;
#elif defined(GE_SYSTEM_UNIX)
# include <pthread.h>
# include <sys/types.h>
# include <unistd.h>
  typedef pthread_mutex_t MutexType;
#endif

#include <gEngine/System/IMutexImplementation.hpp>

class GE_DLL MutexImplementation : public IMutexImplementation {
public:
  MutexImplementation();

  void lock();
  void trylock();
  void unlock();

  ~MutexImplementation();

private:
  MutexImplementation(const MutexImplementation& other);
  MutexImplementation &operator=(const MutexImplementation& other);

  IMutexImplementation::Status status_;
  MutexType	mutex_;
};

#endif		/* MUTEX_IMPLEMENTATION_HH_ */

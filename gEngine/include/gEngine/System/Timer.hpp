
#pragma once

#include <gEngine/System/Time.hpp>
#include <gEngine/Utils/EventDispatcher.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Timer
 * Simple timer
 */
class GE_DLL Timer : public EventDispatcher {
public:
  Timer();
  ~Timer();
  void setRepeat(bool repeat);
  void setFinish(Seconds seconds);
  Seconds getFinish() const;
  void setInterval(Seconds seconds);
  Seconds getInterval() const;
  void update(Seconds dt);
  void start();
  void stop();
  bool hasStarted() const;
private:
  Seconds _finish;
  Seconds _interval;
  Seconds _current;
  size_t _currentTick;
  Seconds _total;
  bool _repeat;
  bool _started;
};

}
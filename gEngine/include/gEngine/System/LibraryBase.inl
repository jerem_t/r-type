
#pragma once

template<typename FunctionType>
FunctionType LibraryBase::getSymbol(std::string const &name) const {
  if (_handler == NULL)
    throw std::runtime_error("Cannot found library \"" + _path + "\".");
  void *fn = _getSymbol(name);
  if (fn == NULL)
    throw std::runtime_error("Cannot load symbol \"" + name + "\"");
  return reinterpret_cast<FunctionType>(fn);
}

template<typename FunctionType, typename ...Args>
void LibraryBase::call(std::string const &name, Args const &...args) {
  getSymbol<FunctionType>(name)(args...);
}
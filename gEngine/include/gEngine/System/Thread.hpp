//
// Thread.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Sun Nov  3 12:11:11 2013 jordy domingos-mansoni
// Last update Wed Nov  6 14:56:56 2013 jordy domingos-mansoni
//

#ifndef	MY_THREAD_HH_
#define	MY_THREAD_HH_

#include <functional>
#include <tuple>
#include <type_traits>
#include <gEngine/Utils/Callable.hpp>
#include <gEngine/System/ThreadImplementation.hpp>

class GE_DLL Thread
{
public:
  template<typename Callable, typename... Args>
  explicit Thread(Callable&& f, Args&&... args);
  
  bool joinable() const;
  void join();
  void detach();
  void run();

  ~Thread();
private:
  Thread(const Thread& other);
  Thread &operator=(const Thread& other);
  Thread();

  ThreadImplementation	*impl_;
  ge::CallablePtr		func_abstraction_;
};

template<typename Callable, typename... Args>
Thread::Thread(Callable&& f, Args&&... args):
  impl_(new ThreadImplementation(this)),
  func_abstraction_(ge::makeCallablePtr(f, args...)) {
}

#endif	/* MY_THREAD_HH_ */

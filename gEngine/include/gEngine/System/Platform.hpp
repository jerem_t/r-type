
#pragma once

// Identify the operating system.
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
# define GE_SYSTEM_WINDOWS
#elif defined(linux) || defined(__linux) || defined(__linux__)
# define GE_SYSTEM_LINUX
#elif defined(__APPLE__) || defined(MACOSX) || defined(macintosh) || defined(Macintosh)
# define GE_SYSTEM_MACOS
#elif defined(__FreeBSD__) || defined(__FreeBSD_kernel__)
# define GE_SYSTEM_FREEBSD
#else
# error This operating system is not supported by gEngine
#endif

#if defined(GE_SYSTEM_LINUX) || defined(GE_SYSTEM_MACOS) || defined(GE_SYSTEM_FREEBSD)
# define GE_SYSTEM_UNIX
#endif

// Define compiler.
#ifdef _MSC_VER
# define GE_COMPILER_VISUAL _MSC_VER
#elif defined(__GNUC__)
# define GE_COMPILER_GNU __GNUC__
#endif

// Define portable import/export macros.
#ifdef GE_SYSTEM_WINDOWS
# define GE_API_EXPORT __declspec(dllexport)
# define GE_API_IMPORT __declspec(dllimport)
# define GE_DLL
#elif GE_COMPILER_GNU >= 4
# define GE_API_EXPORT __attribute__ ((__visibility__ ("default")))
# define GE_API_IMPORT __attribute__ ((__visibility__ ("default")))
# define GE_DLL
#else
# define GE_API_EXPORT
# define GE_API_IMPORT
#endif

#define INLINE     inline
#define ALIGN_16   __attribute__((aligned (16)))

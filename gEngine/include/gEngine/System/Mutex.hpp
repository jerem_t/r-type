//
// Mutex.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Mon Nov  4 12:46:06 2013 jordy domingos-mansoni
// Last update Mon Nov  4 17:17:44 2013 jordy domingos-mansoni
//

#ifndef MUTEX_HH_
#define MUTEX_HH_

#include <gEngine/System/MutexImplementation.hpp>

class GE_DLL Mutex {
public:
  Mutex();

  void lock();
  void trylock();
  void unlock();

  ~Mutex();
private:
  Mutex(const Mutex&);
  Mutex &operator=(const Mutex&);

  MutexImplementation *impl_;
};

#endif	/* MUTEX_HH_ */

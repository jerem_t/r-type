//
// IMutexImplementation.hh for  in /home/doming_a//Project/Tech3
// 
// Made by jordy domingos-mansoni
// Login   <doming_a@epitech.net>
// 
// Started on  Mon Nov  4 12:41:09 2013 jordy domingos-mansoni
// Last update Mon Nov  4 14:39:58 2013 jordy domingos-mansoni
//

#ifndef	IMUTEX_IMPLEMENTATION_HH_
#define IMUTEX_IMPLEMENTATION_HH_

#include <gEngine/System/Platform.hpp>

class GE_DLL IMutexImplementation {
public:
  enum	Status
    {
      LOCK,
      UNLOCK
    };

  virtual void lock() = 0;
  virtual void trylock() = 0;
  virtual void unlock() = 0;

  virtual ~IMutexImplementation() {};
};

#endif /* IMUTEX_IMPLEMENTATION_HH_ */

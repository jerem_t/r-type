
#pragma once

#include <gEngine/System/Platform.hpp>
#include <stdexcept>
#include <string>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class LibraryBase
 * Simple utility to load dynamic libraries symbols.
 */
class GE_DLL LibraryBase {
public:

  /**
   * Default constructor, do nothing.
   */
	LibraryBase() {}

  /**
   * Empty destructor, do nothing.
   */
	virtual ~LibraryBase() {}

  /**
   * Load the shared library from the given path.
   * @param path The path to the library file.
   */
  virtual bool loadFromFile(std::string const &path) = 0;

  /**
   * Close this library.
   */
  virtual void close() = 0;

  /**
   * Get the symbol into the library from its name.
   * @param name The function name's to use.
   */
  template<typename FunctionType>
  FunctionType getSymbol(std::string const &name) const;

  /**
   * Function used to call a function from its name and its
   * parameters.
   * @param name Function's name.
   * @param args Function's parameters.
   */
  template<typename FunctionType, typename ...Args>
  void call(std::string const &name, Args const &...args);

  /**
   * Return the path to the library.
   */
  std::string const &getPath() const;

protected:

  virtual void *_getSymbol(std::string const &path) const = 0;

  std::string _path;  /** The path to the library path */
  void *_handler;     /** A reference on the shared library. */

};

#include <gEngine/System/LibraryBase.inl>

/**
 * @class Library
 * The implementation of the library class.
 */
class Library : public LibraryBase {
public:
	Library() {}
	~Library() {}
	virtual bool loadFromFile(std::string const &path);
	virtual void close();
private:
	virtual void *_getSymbol(std::string const &name) const;
};

}
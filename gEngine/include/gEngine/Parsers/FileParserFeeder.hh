
#pragma once

#include <gEngine/Parsers/AParserFeeder.hh>

#include <fstream>

class GE_DLL FileParserFeeder : public AParserFeeder
{
private:
	std::ifstream 		_file;
	size_t 				_alreadyRead;
	size_t 				_fileSize;

public:
	FileParserFeeder(std::string const &file);
	virtual ~FileParserFeeder();

	virtual std::string 	getChunk();
};
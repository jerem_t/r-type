
#pragma once

#include <gEngine/Parsers/AParserFeeder.hh>

class StringFeeder : public AParserFeeder
{
private:
	std::string 	_src;

public:
	StringFeeder(std::string const &src);
	virtual ~StringFeeder();

	virtual std::string 	getChunk();
};
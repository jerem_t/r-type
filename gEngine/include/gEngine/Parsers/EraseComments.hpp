
#pragma once

#include <string>

namespace ge {

/**
 * Erase comments from a string.
 * @param begin the begin token of the comment.
 * @param end the end token of the comment.
 * @param str the string to epurate.
 */
inline void eraseComments(std::string const &begin,
                  std::string const &end,
                  std::string &str) {
  while (true) {
    size_t beginPos = str.find(begin);
    size_t endPos = str.find(end);
    if (beginPos == std::string::npos)
      return ;
    std::string a = str.substr(0, beginPos);
    if (endPos != std::string::npos)
      endPos += 2;
    std::string b = str.substr(endPos);
    str = a + b;
  }
}

}

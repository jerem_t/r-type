
#pragma once

#define JSON_USE_EXCEPTIONS

/**
 * Include some dependencies from STL.
 */
#include <iostream>
#include <stdexcept>
#include <typeinfo>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <gEngine/Resources/Loadable.hpp>
#include <gEngine/System/Platform.hpp>

#ifdef JSON_USE_EXCEPTIONS
# define JSON_THROW(type, msg) throw type(msg);
#else
# define JSON_THROW(type, msg)
#endif

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Json
 * This class represente a json object to parse/stringify
 * json like in javascript.
 */
class GE_DLL Json : public Loadable {
private:

  /**
   * @enum TokenType
   * Used to know the type of the current token.
   */
  enum TokenType {
    T_UNDEFINED,
    T_STRING,
    T_NUMBER,
    T_LEFT_BRACE,
    T_RIGHT_BRACE,
    T_LEFT_BRACKET,
    T_RIGHT_BRACKET,
    T_COMMA,
    T_COLON,
    T_BOOLEAN,
    T_NULL
  };

  /**
   * @struct Token
   * Used to tokenize json string.
   */
  struct Token {
    std::string value;
    TokenType type;
    Token(std::string value="", TokenType type = T_UNDEFINED)
    : value(value)
    , type(type)
    {}
  };

public:
  /**
   * @enum Type
   * Represente the type of the current Json node.
   */
  enum Type {
    String,
    Object,
    Array,
    Bool,
    Number,
    Null,
    Undefined
  };

public:

  /**
   * Default constructor.
   */
  Json();

  /**
   * Construct a new Json object according a type.
   */
  Json(Type t, std::string const &v);

  /**
   * Copy constructor.
   */
  Json(Json const &other);

  /**
   * Create json object from string value.
   */
  Json(std::string const &value);

  /**
   * Create json object from char * value.
   */
  Json(char const *value);

  /**
   * Create json object from double value.
   */
  Json(double value);

  /**
   * Create json object from boolean value.
   */
  Json(bool value);

  /**
   * Assignement operator.
   */
  Json &operator=(Json const &other);

  /**
   * Assign the given number to this Json value.
   */
  Json &operator=(unsigned int value);

  /**
   * Assign the given number to this Json value.
   */
  Json &operator=(double value);

  /**
   * Assign the given number to this Json value.
   */
  Json &operator=(int value);

  /**
   * Assign the given boolean to this Json value.
   */
  Json &operator=(bool value);

  /**
   * Assign the given string to this Json value.
   */
  Json &operator=(std::string const &value);

  /**
   * Assign the given char * to this Json value.
   */
  Json &operator=(char const *value);

  /**
   * Clear all json object content like if was just created.
   */
  void clear();

  /**
   * Return a stringified Json object.
   */
  std::string toString() const;

  /**
   * Return the type of the json object.
   */
  Type getType();

  /**
   * Add a property to a Json object.
   *
   * @param key the property key.
   * @param v the property value.
   */
  void push(std::string key, Json const &v);

  /**
   * Add a value to a Json _array.
   *
   * @param v the value.
   */
  void push(Json const &v);

  /**
   * Return double value.
   */
  operator double() const;

  /**
   * Return bool value.
   */
  bool is() const;

  /**
   * Return string value.
   */
  operator std::string const &() const;

  /**
   * Return the number of elems into
   * the current json object.
   */
  size_t getSize() const;

  /**
   * Derefence Json array or object from the
   * given index.
   */
  Json &operator[](size_t i);

  /**
   * Dereference Json object from the given key.
   */
  Json &operator[](std::string const &s);

  /**
   * Test the type of the Json value.
   */
  bool operator==(Type type);

  /**
   * Parse the given string to create a Json object.
   */
  virtual bool loadFromMemory(std::string const &str);

  /**
   * Simple stream operator to serialize json into a string.
   */
  void operator>>(std::string &input);

private:

  // All after all is private stuffs, nothing interesting about
  // lib usage ;)

  /**
   * Simple helper to convert any variable to another.
   *
   * @param From Convert from the given value.
   * @param To Convert to the result value.
   */
  template<typename From, typename To>
  static To _convert(From const &val) {
    std::stringstream ss;
    To res;

    ss << val;
    ss >> res;
    return res;
  }

  /**
   * Add some spaces to the current line.
   */
  std::string _addSpaces(int d) const;

  /**
   * Stringify the json, indented by `d`.
   */
  std::string _toString(int d) const;

  /**
   * Return if the given char is a whitespace or not.
   */
  bool _isSpace(char c) const;

  /**
   * Return the index of the next space.
   */
  int _nextSpace(std::string const &data, int i) const;

  /**
   * Skip spaces to the next token.
   */
  int _skipSpaces(std::string const &data, int i) const;

  void _check_string(size_t &k, std::string const &token);

  void _check_comma(size_t &k, std::string const &);

  void _check_true(size_t &k, std::string const &token);

  void _check_false(size_t &k, std::string const &token);

  void _check_null(size_t &k, std::string const &token);

  void _check_undefined(size_t &k, std::string const &token);

  void _check_right_brace(size_t &k, std::string const &);

  void _check_left_brace(size_t &k, std::string const &);

  void _check_right_bracket(size_t &k, std::string const &);

  void _check_left_bracket(size_t &k, std::string const &);

  void _check_colon(size_t &k, std::string const &);

  bool _check_digit(size_t &k, std::string const &token);

  /**
   * Tokenize the given string.
   */
  void _tokenize(std::string const &data);

  /**
   * Parse the given token and get Json object.
   *
   * @todo Refactoring this ugly parsing and modify
   * the current instance.
   */
  Json _parse(std::vector<Token> v, int i, int& r) const;

private:
  std::string _value;
  Type _type;
  std::vector<std::pair<std::string, Json> > _props;
  std::map<std::string, int> _index;
  std::vector<Json> _arr;
  std::vector<Token> _tokens;

};

}
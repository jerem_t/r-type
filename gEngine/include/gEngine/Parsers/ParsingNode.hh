
#pragma once

#include <gEngine/Core/Node.hpp>

class GE_DLL ParsingNode : public ge::Node<ParsingNode>
{
private:
	bool 		_result;
	std::string _value;

public:
	ParsingNode(bool result = false);
	~ParsingNode();

	bool 		getResult() const;
	void 		setResult(bool res);

	std::string	getValue() const;
	void 		setValue(std::string const &value);
};
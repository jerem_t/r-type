
#pragma once

#include <string>
#include <gEngine/System/Platform.hpp>

class GE_DLL AParserFeeder
{
protected:
	size_t 		_chunkSize;

public:
	AParserFeeder(size_t chunkSize);
	virtual ~AParserFeeder();

	virtual std::string 	getChunk() = 0;

	size_t					getChunkSize() const;
	void					setChunkSize(size_t size);
};
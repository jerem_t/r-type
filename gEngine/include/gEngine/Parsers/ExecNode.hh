
#pragma once

#include <gEngine/Core/Node.hpp>
#include <gEngine/Parsers/BnfParser.hh>

#include <list>

class GE_DLL ExecNode : public ge::Node<ExecNode>
{
public:
	enum ERuleLinker
	{
		OR,
		AND
	};

	enum ERuleQualifier
	{
		STAR,
		PLUS,
		QUES,
		NONE
	};

	typedef bool 							(BnfParser::*t_basicRule)(std::string const &);
	typedef std::shared_ptr<ParsingNode> 	(ExecNode::*t_rule)(BnfParser &) const;
	typedef std::shared_ptr<ParsingNode>	(ExecNode::*t_qualif)(t_rule, BnfParser &) const;

	static std::map<ERuleQualifier, t_qualif> 	_qualifiers;

private:
	ERuleLinker			_link;
	ERuleQualifier 		_qualifier;
	t_basicRule 		_basicRule;
	std::string 		_basicRuleParam;
	std::string 		_call;

	std::string 		_name;
	size_t 				_callId;

	std::string 			_callBackName;
	std::list<std::string>	_callBackParams;

	bool 					_attach;

	ExecNode();
	ExecNode(ExecNode const &oth);

	ExecNode 	&operator=(ExecNode const &oth);

	/*
	** used for qualifiers
	*/
	std::shared_ptr<ParsingNode> 	star(t_rule rule,
										 BnfParser &parser) const;
	std::shared_ptr<ParsingNode> 	plus(t_rule rule,
										 BnfParser &parser) const;
	std::shared_ptr<ParsingNode> 	ques(t_rule rule,
										 BnfParser &parser) const;
	std::shared_ptr<ParsingNode> 	none(t_rule rule,
										 BnfParser &parser) const;

	std::shared_ptr<ParsingNode> 	callRule(BnfParser &parser) const;

	std::shared_ptr<ParsingNode> 	basicRule(BnfParser &parser) const;

	std::shared_ptr<ParsingNode> 	execSons(BnfParser &parser) const;


public:
	ExecNode(ERuleLinker link);
	virtual ~ExecNode();

	ERuleLinker 	getLink() const;

	void 			setQualifier(ERuleQualifier qual);
	ERuleQualifier 	getQualifier() const;

	void 			setCall(std::string const &name);

 	void 			setAttachToFather(bool attach);
 	bool 			attachToFather() const;

	void 			setBasicRule(t_basicRule rule, std::string const &param);

	void 			setName(std::string const &name);
	std::string 	getName() const;

	std::shared_ptr<ParsingNode> 	executeParsing(BnfParser &parser);

	void 			setCallBack(std::string const &callBack, std::list<std::string> const &params);
	bool 			isCallBack() const;
};
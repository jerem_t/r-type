
#pragma once

#include <string>
#include <gEngine/Utils/Any.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * Evaluate the given string to generate any data.
 */
Any GE_DLL eval(std::string const &str);

}
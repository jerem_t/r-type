
#pragma once

#include <map>
#include <stack>
#include <string>
#include <cstring>
#include <stdexcept>
#include <fstream>
#include <vector>
#include <gEngine/Core/Node.hpp>

namespace ge {

/**
 * @class Xml
 * Simple xml parser.
 */
class GE_DLL Xml : public Node<Xml> {
public:

  /**
   * Default empty constructor.
   */
   Xml();

  /**
   * Copy constructor.
   */
  Xml(Xml const &other);

  /**
   * Assignement operator.
   */
  Xml &operator=(Xml const &other);

  /**
   * Destructor.
   */
  virtual ~Xml();

  /**
   * Load this xml node from a stream.
   * @param in The input stream.
   */
  bool loadFromStream(std::istream &in);

  /**
   * Load this xml node from a file.
   * @param path The path to the file.
   */
  bool loadFromFile(std::string const &path);

  /**
   * Load this xml node from string in memory.
   * @param data The string which represente xml data.
   */
  bool loadFromMemory(std::string const &data);

  /**
   * Stringify this node and all children.
   * @param indent The size of one tab.
   * @param currentIndent The current indent value.
   */
  std::string toString(unsigned char indent = 2,
                       unsigned char currentIndent = 0) const;

  /**
   * Return the text into this node.
   */
  std::string const &getText() const;

  /**
   * Return the attribute with the given name.
   */
  std::string const &getAttr(std::string const &name) const;

private:

  std::string _text;

private:

  /**
   * @enum State
   * Used to manage current parsing states.
   */
  enum State {
    Out, Comment,
    TagOpening, TagClosing, TagIn, TagEnded,
    NameOpening, NameClosing,
    WaitForValue, WaitForEq,
    AttribName, AttribValue
  };

  /**
   * Some parsing useful variables.
   */
  std::stack<Xml *> _stack;
  State _state;
  std::string _str;
  std::string _currentAttribute;

  /**
   * Parsing methods.
   */
  static bool _isWhiteSpace(char c);
  static bool _lastIsSpace(std::string const &name);
  static std::string _cutName(std::string const &name);
  void _startParsing();
  void _endParsing(); 
  void _parseToken(unsigned char token);

  /**
   * Stringify methods.
   */
  std::string _attribsToString() const;
  std::string _openingTagToString() const;

};

typedef std::shared_ptr<Xml> XmlPtr;
typedef std::vector<XmlPtr> XmlArray;

}
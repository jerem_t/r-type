
#pragma once

#include <gEngine/Parsers/ConsumerParser.hh>
#include <gEngine/Parsers/ParsingNode.hh>
#include <gEngine/System/Platform.hpp>

#include <list>
#include <vector>

class ExecNode;

class GE_DLL BnfParser : public ConsumerParser
{
private:

	typedef std::map<std::string, std::shared_ptr<ParsingNode> >	t_parsingResult;
	typedef std::map<std::string, std::shared_ptr<ExecNode> > 		t_bnfRules;
	typedef bool	(*t_callBack)(std::vector<std::shared_ptr<ParsingNode> > const &);

	t_bnfRules 							_rules;
	std::map<std::string, t_callBack>	_callBacks;

	std::string 						_entryPoint;

	t_parsingResult						_parsingResults;

	/*
	** the rules used to parse the BNF:
	** entry point = RULE
	**
	** RULE    ::= [IGNORE '~'? IGNORE IDENTIFIER IGNORE "::=" IGNORE EXPRESSION IGNORE ";;"]+ eof
	** ;;
	** IGNORE    ::= [' ' | 'n' | 't']*
	** ;;
	** IDCHAR    ::= 'A'..'Z' | 'a'..'z' | '_'
	** ;;
	** IDENTIFIER  ::= IDCHAR [IDCHAR | '0'..'9']*
	** ;;
	** QUALIFIER   ::= ['*' | '+' | '?']?
	** ;;
	** GETNODE 	::=	[':' IDENTIFIER]?
	** ;;
	** CALLBACK 	::=	['#' IDENTIFIER '(' [IGNORE IDENTIFIER IGNORE [',' IGNORE IDENTIFIER IGNORE]*]? ')']?
	** ;;
	** ANDEXPRESSION ::= [IGNORE
	**                     [BASICRULE | IDENTIFIER | '[' EXPRESSION ']' ]
	**                   GETNODE IGNORE QUALIFIER IGNORE CALLBACK]+
	** ;;
	** OREXPRESSION  ::= ['|' ANDEXPRESSION]*
	** ;;
	** EXPRESSION  ::= ANDEXPRESSION OREXPRESSION
	** ;;
	** BASICRULE   ::= [CHAR | RANGE | STRING | READUNTILCHAR | READUNTILSTRING] IGNORE QUALIFIER
	** ;;
	** CHAR    ::= ''' ' '..'~' ''' | 'n' | 't'
	** ;;
	** RANGE   ::= CHAR ".." CHAR
	** ;;
	** STRING    ::= '"' -> '"'
	** ;;
	** READUNTILCHAR   ::= "->" IGNORE CHAR
	** ;;
	** READUNTILSTRING ::= "->" IGNORE STRING
	** ;;
	*/

	void 		_addUtilsRules();

	bool 		_ignore();
	bool 		_idChar();
	bool 		_getIdentifier(std::string &id);
	bool 		_identifier(std::shared_ptr<ExecNode> const &father);
	bool 		_rule();
	bool 		_qualifier(std::shared_ptr<ExecNode> const &father);
	bool 		_getNode(std::shared_ptr<ExecNode> const &father);
	bool 		_callback(std::shared_ptr<ExecNode> const &father);
	bool 		_andExpression(std::shared_ptr<ExecNode> const &father);
	bool 		_orExpression(std::shared_ptr<ExecNode> const &father);
	bool 		_expression(std::shared_ptr<ExecNode> const &father);
	bool 		_basicRules(std::shared_ptr<ExecNode> const &father);
	bool 		_getChar(char *c);
	bool 		_char(std::shared_ptr<ExecNode> const &father);
	bool 		_range(std::shared_ptr<ExecNode> const &father);
	bool 		_getString(std::string &str);
	bool 		_string(std::shared_ptr<ExecNode> const &father);
	bool 		_readUntilChar(std::shared_ptr<ExecNode> const &father);
	bool 		_readUntilString(std::shared_ptr<ExecNode> const &father);

	bool		ruleReadChar(std::string const &str);
	bool		ruleReadString(std::string const &str);
	bool		ruleReadUntilChar(std::string const &str);
	bool		ruleReadUntilString(std::string const &str);
	bool		ruleReadRange(std::string const &str);
	bool		ruleIsEof(std::string const &str);

	bool 		buildExecTree();

public:
	BnfParser(AParserFeeder *feeder);
	virtual ~BnfParser();

	bool 		setBnf(std::string const &bnf);
	void 		setEntryPoint(std::string const &entry);

	void 		addCallBack(std::string const &name, t_callBack fnc);

	std::shared_ptr<ParsingNode> 	callRule(std::string const &name);
	bool 							callCallBack(std::string const &name,
												std::list<std::string> const &paramNames);

	void 							registerResult(std::string const &name,
													std::shared_ptr<ParsingNode> const &result);

	std::shared_ptr<ParsingNode> 	parse();

};
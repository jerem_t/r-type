
#pragma once

#include <string>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * Tokenize the given string, and store the result into the given
 * container.
 * @param str The string to tokenize.
 * @param delim A string which contains the delimiters.
 * @param result The container to store the result.
 * @param keepEmpty True if you want to keep the empty tokens.
 */
template<class Container>
void GE_DLL tokenize(std::string const &str,
              std::string const &delim,
              Container &result,
              bool keepEmpty = false) {
  if (delim.empty()) {
    result.push_back(str);
    return ;
  }
  std::string::const_iterator substart = str.begin(), subend;
  while (true) {
    subend = std::search(substart, str.end(), delim.begin(), delim.end());
    std::string temp(substart, subend);
    if (keepEmpty || !temp.empty())
      result.push_back(temp);
    if (subend == str.end())
      break;
    substart = subend + delim.size();
  }
}

/**
 * Tokenize the given string, and return the result.
 * @param str The string to tokenize.
 * @param delim A string which contains the delimiters.
 * @param keepEmpty True if you want to keep the empty tokens.
 */
template<class Container>
Container GE_DLL tokenize(std::string const &str,
                   std::string const &delim,
                   bool keepEmpty = false) {
  Container result;
  tokenize(str, delim, result, keepEmpty);
  return result;
}

}

#pragma once

#include <algorithm>
#include <map>
#include <gEngine/Resources/Loadable.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Style
 * Simple parser for css-like style declarations.
 */
class GE_DLL Style : public Loadable {
public:
  typedef std::map<std::string, std::map<std::string, std::string>> Data;

  typedef Data::iterator iterator;

  typedef Data::const_iterator const_iterator;

  inline iterator begin() {
    return _data.begin();
  }

  inline const_iterator begin() const {
    return _data.begin();
  }

  inline iterator end() {
    return _data.end();
  }

  inline const_iterator end() const {
    return _data.end();
  }

  Style();
  ~Style();
  virtual bool loadFromMemory(std::string const &str);
private:
  Data _data;
};

}

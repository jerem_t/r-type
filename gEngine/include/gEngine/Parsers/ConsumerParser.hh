
#pragma once

#include <gEngine/Parsers/AParserFeeder.hh>
#include <gEngine/System/Platform.hpp>

#include <string>
#include <map>
#include <stack>

class GE_DLL ConsumerParser
{
private:
	std::stack<size_t> 				_contexts;

	std::map<std::string, size_t> 	_records;
	size_t 							_currentRecord;

	AParserFeeder	*_feeder;
	std::string 	_content;
	size_t 			_index;
	size_t 			_size;

	bool 		fillBuffer();
	void 		clearOldRecords();

	typedef std::map<std::string, size_t>::iterator recordsIt;

public:
	ConsumerParser(AParserFeeder *feeder);
	virtual ~ConsumerParser();

	AParserFeeder 	*getFeeder() const;
	void 			setFeeder(AParserFeeder *feeder);

	bool 		readChar(char c);
	bool		readRange(char begin, char end);
	bool		readString(std::string const &str);
	bool 		readUntil(char c);
	bool		readUntil(std::string const &str);

	bool 		isEof();

	void 		beginRecord(std::string const &name);
	std::string getRecord	(std::string const &name);

	void 		saveContext();
	void 		restoreContext();
	void 		popContext();

	void 		clear();
};
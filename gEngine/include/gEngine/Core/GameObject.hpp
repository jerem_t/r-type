
#pragma once

#include <unordered_map>
#include <gEngine/Core/Node.hpp>
#include <gEngine/Utils/EventDispatcher.hpp>
#include <gEngine/Math/Vector2.hpp>
#include <gEngine/Parsers/Xml.hpp>
#include <gEngine/System/Time.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * Component's forward declaration.
 */
class Component;

/**
 * GameObject's forward declaration.
 */
class GameObject;

/**
 * @typedef ComponentData
 * Representation of a component data stored into the game objects.
 */
typedef std::pair<std::string, std::shared_ptr<Component>> ComponentData;

/**
 * @typedef Components
 * A map of components stored by typeid hash codes.
 */
typedef std::vector<ComponentData> Components;

/**
 * @typedef GameObjectPtr
 * Shared ptr shortcut for game object.
 */
typedef std::shared_ptr<ge::GameObject> GameObjectPtr;

/**
 * @typedef GameObjects
 * Represente a list of game objects.
 */
typedef std::vector<std::shared_ptr<ge::GameObject>> GameObjects;

/**
 * @class GameObject
 * Simple graph node with component system to manage
 * game logic.
 */
class GE_DLL GameObject : public Node<GameObject>,
                   public EventDispatcher {
public:

  /**
   * Create with a specific name.
   * @param name The name of this game object.
   */
  GameObject(std::string const &name = "");

  /**
   * Copy constructor.
   */
  GameObject(GameObject const &other);

  /**
   * Assignement operator.
   */
  GameObject &operator=(GameObject const &other);

  /**
   * Empty destructor, do nothing.
   */
  virtual ~GameObject();

  /**
   * Start this game object and all its children.
   */
  void start();

  /**
   * Update the components at each frame.
   * @param dt The delta time between this frame
   *           and the previous one.
   */
  void update(ge::Seconds dt);

  /**
   * Get the component with the given name to use it.
   */
  Component &component(std::string const &name);

  /**
   * Load the game object from a description file.
   * @param path The path to the config file (Json, XML or binary).
   */
  bool loadFromFile(std::string const &path);

  /**
   * Load the game object from the given xml node.
   */
  bool loadFromXml(ge::XmlPtr const &node);

  /**
   * Check if the given component exists.
   */
  bool hasComponent(std::string const &name) const;

  /**
   * Check if the gameObject is currently enabled.
   */
  bool isEnabled() const;

  bool isDead() const;

  /**
   * Enable the gameObject.
   */
  void enable();

  /**
   * Disable the gameObject.
   */
  void disable();

  /**
   * Set the spawn time.
   */
  void setSpawnTime(float seconds);

  /**
   * Get the spawn time.
   */
  float getSpawnTime() const;

  /**
   *   Sets |is_dead| to true.
   *   The |is_dead| flag is set to true in order to erase the object from
   *   the game graph.
   */
  void die();

  static void setDefine(std::string const &name, Any const &any);

  static Any &getDefine(std::string const &name);

  static bool isDefined(std::string const &name);

  static void setScope(std::string const &name);
  static std::string const &getScope();

private:

  static std::map<std::string, Any> _defines;
  static std::string _scope;

  bool                 _isEnabled;  /** False to not update */
  float                _currentTime;
  float                _spawnTime;
  Components           _components; /** The activated components */
  bool		             _isDead;
  bool                 _hasStarted;
};

}

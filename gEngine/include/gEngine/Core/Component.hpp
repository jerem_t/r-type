
#pragma once

#include <gEngine/System/Platform.hpp>
#include <gEngine/Core/Node.hpp>
#include <gEngine/Parsers/Xml.hpp>
#include <gEngine/System/Time.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * GameObject's forward declaration.
 */
class GameObject;

/**
 * @class Component
 * Base component class, all the game engine components
 * will be based on this class.
 */
class GE_DLL Component : public Node<Component> {
public:

  /**
   * Initialize the component with the given game object.
   * @param gameObject which contains this component.
   */
  Component(GameObject &gameObject);

  /**
   * Empty destructor, do nothing.
   */
  virtual ~Component();

  /**
   * Initialize the component.
   */
  virtual void onStart() = 0;

  /**
   * Update the component behaviour at each frame.
   * @param dt The delta time between this frame
   *           and the previous one.
   */
  virtual void onUpdate(ge::Seconds dt) = 0;

  /**
   * Add some additional checks before initialize the component.
   */
  void start();

  /**
   * Update the component if it has been initialized.
   */
  void update(float dt);

  /**
   * Check if the gameObject is currently enabled.
   */
  bool isEnabled() const;

  /**
   * Enable the gameObject.
   */
  void enable();

  /**
   * Disable the gameObject.
   */
  void disable();

  /**
   * Load this component from the given file.
   */
  bool loadFromFile(std::string const &path);

  /**
   * Load this component from the given xml node.
   */
  bool loadFromXml(XmlPtr const &node);

  /**
   * Set the component dependencies.
   */
  void setDependencies(std::list<std::string> const &deps);

protected:

  /**
   * The game object which contains this component.S
   */
  GameObject &_gameObject;

  /**
   * Check if the component is enabled. It will not be updated
   * if it is disabled.
   */
  bool _isEnabled;

  /**
   * True the if onStart method has been called.
   */
  bool _hasStarted;

  /**
   * The list of components that this one depend on.
   */
  std::list<std::string> _dependencies;

  /**
   * Indicates whether a component is threadable or not.
   */
  bool _isThreadable;
};

/**
* Simple macro used to export a component.
*/
#define GE_EXPORT(ComponentName) \
  extern "C" GE_API_EXPORT ge::Component *create(ge::GameObject &go) { \
    return new ComponentName(go); \
  }

extern "C" GE_API_EXPORT Component *create(GameObject &go);

}

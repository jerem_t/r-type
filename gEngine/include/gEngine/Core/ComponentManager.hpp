
#pragma once

#include <map>
#include <string>
#include <gEngine/Core/Component.hpp>
#include <gEngine/Utils/Singleton.hpp>
#include <gEngine/System/Library.hpp>
#include <gEngine/Resources/Resource.hpp>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class ComponentManager
 * This class manage all components used into the game engine.
 * Its allow to create, and load any components.
 */
class GE_DLL ComponentManager : public Singleton<ComponentManager> {
public:

  /**
   * @typedef Creator
   * The pointer function used to create a component.
   */
  typedef Component *(*Creator)(GameObject &);

  struct Info {
    std::string description;
    std::list<std::string> dependencies;
    Creator creator;
  };

  /**
   * Empty constructor, do nothing.
   */
  ComponentManager();

  /**
   * Empty destructor, do nothing.
   */
  ~ComponentManager();

  /**
   * Load the component creator from the shared library.
   * @param path The path to the component directory.
   */
  bool add(std::string const &path);

  /**
   * Create a component instance of `ComponentType`.
   * @param name The component's name.
   * @param gameObject The gameObject to attach the component.
   */
  std::shared_ptr<Component> create(std::string const &name,
                                    GameObject &gameObject);

  /**
   * Load all the components from the given file.
   */
  bool loadFromFile(std::string const &path);

  std::size_t	getSizeComponents() const;

private:

  std::map<std::string, Info> _components;

};

}
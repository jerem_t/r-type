
#pragma once

#include <algorithm>
#include <iostream>
#include <memory>
#include <unordered_map>
#include <gEngine/Utils/Tag.hpp>
#include <gEngine/Utils/Any.hpp>
#include <list>
#include <gEngine/System/Platform.hpp>

/**
 * @namespace ge
 * Generic game engine with a easy-to-use and powerful
 * entity/component system.
 */
namespace ge {

/**
 * @class Node
 * Generic data structure to reprensente a graph node.
 */
template<typename Type>
class GE_DLL Node {
public:

  typedef std::list<std::shared_ptr<Type>> Children;

  typedef typename Children::iterator iterator;

  typedef typename Children::const_iterator const_iterator;

  inline iterator begin() {
    return _children.begin();
  }

  inline const_iterator begin() const {
    return _children.begin();
  }

  inline iterator end() {
    return _children.end();
  }

  inline const_iterator end() const {
    return _children.end();
  }

  typedef std::unordered_map<std::string, ge::Any> Data;

public:

  Node(std::string const &name = "");

  Node(Node const &other);

  Node &operator=(Node const &other);

  virtual ~Node();

  size_t getId() const;

  void	setId(std::size_t);

  void setName(std::string const &name);

  std::string const &getName() const;

  bool hasTag(ge::BitTag const &tag) const;

  bool hasTag(std::string const &tag) const;

  Node &setTags(size_t tags);

  size_t getTags() const;

  Node &addTag(std::string const &name);

  Node &removeTag(std::string const &tag);

  Node &addChild(std::shared_ptr<Type> const &child);

  template<typename Function>
  void forEach(Function &&function, bool recurse = true);

  Type *getParent() const;

  Type *getRoot();

  std::shared_ptr<Type> at(size_t index) const;

  template<typename Predicate>
  std::shared_ptr<Type> getChild(Predicate &&predicate,
                                 bool recurse = true) const;

  template<typename Predicate, typename Container>
  void getChildren(Predicate &&predicate,
                   Container &children,
                   bool recurse = true) const;

  std::shared_ptr<Type> getChildById(size_t id,
                                     bool recurse = true) const;

  std::shared_ptr<Type> getChildByName(std::string const &name,
                                       bool recurse = true) const;

  std::shared_ptr<Type> getChildByTag(std::string const &name,
                                      bool recurse = true) const;

  template<typename Container>
  void getChildrenByName(std::string const &name,
                         Container &children,
                         bool recurse = true) const;

  template<typename Container>
  void getChildrenByTag(std::string const &name,
                        Container &children,
                        bool recurse = true) const;

  std::shared_ptr<Type> find(std::string const &query) const;

  template<typename Container>
  void findAll(std::string const &query,
                        Container &children) const;

  void erase();

  template<typename Predicate>
  Node &eraseChild(Predicate &&fn, bool recurse = true);

  void eraseChildById(size_t id, bool recurse = true);

  void eraseChildByName(std::string const &name, bool recurse = true);

  void eraseChildByTag(std::string const &name, bool recurse = true);

  template<typename Predicate>
  Node &eraseChildren(Predicate &&fn, bool recurse = true);

  void eraseChildrenById(size_t id, bool recurse = true);

  void eraseChildrenByName(std::string const &name, bool recurse = true);

  void eraseChildrenByTag(std::string const &name, bool recurse = true);

  ge::Any &operator[](std::string const &name);

  ge::Any const &operator[](std::string const &name) const;

  bool has(std::string const &name) const;

protected:

  static Pool<size_t>   _pool;
  Type                  *_parent;
  size_t                _id;
  std::string           _name;
  size_t                _tags;
  Children              _children;
  Data                  _data;

};

#include <gEngine/Core/Node.inl>

}


#pragma once

template<typename Type>
Pool<size_t>  Node<Type>::_pool;

template<typename Type>
Node<Type>::Node(std::string const &name)
  : _parent(NULL)
  , _id(_pool.get())
  , _name(name)
  , _tags(0) {}

template<typename Type>
Node<Type>::Node(Node const &other)
: _id(other._id)
, _name(other._name)
, _tags(other._tags)
, _children(other._children)
, _data(other._data) {}

template<typename Type>
Node<Type> &Node<Type>::operator=(Node const &other) {
  if (this != &other) {
    _id = other._id;
    _name = other._name;
    _tags = other._tags;
    _children = other._children;
    _data = other._data;
  }
  return *this;
}

template<typename Type>
Node<Type>::~Node() {
  _pool.release(_id);
}

template<typename Type>
template<typename Function>
void Node<Type>::forEach(Function &&function, bool recurse) {
  for (auto &child : _children) {
    function(child);
    if (recurse)
      child->forEach(function);
  }
}

template<typename Type>
size_t Node<Type>::getId() const {
  return _id;
}

template<typename Type>
void	Node<Type>::setId(std::size_t id)
{
	_id = id;
}

template<typename Type>
void Node<Type>::setName(std::string const &name) {
  _name = name;
}

template<typename Type>
std::string const &Node<Type>::getName() const {
  return _name;
}

template<typename Type>
Node<Type> &Node<Type>::setTags(size_t tags) {
  _tags = tags;
  return *this;
}

template<typename Type>
size_t Node<Type>::getTags() const {
  return _tags;
}

template<typename Type>
bool Node<Type>::hasTag(ge::BitTag const &tag) const {
  return _tags & tag;
}

template<typename Type>
bool Node<Type>::hasTag(std::string const &tag) const {
  return _tags & BitTag(tag);
}

template<typename Type>
Node<Type> &Node<Type>::addTag(std::string const &tag) {
  _tags |= BitTag(tag);
  return *this;
}

template<typename Type>
Node<Type> &Node<Type>::removeTag(std::string const &tag) {
  _tags &= ~BitTag(tag);
  return *this;
}

template<typename Type>
Node<Type> &Node<Type>::addChild(std::shared_ptr<Type> const &child) {
  child->_parent = dynamic_cast<Type *>(this);
  _children.push_back(child);
  return *this;
}

template<typename Type>
Type *Node<Type>::getParent() const {
  return _parent;
}

template<typename Type>
Type *Node<Type>::getRoot() {
  if (!_parent)
    return static_cast<Type *>(this);
  return _parent->getRoot();
}

template<typename Type>
std::shared_ptr<Type> Node<Type>::at(size_t index) const {
  return _children[index];
}

template<typename Type>
template<typename Predicate>
std::shared_ptr<Type> Node<Type>::getChild(Predicate &&predicate,
                                           bool recurse) const {
  for (auto const &node : _children) {
    if (predicate(node))
      return node;
    if (recurse) {
      std::shared_ptr<Type> tmp = node->getChild(predicate, recurse);
      if (tmp)
        return tmp;
    }
  }
  return std::shared_ptr<Type>();
}

template<typename Type>
template<typename Predicate, typename Container>
void Node<Type>::getChildren(Predicate &&predicate,
                             Container &children,
                             bool recurse) const {
  for (auto const &node : _children) {
    if (predicate(node))
      children.push_back(node);
    if (recurse)
      node->getChildren(predicate, children, recurse);
  }
}

template<typename Type>
std::shared_ptr<Type> Node<Type>::getChildById(size_t id,
                                               bool recurse) const {
  return getChild([&id] (std::shared_ptr<Type> const &node) {
    return node->getId() == id;
  }, recurse);
}

template<typename Type>
std::shared_ptr<Type> Node<Type>::getChildByName(std::string const &name,
                                               bool recurse) const {
  return getChild([&name] (std::shared_ptr<Type> const &node) {
    return node->getName() == name;
  }, recurse);
}

template<typename Type>
std::shared_ptr<Type> Node<Type>::getChildByTag(std::string const &name,
                                               bool recurse) const {
  BitTag tag(name);
  return getChild([&tag] (std::shared_ptr<Type> const &node) {
    return node->hasTag(tag);
  }, recurse);
}

template<typename Type>
template<typename Container>
void Node<Type>::getChildrenByName(std::string const &name,
                                   Container &children,
                                   bool recurse) const {
  getChildren([&name] (std::shared_ptr<Type> const &node) {
    return node->getName() == name;
  }, children, recurse);
}

template<typename Type>
template<typename Container>
void Node<Type>::getChildrenByTag(std::string const &name,
                                  Container &children,
                                  bool recurse) const {
  BitTag tag(name);
  getChildren([&tag] (std::shared_ptr<Type> const &node) {
    return node->hasTag(tag);
  }, children, recurse);
}

template<typename Type>
std::shared_ptr<Type>
Node<Type>::find(std::string const &query) const {
  switch (query[0]) {
    case '.':
      return getChildByTag(query.substr(1));
    default:
      break;
  }
  return getChildByName(query);
}

template<typename Type>
template<typename Container>
void Node<Type>::findAll(std::string const &query,
                         Container &children) const {
  if (query == "*")
    getChildren([] (std::shared_ptr<Type> const &) {
      return true;
    }, children);
  else {
    switch (query[0]) {
      case '.':
        getChildrenByTag(query.substr(1), children);
        return ;
      default:
        getChildrenByName(query, children);
        return ;
    }
  }
}

template<typename Type>
void Node<Type>::erase() {
  if (_parent) {
  auto it = std::find_if(_parent->_children.begin(), _parent->_children.end(), [this] (std::shared_ptr<Type> const &node) {
    return (node.get() == this);
  });
    if (it != _parent->_children.end())
      _parent->_children.erase(it);
  }
}

template<typename Type>
template<typename Predicate>
Node<Type> &Node<Type>::eraseChild(Predicate &&predicate,
                                    bool recurse) {
  for (auto const &node : _children) {
    if (predicate(node)) {
      node->erase();
      return *this;
    }
    if (recurse)
      node->eraseChild(predicate, recurse);
  }
  return *this;
}

template<typename Type>
void Node<Type>::eraseChildById(size_t id, bool recurse) {
  return eraseChild([&id] (std::shared_ptr<Type> const &node) {
    return id == node->getId();
  }, recurse);
}

template<typename Type>
void Node<Type>::eraseChildByName(std::string const &name,
                                  bool recurse) {
  return eraseChild([&name] (std::shared_ptr<Type> const &node) {
    return name == node->getName();
  }, recurse);
}

template<typename Type>
void Node<Type>::eraseChildByTag(std::string const &name,
                                 bool recurse) {
  return eraseChild([&name] (std::shared_ptr<Type> const &node) {
    return node->hasTag(name);
  }, recurse);
}

template<typename Type>
template<typename Predicate>
Node<Type> &Node<Type>::eraseChildren(Predicate &&predicate,
                                      bool recurse) {
  for (auto const &node : _children) {
    if (predicate(node))
      node->erase();
    if (recurse)
      node->eraseChildren(predicate, recurse);
  }
  return *this;
}

template<typename Type>
void Node<Type>::eraseChildrenById(size_t id, bool recurse) {
  return eraseChildren([&id] (std::shared_ptr<Type> const &node) {
    return id == node->getId();
  });
}

template<typename Type>
void Node<Type>::eraseChildrenByName(std::string const &name,
                                     bool recurse) {
  return eraseChildren([&name] (std::shared_ptr<Type> const &node) {
    return name == node->getName();
  });
}

template<typename Type>
void Node<Type>::eraseChildrenByTag(std::string const &name,
                                    bool recurse) {
  return eraseChildren([&name] (std::shared_ptr<Type> const &node) {
    return node->hasTag(name);
  });
}

template<typename Type>
ge::Any &Node<Type>::operator[](std::string const &name) {
  return _data[name];
}

template<typename Type>
ge::Any const &Node<Type>::operator[](std::string const &name) const {
  if (_data.find(name) == _data.end())
    throw std::out_of_range("Cannot find \"" + name + "\" data in Node.");
  return _data.at(name);
}

template<typename Type>
bool Node<Type>::has(std::string const &name) const {
  return _data.find(name) != _data.end();
} 

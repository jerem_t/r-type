
#pragma once

#include <gEngine/Math/Box.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/System/Platform.hpp>

namespace ge {

/**
 * @class SpriteData
 * Sprite data reprensentation.
 */
class GE_DLL SpriteData {
public:

  SpriteData();
  SpriteData(SpriteData const &other);
  SpriteData &operator=(SpriteData const &other);
  ~SpriteData();

  void setTexture(std::string const &texture);
  void setTextureRect(ge::Boxf const &textureRect);
  void setColor(ge::Color const &color);

  std::string const &getTexture() const;
  ge::Boxf const &getTextureRect() const;
  ge::Color const &getColor() const;

  bool hasTexture() const;
  bool hasTextureRect() const;
  bool hasColor() const;
private:
  enum Field {
    Texture = 1 << 0,
    TextureRect = 1 << 1,
    Color = 1 << 2,
  };
  std::string _texture;
  ge::Boxf _textureRect;
  ge::Color _color;
  size_t _fields;
};

}
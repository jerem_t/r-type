
#pragma once

#include <gEngine/Math/Vector4.hpp>

namespace ge {

class GE_DLL TextData {
public:

  TextData();
  TextData(TextData const &other);
  TextData &operator=(TextData const &other);
  ~TextData();

  void setString(std::string const &str);
  void setFont(std::string const &str);
  void setColor(ge::Color const &color);
  void setCharacterSize(float size);

  std::string const &getString() const;
  std::string const &getFont() const;
  ge::Color const &getColor() const;
  float getCharacterSize() const;

  bool hasString() const;
  bool hasFont() const;
  bool hasColor() const;
  bool hasCharacterSize() const; 

private:

  enum Field {
    String = 1 << 0,
    Font = 1 << 1,
    Color = 1 << 2,
    CharacterSize = 1 << 3
  };

  std::string _str;
  std::string _font;
  ge::Color _color;
  float _size;
  size_t _fields;

};

}
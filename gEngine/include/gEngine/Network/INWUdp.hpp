#ifndef INWUDP_HPP_
#define	INWUDP_HPP_

#include <string>

#include "gEngine/Network/INWAbstract.hpp"

class GE_DLL INWUdp : public INWAbstract
{
public:
	virtual	INWAbstract::Status	send(const std::string &ip, USHORT port, const std::vector<char> &data) = 0;
	virtual INWAbstract::Status	receive(const std::string &ip, USHORT port, std::vector<char> &buffer, std::size_t size, std::size_t &received) = 0;
	virtual INWAbstract::Status	bind(USHORT port) = 0;
	virtual void				unbind() = 0;

	virtual	~INWUdp() {}
};

#endif // !INWUDP_HPP_

#ifndef NWTCP_HPP_
# define NWTCP_HPP_

#include <string>

#include "gEngine/Network/INWTcp.hpp"

class GE_DLL NWTcp : public INWTcp
{
public:
	NWTcp();
	~NWTcp();

	INWAbstract::Status	connect(const std::string &ip, const std::string &port);
	void				disconnect();
	INWAbstract::Status	send(const std::vector<char> &data);
	INWAbstract::Status	send(const NWPacket &data);
	INWAbstract::Status	receive(std::vector<char> &buffer, std::size_t size, std::size_t &received);
	INWAbstract::Status	receive(NWPacket &packet, std::size_t size, std::size_t &received);
	const std::string	&getRemoteIp() const;
	const std::string	&getRemotePort() const;
	void				setBlocking(bool blocking);
	bool				isBlocking() const;
	INWAbstract::Type	getType() const;
	void				setSocket(NWSOCKET socket);
	NWSOCKET			getSocket() const;
	const std::string		&getIp() const;
	void				setIp(const std::string &ip);

	bool				operator==(const NWTcp &other);

private:
	NWTcp(const NWTcp &other);
	NWTcp	&operator=(const NWTcp &other);

	void	create();
	void	close();


	NWSOCKET	m_socket;
	bool		m_blocking;

	std::string	m_ip;
	std::string	m_port;
	std::string	_ip;
};

#endif // !NWTCP_HPP_

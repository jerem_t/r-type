/*
** Commun interface for windows and linux socket class
*/
#ifndef INWABSTRACT_HPP_
#define INWABSTRACT_HPP_

#include <string>

#include "gEngine/Network/Network.hpp"

class GE_DLL INWAbstract
{
public:
	enum Status
	{
		SC_DONE,			// The last operation with the socket is done
		SC_NOTREADY,		// The socket is not ready to sent or receive data yet 
		SC_DISCONNECTED,	// The socket has been disconnected (only TCP socket)
		SC_ERROR			// An error happened
	};

	enum Type
	{
		SC_TCP,			// TCP
		SC_UDP			// UDP
	};

	virtual Type		getType() const = 0;
	virtual NWSOCKET	getSocket() const = 0;

	virtual ~INWAbstract() {}

protected:
	virtual void	create() = 0;
	virtual void	close() = 0;
	virtual	void	setBlocking(bool blocking) = 0;
	virtual bool	isBlocking() const = 0;
	virtual void	setSocket(NWSOCKET socket) = 0;
};

#endif // !INWABSTRACT_HPP_

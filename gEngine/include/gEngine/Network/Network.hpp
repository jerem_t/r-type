#ifndef NETWORK_HPP
#define	NETWORK_HPP

/*********************************
** Headers
**********************************/

#include "gEngine/System/Platform.hpp"
#include "gEngine/Network/NWSocket.hpp"
#include "gEngine/Network/NWPacket.hpp"

/*********************************
** Headers System
**********************************/

#ifdef GE_SYSTEM_WINDOWS
	#include <WinSock2.h>
	#include <WS2tcpip.h>
	#pragma comment(lib, "Ws2_32.lib")
#elif defined GE_SYSTEM_LINUX
	# include <sys/ioctl.h>
	# include <sys/time.h>
	# include <sys/types.h>
	# include <sys/socket.h>
	# include <netinet/in.h>
	# include <arpa/inet.h>
	# include <netdb.h>
	# include <unistd.h>
#endif

# include <cstring>
# include <stdexcept>

#endif // !NETWORK_HPP

#ifndef SCTCPLISTENERWINDOWS_HPP_
#define SCTCPLISTENERWINDOWS_HPP_

#include <gEngine/System/Platform.hpp>

#ifdef GE_SYSTEM_WINDOWS

#include <gEngine/Network/INWAbstract.hpp>

class NWTcpWindows;

class NWTcpListenerWindows : public INWAbstract
{
public:
	NWTcpListenerWindows();
	~NWTcpListenerWindows();

	Type				getType() const;
	void				setBlocking(bool blocking);
	bool				isBlocking() const;
	INWAbstract::Status	listen(USHORT port);
	INWAbstract::Status	accept(NWTcpWindows &socket);
	void				close();

private:
	NWTcpListenerWindows(const NWTcpListenerWindows &other);
	NWTcpListenerWindows	&operator=(const NWTcpListenerWindows &other);

	void	create();

	SOCKET	m_socket;
	bool	m_blocking;
};

#endif

#endif // !SCTCPLISTENERWINDOWS_HPP_

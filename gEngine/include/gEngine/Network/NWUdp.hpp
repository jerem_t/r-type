#ifndef NWUDP_HPP_
# define NWUDP_HPP_

#include <gEngine/Network/INWUdp.hpp>

class GE_DLL NWUdp : public INWUdp
{
public:
	NWUdp();
	~NWUdp();

	INWAbstract::Status	send(const std::string &ip, USHORT port, const std::vector<char> &data);
	INWAbstract::Status	receive(const std::string &ip, USHORT port, std::vector<char> &buffer, std::size_t size, std::size_t &received);
	INWAbstract::Status	send(const std::string &ip, USHORT port, const NWPacket &data);
	INWAbstract::Status	receive(const std::string &ip, USHORT port, NWPacket &packet, std::size_t size, std::size_t &received);
	INWAbstract::Status	bind(USHORT port);
	void				unbind();
	INWAbstract::Type	getType() const;
	void				setBlocking(bool blocking);
	bool				isBlocking() const;
	void				setSocket(NWSOCKET socket);
	NWSOCKET			getSocket() const;

private:
	NWUdp(const NWUdp &other);
	NWUdp	&operator=(const NWUdp &other);

	void	create();
	void	close();

	NWSOCKET	m_socket;
	bool		m_blocking;
};

#endif // !NWUdp_HPP_
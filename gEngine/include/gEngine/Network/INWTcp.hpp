#ifndef INWTCP_HPP_
#define	INWTCP_HPP_

#include <string>

#include "gEngine/Network/INWAbstract.hpp"

class GE_DLL INWTcp : public INWAbstract
{
public:
	virtual INWAbstract::Status	connect(const std::string &ip, const std::string &port) = 0;
	virtual void				disconnect() = 0;
	virtual INWAbstract::Status	send(const std::vector<char> &data) = 0;
	virtual INWAbstract::Status	receive(std::vector<char> &buffer, std::size_t size, std::size_t &received) = 0;
	virtual const std::string	&getRemoteIp() const = 0;
	virtual const std::string	&getRemotePort() const = 0;
	virtual const std::string	&getIp() const = 0;

	virtual	~INWTcp() {};
};

#endif // !INWTCP_HPP_

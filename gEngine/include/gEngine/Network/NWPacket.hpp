#ifndef NWPACKET_HPP_
# define NWPACKET_HPP_

#include <string>
#include <gEngine/Math/Box.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Math/Vector2.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Quaternion.hpp>
#include <gEngine/Math/Transform.hpp>

class GE_DLL NWPacket
{
public:
  NWPacket();
  ~NWPacket();

  typedef bool							Testicule;
  typedef std::pair<Testicule, Testicule>	Couilles;

  void					append(const void *data, std::size_t size);
  void					clear();
  const std::vector<char>	&getData() const;
  void					setData(const std::vector<char> &data);
  std::size_t				getDataSize() const;

  NWPacket	&operator<<(ge::Vector2f const &data);
  NWPacket	&operator<<(ge::Vector3f const &data);
  NWPacket	&operator<<(ge::Vector4f const &data);
  NWPacket	&operator<<(ge::Quaternionf const &data);
  NWPacket	&operator<<(ge::Boxf const &data);
  NWPacket	&operator<<(std::size_t data);
  NWPacket	&operator<<(char data);
  NWPacket	&operator<<(int data);
  NWPacket	&operator<<(float data);
  NWPacket	&operator<<(std::string const &data);
  NWPacket	&operator<<(Testicule data);
  NWPacket	&operator<<(ge::GameObject &data);
  NWPacket	&operator<<(ge::Transform const &data);

  NWPacket	&operator>>(ge::Vector2f &data);
  NWPacket	&operator>>(ge::Vector3f &data);
  NWPacket	&operator>>(ge::Vector4f &data);
  NWPacket	&operator>>(ge::Quaternionf &data);
  NWPacket	&operator>>(ge::Boxf &data);
  NWPacket	&operator>>(std::size_t &data);
  NWPacket	&operator>>(char &data);
  NWPacket	&operator>>(int &data);
  NWPacket	&operator>>(float &data);
  NWPacket	&operator>>(std::string &data);
  NWPacket	&operator>>(Testicule &data);
  NWPacket	&operator>>(ge::GameObject &data);
  NWPacket	&operator>>(ge::Transform &data);

 private:
  std::vector<char>	_data;
  std::size_t		_cursor;
};

#endif // !NWPACKET_HPP_

#ifndef NWSOCKET_HPP_
#define	NWSOCKET_HPP_

#include "gEngine/System/Platform.hpp"

#ifdef GE_SYSTEM_WINDOWS
	#define	NWSOCKET SOCKET
#elif defined(GE_SYSTEM_UNIX)
	typedef int			NWSOCKET;
	typedef struct sockaddr		SOCKADDR;
	typedef struct sockaddr_in	SOCKADDR_IN;
	typedef struct timeval		TIMEVAL;
	typedef unsigned short		USHORT;
	#define INVALID_SOCKET	~0
	#define NO_ERROR 0L
	#define SOCKET_ERROR -1
#endif

#endif // !NWSOCKET_HPP_

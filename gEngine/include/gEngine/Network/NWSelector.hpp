#ifndef NWSELECTOR_HPP_
#define	NWSELECTOR_HPP_
#include "gEngine/Network/Network.hpp"
#include "gEngine/Network/INWAbstract.hpp"

class GE_DLL NWSelector
{
public:
	NWSelector();
	~NWSelector();

	void	addRead(INWAbstract &socket);
	void	addWrite(INWAbstract &socket);
	void	removeRead(INWAbstract &socket);
	void	removeWrite(INWAbstract &socket);
	void	clear();
	bool	wait(bool block = true, int second = 0, int microseconds = 0);
	bool	isReadyToReceive(INWAbstract &socket);
	bool	isReadyToWrite(INWAbstract &socket);

private:
	NWSelector(const NWSelector &other);
	NWSelector	operator=(const NWSelector &other);

	fd_set	_setfdrs;
	fd_set	_setfdws;
	fd_set	_readfds;
	fd_set	_writefds;
	TIMEVAL	_timer;
	int		_maxFd;
};

#endif // !NWSELECTOR_HPP_

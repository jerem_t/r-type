#ifndef NWTCPLISTENER_HPP_
#define NWTCPLISTENER_HPP_

#include <gEngine/Network/INWAbstract.hpp>
#include "NWTcp.hpp"

class GE_DLL NWTcpListener : public INWAbstract
{
public:
	NWTcpListener();
	~NWTcpListener();

	void				setSocket(NWSOCKET socket);
	NWSOCKET			getSocket() const;
	Type				getType() const;
	void				setBlocking(bool blocking);
	bool				isBlocking() const;
	INWAbstract::Status		listen(USHORT port);
	INWAbstract::Status		accept(NWTcp &socket);
	void				close();
	void				disconnect();

private:
	NWTcpListener(const NWTcpListener &other);
	NWTcpListener	&operator=(const NWTcpListener &other);

	void	create();

	NWSOCKET	m_socket;
	bool		m_blocking;
};

#endif

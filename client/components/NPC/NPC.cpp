
#include <gEngine/Core/GameObject.hpp>
#include "NPC.hpp"

NPC::ActionMap NPC::_actions = {
  { MOVE_UP, &NPC::_moveUp },
  { MOVE_DOWN, &NPC::_moveDown },
  { STOP, &NPC::_stop }
};

NPC::NPC(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _state(MOVE_UP)
, _currentTime(0)
, _duration(0) {
  _data["duration"] = 1.0f;
  _data["bubble"] = std::string("bubble");
}

NPC::~NPC() {}

void NPC::onStart() {
  _duration = _data["duration"];
  _gameObject.getChildByName("bubble")->component("Sprite")["texture"] =
      _data["bubble"];
}

void NPC::onUpdate(ge::Seconds dt) {
  _currentTime += dt;
  (this->*_actions[_state])();
}

void NPC::_moveUp() {
  if (_currentTime > _duration) {
    _state = STOP;
    _currentTime = 0.0f;
    _gameObject.component("Move")["direction"] = ge::Vector3f();
  }
}

void NPC::_moveDown() {}

void NPC::_stop() {
  if (_currentTime > _duration) {
    _state = MOVE_DOWN;
    _gameObject.component("Move")["direction"] = ge::Vector3f(0, 1, 0);
  }
}

GE_EXPORT(NPC)



#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class NPC
 * Simple NPC which display some informations.
 */
class NPC : public ge::Component {
public:

  enum State {
    MOVE_UP,
    MOVE_DOWN,
    STOP
  };

  typedef void (NPC::*Action)();
  typedef std::map<State, Action> ActionMap;

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  NPC(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~NPC();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(ge::Seconds dt);

private:
  static ActionMap _actions;
  State _state;
  ge::Seconds _currentTime;
  ge::Seconds _duration;
private:
  void _moveUp();
  void _moveDown();
  void _stop();

};


#include <unistd.h>
#include <gEngine/Core/GameObject.hpp>
#include "Life.hpp"
#include	<gEngine/Math/Box.hpp>

Life::Life(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _currentTime(0.0f)
, _animStarted(false) {
  _data["quantity"] = 100.0f;
  _data["hit"] = std::string();
  _data["animationTime"] = 0.05f;
}

Life::~Life() {}

void Life::onStart() {
  _animTime = _data["animationTime"];
  _transform = _gameObject.component("Transformable")["transform"];
  _gameObject.on("Collider:collision", [this] (ge::Array &event) {
    ge::GameObject *other = event[0];      
    float &lifes = _data["quantity"];
    if (_gameObject.hasTag("INVINSIBLE"))
      return ;

    if (other->hasTag(_data["hit"].get<std::string>())) {
      if (lifes < 1) {
        size_t &score = (*_gameObject.getRoot())["score"];
        if (_gameObject.hasTag("MONSTER")) {
          score += 10;
        } else if (_gameObject.hasTag("BOSS")) {
          score += 1000;
        }
        if (!_gameObject.isDead()) {
          _gameObject.getRoot()
              ->getChildByName("scoreText")
              ->component("Text")["value"] = std::string("Score: " + ge::convert<size_t, std::string>(score));
        }
        _gameObject.die();
        ge::GameObjectPtr explosion(new ge::GameObject("explosion"));
        _gameObject.getParent()->addChild(explosion);
        explosion->component("Sprite")["texture"] = std::string("explosion");
        if (_gameObject.hasTag("BOSS"))
          explosion->component("Sprite")["size"] = ge::Vector3f(2.0, 4.0, 0);
        else{
          ge::TransformPtr t = _gameObject.component("Transformable")["transform"];
          ge::Vector3f scale = t->getScale();
//          std::cout << scale.x << " " << scale.y << " " << scale.z << std::endl;
          explosion->component("Sprite")["size"] = ge::Vector3f(0.6, 0.9, 0);
        }
        explosion->component("Sprite")["textureRect"] = ge::Boxf();
        explosion->component("Transformable")["position"] = _transform->getPosition();
        explosion->component("Move");
        explosion->component("Animation")["path"] = std::string("resources/data/animations/explosion_animation.xml");
        explosion->component("Animation")["interval"] = 0.02f;
        explosion->start();
        explosion->trigger("Animation:play", std::string("explose"));
        _gameObject.getRoot()->trigger("Audio:playSound", std::string("explosionsound"), false, 0.0f, 20.0f);
      } else {
        --lifes;
        _animStarted = true;
        _gameObject.component("Sprite")["color"] = ge::Color(255, 75, 50, 255);
        _gameObject.trigger("Sprite:change");
      }
    }

  });
}

void Life::onUpdate(float dt) {
  if (_gameObject.isDead())
    return ;
  if (_animStarted) {
    _currentTime += dt;
    if (_currentTime > _animTime) {
      _currentTime = 0;
      _animStarted = false;
      _gameObject.component("Sprite")["color"] = ge::Color(255, 255, 255, 255);
      _gameObject.trigger("Sprite:change");
    }
  }
}

GE_EXPORT(Life)


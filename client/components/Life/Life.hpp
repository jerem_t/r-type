
#pragma once

#include <gEngine/Core/Component.hpp>
#include <gEngine/Math/Transform.hpp>

/**
 * @class Life
 * Handle the life of the object and update its display.
 */
class Life : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Life(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Life();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  ge::TransformPtr _transform;
  float _currentTime;
  float _animTime;
  bool _animStarted;
};

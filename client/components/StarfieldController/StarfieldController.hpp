
#pragma once

#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Core/Component.hpp>

/**
 * @class StarfieldController
 * Simple component used to manage starfield's behaviour.
 */
class StarfieldController : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  StarfieldController(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~StarfieldController();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  ge::GameObjectPtr _createStar(unsigned int depth);

};

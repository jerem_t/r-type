
#include <gEngine/Math/Random.hpp>
#include <gEngine/Utils/Convert.hpp>
#include <gEngine/Math/Box.hpp>
#include "StarfieldController.hpp"

StarfieldController::StarfieldController(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["box"] = ge::Boxf(0, 0);
  _data["depth"] = 1.0f;
  _data["speed"] = 0.0f;
  _data["quantity"] = 300.0f;
  _data["texture"] = std::string("star");
}

StarfieldController::~StarfieldController() {}

void StarfieldController::onStart() {
  size_t depth = _data["depth"].get<float>();
  size_t size = _data["quantity"].get<float>();
  std::string name;
  for (size_t i = 0; i < depth; ++i) {
    name = _name + "_" + ge::convert<unsigned int, std::string>(i);
    _gameObject.addChild(ge::GameObjectPtr(new ge::GameObject(name)));
    for (size_t j = 0; j < size / depth; ++j)
      _gameObject.getChildByName(name)->addChild(_createStar(i));
  }
  _gameObject.on("Starfield:play", [this] (ge::Array &) {
    ge::GameObjects stars;
    _gameObject.getChildrenByName("star", stars);
    for (auto &star : stars)
      star->component("Move")["direction"] = ge::Vector3f(-1, 0, 0);
  });
  _gameObject.on("Starfield:stop", [this] (ge::Array &) {
    ge::GameObjects stars;
    _gameObject.getChildrenByName("star", stars);
    for (auto &star : stars)
      star->component("Move")["direction"] = ge::Vector3f();
  });
}

void StarfieldController::onUpdate(float) {}

ge::GameObjectPtr StarfieldController::_createStar(unsigned int depth) {
  ge::Boxf const &box = _data["box"];
  ge::GameObjectPtr star(new ge::GameObject("star"));
  star->component("Transformable")["position"] =
    ge::linearRandom(box.pos, box.size);
  float scale = ge::linearRandom(0.2, 0.4);
  star->component("Transformable")["scale"] =
    ge::Vector3f(scale, scale, scale);
  star->component("Sprite")["texture"] = _data["texture"];
  star->component("Sprite")["size"] = ge::Vector3f(0.04, 0.07, 0);
  star->component("Move")["speed"] = float(_data["speed"]) * (depth + 1);
  star->component("Move")["direction"] = ge::Vector3f(-1, 0, 0);
  star->component("Collider")["box"] = ge::Boxf(0.1, 0.1); // TODO
  star->component("OutOfBounds")["area"] = box;
  return star;
}

GE_EXPORT(StarfieldController)


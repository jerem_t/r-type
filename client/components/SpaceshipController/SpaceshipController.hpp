
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class SpaceshipController
 * Simple component used to manage spaceship's behaviour.
 */
class SpaceshipController : public ge::Component {
public:

  /**
   * @enum Bonus
   * Contains all bonus which can be applied on the spaceship.
   */
  enum Bonus {
    Double = 1 << 0,
    Ghost = 1 << 2,
    Power = 1 << 3
  };

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  SpaceshipController(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~SpaceshipController();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  bool _ghostStarted;
  float _ghostTime;
};


#include <iostream>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Transform.hpp>
#include <gEngine/Math/Box.hpp>
#include "SpaceshipController.hpp"

SpaceshipController::SpaceshipController(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _ghostStarted(false)
, _ghostTime(0) {
  _data["bonusDuration"] = 20.0f;
}

SpaceshipController::~SpaceshipController() {}

void SpaceshipController::onStart() {

  _gameObject.on("Collider:collision", [this] (ge::Array &event) {
    if (!event[0].hasType<ge::GameObject *>())
      return ;
    ge::GameObject *other = event[0];
    if (!other->hasTag("BONUS"))
      return ;
    if (other->getName() == "bonus_ghost") {
      _ghostStarted = true;
      _ghostTime = 0;
      _gameObject.addTag("INVINSIBLE");
      _gameObject.component("Sprite")["color"] = ge::Color(255, 255, 255, 100);
      _gameObject.trigger("Sprite:change");
    }
    if (other->getName() == "bonus_double")
      _gameObject.component("Shoot")["double"] = true;
    if (other->getName() == "bonus_power")
      _gameObject.component("Shoot")["power"] = true;
    if (other->getName() == "bonus_score")
      (*_gameObject.getRoot())["score"].get<size_t>() *= 2;
    other->die();
  });

  // Move
  _gameObject.on("Spaceship:move", [this] (ge::Array &event) {
    std::string const &code = event[1];
    ge::Vector3f &vector = _gameObject.component("Move")["direction"];
    if (code == "W" || code == "Z")
      vector.y = -1.0f;
    else if (code == "S")
      vector.y = 1.0f;
    else if (code == "A" || code == "Q")
      vector.x = -1.0f;
    else if (code == "D")
      vector.x = 1.0f;
  });

  // Stop
  _gameObject.on("Spaceship:stop", [this] (ge::Array &event) {
    std::string const &code = event[1];
    ge::Vector3f &vector = _gameObject.component("Move")["direction"];
    if (code == "W" || code == "Z" || code == "S")
      vector.y = 0.0f;
    else if (code == "A" || code == "Q" || code == "D")
      vector.x = 0.0f;
  });
}

void SpaceshipController::onUpdate(float dt) {

  // Update life in HUD

  std::string lifeText = "Life: " + ge::convert<float, std::string>(_gameObject.component("Life")["quantity"].get<float>());
  _gameObject.getRoot()
      ->getChildByName("lifeText")
      ->component("Text")["value"] = lifeText;

  // Update ghost bonus

  if (_ghostStarted) {
    _ghostTime += dt;
    if (_ghostTime > 5) {
      _ghostStarted = false;
      _gameObject.removeTag("INVINSIBLE");
      _gameObject.component("Sprite")["color"] = ge::Color(255, 255, 255, 255);
      _gameObject.trigger("Sprite:change");
    }
  }

}

GE_EXPORT(SpaceshipController)

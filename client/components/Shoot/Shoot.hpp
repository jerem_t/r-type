
#pragma once

#include <gEngine/Core/Component.hpp>
#include <gEngine/System/Timer.hpp>
#include <gEngine/Math/Transform.hpp>

/**
 * @class Shoot
 * Simple component to shoot bullets.
 */
class Shoot : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Shoot(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Shoot();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:
  ge::GameObjectPtr _createBullet(ge::Vector3f const &);

  ge::Timer _timer;
  ge::TransformPtr _transform;

};

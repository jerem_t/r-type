
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Math/Box.hpp>
#include "Shoot.hpp"

Shoot::Shoot(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["interval"] = 0.2f;
  _data["direction"] = ge::Vector3f(1, 0, 0);
  _data["autostart"] = false;
  _data["double"] = false;
  _data["power"] = false;
}

Shoot::~Shoot() {}

ge::GameObjectPtr Shoot::_createBullet(ge::Vector3f const &pos){
  ge::GameObjectPtr bullet(new ge::GameObject);
  bullet->component("Transformable")["position"] = pos;
  if (_data["power"].get<bool>())
    bullet->loadFromFile("resources/data/spaceship/bulletpower.xml");
  else
    bullet->loadFromFile("resources/data/spaceship/bullet.xml");
  bullet->component("Move")["direction"] = _data["direction"];
  _gameObject.getParent()->addChild(bullet);
  bullet->setTags(_gameObject.getTags());
  bullet->component("Life")["hit"] = _gameObject.component("Life")["hit"].get<std::string>();
  bullet->start();
  return bullet;
}

void Shoot::onStart() {

  // Get the object's transform and bounding box

  _transform = _gameObject.component("Transformable")["transform"];
  ge::Boxf &box = _gameObject.component("Collider")["box"];

  // Set the interval between each bullet apparition
  _timer.setInterval(_data["interval"]);

  // Create a new bullet on each tick

  _timer.on("tick", [this, &box] (ge::Array &) {
    if (_gameObject.isEnabled() == false || !_timer.hasStarted())
      return ;
    _gameObject.getRoot()->trigger("Audio:playSound", std::string("gun"), false, 0.15f, 100.0f);
    bool doubleShoot = _data["double"];
    ge::Vector3f pos;
    pos.x = _transform->getGlobalPosition().x + box.size.x;
    pos.y = _transform->getGlobalPosition().y + box.size.y / 2;
    ge::GameObjectPtr bullet;
    if (doubleShoot){
      bullet = _createBullet(ge::Vector3f(pos.x, pos.y - 0.02, pos.z));
      _gameObject.getParent()->addChild(bullet);
      bullet = _createBullet(ge::Vector3f(pos.x, pos.y + 0.02, pos.z));
      _gameObject.getParent()->addChild(bullet);
    }
    else{
      bullet = _createBullet(pos);
      _gameObject.getParent()->addChild(bullet);
    }
  });

  // Start shooting

  _gameObject.on("Shoot:start", [this] (ge::Array &) {
    if (!_timer.hasStarted())
      _timer.start();
  });

  // Stop shooting

  _gameObject.on("Shoot:stop", [this] (ge::Array &) {
    _timer.stop();
  });

  // Handle autostart
  if (_data["autostart"])
    _timer.start();

}

void Shoot::onUpdate(float dt) {
  if (!_gameObject.isDead())
    _timer.update(dt);
}

GE_EXPORT(Shoot)


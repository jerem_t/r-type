
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Random.hpp>
#include <gEngine/Math/Box.hpp>
#include "TrollController.hpp"

TrollController::TrollController(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _currentTime(0)
, _nextSpawnTime(5)
, _started(false) {}

TrollController::~TrollController() {}

void TrollController::onStart() {}

void TrollController::onUpdate(float dt) {
  _currentTime += dt;
  if (!_started && _currentTime > 3.0f) {
    _started = true;
    _gameObject.component("Move")["speed"] = 0.0f;
  } 
  else if (_currentTime > _nextSpawnTime) {
    _currentTime = 0;
    _nextSpawnTime = ge::linearRandom(0.2, 0.8);
    ge::GameObjectPtr babyTroll(new ge::GameObject("babyTroll"));
    babyTroll->addTag("BAD");
    _gameObject.getParent()->addChild(babyTroll);
    babyTroll->component("Transformable")["position"] = ge::Vector3f(0.45, 0.45, 0);
    babyTroll->component("Sprite")["texture"] = std::string("troll");
    float s = ge::linearRandom(0.5, 1.0);
    babyTroll->component("Sprite")["size"] = ge::Vector3f(0.15 * s, 0.2 * s, 0);
    babyTroll->component("Collider")["box"] = ge::Boxf(0.15 * s, 0.2 * s, 0);
    babyTroll->component("Collider")["quadtree"] = true;
    babyTroll->component("Life")["quantity"] = 2.0f;
    babyTroll->component("Life")["hit"] = std::string("GOOD");
    babyTroll->component("Move")["direction"] =
      ge::Vector3f(ge::linearRandom(-1.0, 1.0),
                   ge::linearRandom(-1.0, 1.0), 0);
    babyTroll->component("Move")["speed"] = 0.2f;
    babyTroll->start();
  }
}

GE_EXPORT(TrollController)



#pragma once

#include <gEngine/Core/Component.hpp>
#include <gEngine/Math/Transform.hpp>

/**
 * @class TrollController
 * Simple controller to manager Troll boss behaviour.
 */
class TrollController : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  TrollController(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~TrollController();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(ge::Seconds dt);

private:
  ge::TransformPtr _transform;
  ge::Seconds _currentTime;
  ge::Seconds _nextSpawnTime;
  bool _started;

};

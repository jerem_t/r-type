#pragma once

#include <gEngine/Core/Component.hpp>
#include <gEngine/Network/NWTcp.hpp>
#include <gEngine/Network/NWUdp.hpp>
#include <gEngine/Network/Network.hpp>
#include <gEngine/Network/NWSelector.hpp>
#include "CommandManager.hpp"
#include "PacketExtractor.hpp"

/**
 * @class RTypeClientProtocol
 * 
 */
class RTypeClientProtocol : public ge::Component {
public:

  typedef bool	(*t_extract)(std::vector<char> &, NWPacket &);
  typedef void	(CommandManager::*t_command)(NWPacket &, NWPacket &);
  typedef std::map<std::string , std::pair<bool, std::string> >	Config;
  typedef std::map<std::size_t, ge::GameObjectPtr> Players;

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  RTypeClientProtocol(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~RTypeClientProtocol();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

  /**
   * Getter config
   */
  Config	&getConfig();

  /**
   * Getter players
   */
  Players	&getPlayersInGame();

  void		setId(std::size_t id);
  std::size_t	getId() const;
  bool		getConnected() const;

private:
  void	resetSelector();
  void	handleUdp();
  void	handleTcp();
  bool	dispatchCommands(char localCommandId = 0);
  bool	dispatchUdp();

  std::size_t						_id;
  std::string						_ip;
  std::string						_port;
  bool							_isConnected;
  CommandManager					_manager;
  Config						_config;
  std::map<char, std::pair<t_extract, t_command> >	_commands;
  NWTcp							*_tcp;
  NWUdp							*_udp;
  NWSelector						_selector;
  std::vector<char>					_udpBuff;
  std::vector<char>					_readBuffer;
  std::vector<char>					_writeBuffer;
  Players						_playersInGame;
};

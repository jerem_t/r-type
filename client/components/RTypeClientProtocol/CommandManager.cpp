#include "CommandManager.hpp"
#include "RTypeClientProtocol.hpp"
#include <gEngine/Math/Random.hpp>

CommandManager::CommandManager(ge::GameObject &go, RTypeClientProtocol &protocol) 
  : _go(go), _protocol(protocol)
{}

CommandManager::~CommandManager()
{};

//////////////////////////////////
// Connected
//////////////////////////////////

// in : id | out : nothing
void	CommandManager::connected(NWPacket &input, NWPacket &)
{
  std::size_t	id;

  input >> id;
  std::cout << "call cmd connected : " << id << std::endl;
  _protocol.setId(id);
}

//////////////////////////////////
// Create Room
//////////////////////////////////

// in : nothing | out : 3 (id cmd) room name
void	CommandManager::createRoom(NWPacket &input, NWPacket &output)
{
  std::string	room = _protocol.getConfig()["Room"].second;

  std::cout << "cmd create room : " << room << std::endl;
  output << static_cast<char>(2) << room;
}

void	CommandManager::backCreateRoom(NWPacket &input, NWPacket &output)
{
  char	ret;

  input >> ret;
  std::cout << "back create room : " << (int)ret << std::endl;

  if (ret == 0)
    {
      std::cout << "Create room failed" << std::endl;
      return ;
    }
  std::string	name = _protocol.getConfig()["Room"].second;
  ge::GameObjectPtr room(new ge::GameObject(name));
  _go.addChild(room);
  ge::GameObjectPtr spaceship_1(new ge::GameObject("spaceship_1"));
  room->addChild(spaceship_1);
  if (!(spaceship_1->loadFromFile("resources/data/spaceship/spaceship_1.xml")))
    throw std::runtime_error("Load spaceship failed");
}

//////////////////////////////////
// Start Game
//////////////////////////////////

void	CommandManager::startGame(NWPacket &input, NWPacket &output)
{
  std::cout << "send start game" << std::endl;

  std::string	mode =  _protocol.getConfig()["Mode"].second;
  std::string	map =  _protocol.getConfig()["Map"].second;
  std::string	level =  _protocol.getConfig()["Level"].second;

  if (!_protocol.getConnected())
    backStartGame(input, output);
  else
    output << static_cast<char>(11) << mode << map << level;
}

// must wait backStart to start the game
void	CommandManager::backStartGame(NWPacket &input, NWPacket &output)
{
  std::string	mode;
  std::string	map;
  std::string	level;

  ge::fakeSeed(0);
  ge::GameObjectPtr game(new ge::GameObject("game"));
  std::size_t	nb_players;
  std::size_t		id;

  _go["score"] = std::size_t(0);
  _go["inGame"] = true;
  _go.getChildByName("menu")->addChild(game);
  (*_go.getChildByName("menu"))["currentGame"] = game->getId();
  if (_protocol.getConnected())
    {
      // Get room information
      input >> mode >> map >> level;
      input >> nb_players;
      std::cout << "nb players : " << nb_players << std::endl;
      // Create all spaceships
      for (std::size_t i = 0; i < nb_players; ++i)
	{
	  RTypeClientProtocol::Players &playersInGame = _protocol.getPlayersInGame();
	  input >> id;
	  std::cout << "backstart playerId = " << id << std::endl;
	  // Create the spaceship and add it to the game
	  std::string name = "spaceship_" + ge::convert<size_t, std::string>(i + 1);
	  ge::GameObjectPtr spaceship(new ge::GameObject(name));
	  game->addChild(spaceship);
	  if (!spaceship->loadFromFile("resources/data/spaceship/" + name + ".xml"))
	    throw std::runtime_error("Cannot open spaceship XML \"" + name + "\".");
	  playersInGame[id] = spaceship;
	  std::cout << "id start game : " << (int)id << std::endl;
	}
    } else { // Load only one spaceship and get the map, mode and level from GUI
    mode = _protocol.getConfig()["Mode"].second;
    map = _protocol.getConfig()["Map"].second;
    level = _protocol.getConfig()["Level"].second;
    ge::GameObjectPtr spaceship(new ge::GameObject("spaceship_1"));
    game->addChild(spaceship);
    if (!spaceship->loadFromFile("resources/data/spaceship/spaceship_1.xml"))
      throw std::runtime_error("Cannot open spaceship XML \"spaceship_1\".");
  }

  // hide GUI
  _go.getChildByName("Gui-startPage")->trigger("Page:hide");

  // Load map

  ge::GameObjectPtr gameMap(new ge::GameObject("map"));
  game->addChild(gameMap);
  if (mode == "Adventure") {
    if (!gameMap->loadFromFile("resources/data/levels/" + level + ".xml"))
      throw std::runtime_error("Cannot load adventure mode.");
  } else if (mode == "Arcade") {
    if (!gameMap->loadFromFile("resources/data/maps/" + map + ".xml"))
      throw std::runtime_error("Unknown map " + map + ".");
  }

  // Launch the game

  game->start();

  // Initialize end messages

  ge::GameObjectPtr gameovermsg = ge::GameObjectPtr(new ge::GameObject("gameovermsg"));
  gameovermsg->loadFromFile("resources/data/decor/gameover.xml");
  _go.addChild(gameovermsg);
  gameovermsg->component("Text")["color"] = ge::Color(0,0,0,0);

  ge::GameObjectPtr succeedmsg = ge::GameObjectPtr(new ge::GameObject("succeedmsg"));
  succeedmsg->loadFromFile("resources/data/decor/succeed.xml");
  _go.addChild(succeedmsg);
  succeedmsg->component("Text")["color"] = ge::Color(0,0,0,0);
}

// in : nothing | out : 4 (id cmd gameover)
void	CommandManager::gameOver(NWPacket &input, NWPacket &output)
{
  (void)input;
  (void)output;
}

// in : id + position other player - out : nothing (UDP)
void	CommandManager::move(NWPacket &input, NWPacket &output)
{
  (void)input;
  (void)output;
}

//////////////////////////////////
// CheckRooms
//////////////////////////////////

// in : nothing | out : 6 (id cmd update room)
void	CommandManager::checkRooms(NWPacket &input, NWPacket &output)
{
  std::string	room = _protocol.getConfig()["Room"].second;

  std::cout << "cmd check room : " << room << std::endl;
  output << static_cast<char>(5) << room;
}

// in : size - name - nb players | out : nothing
void	CommandManager::backCheckRooms(NWPacket &input, NWPacket &output)
{
  std::string	room = _protocol.getConfig()["Room"].second;
  (void)input;
  (void)output;
  std::cout << "cmd back check room : " << std::endl;
}

// in : nothing | out : 8 (id cmd addBullet) - type of bullet - current position
void	CommandManager::shoot(NWPacket &input, NWPacket &output)
{
  (void)input;
  (void)output;
}

// in : type of bullet - current position | out : nothing
void	CommandManager::addBullet(NWPacket &input, NWPacket &output)
{
  (void)input;
  (void)output;
}

//////////////////////////////////
// JoinRoom
//////////////////////////////////

void	CommandManager::joinRoom(NWPacket &input, NWPacket &output)
{
  std::string	room = _protocol.getConfig()["Room"].second;

  std::cout << "cmd join room : " << room << std::endl;
  output << static_cast<char>(14) << room;
}


// in : nothing | out : room name
void	CommandManager::backJoinRoom(NWPacket &input, NWPacket &output)
{
  std::string		name;
  ge::GameObjectPtr	room;
  ge::GameObjects       spaceships;
  ge::GameObjectPtr	spaceship;
  char			ret;

  input >> ret;
  if (ret == 0)
    {
      _protocol.getConfig().clear();
      throw std::runtime_error("Join failed");
    }
  name =  _protocol.getConfig()["Room"].second;
  std::cout << "room : " << name << " joined" << std::endl;
}

void	CommandManager::closeRoom(NWPacket &input, NWPacket &output)
{
  (void)input;
  (void)output;
}

// in : nothing | out : 10 (id cmd input) - timestamp - current position
void	CommandManager::input(NWPacket &input, NWPacket &output)
{
  (void)input;
  (void)output;
}

#include <gEngine/Core/GameObject.hpp>
#include "RTypeClientProtocol.hpp"
#include <sstream>

const std::size_t	readSize = 4096;
const std::size_t	udpInputSize = sizeof(std::size_t) * 2 + 1;
 
RTypeClientProtocol::RTypeClientProtocol(ge::GameObject &gameObject)
  : ge::Component(gameObject), _id(0), _ip(""), _port(""), _isConnected(false),
    _manager(gameObject, *this)
{}

RTypeClientProtocol::~RTypeClientProtocol() 
{
  delete _tcp;
  delete _udp;
}

void RTypeClientProtocol::onStart()
{
  _commands = 
    {
      {1,  {PacketExtractor::connected, &CommandManager::connected}},
      {2,  {PacketExtractor::emptyExtractor, &CommandManager::createRoom}},
      {4,  {PacketExtractor::move, &CommandManager::move}},
      {5, {PacketExtractor::emptyExtractor, &CommandManager::checkRooms}},
      {6, {PacketExtractor::backCheckRooms, &CommandManager::backCheckRooms}},
      {8,  {PacketExtractor::addBullet, &CommandManager::addBullet}},
      {10, {PacketExtractor::backCreateRoom, &CommandManager::backCreateRoom}},
      {14, {PacketExtractor::emptyExtractor, &CommandManager::joinRoom}},
      {15, {PacketExtractor::backJoinRoom, &CommandManager::backJoinRoom}},
      {11, {PacketExtractor::emptyExtractor, &CommandManager::startGame}},
      {12, {PacketExtractor::backStartGame, &CommandManager::backStartGame}},
    };

  _gameObject.on("RTypeClientProtocol:connect", [this](ge::Array &event)
		 {
		   std::string	address = event[0];
		   std::size_t	pos = address.find(":");
		   if (pos == std::string::npos)
		     throw std::runtime_error("Invalid address to connect to server");
		   _ip = address.substr(0, pos);
		   _port = address.substr(pos + 1);
		 });
  _gameObject.on("RTypeClientProtocol:join", [this](ge::Array &event)
		 {
		   std::string	room = event[0];
		   _config["Room"] = std::make_pair(true, room);
		   dispatchCommands(14);
		 });
  _gameObject.on("RTypeClientProtocol:createRoom", [this](ge::Array &event)
		 {
		   std::string	room = event[0];
		   _config["Room"] = std::make_pair(true, room);
		   dispatchCommands(2);
		 });
  _gameObject.on("RTypeClientProtocol:checkRooms", [this](ge::Array &event)
		 {
		   std::string	room = event[0];
		   _config["Room"] = std::make_pair(true, room);
		   dispatchCommands(5);
		 });
  _gameObject.on("RTypeClientProtocol:startGame", [this](ge::Array &event)
		 {
		   std::string	mode = event[0];
		   std::string	map = event[1];
		   std::string	level = event[2];

		   _config["Mode"] = std::make_pair(true, mode);
		   _config["Map"] = std::make_pair(true, map);
		   _config["Level"] = std::make_pair(true, level);
		   dispatchCommands(11);
		 });
  _gameObject.on("RTypeClientProtocol:gameOver", [this](ge::Array &event)
		 {
		   (void)event;
		   _config["Mode"] = std::make_pair(false, "");
		   _config["Map"] = std::make_pair(false, "");
		   _config["Level"] = std::make_pair(false, "");
		   _config["Root"] = std::make_pair(false, "");
		   std::cout << "game over " << std::endl;
		 });

  _tcp = new NWTcp();
  _udp = new NWUdp();
}

void RTypeClientProtocol::onUpdate(float) 
{
  if (!_isConnected &&
      !_ip.empty() && !_port.empty() && (_tcp->connect(_ip, _port) == INWAbstract::SC_DONE))
    {
      _isConnected = true;
    }
  if (_isConnected)
    {
      resetSelector();
      _selector.wait(false);
      handleTcp();
      handleUdp();
    }
}

void RTypeClientProtocol::resetSelector()
{
  _selector.clear();

  _selector.addRead(*_tcp);
  _selector.addRead(*_udp);
  if (!_writeBuffer.empty())
    _selector.addWrite(*_tcp);
}

void	RTypeClientProtocol::handleTcp()
{
  std::size_t	readed;

  if (_selector.isReadyToReceive(*_tcp))
    {
      std::cout << "	-client can receive" << std::endl;
      if (_tcp->receive(_readBuffer, readSize, readed) != INWAbstract::SC_DONE)
	{
	  std::cout << "Error on receive" << std::endl;
	  _isConnected = false;
	}
    }

  while (dispatchCommands());

  if (_selector.isReadyToWrite(*_tcp))
    {
      std::cout << "	-can send on client" << std::endl;
      if (_tcp->send(_writeBuffer) != INWAbstract::SC_DONE)
	{
	  std::cout << "Error on send" << std::endl;
	  _isConnected = false;
	}
      _writeBuffer.clear();
    }
}

void	RTypeClientProtocol::handleUdp()
{
  std::istringstream	ss(_port);
  unsigned short	port;
  std::size_t		received;

  if (_selector.isReadyToReceive(*_udp))
    {
      ss >> port;
      _udp->receive(_ip, port, _udpBuff, readSize, received);
      while (dispatchUdp());
    }
}

bool	RTypeClientProtocol::dispatchUdp()
{
  std::istringstream	ss(_port);
  unsigned short	port;
  std::vector<char>	subBuff;
  NWPacket		input;
  NWPacket		output;
  NWPacket		udpHeader;
  size_t		clientId;
  size_t		timestamp;
  char			commandId;

  ss >> port;
  if (_udpBuff.size() < udpInputSize)
    return false;
  udpHeader.setData(std::vector<char>(_udpBuff.begin(), _udpBuff.begin() + udpInputSize));
  udpHeader >> clientId >> timestamp >> commandId;
  auto it = _commands.find(commandId);
  subBuff = std::vector<char>(_udpBuff.begin() + udpInputSize - 1, _udpBuff.end());
  if (it == _commands.end() || it->second.first(subBuff, input) == false)
    return (false);
  _udpBuff = subBuff;
  (_manager.*(it->second.second))(input, output);
  _udp->send(_ip, port, output);
  return (true);
}

bool	RTypeClientProtocol::dispatchCommands(char localCommandId)
{
  NWPacket		input;
  NWPacket		output;
  char			commandId;

  if (_readBuffer.empty() && localCommandId == 0)
    return (false);
  commandId = (localCommandId != 0) ? localCommandId : _readBuffer[0];
  auto it = _commands.find(commandId);
  if (it == _commands.end() || it->second.first(_readBuffer, input) == false)
    return (false);
  std::cout << "here 1 " << std::endl;
  (_manager.*(it->second.second))(input, output);
  std::cout << "here 2 " << std::endl;
  _writeBuffer.insert(_writeBuffer.end(),
		      output.getData().begin(),
		      output.getData().end());
  std::cout << "here 3 " << std::endl;
  return (true);
}

RTypeClientProtocol::Config	&RTypeClientProtocol::getConfig()
{
  return _config;
}

RTypeClientProtocol::Players	&RTypeClientProtocol::getPlayersInGame()
{
  return _playersInGame;
}

void				RTypeClientProtocol::setId(std::size_t id)
{
  _id = id;
}

std::size_t			RTypeClientProtocol::getId() const
{
  return _id;
}

bool				RTypeClientProtocol::getConnected() const
{
  return _isConnected;
}

GE_EXPORT(RTypeClientProtocol)

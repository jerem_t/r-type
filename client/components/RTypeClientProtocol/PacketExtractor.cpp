#include "PacketExtractor.hpp"
#include <gEngine/Math/Box.hpp>
#include <gEngine/Math/Vector4.hpp>
#include <gEngine/Math/Vector2.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Quaternion.hpp>
#include <gEngine/Math/Transform.hpp>

PacketExtractor::PacketExtractor()
{}

PacketExtractor::~PacketExtractor()
{}

bool	PacketExtractor::connected(std::vector<char> &readBuffer, NWPacket &input)
{
  std::size_t	size = 1 + sizeof(std::size_t);

  if (readBuffer.size() >= size)
    {
      input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + size));
      readBuffer = std::vector<char>(readBuffer.begin() + size, readBuffer.end());
      return true;
    }
  return false;
}

bool	PacketExtractor::move(std::vector<char> &readBuffer, NWPacket &input)
{
  std::size_t	size = 1 + sizeof(std::size_t) + (sizeof(ge::Vector3f) * 2) + sizeof(ge::Quaternionf);

  if (readBuffer.size() >= size)
    {
      input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + size));
      readBuffer = std::vector<char>(readBuffer.begin() + size, readBuffer.end());
      return true;
    }
  return false;
}

bool	PacketExtractor::backCheckRooms(std::vector<char> &readBuffer, NWPacket &input)
{
  NWPacket	tmp;
  std::size_t	sizeName;
  std::size_t	fullSize;

  if (readBuffer.size() >= 1 + sizeof(std::size_t))
    {
      tmp.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + 1 + sizeof(std::size_t)));
      tmp >> sizeName;
      fullSize = 1 + sizeof(std::size_t) + sizeName + sizeof(std::size_t);
      if (readBuffer.size() >= fullSize)
	{
	  input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + fullSize));
	  readBuffer = std::vector<char>(readBuffer.begin() + fullSize, readBuffer.end());
	  return true;
	}
    }
  return false;
}

bool	PacketExtractor::addBullet(std::vector<char> &readBuffer, NWPacket &input)
{
  std::size_t	size = 1 + (sizeof(ge::Vector3f) * 2) + sizeof(ge::Quaternionf);
 
  if (readBuffer.size() >= size)
    {
      input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + size));
      readBuffer = std::vector<char>(readBuffer.begin() + size, readBuffer.end());
      return true;
    }
  return false;
}

bool	PacketExtractor::backCreateRoom(std::vector<char> &readBuffer, NWPacket &input)
{
  std::size_t	size = 2;

  if (readBuffer.size() >= size)
    {
      input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + size));
      readBuffer = std::vector<char>(readBuffer.begin() + size, readBuffer.end());
      return true;
    }
  return false;
}

bool	PacketExtractor::backStartGame(std::vector<char> &readBuffer, NWPacket &input)
{
  NWPacket	tmp;
  std::size_t	nbPlayers;
  std::size_t	size;

  if (readBuffer.size() >= 1)
    {
      if (stringExtractor(3, std::vector<char>(readBuffer.begin() + 1, readBuffer.end()), size) == false)
	return (false);
      size += 1;
      if (readBuffer.size() >= size + sizeof(std::size_t))
	{
	  tmp.setData(std::vector<char>(readBuffer.begin() + size, readBuffer.begin() + size +
					sizeof(std::size_t)));
	  tmp >> nbPlayers;
	  size += sizeof(std::size_t) * nbPlayers + sizeof(std::size_t);
	  std::cout << "backstartgame size = " << size << std::endl;
	  if (readBuffer.size() >= size)
	    {
	      input.setData(std::vector<char>(readBuffer.begin() + 1,
					      readBuffer.begin() + size));
	      readBuffer = std::vector<char>(readBuffer.begin() + size, readBuffer.end());
	      return true;
	    }
	}
    }
  return false;
}

bool	PacketExtractor::backJoinRoom(std::vector<char> &readBuffer, NWPacket &input) {
  std::size_t	size = 2;

  if (readBuffer.size() >= size)
    {
      std::cout << "extract join start : " << size << std::endl;
      input.setData(std::vector<char>(readBuffer.begin() + 1, readBuffer.begin() + size));
      readBuffer = std::vector<char>(readBuffer.begin() + size, readBuffer.end());
      std::cout << "extract join end : " << size << std::endl;
      return true;
    }
  return false;
}

bool	PacketExtractor::emptyExtractor(std::vector<char> &, NWPacket &)
{
  return true;
}

bool	PacketExtractor::stringExtractor(size_t nbr, std::vector<char> const &readBuffer, size_t &size)
{
  size_t	totalSize = 0;

  for (size_t i = 0; i < nbr; ++i)
    {
      NWPacket	curSize;
      size_t	strSize;

      totalSize += sizeof(size_t);
      // get size of str1
      if (readBuffer.size() >= totalSize)
	curSize.setData(std::vector<char>(readBuffer.begin() + totalSize - sizeof(size_t),
					  readBuffer.begin() + totalSize));
      else
	return (false);
      curSize >> strSize;
      totalSize += strSize;
    }
  size = totalSize;
  return (true);
}

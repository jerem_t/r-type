#pragma once

#include "CommandManager.hpp"

class	PacketExtractor
{
public:
  PacketExtractor();
  ~PacketExtractor();

  static bool	connected(std::vector<char> &readBuffer, NWPacket &input);
  static bool	move(std::vector<char> &readBuffer, NWPacket &input);
  static bool	backCheckRooms(std::vector<char> &readBuffer, NWPacket &input);
  static bool	addBullet(std::vector<char> &readBuffer, NWPacket &input);
  static bool	backCreateRoom(std::vector<char> &readBuffer, NWPacket &input);
  static bool	backStartGame(std::vector<char> &readBuffer, NWPacket &input);
  static bool	emptyExtractor(std::vector<char> &readBuffer, NWPacket &input);
  static bool	stringExtractor(size_t nbr, std::vector<char> const &readBuffer, size_t &size);
  static bool	backJoinRoom(std::vector<char> &readBuffer, NWPacket &input);

private:

};

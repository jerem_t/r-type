#pragma once

#include <string>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Network/Network.hpp>

class RTypeClientProtocol;

class CommandManager
{
public:
  CommandManager(ge::GameObject &go, RTypeClientProtocol &protocol);
  ~CommandManager();

  void	connected(NWPacket &input, NWPacket &output);
  void	createRoom(NWPacket &input, NWPacket &output);
  void	joinRoom(NWPacket &input, NWPacket &output);
  void	startGame(NWPacket &input, NWPacket &output);
  void	closeRoom(NWPacket &input, NWPacket &output);
  void	gameOver(NWPacket &input, NWPacket &output);
  void	move(NWPacket &input, NWPacket &output);
  void	checkRooms(NWPacket &input, NWPacket &output);
  void	updateRooms(NWPacket &input, NWPacket &output);
  void	shoot(NWPacket &input, NWPacket &output);
  void	addBullet(NWPacket &input, NWPacket &output);
  void	input(NWPacket &input, NWPacket &output);

  void	backStartGame(NWPacket &input, NWPacket &output);
  void	backCreateRoom(NWPacket &input, NWPacket &output);
  void	backJoinRoom(NWPacket &input, NWPacket &output);
  void	backCheckRooms(NWPacket &input, NWPacket &output);

private:
  ge::GameObject	&_go;
  RTypeClientProtocol	&_protocol;
};


#pragma once

#include <gEngine/Math/Transform.hpp>
#include <gEngine/Core/Component.hpp>

/**
 * @class DeathStarController
 * Controls the death star boss behaviour.
 */
class DeathStarController : public ge::Component {
public:

  enum State {
    START,
    MOVE,
    SHOOT,
    STOP
  };

  typedef void (DeathStarController::*Action)();
  typedef std::map<State, Action> ActionMap;

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  DeathStarController(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~DeathStarController();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);
private:
  void _start();
  void _move();
  void _shoot();
  void _stop();
private:
  static ActionMap _actions;
  ge::GameObjectPtr _laser;
  ge::TransformPtr _transform;
  float _currentTime;
  float _nextShootTime;
  State _state;

};

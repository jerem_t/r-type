
#include <gEngine/Math/Vector3.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Math/Random.hpp>
#include <gEngine/Data/SpriteData.hpp>
#include "DeathStarController.hpp"

DeathStarController::ActionMap DeathStarController::_actions = {
  { START, &DeathStarController::_start },
  { MOVE, &DeathStarController::_move },
  { SHOOT, &DeathStarController::_shoot },
  { STOP, &DeathStarController::_stop }
};

DeathStarController::DeathStarController(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _currentTime(0)
, _nextShootTime(5.0f)
, _state(START) {}

DeathStarController::~DeathStarController() {}

void DeathStarController::onStart() {
  _laser = ge::GameObjectPtr(new ge::GameObject("laser"));
  _transform = _gameObject.component("Transformable")["transform"];
  _gameObject.addChild(_laser);
  _laser->component("Transformable")["position"] = ge::Vector3f(-0.7, -0.05, 0.0);
  _laser->component("Transformable").start();
  _laser->component("Sprite")["texture"] = std::string("laser");
  _laser->component("Sprite")["size"] = ge::Vector3f(0.9, 0.6, 0);
  _laser->component("Sprite")["color"] = ge::Color(0);
  _laser->component("Collider")["box"] = ge::Boxf(0.9, 0.4, 0);
  _laser->component("Collider")["quadtree"] = true;

  // TODO voir les tags et tout pour que ca marche
  _laser->component("Collider").start();
  _laser->component("Sprite").start();
  _laser->component("Move").start();
}

void DeathStarController::onUpdate(float dt) {
  _currentTime += dt;
  (this->*_actions[_state])();
}

void DeathStarController::_start() {
  if (_currentTime > 2.0f) {
    _gameObject.component("Move")["direction"] = ge::Vector3f(0, -1, 0);
    _state = MOVE;
    _currentTime = 0.0f;
  }
}

void DeathStarController::_move() {
  if (_transform->getGlobalPosition().y <= 0.0f)
    _gameObject.component("Move")["direction"] = ge::Vector3f(0, 1, 0);
  else if (_transform->getGlobalPosition().y >= 0.3f)
    _gameObject.component("Move")["direction"] = ge::Vector3f(0, -1, 0);
  if (_currentTime > _nextShootTime) {
    _nextShootTime = ge::linearRandom(1, 2);
    _state = STOP;
    _currentTime = 0.0f;
    _gameObject.component("Move")["direction"] = ge::Vector3f(0, 0, 0);
  }
}

void DeathStarController::_shoot() {
  if (_currentTime > 1.0f) {
    ge::SpriteData data;
    data.setColor(ge::Color(0, 0, 0, 0));
    _laser->removeTag("BAD");
    _laser->trigger("Sprite:change", data);
    _state = MOVE;
    _gameObject.component("Move")["direction"] = ge::Vector3f(0, 1, 0);
    _currentTime = 0.0f;
  }
}

void DeathStarController::_stop() {
  if (_currentTime > 0.5f) {
    ge::SpriteData data;
    _laser->addTag("BAD");
    data.setColor(ge::Color(255, 255, 255, 255));
    _laser->trigger("Sprite:change", data);
    _state = SHOOT;
    _currentTime = 0.0f;
  }
}

GE_EXPORT(DeathStarController)



cmake_minimum_required(VERSION 2.6)

project(gEngine_Slots)
include_directories(.)
file(GLOB_RECURSE SOURCES *.cpp)
add_library(gEngine_Slots SHARED ${SOURCES})
if (MSVC)
	target_link_libraries(gEngine_Slots gEngine.lib)
endif (MSVC)


#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class Slots
 * Display the slots for this room.
 */
class Slots : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  Slots(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~Slots();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

};

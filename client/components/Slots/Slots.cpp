
#include <gEngine/Core/GameObject.hpp>
#include "Slots.hpp"

Slots::Slots(ge::GameObject &gameObject)
: ge::Component(gameObject) {
  _data["you"] = 0.0f;
  _data["num"] = 3.0f;
}

Slots::~Slots() {}

void Slots::onStart() {
  for (size_t i = 0; i < 4; ++i) {
    if (i == _data["you"].get<float>())
      {}//create opaque colored
    else if (i < _data["num"].get<float>())
      {}//create colored transparent
    else
      {} // create gray transparent
  }
}

void Slots::onUpdate(float) {}

GE_EXPORT(Slots)


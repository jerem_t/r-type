
#include <gEngine/Math/Random.hpp>
#include <gEngine/Core/GameObject.hpp>
#include "MenuController.hpp"

MenuController::MenuController(ge::GameObject &gameObject)
: ge::Component(gameObject)
, _mainMenuStarted(false)
, _mainMenuDelay(0)
, _mainMenuTime(0) {
  _gameObject["currentGame"]  = std::size_t(0);
}

MenuController::~MenuController() {}

void MenuController::onStart() {
  _gameObject.on("Menu:mainMenu", [this] (ge::Array &event) {
    if (event.getSize() == 0) {
      _gameObject.getRoot()->trigger("Context:close");
      return ;
    }
    _gameObject.getRoot()->trigger("RTypeClientProtocol:gameOver");
    if (event[0].hasType<float>()){
      _mainMenuDelay = event[0];
      if (event[1].get<std::string>() == "bubble_win")
        _gameObject.getRoot()->getChildByName("succeedmsg")->component("Text")["color"] = ge::Color(65,180,40,100);
      else
        _gameObject.getRoot()->getChildByName("gameovermsg")->component("Text")["color"] = ge::Color(230,100,40,100);
    }
    else
      _mainMenuDelay = 0;
    _mainMenuStarted = true;
    _mainMenuTime = 0;
    _gameObject.getRoot()->getChildByName("scoreText")->component("Text")["value"] = std::string("");
    _gameObject.getRoot()->getChildByName("lifeText")->component("Text")["value"] = std::string("");
  });

  _gameObject.on("Menu:updateRooms", [this] (ge::Array &e) {
    std::vector<std::string> names = e[0];
    ge::GameObjectPtr page = _gameObject.getChildByName("Gui-joinPage");
    if (!page)
      return ;
    ge::GameObjects textBoxes;
    page->getChildrenByName("Gui-textBox", textBoxes);
    for (size_t i = 0; i < 3; ++i) {
      if (i + 4 < textBoxes.size()) {
        if (i < names.size())
          textBoxes[i + 4]->component("TextBox")["text"] = names[i];
        else
          textBoxes[i + 4]->component("TextBox")["text"] = std::string("");
        textBoxes[i + 4]->trigger("TextBox:update");
      }
    }
  });

  _gameObject.on("Menu:saveOptions", [this] (ge::Array &) {

    ge::GameObjectPtr page = _gameObject.getChildByName("Gui-optionPage");
    if (!page)
      return ;
    ge::GameObjectPtr addr = page->getChildByName("Gui-textInput");
    if (!addr)
      return ;
    std::string serverAddress = addr->component("TextBox")["text"];

    _gameObject.getRoot()->trigger("RTypeClientProtocol:connect", serverAddress);

    ge::GameObjects choices;
    page->getChildrenByName("Gui-choice-choice", choices);
    if (choices.size() < 2)
      return ;

    std::string const &sound = choices[0]->component("TextBox")["text"];
    bool hasSound = sound == "ON";

    bool prevSound = _gameObject.getRoot()->component("SFMLAudio")["sound"];
    if (hasSound && !prevSound){
        _gameObject.getRoot()->component("SFMLAudio")["sound"] = true;
        _gameObject.getRoot()->trigger("Audio:playMusic", std::string("gameMusic"));
    }
    if (!hasSound && prevSound){
        _gameObject.getRoot()->component("SFMLAudio")["sound"] = false;
        _gameObject.getRoot()->trigger("Audio:pauseMusics");
    }

    std::string const &size = choices[1]->component("TextBox")["text"];
    float w, h;
    if (size == "800x480") { w = 800; h = 480; }
    else if (size == "1280x720") { w = 1280; h = 720; }
    else if (size == "1600x900") { w = 1600; h = 900; }

    _gameObject.getRoot()->trigger("Context:update", w, h);

  });

  _gameObject.on("Menu:createRoom", [this] (ge::Array &) {
    ge::GameObjectPtr page = _gameObject.getChildByName("Gui-createPage");
    if (!page)
      return ;
    ge::GameObjectPtr el = page->getChildByName("Gui-textInput");
    std::string name = el->component("TextBox")["text"];
    _gameObject.getRoot()->trigger("RTypeClientProtocol:createRoom", name);
    _gameObject.trigger("Menu:changeMode");
  });

  _gameObject.on("Menu:joinRoom", [this] (ge::Array &) {
    ge::GameObjectPtr page = _gameObject.getChildByName("Gui-joinPage");
    if (!page)
      return ;
    ge::GameObjects textBoxes;
    page->getChildrenByName("Gui-textBox", textBoxes);
    for (auto &textBox : textBoxes) {
      if (textBox->component("TextBox")["focused"].get<bool>()) {
        _gameObject.getRoot()->trigger("RTypeClientProtocol:join", textBox->component("TextBox")["text"]);
        return ;
      }
    }
    std::string roomName = page->getChildByName("Gui-textInput")->component("TextBox")["text"];
    _gameObject.getRoot()->trigger("RTypeClientProtocol:join", roomName);
    _gameObject.trigger("Menu:changeMode");
  });

  _gameObject.on("Menu:startGame", [this] (ge::Array &) {
    ge::GameObjectPtr page = _gameObject.getChildByName("Gui-startPage");
    if (!page)
      return ;
    ge::GameObjects choices;
    page->getChildrenByName("Gui-choice-choice", choices);
    if (choices.size() < 2)
      return ;
    std::string mode = choices[0]->component("TextBox")["text"];
    std::string map = choices[1]->component("TextBox")["text"];
    std::string level = choices[2]->component("TextBox")["text"];

    _gameObject.getRoot()->getChildByName("scoreText")
                         ->component("Text")["value"] = std::string("Score:0");

    _gameObject.getRoot()->trigger("RTypeClientProtocol:startGame", mode, map, level);
  });

  _gameObject.on("Menu:changeMode", [this] (ge::Array &){
    ge::GameObjectPtr page = _gameObject.getChildByName("Gui-startPage");
    if (!page)
      return ;
    ge::GameObjects choices;
    ge::GameObjectPtr child;
    page->getChildrenByName("Gui-choice", choices);
    if (choices.size() < 2)
      return ;
    std::string mode = choices[0]->getChildByName("Gui-choice-choice")->component("TextBox")["text"].get<std::string>();
    if (mode == "Adventure"){
      child = choices[1]->getChildByName("Gui-choice-left");
      child->component("Sprite")["color"] = ge::Color(50, 50, 50, 50);
      child->component("TextBox")["enabled"] = false;
      child->trigger("Sprite:change");
      child = choices[1]->getChildByName("Gui-choice-right");
      child->component("Sprite")["color"] = ge::Color(50, 50, 50, 50);
      child->component("TextBox")["enabled"] = false;
      child->trigger("Sprite:change");

      child = choices[2]->getChildByName("Gui-choice-left");
      child->component("Sprite")["color"] = child->component("TextBox")["background-color"];
      child->trigger("Sprite:change");
      child->component("TextBox")["enabled"] = true;
      child = choices[2]->getChildByName("Gui-choice-right");
      child->component("Sprite")["color"] = child->component("TextBox")["background-color"];
      child->trigger("Sprite:change");
      child->component("TextBox")["enabled"] = true;
    }
    else{
      child = choices[2]->getChildByName("Gui-choice-left");
      child->component("Sprite")["color"] = ge::Color(50, 50, 50, 50);
      child->trigger("Sprite:change");
      child->component("TextBox")["enabled"] = false;
      child = choices[2]->getChildByName("Gui-choice-right");
      child->component("Sprite")["color"] = ge::Color(50, 50, 50, 50);
      child->component("TextBox")["enabled"] = false;
      child->trigger("Sprite:change");

      child = choices[1]->getChildByName("Gui-choice-left");
      child->component("Sprite")["color"] = child->component("TextBox")["background-color"];
      child->trigger("Sprite:change");
      child->component("TextBox")["enabled"] = true;
      child = choices[1]->getChildByName("Gui-choice-right");
      child->component("Sprite")["color"] = child->component("TextBox")["background-color"];
      child->trigger("Sprite:change");
      child->component("TextBox")["enabled"] = true;
    }
  });

}

void MenuController::onUpdate(float dt) {
  if (_mainMenuStarted) {
    _mainMenuTime += dt;
    if (_mainMenuTime > _mainMenuDelay) {
      if (_gameObject["currentGame"].get<std::size_t>() == 0)
        return ;
      _gameObject.getRoot()->getChildByName("gameovermsg")->component("Text")["color"] = ge::Color(0,0, 0,0);
      _gameObject.getRoot()->getChildByName("gameovermsg")->trigger("Text:change");
      _gameObject.getRoot()->getChildByName("succeedmsg")->component("Text")["color"] = ge::Color(0,0, 0,0);
      _gameObject.getRoot()->getChildByName("succeedmsg")->trigger("Text:change");

      ge::GameObjectPtr game = _gameObject.getChildById(_gameObject["currentGame"].get<std::size_t>());
      if (game && !game->isDead()) {
        game->forEach([] (ge::GameObjectPtr node) {
          node->die();
        });
        game->die();
      }
      _gameObject.trigger("Gui:changePage", std::string("mainPage"));
      _mainMenuStarted = false;
    }
  }
}

GE_EXPORT(MenuController)


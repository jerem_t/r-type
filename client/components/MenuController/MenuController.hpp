
#pragma once

#include <gEngine/Core/Component.hpp>

/**
 * @class MenuController
 * Handle menu events and data.
 */
class MenuController : public ge::Component {
public:

  /**
   * Instanciate the component with the gameObject.
   * @param gameObject The referenced gameObject.
   */
  MenuController(ge::GameObject &gameObject);

  /**
   * Component's destructor.
   */
  ~MenuController();

  /**
   * Start the component and initialize all behaviours.
   */
  void onStart();

  /**
   * Update the component at each frame.
   */
  void onUpdate(float dt);

private:

  size_t _currentGame;
  bool _mainMenuStarted;
  float _mainMenuDelay;
  float _mainMenuTime;

};

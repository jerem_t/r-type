// Debug
#include <iostream>

// Loaders
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

// gEngine
#include <gEngine/Resources/Loader.hpp>
#include <gEngine/Core/GameObject.hpp>
#include <gEngine/Core/ComponentManager.hpp>
#include <gEngine/System/FrameHandler.hpp>

int main() {

	ge::GameObjectPtr root(new ge::GameObject("root"));

	try {

		// Load resources

		ge::Loader loader;
		loader.on("progress", [](ge::Array &event) {
			std::cout << "Load resource \"" << event[1].get<std::string>()
				<< "\" -- " << int(float(event[0]) * 100)
				<< "%" << std::endl;
		});
		loader.on("finish", [](ge::Array &) {
			std::cout << "All resources loaded." << std::endl;
		});
		loader.addType<sf::Texture>("png|jpg|jpeg|gif|bmp");
		loader.addType<sf::Font>("ttf");
		loader.addType<sf::SoundBuffer>("wav|mp3|ogg");
		loader.addType<ge::Xml>("ui|xml");
		if (!loader.loadFromFile("resources/resources.xml"))
			throw std::runtime_error("Unable to load resources.");

		// Load components

		ge::ComponentManager::getInstance();
		if (!ge::ComponentManager::getInstance()
			.loadFromFile("resources/client_components.xml")) {
			throw std::runtime_error("Unable to load components.");
		}

		// Load game

		if (!root->loadFromFile("resources/data/client.xml"))
			throw std::runtime_error("Unable to load client from XML.");
		root->on("error", [](ge::Array &event) {
			throw std::runtime_error(event[0].get<std::string>());
		});
		(*root)["inGame"] = false;
		root->on("Menu:mainMenu", [root] (ge::Array &event) {
			if ((*root)["inGame"].get<bool>()) {
				(*root)["inGame"] = false;
				root->getChildByName("menu")->trigger("Menu:mainMenu", event[0], event[1]);
			}
		});
		root->getChildByName("menu")->on("quit", [root] (ge::Array &) {
			root->trigger("Context:close");
		});
		root->start();

		root->trigger("Audio:playMusic", std::string("gameMusic"));
		// Loop

		ge::FrameHandler loop;
		loop.setFrameRate(60);
		root->on("Context:close", [&loop](ge::Array &) {
			loop.stop();
		});
		loop.on("frame", [&root] (ge::Array &event) {
			ge::GameObjectPtr fps = root->getChildByName("fps");
			if (fps) {
				float val = event[1];
				fps->component("Text")["value"] = std::string("FPS: ") + ge::convert<int, std::string>(val);
			}
			try {
				root->update(event[0]);
			} catch (std::runtime_error const &e) {
				root->getChildByName("menu")->trigger("Gui:changePage", std::string("errorPage"));
				ge::GameObjects elems;
				root->getChildByName("Gui-errorPage")->getChildrenByName("Gui-textBox", elems);
				if (elems.size() > 3) {
					elems[3]->component("TextBox")["text"] = std::string(e.what());
					elems[3]->trigger("TextBox:update");
				}
			}
		});
		loop.start();

		// Free component manager

		ge::ComponentManager::destroyInstance();

	} catch (std::runtime_error const &e) {
		root->trigger("Context:close");
		std::cout << std::endl
							<< "  Exception catched \"" << e.what() << "\"."
							<< std::endl
							<< "  (Press any key to exit)"
							<< std::endl;
		getchar();
	}
  return 0;
}


"""
Description

1) come from the right
2) randomly focus on one player
3) shoot big lazer "pseudo randomly"
4) go back to step two

"""

import gEngine

class DeathStar(gEngine.GameObject):
  """Simple AI for the DeathStar boss"""
  def __init__(self, arg):
    pass

  def onStart(self):

    # Update component data
    self.components["Move"]["direction"] = (-1, 0, 0)
    self.components["Move"]["speed"] = 100.0

    # Trigger event
    self.trigger("Starfield:stop")

    # Set infos
    self.state = "Focus"

  def onUpdate(self, dt):
    pass